import os

#==============================================
#Checking Server Status
#==============================================
def _serverstatus(ServerName):
    try:
        #        print 'Server reached:'+ServerName
        cd('domainRuntime:/ServerLifeCycleRuntimes/'+ServerName);
        #        print 'Cd successful';
        #        ls();
        serverState = cmo.getState()
        #        print 'Server State'+serverState;
        if serverState == "RUNNING":
            print 'Server ' + ServerName + ' is :\033[1;32m' + serverState + '\033[0m';
        elif serverState == "STARTING":
            print 'Server ' + ServerName + ' is :\033[1;33m' + serverState + '\033[0m';
        elif serverState == "UNKNOWN":
            print 'Server ' + ServerName + ' is :\033[1;34m' + serverState + '\033[0m';
        else:
            print 'Server ' + ServerName + ' is :\033[1;31m' + serverState + '\033[0m';
        return serverState
    except:
        print 'Not able to get the' + serverState +'server status. Please try again\n';
        print 'Please check logged in user has full access to complete the requested operation on ' +ServerName+ '\n';
        exit()


#==============================================
#Start Server Block
#==============================================

def _startServer(ServerName):

    try:
        cd('domainRuntime:/ServerLifeCycleRuntimes/'+ServerName);
        cmo.start();
    except:
        print 'Error in getting current status of ' +ServerName+ '\n';
        print 'Please check logged in user has full access to complete the start operation on ' +ServerName+ '\n';
        exit()


#==============================================
#Start Server Block
#==============================================

def _checkIfServerStarted(ServerName):

    try:
        cd('domainRuntime:/ServerLifeCycleRuntimes/'+ServerName);
        state=_serverstatus(ServerName);
        while (state!='RUNNING'):
            state=_serverstatus(ServerName);
            java.lang.Thread.sleep(5000);
    except:
        print 'Error in getting current status of ' +ServerName+ '\n';
        print 'Please check logged in user has full access to complete the start operation on ' +ServerName+ '\n';
        exit()


#==============================================
#Stop Server Block
#==============================================

def _stopServer(ServerName):

    try:
        cd('domainRuntime:/ServerLifeCycleRuntimes/'+ServerName);
        cmo.forceShutdown();
        state=_serverstatus(ServerName);
        while (state!='SHUTDOWN'):
            state=_serverstatus(ServerName);
            java.lang.Thread.sleep(5000);
    except:
        print 'Error in getting current status of ' +ServerName+ '\n';
        print 'Please check logged in user has full access to complete the stop operation on ' +ServerName+ '\n';
        exit()



connect(os.environ['user'],os.environ['password'],os.environ['adminurl'])
svrs = os.environ['serversToRestart'].split(',')
print svrs

for aServer in svrs:
    _stopServer(aServer);

for aServer in svrs:
    _startServer(aServer);

for aServer in svrs:
    _checkIfServerStarted(aServer);