-- Limpar dados no Oracle
truncate table "PAAP"."SAMPLE" drop storage;
truncate table "PAAP"."SAMPLE_GROUP10M" drop storage;
truncate table "PAAP"."SAMPLE_GROUP1H" drop storage;
truncate table "PAAP"."SAMPLE_GROUP24H" drop storage;
truncate table "PAAP"."SAMPLE_GROUP2M" drop storage;
truncate table "PAAP"."SAMPLE_GROUP6H" drop storage;
truncate table "PAAP"."TAGPIFILTER_MOVING_AVG_SAMPLE" drop storage;
truncate table "PAAP"."JOB_LAST_DATE_RUN" drop storage;
truncate table "PAAP"."PROCESS_STATUS";
truncate table "PAAP"."EVENT" drop storage;
update "PAAP"."TAGPIFILTER" set samples_sum=0, PARAMP=0, LAST_DATE=null, LAST_VALUE=null, MINUSP=null, PARAMK=null, XHATMINUS=null, COLD_START_TOTAL=0;


-- Limpar dados no Postgre
ALTER SEQUENCE hibernate_sequence RESTART WITH 1;
ALTER SEQUENCE event_sequence RESTART WITH 1;
truncate table "sample";
truncate table "sample_group10m";
truncate table "sample_group1h";
truncate table "sample_group24h";
truncate table "sample_group2m";
truncate table "sample_group6h";
truncate table "tagpifilter_moving_avg_sample";
truncate table "job_last_date_run";
truncate table "process_status";
truncate table "event";
update "tagpifilter" set samples_sum=0, paramp=0, last_date=null, last_value=null, minusp=null, paramk=null, xhatminus=null, cold_start_total=0;


-- Checa a consistencia do moving average
select f.samples_sum-(select sum(value) from TAGPIFILTER_MOVING_AVG_SAMPLE s where s.moving_average_id=f.id) from tagpifilter f where CLASS='br.com.petrobras.paap.TagPIFilterMovingAverage'

-- Atualiza a o moving average com os valores corretos
UPDATE TAGPIFILTER F
SET F.SAMPLES_SUM = (SELECT SUM(VALUE) FROM TAGPIFILTER_MOVING_AVG_SAMPLE S WHERE S.MOVING_AVERAGE_ID=F.ID)
WHERE CLASS='br.com.petrobras.paap.TagPIFilterMovingAverage'
