includeTargets << grailsScript("_GrailsInit")

target(main: "Script para realizar restart no weblogic") {
    depends(parseArguments)

    def bea_home, adminurl, targets, user, password

    adminurl = ant.antProject.properties.adminurl ?: argsMap["adminurl"]
    if (!adminurl) {
        println "Parâmetro adminurl não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "adminurl: ${adminurl}"

    targets = ant.antProject.properties.targets ?: argsMap["targets"]
    if (!targets) {
        println "Parâmetro targets não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "targets: ${targets}"

    serversToRestart = ant.antProject.properties.serversToRestart ?: argsMap["serversToRestart"]
    if (!serversToRestart) {
        println "Parâmetro servers não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "serversToRestart: ${serversToRestart}"

    bea_home = ant.antProject.properties.bea_home ?: argsMap["bea_home"]
    if (!bea_home) {
        println "Parâmetro bea_home não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "bea_home: ${bea_home}"

    user = ant.antProject.properties.user ?: argsMap["user"]
    if (!user) {
        println "Parâmetro user não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "user: ${user}"

    password = ant.antProject.properties.password ?: argsMap["password"]
    if (!password) {
        println "Parâmetro password não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "password: ${password}"

    ant.path(id:'weblogic-deploy-path'){
        ant.pathelement(location:"${bea_home}/wlserver/server/lib/weblogic.jar")
    }

    //ant.taskdef(name:'wlst', classname:'weblogic.ant.taskdefs.management.WLSTTask', classpathref:'weblogic-deploy-path')
    
    //ant.wlst(debug:"true", executeScriptBeforeFile:"false", failOnError:"true", fileName:"./restart.py")



    def process = "env WL_HOME=${bea_home}/wlserver env user=${user} env password=${password} env adminurl=${adminurl} env targets=${targets} env serversToRestart=${serversToRestart} ${bea_home}/wlserver/common/bin/wlst.sh ./scripts/restart.py".execute()

    process.in.eachLine { line -> println line }

    process.waitFor()   // Wait for the command to finish

    // Obtain status and output
    println "return code: ${ process.exitValue()}"
    println "stderr: ${process.err.text}"


}

setDefaultTarget(main)