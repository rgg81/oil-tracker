String appCode = getConfig().appCode
String libs = getEnvProperties().petrobras.libs.dir
String appfiles = getEnvProperties().petrobras.appfiles.dir + "/" + appCode

eventSetClasspath = { rootLoader ->
    println "add classpath"
    addResourceBundlesToClasspath(libs,rootLoader)

    // Adiciona todos os *.jar
    def folder = new File(libs)
    folder.eachFileMatch (~/.*.jar/)  { jarFile ->
        println "Adicionando lib ${jarFile}..."
        addResourceBundlesToClasspath(jarFile,rootLoader)
    }
}

eventConfigureTomcat = {tomcat ->
	println "Configurando Tomcat..."
	tomcat.addUser("admin", "1234")
	tomcat.addRole("admin", "tomcat")
	
	def loader = tomcat.class.classLoader
	addResourceBundlesToClasspath(new File(libs),loader)

	// Adiciona todos os *.jar
	def folder = new File(libs)
	folder.eachFileMatch (~/.*.jar/)  { jarFile ->
		println "Adicionando lib ${jarFile}..."
		addResourceBundlesToClasspath(jarFile,loader)
	}
}

private def addResourceBundlesToClasspath(String strDir, rootLoader) {
    addResourceBundlesToClasspath(new File(strDir), rootLoader)
}

private def addResourceBundlesToClasspath(File externalDir, rootLoader){

	if (grailsSettings.grailsEnv != "production") {
		classpathSet = false
		rootLoader.addURL( externalDir.toURI().toURL() )
	}
}

private def getConfig(){
	return getConfFile("Config")
}

/**
 * {@see http://stackoverflow.com/questions/5654416/access-settings-in-buildconfig}
 * @param fileName
 * @return
 */
private def getConfFile(String fileName){
	String configPath = "${basedir}${File.separator}grails-app${File.separator}conf${File.separator}${fileName}.groovy"
	return new ConfigSlurper(grailsSettings.grailsEnv).parse(new File(configPath).toURI().toURL())
}

private def getEnvProperties(){
	return new ConfigSlurper().parse(new File("${userHome}/paap-env.groovy").toURI().toURL())
}
