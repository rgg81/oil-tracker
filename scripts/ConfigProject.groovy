includeTargets << grailsScript("_GrailsInit")
/**
 * Esse script faz o seguinte:
 * 1- solicita do usuario o caminho dos diretorios "libs", "logs" e "appfiles"
 * 2- copia o arquivo template/paap-env.groovy para a raiz da home do usuario (ex.: C:\Users\<chave>)
 * 3- pega os valores do passo 1 e joga no arquivo USER_HOME/paap-env.groovy
 * 4- copia os arquivos contidos em /dominio/paap/ e copia para os diretorios adequados, de acordo com o que o usuario
 *    informou no passo 1
 * 5- os arquivos copiados no passo 4 sao entao manipulados e configurados automaticamente
 * 6- Copia o certificado do serviço do PI para o java
 */
target(main: "Script para configurar automaticamente o ambiente local do desenvolvedor") {

    def propsFileName = "paap-env.groovy"

    // passo 1
    ant.input(message: "Qual o caminho do diretorio libs (aka path/to/domain/classpath)? [Default: D:/Projetos/libs]",
            defaultvalue: "D:/Projetos/libs",
            addproperty: "paap.libs")
    def libsPath = ant.antProject.properties."paap.libs"

    ant.input(message: "Qual o caminho do diretorio logs? [Default: D:/Projetos/logs]",
            defaultvalue: "D:/Projetos/logs",
            addproperty: "paap.logs")
    def logsPath = ant.antProject.properties."paap.logs"

    // passo 1
    ant.input(message: "Qual o caminho do diretorio appfiles? [Default: D:/Projetos/appfiles]",
            defaultvalue: "D:/Projetos/appfiles",
            addproperty: "paap.appfiles")
    def appfilesPath = ant.antProject.properties."paap.appfiles"
	
	// passo 1
	ant.input(message: "Qual sua chave de acesso ao proxy? [Default: xxxx]",
			defaultvalue: "xxxx",
			addproperty: "paap.proxyUser")
	def proxyUser = ant.antProject.properties."paap.proxyUser"
	
	// passo 1
	ant.input(message: "Qual sua senha de acesso ao proxy? [Default: xxxx]",
			defaultvalue: "xxxx",
			addproperty: "paap.proxyPassword")
	def proxyPassword = ant.antProject.properties."paap.proxyPassword"

    // passo 2
    ant.copy(file: "${basedir}/scripts/template/${propsFileName}",
            todir: "${userHome}", overwrite: true)

    // passo 3
    ant.replace(file: "${userHome}/${propsFileName}", token: "@libs@", value: libsPath)
    ant.replace(file: "${userHome}/${propsFileName}", token: "@logs@", value: logsPath)
    ant.replace(file: "${userHome}/${propsFileName}", token: "@appfiles@", value: appfilesPath)
	ant.replace(file: "${userHome}/${propsFileName}", token: "@proxyUser@", value: proxyUser)
	ant.replace(file: "${userHome}/${propsFileName}", token: "@proxyPassword@", value: proxyPassword)

    // passo 4
    def paapLibsPath = libsPath + "/paap"

    if(!new File(paapLibsPath).exists()){
        ant.mkdir(dir: paapLibsPath)
    }

    ant.copy(file: "${basedir}/dominio/paap/config.properties",
            todir: new File(paapLibsPath), overwrite: true)

    ant.copy(file: "${basedir}/dominio/paap/log4j.properties",
            todir: new File(paapLibsPath), overwrite: true)

    ant.copy(file: "${basedir}/dominio/libs/mysql-connector-java-5.1.26-bin.jar",
            todir: new File(libsPath), overwrite: true)

    ant.copy(file: "${basedir}/dominio/libs/ojdbc5.jar",
            todir: new File(libsPath), overwrite: true)

    // passo 5
    def logFilePath = logsPath + "/paap/paap.log"
    ant.replace(file: "${paapLibsPath}/config.properties", token: "@log4jpath@", value: "${paapLibsPath}/log4j.properties")
    ant.replace(file: "${paapLibsPath}/log4j.properties", token: "@logpath@", value: logFilePath)

	// passo 6
	ant.copy(file: "${basedir}/dominio/cacerts",
		todir: "${System.env.JAVA_HOME}/jre/lib/security/", overwrite: true)

}

setDefaultTarget(main)
