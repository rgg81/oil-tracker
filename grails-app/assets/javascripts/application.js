// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery
//= require angular
//= require angular-locale_pt-br
//= require sockjs.min
//= require angular-websocket
//= require jquery.cookie
//= require bootstrap
//= require highcharts
//= require highcharts-theme
//= require exporting
//= require rgbcolor.js
//= require StackBlur.js
//= require canvg.js
//= require Blob.js
//= require FileSaver.js
//= require canvas-toBlob.js
//= require technical-indicators.src
//= require ie10-viewport-bug-workaround
//= require slider
//= require ng-table.min
//= require_self

// 'Constants'
var TIME = 0 ;
var VALUE = 1 ;

if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

function countProperties(obj) {
    var key, count = 0;
    for(key in obj) {
        if(obj.hasOwnProperty(key)) {
            count++;
        }
    }
    return count;
}


angular.module('paapFilters', []).filter('loadingNumber', ['$filter', function($filter) {
    var numberFilter = $filter('number');
    return function(input, digits) {
        if ( input != null && typeof(input) != 'undefined' && !isNaN(input) ) {
            return numberFilter(input, digits) ;
        } else {
            return '―' ;
        }
    };
}]).filter('loadingSnapshot', ['$filter', function($filter) {
    var numberFilter = $filter('number');
    return function(input, digits) {
        if ( input != null && typeof(input) != 'undefined' && !isNaN(input) ) {
            return "17h: " + numberFilter(input, digits) ;
        } else {
            return ' ' ;
        }
    };
}]);



var paapApp = angular.module("paapApp",['angular-websocket', 'paapFilters', 'ngTable']);

paapApp.config(['$locationProvider', function($locationProvider) {
	$locationProvider.html5Mode({enabled: false,requireBase:false});
}]);

paapApp.config(['$httpProvider', function($httpProvider) {
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}]);

paapApp.controller("headerController", ['$rootScope', '$scope', '$interval', '$http', '$location', function($rootScope, $scope, $interval, $http, $location) {

    $scope.now = new Date() ;
    $interval(function() {
        $scope.now = new Date() ;
    }, 1000) ;

    $rootScope.wi100 = $.cookie("wi100") == 'true';

    $scope.uos = [];
    $scope.departments = [];
    $scope.filterKey = null;
    $scope.filterValue = null;
    $rootScope.events = [];
    $http.get("/painelep/api/UEP/uos").
        success(function(data, status, headers, config) {
            $scope.uos = data;
        }).error(function(data, status, headers, config) {
            console.error("Filters: Couldn't get UO List");
        });
    $http.get("/painelep/api/UEP/departments").
        success(function(data, status, headers, config) {
            $scope.departments = data;
        }).error(function(data, status, headers, config) {
            console.error("Filters: Couldn't get Department List");
        });
    $http.get("/painelep/PIData/events").
        success(function(data, status, headers, config) {
            if (!data.error) {
                $rootScope.events = data.events;
            }
        }).error(function(data, status, headers, config) {
            console.error("Couldn't get events List");
        });

    $scope.init = function(params) {
        $scope.filterKey = params.filterKey;
        $scope.filterValue = params.filterValue;

    }

    $scope.toggleWI100 = function() {
        $rootScope.wi100 = ! $rootScope.wi100 ;
        $.cookie("wi100", $rootScope.wi100, { path: '/'} ) ;
    }
}]);

// manter sessão ativa
setInterval(
    function(){
        $.ajax( {url: "/painelep/monitor/index", cache: false })
            .done(function() {
                console.log("monitor ok");
            })
            .fail(function() {
                console.log( "monitor error" );
            });
    },
    1000*60*3);

$(function () {

    /**
     * Experimental Highcharts plugin to allow download to PNG (through canvg) and SVG
     * using the HTML5 download attribute.
     *
     * WARNING: This plugin uses the HTML5 download attribute which is not generally
     * supported. See http://caniuse.com/#feat=download for current uptake.
     *
     * TODO:
     * - Find a flowchart at Dropbox\Highsoft\Teknisk\Docs. The flowchart has not been updated to fit the new logic of implementing FileSaver ourselves.
     * - Existing code was abandoned in the middle of a revamp, probably needs cleanup.
     * - Implement "FileSaver.js"-like functionality for cross-browser support. Where Blob would need to be pulled in (old FF/Opera/Safari), emit error msg.
     * - Option to fall back to online export server on missing support. Display error to user if option disabled and there is no support.
     */
    (function (Highcharts) {

        // Dummy object so we can reuse our canvas-tools.js without errors
        Highcharts.CanVGRenderer = {};

        /**
         * Downloads a script and executes a callback when done.
         * @param {String} scriptLocation
         * @param {Function} callback
         */
        function getScript(scriptLocation, callback) {
            var head = document.getElementsByTagName('head')[0],
                script = document.createElement('script');

            script.type = 'text/javascript';
            script.src = scriptLocation;
            script.onload = callback;

            head.appendChild(script);
        }

        /**
         * Add a new method to the Chart object to invoice a local download
         */
        Highcharts.Chart.prototype.exportChartLocal = function (options) {

            var chart = this,
                svg = this.getSVGForExport(options,options.exporting.chartOptions),// this.getSVG(), // Get the SVG
                canvas,
                canvasCxt,
                a,
                href,
                extension,
                download = function () {

                    var blob;

                    // IE specific
                    if (navigator.msSaveOrOpenBlob) {

                        // Get PNG blob
                        if (extension === 'png') {
                            blob = canvas.msToBlob();

                        // Get SVG blob
                        } else {
                            blob = new MSBlobBuilder;
                            blob.append(svg);
                            blob = blob.getBlob('image/svg+xml');
                        }

                        navigator.msSaveOrOpenBlob(blob, options.exporting.filename? options.exporting.filename : 'chart.' + extension);

                    } else {
                        a = document.createElement('a');
                        if (typeof a.download !== 'undefined') {

                            // HTML5 download attribute
                            a.href = href;
                            a.download = options.exporting.filename? options.exporting.filename : 'chart.' + extension;
                            document.body.appendChild(a);
                            a.click();

                        } else {

                            canvas.toBlob(function(blob) {
                                saveAs(blob, options.exporting.filename? options.exporting.filename : 'chart.' + extension);
                            });
                            //window.open(href, "_blank");
                            // Implement FileSaver functionality, or fall back to export server
                        }

                        a.remove();
                    }
                },
                prepareCanvas = function () {
                    canvas = document.createElement('canvas'); // Create an empty canvas
                    window.canvg(canvas, svg); // Render the SVG on the canvas
                    console.log(canvas);
                    href = canvas.toDataURL('image/png');
                    extension = 'png';
                };

            // Add an anchor and apply the download to the button
            if (options && options.type === 'image/svg+xml') {
                href = 'data:' + options.type + ',' + svg;
                extension = 'svg';
                download();

            } else {

                // It's included in the page or preloaded, go ahead
                if (window.canvg) {
                    prepareCanvas();
                    download();

                // No CanVG
                } else {
                    // If browser supports SVG canvas rendering directly - do that
                    canvas = document.createElement('canvas');
                    canvasCxt = canvas.getContext && canvas.getContext('2d');
                    if (canvasCxt /* && canvasCxt.drawImage --NOTE: do we need this? */) {
                        canvasCxt.drawImage(svg, 0, 0);
                        download();
                    } else {
                        // We need to load canvg before continuing
                        // TODO: If browser does not support SVG & canvas, fallback to export server
                        this.showLoading();
                        getScript(Highcharts.getOptions().global.canvasToolsURL, function () {
                            chart.hideLoading();
                            prepareCanvas();
                            download();
                        });
                    }
                }
            }
        };

    }(Highcharts));

});
