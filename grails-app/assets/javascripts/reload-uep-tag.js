//= require filters

paapApp.controller("reloadController", ['$scope','$http', function($scope, $http) {

    $scope.checkbox = {};
    $scope.relatedUeps = [];
    $scope.childrenTags = [];
    $scope.relatedUepsString = '';
    $scope.ueptag;
    $scope.uepid;
    $scope.cReloading = true;
    $scope.uReloading = true;

    $scope.init = function(ueptag,uepid){
        $scope.uepid = uepid;
        $scope.ueptag = ueptag;
        var url = "/painelep/admin/TagPI/childrentags/" + ueptag;
        $scope.cReloading = true;
        $http.get(url).success(function(data, status, headers, config) {
            $scope.childrenTags = data;
            for (i = 0; i < $scope.childrenTags.length; i++) {
                $scope.checkbox[$scope.childrenTags[i].id] = true;
            }
            $scope.reloadRelatedUeps();
            $scope.cReloading = false;
        }).error(function(data, status, headers, config) {
            $scope.cReloading = false;
            console.error("Failed while getting ");
        });
        

    }
    $scope.reloadRelatedUeps = function(){        
        var tags = [$scope.ueptag]
        for (var key in $scope.checkbox) {
           if ($scope.checkbox.hasOwnProperty(key)) {
                if($scope.checkbox[key]){
                    tags.push(key);
                }
           }
        }
        
        var url = "/painelep/admin/TagPI/relatedUeps?ids=" + tags.join('&ids=');
        $scope.relatedUepsString = 'Carregando ...';
        $scope.uReloading = true;
        $http.get(url).success(function(data, status, headers, config) {
            
            $scope.relatedUepsString = '';
            for(var i in data){
                if($scope.uepid != data[i].id){
                    $scope.relatedUeps[i] = data[i];
                }
            }
            

            for (var key in $scope.relatedUeps) {

                if ($scope.relatedUeps.hasOwnProperty(key)) {
                    if($scope.relatedUeps[key]){
                        $scope.relatedUepsString = $scope.relatedUepsString + $scope.relatedUeps[key].name + ', ';
                    }
                }
            }
            $scope.relatedUepsString = $scope.relatedUepsString.substring(0,$scope.relatedUepsString.length-2);
            $scope.uReloading = false;
        }).error(function(data, status, headers, config) {
            $scope.uReloading = false;
            console.error("Failed while getting ");
        });
    }  

}]);
