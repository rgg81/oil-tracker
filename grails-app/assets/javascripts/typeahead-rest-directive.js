paapApp.directive('autocomplete', ['$http', function($http) {
	return {
    	restrict: 'A',
      	replace: 'false',
      	link: function(scope, elem, attrs) {

      		var autocomplete = function(element,url, suggestions) {  
				$http.get(url).
			    success(function(data, status, headers, config) {
			      suggestions = data;
			    }).error(function(data, status, headers, config) {
			      console.error("Failed while getting the total average from the last 24 hours");
			    });

			    var obtainer = function(query, cb) {
			      	var filteredList = $.grep(suggestions, function (item, index) {
			              return item.match(query);
			          });
			          mapped = $.map(filteredList, function (item) { return { value: item } });
			          cb(mapped);
			      }

			    element.typeahead({
			        hint: false,
			        highlight: false,
			        minLength: 0
			    }, {
			        name: "noname",
			        displayKey: "value",
			        source: obtainer
			    });

			    element.on("typeahead:opened", function () {
			        var initial = element.val(), ev = $.Event("keydown");
			        ev.keyCode = ev.which = 40;
			        $(this).trigger(ev);
			        if(element.val()!=initial)element.val("");
			        return true
			    });
			}

			var suggestionArray = [];
			autocomplete(elem,attrs.autocompleteUrl,suggestionArray);
    	}
  	};
}]);