Highcharts.theme = {
    // Series colors
	colors: ["#06a6fd", "#08cc69"],

	chart: {
		backgroundColor: 'transparent',
		style: {
			fontFamily: "petrobras_sansbold, sans-serif"
		},
		plotBorderColor: '#6f767c',
        spacingBottom: 12,
        spacingTop: 5,
        spacingLeft: 5,
        spacingRight: 10,
//        height: 180
	},
	loading: {
	    labelStyle: {
	        color: '#fff'
	    },
	    style: {
        	backgroundColor: '#21272c',
        	opacity: 0.5,
        	textAlign: 'center'
        }
	},
	title: {
		style: {
			color: '#fff',
//			textTransform: 'uppercase',
//			fontSize: '20px'
		}
	},
	subtitle: {
//		style: {
//			color: '#E0E0E3',
//			textTransform: 'uppercase'
//		}
	},
	xAxis: {
	    // Estas duas propriedades forçam o gráfico a terminar no último valor
	    //endOnTick: true,
	    maxPadding: 0,
//		gridLineColor: '#707073',
		labels: {
		    rotation: 90,
		    x: -4,
			style: {
				color: '#6f767c',
                fontSize: '10px'
			}
		},
		offset: 0,
		lineColor: '#6f767c',
//		minorGridLineColor: '#ff0000',
		tickColor: '#6f767c',
		tickLength: 3,
//		title: {
//			style: {
//				color: '#A0A0A3'
//
//			}
//		}
	},
	yAxis: {
		gridLineColor: '#424b54',
		gridLineDashStyle: 'dot',
		gridLineWidth: 0,
		labels: {
			/*formatter: function() {
			    var stepSize = (this.axis.max-this.axis.min) / (this.axis.tickAmount-1)
			    if (stepSize <= 0) stepSize = this.value;
			    var digits = Math.floor(Math.log10(stepSize)+1)
			    var extraDigits = digits < 4 ? 4 - digits : 0 ;
			    //console.log(this.chart.title.textStr + " = " + stepSize + " digits = " + digits )
			    return (this.value/1000).toFixed(extraDigits) + "k"
			},*/
			style: {
				color: '#6f767c',
                fontSize: '12px'
			},
			align: 'right',
			x: 0
		},
		offset: 5,
//		lineColor: '#707073',
//		minorGridLineColor: '#505053',
//		tickColor: '#ff0000',
//		tickLength: 5,
//		tickWidth: 1,
//		title: {
//			style: {
//				color: '#A0A0A3'
//			}
//		}
	},
	tooltip: {
		valueDecimals: 0
//		backgroundColor: 'rgba(0, 0, 0, 0.85)',
//		style: {
//			color: '#F0F0F0'
//		}
	},
	plotOptions: {
		series: {
//			dataLabels: {
//				color: '#B0B0B3'
//			},
			marker: {
			    enabled: false
			}
		},
//		boxplot: {
//			fillColor: '#505053'
//		},
//		candlestick: {
//			lineColor: 'white'
//		},
//		errorbar: {
//			color: 'white'
//		}
	},
//	legend: {
//		itemStyle: {
//			color: '#E0E0E3'
//		},
//		itemHoverStyle: {
//			color: '#FFF'
//		},
//		itemHiddenStyle: {
//			color: '#606063'
//		}
//	},
//	credits: {
//		style: {
//			color: '#666'
//		}
//	},
//	labels: {
//		style: {
//			color: '#707073'
//		}
//	},
//
//	drilldown: {
//		activeAxisLabelStyle: {
//			color: '#F0F0F3'
//		},
//		activeDataLabelStyle: {
//			color: '#F0F0F3'
//		}
//	},
//
//	navigation: {
//		buttonOptions: {
//			symbolStroke: '#DDDDDD',
//			theme: {
//				fill: '#505053'
//			}
//		}
//	},
//
//	// scroll charts
//	rangeSelector: {
//		buttonTheme: {
//			fill: '#505053',
//			stroke: '#000000',
//			style: {
//				color: '#CCC'
//			},
//			states: {
//				hover: {
//					fill: '#707073',
//					stroke: '#000000',
//					style: {
//						color: 'white'
//					}
//				},
//				select: {
//					fill: '#000003',
//					stroke: '#000000',
//					style: {
//						color: 'white'
//					}
//				}
//			}
//		},
//		inputBoxBorderColor: '#505053',
//		inputStyle: {
//			backgroundColor: '#333',
//			color: 'silver'
//		},
//		labelStyle: {
//			color: 'silver'
//		}
//	},
//
//	navigator: {
//		handles: {
//			backgroundColor: '#666',
//			borderColor: '#AAA'
//		},
//		outlineColor: '#CCC',
//		maskFill: 'rgba(255,255,255,0.1)',
//		series: {
//			color: '#7798BF',
//			lineColor: '#A6C7ED'
//		},
//		xAxis: {
//			gridLineColor: '#505053'
//		}
//	},
//
//	scrollbar: {
//		barBackgroundColor: '#808083',
//		barBorderColor: '#808083',
//		buttonArrowColor: '#CCC',
//		buttonBackgroundColor: '#606063',
//		buttonBorderColor: '#606063',
//		rifleColor: '#FFF',
//		trackBackgroundColor: '#404043',
//		trackBorderColor: '#404043'
//	},
//
//	// special colors for some of the
//	legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
//	background2: '#505053',
//	dataLabelsColor: '#B0B0B3',
//	textColor: '#C0C0C0',
//	contrastTextColor: '#F0F0F3',
//	maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);

Highcharts.setOptions({
	lang: {
		months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
		weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		loading: ['Atualizando o gráfico...aguarde'],
		contextButtonTitle: 'Exportar gráfico',
		decimalPoint: ',',
		thousandsSep: '.',
		downloadJPEG: 'Baixar imagem JPEG',
		downloadPDF: 'Baixar arquivo PDF',
		downloadPNG: 'Baixar imagem PNG',
		downloadSVG: 'Baixar vetor SVG',
		printChart: 'Imprimir gráfico',
		rangeSelectorFrom: 'De',
		rangeSelectorTo: 'Para',
		rangeSelectorZoom: 'Zoom',
		resetZoom: 'Limpar Zoom',
		resetZoomTitle: 'Voltar Zoom para nível 1:1',
	}
});