//= require filters

paapApp.controller("testChartController", ['$scope','$http', '$filter', function($scope, $http, $filter) {
    $scope.formula = "";
    $scope.tagType = "";
    $scope.tagLocation = "" ;
    $scope.selectedFilters = [] ;

    $scope.piData = null ;

    $scope.labels = {
        average: {
            windowInMinutes: 'Janela de tempo (em minutos)'
        },
        kalman: {
            processVariance: 'Variância do processo',
            measureVariance: 'Variância da medida'
        },
        minmax: {
            min: 'Mínimo',
            max: 'Máximo'
        }
    }

    // Propriedades default (estão como String pois este será o padrão quando for modificado no input)
    $scope.filtersConfig = {
        average: {
            windowInMinutes: 2
        },
        kalman: {
            processVariance: 100,
            measureVariance: 100000
        },
        minmax: {
            min: null,
            max: null
        }
    } ;

    $scope.updateFilter = function(selectedFilter) {
        selectedFilter.properties = angular.copy($scope.filtersConfig[selectedFilter.kind]) ;
        $scope.updateChartWithCachedData() ;
    } ;

    $scope.removeFilter = function(selectedFilter) {
        $scope.selectedFilters.splice($scope.selectedFilters.indexOf(selectedFilter), 1);
        $scope.updateChartWithCachedData() ;
    }

    $scope.upFilter = function(filterIndex) {
        var tmp = $scope.selectedFilters[filterIndex-1] ;
        $scope.selectedFilters[filterIndex-1] = $scope.selectedFilters[filterIndex] ;
        $scope.selectedFilters[filterIndex] = tmp ;
        $scope.updateChartWithCachedData() ;
    }

    $scope.downFilter = function(filterIndex) {
        var tmp = $scope.selectedFilters[filterIndex+1] ;
        $scope.selectedFilters[filterIndex+1] = $scope.selectedFilters[filterIndex] ;
        $scope.selectedFilters[filterIndex] = tmp ;
        $scope.updateChartWithCachedData() ;
    }

    $scope.addFilter = function() {
        $scope.selectedFilters.push({
            kind: 'average',
            enabled: true,
            properties: angular.copy($scope.filtersConfig['average'])
        }) ;
        $scope.updateChartWithCachedData() ;
    }

	$scope.updateChart = function() {
        $scope.chart.showLoading() ;
        $http.get("/painelep/admin/tagPI/calculate", {
            params: {
                formula: $scope.formula,
                tagType: $scope.tagType,
                tagLocation: $scope.tagLocation,
                date: $filter('date')($scope.dt, 'yyyy-MM-dd')
            } } )
        .success(function(data, status, headers, config) {
            $scope.chart.hideLoading() ;
            $scope.piData = data.samples ;
            $scope.lastDt = $scope.dt ;
            $scope.updateChartWithCachedData() ;
        })
        .error(function(data, status, headers, config) {
            $scope.chart.hideLoading() ;
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            if ( data.error ) {
                alert("Erro ao obter dados: " + data.error) ;
            } else {
                alert("Erro ao obter dados") ;
            }
        });
	};

	$scope.updateChartWithCachedData = function() {
	    // Crate the filters
	    if( $scope.piData != null ) {
            var filters = [] ;
            for( var filterIndex in $scope.selectedFilters ) {
                var filterSpec = $scope.selectedFilters[filterIndex] ;
                if ( filterSpec.enabled ) {
                    if( filterSpec.kind == "average" ) {
                        filters.push( new AverageFilter(filterSpec.properties.windowInMinutes) ) ;
                    } else if( filterSpec.kind == "kalman" ) {
                        filters.push(new KalmanFilter(filterSpec.properties.processVariance, filterSpec.properties.measureVariance)) ;
                    } else if( filterSpec.kind == "minmax" ) {
                        filters.push(new MinMaxFilter(filterSpec.properties.min, filterSpec.properties.max)) ;
                    }
                }
            }

            var filteredData = [] ;
            for(var i in $scope.piData) {
                // Create a copy of piData's sample
                var sample = [$scope.piData[i][0], $scope.piData[i][1]] ;

                try {
                    // Apply filters
                    for(var f in filters) {
                        sample[1] = filters[f].apply(sample)
                    }

                    // Add to result array
                    filteredData.push(sample)
                } catch (e) {
                    // Erro esperado
                    //console.log(e) ;
                }
            }

            // adds a plotline only one point should be displayed
            if (filteredData.length  == 1) {
                $scope.chart.yAxis[0].addPlotLine({
                    value: filteredData[0][1],
                    color: '#06a6fd',
                    width: 1,
                    dashStyle: 'line',
                });
            }
            // add the point
            $scope.chart.series[0].setData(filteredData, true, true, true);
        }
	};

    $scope.checkFormula = function() {
        $http.get("/painelep/admin/tagPI/checkFormula", { params: {formula: $scope.formula} } ).
            success(function(data, status, headers, config) {

            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    };

    $scope.lastDt = null;
    $scope.dt = new Date();
    $scope.dt.setHours(0,0,0,0) ;

	$scope.init = function(divId, formula, tagType, tagLocation, filters) {
		var container = $('#' + divId);
		$scope.formula = formula;
		$scope.tagType = tagType ;
		$scope.tagLocation = tagLocation ;
		$scope.selectedFilters = filters ;

		// Add 'enabled' property
		for( var i in $scope.selectedFilters ) {
		    $scope.selectedFilters[i].enabled = true ;
		}

        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });

        $scope.chart = new Highcharts.Chart({
            chart: {
                renderTo: container[0],
                zoomType: 'x',
                height: 300
            },
            credits: false,
            exporting: 'disabled',
            title: {
                text: 'Gráfico Simulado'
            },
            yAxis: {
                title: {
                    text: null
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }],
                min: 0
            },
            xAxis: {
                type: 'datetime',
                title: {
                    text: null
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                data: [],
                allowPointSelect: false,
                lineWidth: 1,
                id: 'primary'
            }]
        });
	};
}]);
