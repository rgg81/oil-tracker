//= require angular-sync

paapApp.factory("TagData", ['$websocket', '$rootScope', '$location', function($websocket, $rootScope, $location) {

    $rootScope.online = false ;
    $rootScope.initialLoading = true ;

	var protocol = $location.protocol().replace("http","ws");
	var dataStream = createWebsocketInstance();

    function createWebsocketInstance() {
        var checkSessionWS = function () {
            $.ajax( {url: "/painelep/monitor/index", cache: false }).done(function() {
                setTimeout(function(){ location.reload(true); }, 60000);
            }).fail(function() {
                setTimeout(function(){ $('#error-message-modal').modal('show'); }, 3000);
            });
        }
        var dataStream = $websocket(protocol + '://' + $location.host() + ':' + $location.port() + '/painelep/pi-ws');
        dataStream.onClose(function() {
            $rootScope.online = false ;
            checkSessionWS();
            $rootScope.$apply() ;
            //console.log("reconnecting....")
            //dataStream = createWebsocketInstance();
        });
        dataStream.onError(function() {
            $rootScope.online = false ;
            checkSessionWS();
            $rootScope.$apply() ;
        });
        dataStream.onOpen(function() {
            $rootScope.online = true ;
            $rootScope.$apply() ;
        });
        var newMessage = "";
        dataStream.onMessage(function(message) {
            newMessage = JSON.parse(message.data)
            console.log(newMessage)
            if (Object.keys(newMessage).length > 0) {
                $rootScope.lastMessage = newMessage;
            }
        });

        return dataStream;
    }

    var methods = {
        get: function() {
          dataStream.send(JSON.stringify({ action: 'get' }));
        }
    };
    return methods;


}]);

paapApp.controller("webSocketController", ['$rootScope','$scope','$http','$window','$element','TagData', function($rootScope, $scope, $http, $window, $element, TagData) {
    $scope.lastStoredTimes = {};

	$rootScope.sitopDate = false ;
	$rootScope.bdoDate = false ;
    $scope.$parent.newestDataDate = null ;
    $rootScope.lastSitop = 0 ;

    $scope.properties = new ContextProperties($scope);

	$scope.init = function(visibleUepsIds, cardView) {
		$http.get("/painelep/api/UEP/getAllOnlineUEPs").
		success(function(data, status, headers, config) {
			$scope.ueps = data;
		}).error(function(data, status, headers, config) {

		});

        $http.get("/painelep/PIData/uepsInitialData", {params: {ids: visibleUepsIds, nosamples: cardView} } ).
        success(function(data, status, headers, config) {
            var ids = visibleUepsIds.split(",")
            for (var i in ids) {
                var uepId = ids[i];
                $scope.$broadcast("new-uep-data-" + uepId, {
                    event: 'initialData',
                    data: data[uepId]
                });
            }
        }).error(function(data, status, headers, config) {

        });


        $http.get("/painelep/PIData/lastTimes/").
        success(function(data, status, headers, config) {
            $scope.updateClock(data);
        }).error(function(data, status, headers, config) {
        });

	};

    $scope.updateClock = function(timesByTag){
        var max = 0;
        for(var tag in timesByTag){
            if(timesByTag[tag] > max){
                max = timesByTag[tag]
            }
        }
        if(max != 0){
            $scope.$parent.newestDataDate = new Date(max)
        }
    }

    $rootScope.updateSitopDate = function (sitopDate) {
        if( !$scope.sitopDate || $scope.sitopDate < sitopDate ) {
            $scope.sitopDate = sitopDate ;
            $scope.bdoDate = new Date(sitopDate.getTime()) ;
            $scope.bdoDate.setDate($scope.sitopDate.getDate()-1) ;
        }
    }

	$rootScope.$watch(function() { return $rootScope.wi100; }, function() {
        for (var i in $scope.ueps) {
            var uep = $scope.ueps[i];
            $scope.$broadcast("new-uep-data-" + uep.id, {event: 'wiOptionChanged'});
        }
	}) ;

	$rootScope.$watch(function() { return $rootScope.lastMessage; }, function() {
		if ($rootScope.lastMessage)
		{
		    // Event message
		    if ( $rootScope.lastMessage.event ) {
                var event = $rootScope.lastMessage.event ;
                var eventData = $rootScope.lastMessage.data ;
                // Sitop and BDO event
                if ( event == 'updateSitopBdo' ) {
                    $rootScope.sitopDate = false ;
                    $rootScope.bdoDate = false ;
                    $rootScope.lastSitop = 0 ;

                    for (var i in $scope.ueps) {
                        var uep = $scope.ueps[i];
                        $scope.$broadcast("new-uep-data-" + uep.id, $rootScope.lastMessage);
                    }

                // UEP-Reload event
                } else if ( event == 'uepReloadStart' || event == 'uepReloadFinished' ) {
                    // eventData contains the UEP's ids to reload
                    for (var i in eventData) {
                        var uepId = eventData[i] ;
                        $scope.$broadcast("new-uep-data-" + uepId, $rootScope.lastMessage);
                    }

                } else if ( event == 'newSnapshot24h') {
                    for (var i in eventData.ueps) {
                        $scope.$broadcast("new-uep-data-" + i, $rootScope.lastMessage);
                    }
                }else if(event == 'newdata'){
                    var tagsReceived = $rootScope.lastMessage.data;
                    console.log(tagsReceived) ;

                    //Verifies if all expected ueps are replying
                    for (var i in $scope.ueps) {
                        var uep = $scope.ueps[i];
                        if (tagsReceived[uep.id]) {
                            var thisUEP = tagsReceived[uep.id].all;
                            //check if the UEP has sent any actual data and sums
                            if (thisUEP && thisUEP.length > 0) {
                                $scope.lastStoredTimes[uep.id] = thisUEP[thisUEP.length - 1][TIME];
                                $scope.$broadcast("new-uep-data-" + uep.id,tagsReceived[uep.id]);
                            }
                           //If the UEP fails to send data, it's added to the corresponding array. Last sum result is kept on the screen
                            else {
                                console.log("UEP " + uep.name + " failed to reply");
                            }
                        }
                    }

                    $rootScope.events = tagsReceived.events ;

                    $scope.updateClock($scope.lastStoredTimes);
                }
		        return ;
		    }
		}
	});
}]);

function TimeArrayWithWindow(name, windowInMs) {
    this.windowInMs = windowInMs ;
    this.name = name ;
    this.samples = [] ;

    this.getSamples = function() {
        return this.samples ;
    }

    this.length = function () {
        return this.samples.length ;
    }

    this.last = function () {
        return (this.samples.length > 0) ? this.samples[this.samples.length-1] : null ;
    }

    this.clear = function() {
        this.samples = [];
    }

    this.addSamples = function(newSamples) {
        if( newSamples.length > 0 ) {
            if ( this.samples.length == 0 ) {
                // TODO: cut array since splitDate?
                this.samples = newSamples ;
            } else {
                var newestData = newSamples[newSamples.length-1][TIME] ;
                var splitDate = newestData - windowInMs ;

                // Find data outside window
                var cutoutIdx = -1 ;
                for( var i in this.samples ) {
                    var sample = this.samples[i] ;
                    if ( sample[TIME] < splitDate ) {
                        cutoutIdx = i ;
                    } else {
                        break ;
                    }
                }

                // Remove data outside window
                if( cutoutIdx >= 0 ) {
                    this.samples.splice(0, cutoutIdx+1) ;
                }

                // Update the last sample (if so)
                var currentNewestData = this.samples[this.samples.length-1] ;
                if ( currentNewestData[TIME] == newSamples[0][TIME] ) {
                    currentNewestData[VALUE] = newSamples[0][VALUE] ;
                }

                // Add newer samples...
                for( var i in newSamples ) {
                    var newSample = newSamples[i] ;
                    if( newSample[TIME] > currentNewestData[TIME] ) {
                        this.samples.push(newSample) ;
                    }
                }
            }
        }
    }
}

function ContextProperties(scope) {
    scope.change = 0;
    this.children = [] ;
    this.parent = scope.$parent.properties;

    this.get = function(propName){
        if(this[propName] == null){
            var sum = null;
            for( var i in this.children ) {
                if(this.children[i] != null){
                    var propValue = this.children[i].get(propName);
                    if(isFinite(propValue)){
                        sum = (sum == null ? propValue : sum + propValue);
                    }
                }
            }
            return sum;
        }else{
            return this[propName];
        }

    }

    this.set = function(propName,value) {
        this[propName] = value;
    }

    this.setAll = function(properties) {
        for(var propName in properties) {
            this.set(propName, properties[propName]) ;
        }
    }

    // Add a children to this
    this.registerChildren = function(child) {
        this.children.push(child) ;
    }

    // Register itself with parent
    if ( this.parent ) {
        this.parent.registerChildren(this) ;
    }
}

paapApp.controller("uoController", ['$rootScope','$scope', '$http', function($rootScope, $scope, $http) {
    $scope.properties = new ContextProperties($scope);
}]);

paapApp.controller("baseController", ['$rootScope','$scope', '$http', '$syncCallback', function($rootScope, $scope, $http, $syncCallback) {
    $scope.properties = new ContextProperties($scope);
	$scope.base = {} ;
    $scope.isReloading = false ;
    $scope.reloadfail = false ;
    $scope.cutoff = 500;
    $scope.wi = 100 ;

    $scope.base.correctValueArray = function(data) {
        for(var i in data) {
            var sample = data[i] ;
            sample[VALUE] = $scope.base.correctValue(sample[VALUE]) ;
        }
    }

    $scope.base.correctValue = function(value) {
        if ( value && $rootScope.wi100 ) {
            return value * 100 / $scope.wi ;
        } else {
            return value ;
        }
    }

    var AVG_VALUE_24H = '24h_avg';

    function processInitialData(data, sync) {

        // Pre-process data object
        data.avg24 = $scope.base.correctValue(data.avg24) ;
        data.expected = $scope.base.correctValue(data.expected) ;
        data.lastReceivedValue = $scope.base.correctValue(data.lastReceivedValue) ;
        data.preliminary = $scope.base.correctValue(data.preliminary) ;
        $scope.base.correctValueArray(data.highSamples) ;
        $scope.base.correctValueArray(data.lowSamples) ;

        var postProcess = function() {  // Decrease from callback sync
              if ( !$scope.base.processErrorData(data) ) {
                  // Update all properties
                  $scope.properties.setAll(data) ;

                  if( typeof(data.expected) != "undefined" ) {
                      // Guarda valores
                      var sitopDate = new Date(data.sitopDate) ;
                      $rootScope.updateSitopDate(sitopDate) ;
                  }

                  if ( $scope.onInitialData ) {
                      $scope.onInitialData(data) ;
                  }
              }
              $rootScope.initialLoading = false ;
          }


        if ( sync ) {
            $syncCallback('initial-data', postProcess) ;
        } else {
            postProcess() ;
        }
    }

    $scope.base.initialData = function(uepId, callback) {
        $syncCallback('initial-data') ; // Add to callback sync
        $rootScope.initialLoading = true ;
        $http.get("/painelep/PIData/uepInitialData/" + uepId, {params: {nosamples: $scope.nosamples} } )
            .success(function(data, status, headers, config) {
                processInitialData(data, true) ;
            }).error(function(data, status, headers, config) {
                $syncCallback('initial-data', function() {
                    if ( callback ) {
                        callback(null) ;
                    }
                });
                return false;
            });
    };

    $scope.base.average24hours = function(uepId,success) {
        $http.get("/painelep/PIData/average24hours/" + uepId ).
        success(function(data, status, headers, config) {
            success($scope.base.correctValue(data.avg));
        })
        .error(function(data, status, headers, config) {
            return false;
        });
    };

    // Get Snapshots avg 24 hours
    $scope.base.snapshotAverage24hours = function(uepId) {
        $http.get("/painelep/PIData/snapshotAverage24hours/" + uepId)
            .success(function(data, status, headers, config) {
                $scope.properties.set('currentSnapshotValue',$scope.base.correctValue(data.currentSnapshotValue));
                $scope.properties.set('previousSnapshotValue',$scope.base.correctValue(data.previousSnapshotValue));
            });
    }

	$scope.base.requestSitopBdoData = function(uepId,chartCallback) {
	    // Get SITOP and BDO
		$http.get("/painelep/PIData/sitopBdo/" + uepId )
            .success(function(data, status, headers, config) {
                // Checa se dados existem...
                if( typeof(data.expected) != "undefined" ) {
                    // Guarda valores
                    $scope.properties.set('expected',$scope.base.correctValue(data.expected));
                    $scope.properties.set('preliminary',$scope.base.correctValue(data.preliminary));
                    var sitopDate = new Date(data.sitopDate) ;
                    $rootScope.updateSitopDate(sitopDate) ;
                    if(chartCallback){
                        chartCallback();
                    }
                }
            });
	};
    $scope.uepStopped = function(value){
        var input = $scope.properties.get('lastReceivedValue');
        if( input != null && typeof(input) != 'undefined' && !isNaN(input)){
            return input < $scope.cutoff;
        }
        return false;
    }


    //also sets the indicator with the appropriate alert class
	$scope.alertClass = function() {
        if($scope.isReloading){
            return "";
        }

        if ( $scope.properties.get('alert') != null ) {
            if ( $scope.properties.get('alert') == -1 ) {
                $("#indicator-uepid" + $scope.uepId).addClass("alerta");
                return "alerta" ;
            } else if ( $scope.properties.get('alert') == 1 ) {
                $("#indicator-uepid" + $scope.uepId).addClass("alerta-positivo");
                return "alerta-positivo" ;
            }
        }
        $("#indicator-uepid" + $scope.uepId).removeClass("alerta").removeClass("alerta-positivo");
        return "" ;
	}

    $scope.stoppedClass = function() {
        if($scope.isReloading){
            return "";
        }
        if ($scope.uepStopped()) {
            $("#indicator-uepid" + $scope.uepId).addClass("stopped-opacity");
            return "stopped-opacity"
        } else {
            $("#indicator-uepid" + $scope.uepId).removeClass("stopped-opacity");
            return ""
        }
    }

    $scope.base.onEvents = function(scope,event,newTagValues){
        // Processa eventos
        if( newTagValues.event == 'updateSitopBdo' ) {
            scope.requestSitopBdoData();
            scope.snapshotAverage24hours();
        } else if( newTagValues.event == 'newSnapshot24h' ) {
            scope.snapshotAverage24hours();
        } else if( newTagValues.event == 'uepReloadStart' ) {
            scope.reloadfail = false ;
            scope.isReloading = true ;
        } else if( newTagValues.event == 'uepReloadFinished' ) {
            scope.isReloading = false ;
            // Reload all
            $scope.base.initialData($scope.uepId) ;
        } else if( newTagValues.event == 'wiOptionChanged' ) {
            // Reload all if wi != 100
            if ( scope.wi != 100 ) {
                console.log("Reloading UEP " + $scope.uepId) ;
                $scope.timeOption = "24";
                $scope.base.initialData($scope.uepId) ;
            }
        } else if ( newTagValues.event == 'initialData' ) {
            processInitialData(newTagValues.data, false) ;
        } else {

            // Not an event -> new data available
            scope.onNewData(newTagValues);
        }
    }

    $scope.$watch(function() { return $scope.isReloading }, function(newValue, oldValue) {
        if(!$scope.reloadfail)
            $scope.base.uepReloadStatus(newValue,"Em atualização");
    }) ;

    $scope.$watch(function() { return $scope.reloadfail }, function(newValue, oldValue) {
        if(!$scope.isReloading)
            $scope.base.uepReloadStatus(newValue,"Atualização Falhou");
    }) 

    $scope.base.uepReloadStatus = function(status,msg){
        if(status){
            $scope.properties.set('avg24',null);
            $scope.properties.set('lastReceivedValue',null);
        }
        if($scope.uepReloadStatus){
            $scope.uepReloadStatus(status,msg);
        }
    }

    $scope.base.processErrorData = function (data) {
        if ( data.error ) {
            $scope.isReloading = (data.error == 'reloading') ;
            $scope.reloadfail = (data.error == 'reloadfail') ;
            return true ;
        } else {
            $scope.isReloading = $scope.reloadfail = false ;
            return false ;
        }
    }
}]);

paapApp.controller("cardController", ['$rootScope','$scope','$http','$window','$element', '$timeout', '$controller', function($rootScope, $scope, $http, $window, $element, $timeout, $controller) {
    // Cria extensão do baseController
    angular.extend(this, $controller('baseController', {$scope: $scope}));

    var HIGH_RES_SAMPLES = 'all' ;
    var AVG_VALUE_24H = '24h_avg';
    $scope.nosamples = true ;

    $scope.init = function(uepId, groupName, wi) {
        $scope.uepId = uepId ;
        $scope.groupName = groupName
        $scope.wi = wi ;
        $scope.$on("new-uep-data-"+$scope.uepId, function(event,newTagValues) {
            $scope.base.onEvents($scope,event,newTagValues);
        });

    };

    $scope.requestSitopBdoData = function(){
        $scope.base.requestSitopBdoData($scope.uepId);
    }

    $scope.snapshotAverage24hours = function(){
        $scope.base.snapshotAverage24hours($scope.uepId);
    }

    $scope.onNewData = function(newTagValues) {
        //console.log(newTagValues);

        var newHighResData = newTagValues[HIGH_RES_SAMPLES] ;
        $scope.properties.set('alert', newTagValues.alert) ;

        if ( newHighResData.length > 0 ) {
            var newestData = newHighResData[newHighResData.length-1] ;
            $scope.properties.set('lastReceivedValue',Math.round($scope.base.correctValue(newestData[VALUE])));
        } else {
            console.log("No data received from " + $scope.chartName);
        }
        $scope.properties.set('avg24',$scope.base.correctValue(newTagValues[AVG_VALUE_24H]));
    };
}]);

paapApp.controller("chartController", ['$rootScope','$scope','$http','$window','$element', '$timeout', '$controller', '$syncTimeout', function($rootScope, $scope, $http, $window, $element, $timeout, $controller, $syncTimeout) {
    // Cria extensão do baseController
    angular.extend(this, $controller('baseController', {$scope: $scope}));

	var MS_IN_AN_HOUR = 1000 * 60 * 60 ;
	var LOW_RES_SAMPLES = 'medium' ;
	var HIGH_RES_SAMPLES = 'all' ;
    $scope.nosamples = false ;

    var AVG_VALUE_24H = '24h_avg';

	$scope.chartName = "";
    // lowResData: data from the last 24hours
	$scope.lowResData = new TimeArrayWithWindow('lowResData', 24 * MS_IN_AN_HOUR) ;
    // highResData: data from the last hour
	$scope.highResData = new TimeArrayWithWindow('highResData', MS_IN_AN_HOUR) ;

    $scope.slider = null;

    $scope.uepReloadStatus = function(status,msg){
        if(status == true){
            $scope.chart.get('primary').setData([]);
            $scope.chart.get('series-live').setData([]);
            $scope.chart.showLoading(msg) ;
        }else{
            $scope.chart.hideLoading() ;
        }
    }

	$scope.updateChart = function(updateAll) {
	    // Update all chart
	    if( $scope.highResData.length() > 0 || $scope.lowResData.length() > 0 ) {
	        var highSamples = $scope.highResData.getSamples() ;
	        var lowSamples = $scope.lowResData.getSamples() ;
	        var oldestHighRes = (highSamples.length > 0) ? highSamples[0][TIME] : Number.MAX_VALUE ;

	        var result = [] ;

	        // Add low-res samples until oldestHighRes
	        for(var i in lowSamples) {
	            var sample = lowSamples[i] ;
	            if( sample[TIME] < oldestHighRes ) {
	                result.push(sample) ;
	            } else {
	                break ;
	            }
	        }

	        // Add high-res samples
	        for(var i in highSamples) {
                result.push(highSamples[i]) ;
            }

            $scope.chart.get('primary').setData(result, /*redraw:*/ updateAll, /*animation:*/ false, /*updatePoints:*/ false);
	    }
	}

    $scope.requestSitopBdoData = function(){
        $scope.base.requestSitopBdoData($scope.uepId, $scope.applySitopBdoToChart);
    }

    $scope.snapshotAverage24hours = function(){
        $scope.base.snapshotAverage24hours($scope.uepId);
    }

    $scope.onInitialData = function(data) {
           $scope.highResData.clear();
           $scope.lowResData.clear();
           $scope.highResData.addSamples(data.highSamples) ;
           $scope.lowResData.addSamples(data.lowSamples) ;
           $scope.applySitopBdoToChart() ;
           $scope.applyAverage24hToChart() ;
           $scope.updateChart(true) ;
    }

    $scope.applyAverage24hToChart =  function(){
        $scope.chart.yAxis[0].removePlotLine('avg24');
        $scope.chart.yAxis[0].addPlotLine({
            value: $scope.properties.avg24,
            color: '#ffffff',
            width: 2,
            dashStyle: 'ShortDot',
            id: 'avg24',
            zIndex: 1000
        });
    }

    $scope.apply17hLineToChart =  function(){
        //new Date(year, month, day, hours, minutes, seconds, milliseconds)
        var h17 = new Date();
        h17.setHours(17);
        h17.setMinutes(0);
        h17.setSeconds(0);

        var h17_yesterday = new Date(h17.getTime());
        h17_yesterday.setDate(h17.getDate()-1);
        var dates = [h17, h17_yesterday];
        for(i in dates){
            $scope.chart.xAxis[0].addPlotLine({
                label: {
                    text: '17h',
                     style: {
                        fontSize: '12px',
                        color: 'red'
                    }

                },
                color: 'red',
                dashStyle: 'Dot',
                value: dates[i].getTime(),
                width: 1,
                zIndex: 999
            });
        }
    }

    $scope.applySitopBdoToChart =  function(){
        $scope.chart.yAxis[0].removePlotLine('expected');
        $scope.chart.yAxis[0].addPlotLine({
            value: $scope.properties.expected,
            color: '#cb8318',
            width: 1,
            dashStyle: 'line',
            id: 'expected'
        });

        $scope.chart.yAxis[0].removePlotLine('preliminary');
        $scope.chart.yAxis[0].addPlotLine({
            value: $scope.properties.preliminary,
            color: '#9d22ff',
            width: 1,
            dashStyle: 'LongDash',
            id: 'preliminary'
        });

        // Atualiza gráfico para exibir minimamente as linhas
        $scope.chart.yAxis[0].update( {
            minRange: Math.max($scope.properties.expected, $scope.properties.preliminary)
        }) ;
    }

    $scope.download =  function(){
         $scope.chart.exportChartLocal({
            exporting: {
                chartOptions: {
                    title: {
                        text: $scope.chartName
                    },
                    subtitle: {
                        text: $scope.properties.get('lastReceivedValue').toLocaleString(),
                        style: { "color": "#05a8fd" }
                    },
                    chart: { backgroundColor: "#21272c"}
                },
                filename:  "grafico-" + $scope.chartName
            }
        });
    }

    $scope.initData = function() {
    }

	$scope.init = function(uepId, chartName, groupName, enableZoom, enable17hline, wi) {
        $scope.slider = $("#"+groupName+".slider");
        $scope.enable17hline = enable17hline;
		$scope.uepId = uepId ;
        $scope.groupName = groupName
        $scope.wi = wi ;
        $scope.$on("new-uep-data-"+$scope.uepId, function(event,newTagValues) {
            $scope.base.onEvents($scope,event,newTagValues);
        });

		$scope.chartName = chartName;
        var $container = $element.find('.chart-container').length > 0 ?
                            $element.find('.chart-container')[0] :
                                $('<div>').attr('class', 'chart-container').appendTo($element)[0];

		Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });

        $scope.chart = new Highcharts.Chart({
            exporting:{
                buttons:{ contextButton :{ enabled : false}},
            },
            chart: {
                renderTo: $container,
                events: {
                    load: $scope.initData
                },
                zoomType: enableZoom ? "x" : null,
            },
            tooltip: {
                followPointer: false,
                followTouchMove: false,
            },
            plotOptions: {
                line: {
//                    enableMouseTracking: enableZoom   // Disabling mouse-tracking saves some memory and CPU on page
                }
            },
            credits: false,
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            yAxis: {
                title: {
                    text: ''//'barris/dia'
                },
                min: 0,
                minRange: 500,   // Evita que gráficos com baixa produção apresentem muito ruído
                labels: {
                    formatter: function() {

                        if(this.value > 1000){
                            return '<tspan style="font-size:9px">' + this.value/1000 + 'mil</tspan>';
                        }

                        return this.value

                    }
                },
            },
            xAxis: {
                type: 'datetime',
                tickInterval: 60*60*1000*2,
                title: {
                    text: null
                },
                labels: {
                	formatter: function() {
                		var hour = Highcharts.dateFormat('%H', this.value);
                		if (hour == 0) {
                			return Highcharts.dateFormat('%e. %b',this.value)
                		}
                		else if (hour < 10)
                			hour = hour.substring(1,2);
                		return hour + "h";
                	}
                },
                dateTimeLabelFormats: {


            	}
            },
            legend: {
                enabled: false
            },
            series: [{
                data: [],
                allowPointSelect: false,
                lineWidth: 2,
                id: 'primary',
                color: '#06a6fd',
                name: chartName
            }, {
               data: [],
               allowPointSelect: false,
               lineWidth: 2,
               id: 'series-live',
               color: '#55D788',
               marker: {
                    enabled: true,
                    radius: 4,
                    symbol: 'circle'
               }
           }]
        });
        if($scope.enable17hline){
            $scope.apply17hLineToChart();
        }
	};

    $scope.timeOption = "24"
    $scope.changeTimeOption = function(newValue) {
        $scope.timeOption = newValue ;
        $http.get("/painelep/PIData/lastSamplesUep/" + $scope.uepId + "/" + $scope.timeOption).
            success(function(data, status, headers, config) {
                $scope.highResData.clear();
                $scope.lowResData.clear();

                $scope.lowResData = new TimeArrayWithWindow('lowResData', $scope.timeOption * MS_IN_AN_HOUR) ;
                $scope.base.correctValueArray(data.highSamples) ;
                $scope.base.correctValueArray(data.lowSamples) ;
                $scope.highResData.addSamples(data.highSamples);
                $scope.lowResData.addSamples(data.lowSamples);
                //$scope.chart.get('primary').setData(data.lowSamples, true, true, true);
                $scope.updateChart(true);
            }).error(function(data, status, headers, config) {
                console.error("Failed while getting the total average from the last 24 hours");
            });
    }


    $scope.onNewData = function(newTagValues) {
        stopSlide();
        //console.log(newTagValues);
        var newHighResData = newTagValues[HIGH_RES_SAMPLES] ;
        var newLowResData = newTagValues[LOW_RES_SAMPLES] ;

        // Update WI for new values
        $scope.base.correctValueArray(newHighResData) ;
        $scope.base.correctValueArray(newLowResData) ;

        $scope.properties.set('alert', newTagValues.alert) ;

        if ( newHighResData.length > 0 ) {
            $scope.highResData.addSamples(newHighResData) ;
            $scope.lowResData.addSamples(newLowResData) ;

            $scope.updateChart(false) ;

            // Atualiza o dado ao vivo e seta timer para esconde-lo
            var seriesLive = $scope.chart.get('series-live');
            var newestData = newHighResData[newHighResData.length-1] ;
            var addNewPoint = function(liveserie,data){
                liveserie.setData([data], /*redraw:*/ true, /*animation:*/ false, /*updatePoints:*/ false);
                $syncTimeout('new-data', 3000, function() {
                    if ( liveserie.data.length > 0 ) {
                        liveserie.data[0].remove(/*redraw:*/ true, /*animation:*/ false) ;
                    }
                });
            }
            //adiciona o ponto somente ao fim da transição ou caso ela nao esteja ocorrendo
            if($scope.slider.hasClass('slider-animated')){
                $scope.slider.one('sliderAnimationEnded',function(event) {
                    console.log('late add point');
                    addNewPoint(seriesLive,newestData);
                });
            }else{
                addNewPoint(seriesLive,newestData);
            }

            //console.log(inserted + " inserted and " + updated + " updated of a total of " + numNewSeriePoints + " points");
            $scope.properties.set('lastReceivedValue',Math.round(newestData[VALUE]));
        } else {
            console.log("No data received from " + $scope.chartName);
        }
        $scope.properties.set('avg24', $scope.base.correctValue(newTagValues[AVG_VALUE_24H])) ;
        $scope.applyAverage24hToChart();

        if($scope.enable17hline){
            $scope.apply17hLineToChart();
        }
        setTimeout(function(){ initSlide(); }, 3000);


    }

}]);
