//= require filters

paapApp.controller("uepController", ['$scope','$http', function($scope, $http) {

    var lowProduction = 1000 ;

    $scope.shortWindow = null ;
    $scope.longWindow = null ;
    $scope.alertPercent = null ;
    $scope.tagId = null ;
    $scope.samples = null ;

    $scope.clearAlert = function() {
        $scope.shortWindow = null ;
        $scope.longWindow = null ;
        $scope.alertPercent = null ;

        $scope.updateBands() ;
    }

    function valid(number) {
        return number && !isNaN(number) && parseInt(number) != NaN ;
    }

    $scope.updateBands = function() {
        if ( $scope.samples && $scope.samples.length > 0 && valid($scope.shortWindow) && valid($scope.longWindow) && valid($scope.alertPercent) ) {
            var lowSamples = $scope.samples ;
            var derivate = [] ;
            var movingAvgLong = [] ;
            var movingAvgShort = [] ;
            var newBands = [] ;

            var longMovingAverage = new AverageFilter($scope.longWindow) ;
            var shortMovingAverage = new AverageFilter($scope.shortWindow) ;
            var green = 'rgba(0, 255, 0, 0.5)' ;
            var red = 'rgba(255, 0, 0, 0.5)' ;

            var beginLow = -1 ;
            var beginHigh = -1 ;
            var current ;

            function closeHighBands() {
                if ( beginHigh != -1 ) {
                    newBands.push({
                        color: green,
                        from: beginHigh,
                        to: current[TIME]
                    }) ;
                    beginHigh = -1 ;
                }
            }

            function closeLowBands() {
                if ( beginLow != -1 ) {
                    newBands.push({
                        color: red,
                        from: beginLow,
                        to: current[TIME]
                    }) ;
                    beginLow = -1 ;
                }
            }

            var range = parseFloat($scope.alertPercent) ;

            for( var i in lowSamples ) {
                current = lowSamples[i] ;
                var longAvgValue = longMovingAverage.apply(current) ;
                var shortAvgValue = shortMovingAverage.apply(current) ;

                movingAvgLong.push([current[TIME], longAvgValue]) ;
                movingAvgShort.push([current[TIME], shortAvgValue]) ;
                if ( i > 0 ) {
                    var last = lowSamples[i-1] ;
                    var diffValue = shortAvgValue - longAvgValue ;

                    if ( longAvgValue > lowProduction ) {
                        var rate = diffValue / longAvgValue * 100 ;

                        if ( rate > range ) {
                            closeLowBands() ;
                            if ( beginHigh == -1 ) {
                                beginHigh = current[TIME] ;
                            }
                        } else if ( rate < -range ) {
                            closeHighBands() ;
                            if ( beginLow == -1 ) {
                                beginLow = current[TIME] ;
                            }
                        } else {
                            closeHighBands() ;
                            closeLowBands() ;
                        }
                        derivate.push( [current[TIME], rate] ) ;
                    } else {
                        closeHighBands() ;
                        closeLowBands() ;
                    }
                }
            }

            closeHighBands() ;
            closeLowBands() ;

            var correctBegin = lowSamples[0][TIME] + Math.max($scope.longWindow, $scope.shortWindow) * 1000 * 60 ;

            $scope.chart.xAxis[0].update({
                plotBands: newBands,
                plotLines : [{
                    value : correctBegin,
                    color : 'red',
                    dashStyle : 'shortdash',
                    width : 1,
                    label : {
                        text : 'Cut-off'
                    }
                }]
            }) ;

            $scope.chart.get('variation').setData(derivate, /*redraw:*/ true, /*animation:*/ false, /*updatePoints:*/ true);
            $scope.chart.get('avg-long').setData(movingAvgLong, /*redraw:*/ true, /*animation:*/ false, /*updatePoints:*/ true);
            $scope.chart.get('avg-short').setData(movingAvgShort, /*redraw:*/ true, /*animation:*/ false, /*updatePoints:*/ true);
        } else {
            // Remove bands and plotlines
            $scope.chart.xAxis[0].update({
                plotBands: [],
                plotLines : []
            }) ;
        }
    } ;

    $scope.reloadGraph = function() {
        if ( valid($scope.tagId) ) {
            $http.get("/painelep/PIData/lastsamplestag/" + $scope.tagId + "?resolution=all").
                success(function(data, status, headers, config) {
                    $scope.samples = data ;
                    $scope.chart.get('primary').setData(data, true, true, true);
                    $scope.updateBands() ;
                }).error(function(data, status, headers, config) {
                    console.error("Failed while getting the total average from the last 24 hours");
                });
        }
    }

	$scope.init = function(divId, shortWindow, longWindow, alertPercent, tagId) {
		var container = $('#' + divId);
		$scope.shortWindow = parseInt(shortWindow) || null;
		$scope.longWindow = parseInt(longWindow) || null ;
		$scope.alertPercent = parseInt(alertPercent) || null ;
		$scope.tagId = parseInt(tagId) || 0 ;

        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });

        $scope.chart = new Highcharts.Chart({
            chart: {
                renderTo: container[0],
                zoomType: 'x',
                height: 500
            },
            credits: false,
            title: {
                text: 'Gráfico Simulado'
            },
            exporting: 'disabled',
            yAxis: [{
                title: {
                    text: ''
                },
                plotLines : [{
                    value : lowProduction,
                    color : 'red',
                    dashStyle : 'shortdash',
                    width : 1,
                    label : {
                        text : 'Cut-off'
                    }
                }],
                min: 0,
                minRange: 500   // Evita que gráficos com baixa produção apresentem muito ruído
            }, {
                title: {
                    text: ''
                },
                opposite: true,
            }],
            xAxis: {
                type: 'datetime',
                title: {
                    text: null
                }
            },
            legend: {
                enabled: true
            },
            series: [{
                id: 'variation',
                data:[],
                lineWidth: 2,
                yAxis: 1,
                color: '#ff00ff',
                name: 'Variation',
                visible: false
            }, {
                id: 'avg-long',
                data:[],
                lineWidth: 2,
                color: '#aa0',
                name: 'Long window'
            }, {
                id: 'avg-short',
                data:[],
                lineWidth: 2,
                color: '#0aa',
                name: 'Short Window',
                visible: false
            }, {
               data: [],
               allowPointSelect: false,
               lineWidth: 2,
               id: 'primary',
               name: 'Production',
               color: 'black'
           }]
        });

        $scope.reloadGraph() ;
	};


}]);



