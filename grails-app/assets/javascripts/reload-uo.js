//= require filters

paapApp.controller("reloadUOController", ['$scope','$http', function($scope, $http) {

    $scope.checkbox = {};
    $scope.data = {};
    $scope.rows = [];
    $scope.modal;
    $scope.reloading = true;

    $scope.init = function(uoname,ueps,modalId){
        $scope.uoname = uoname;
        $scope.ueps = ueps;

        $('#'+ modalId).on('shown.bs.modal', $scope.getUepTags);
        $scope.modal = $('#'+ modalId);
    }

    $scope.getUepTags = function(){
        var url = "/painelep/admin/UEP/childrenTagsFromUEPS?ueps=" + $scope.ueps.join('&ueps=');
        $scope.reloading = true;
        $http.get(url).success(function(data, status, headers, config) {
            $scope.data = data;
            for(var uep_id in data){
                var uep = data[uep_id];
                for (i = 0; i < uep.tags.length; i++) {
                    if(!$scope.checkbox[uep.id])
                        $scope.checkbox[uep.id] = {}
                    $scope.checkbox[uep.id][uep.tags[i].id] = !(uep.tags[i].reloading || uep.tags[i].reloadStatus == 'QUEUE');
                }
            }
            $scope.rows = $scope.torows(data,3);
            $scope.reloading = false;
        }).error(function(data, status, headers, config) {
            $scope.reloading = false;
            console.error("Failed while getting ");
        });

    }

    $scope.reload = function(){
        //console.log($scope.checkbox)
        $scope.reloading = true;
        var data = [] ;
        for (var uepid in $scope.checkbox) {
            var tags = []
            for (var tagid in $scope.checkbox[uepid]) {
                if($scope.checkbox[uepid][tagid]){
                    tags.push(tagid);
                }
            }
            data.push(tags);
        }

        var url = "/painelep/admin/UEP/reloadUEPS";
        $http.post(url,data).success(function(data, status, headers, config) {
            $scope.reloading = false;
            $scope.modal.modal('hide');

            //$scope.getUepTags();
        }).error(function(data, status, headers, config) {
            $scope.reloading = false;
            console.error("Failed while putting ");
        });
    }

    $scope.markGroup = function(uep,mark){
        for (var tagid in $scope.checkbox[uep.id]){
            $scope.checkbox[uep.id][tagid] = mark;
        }
    }

    $scope.torows = function(data,cols){
        var len = data.length,out = [], i = 0;
        while (i < len) {
            var size = i + cols > len ? len : cols
            out.push(data.slice(i, i += size));
        }

        return out;
    }

    $scope.openClose = function(uep){
        var display = $('#reload-detail-' + uep.id).css('display');
        if(display == 'none')
            $('#reload-detail-' + uep.id).show();
        else
            $('#reload-detail-' + uep.id).hide();
    } 

    $scope.anyTagToReload = function(){
        var ueps = $scope.checkbox;

        for(var i in ueps){
            for(var j in ueps[i]){
                if(ueps[i][j]){
                    return true;
                }
            }
        }
        return false;
    }

    $scope.uepClass = function(uep){
        var tags = $scope.checkbox[uep.id];
        var sum_true = 0;
        var len = 0
        for(var i in tags){
            if(tags[i]){
                sum_true++;
            }
            len++;
        }
        /*if(sum_true == 0)
            return 'panel panel-default';*/

        /*if(sum_true == len)
            return 'panel panel-info';*/

        return 'panel panel-warning';
    }

    $scope.onqueue = function(uep){
        for(var i in uep.tags){
            if(uep.tags[i].reloadStatus == 'QUEUE'){
                return true
            }
        }
        return false;
    }

    $scope.onreloading = function(uep){
        for(var i in uep.tags){
            if(uep.tags[i].reloading){
                return true
            }
        }
        return false;
    }

    $scope.onfailed = function(uep){
        for(var i in uep.tags){
            if(uep.tags[i].reloadStatus == 'FAILED'){
                return true
            }
        }
        return false;
    }

    

    $scope.checkonlycalc = function(){

        var ueps = $scope.data;
        for(var i in ueps){
            var uep = ueps[i];
            for(var j in uep.tags){
                var tag = ueps[i].tags[j];
                if(tag.type !="CALCULATED"){
                    $scope.checkbox[uep.id][tag.id] = false;
                }
            }
        }
    }
}]);
