// OBS: a implementação deste filtro deve estar sincronizada com TagPIFilterMovingAverage.groovy
function AverageFilter(windowInMinutes) {
    this.windowInMs = windowInMinutes * 60 * 1000 ;
    this.samplesWindow = [] ;

    this.areaSincePrevious = function(idx) {
        // Trapezoid area
        return (this.samplesWindow[idx][VALUE] + this.samplesWindow[idx-1][VALUE]) * (this.samplesWindow[idx][TIME] - this.samplesWindow[idx-1][TIME]) / 2 ;
    }

    this.apply = function(sample) {
        // Checa se o novo sample é mais antigo do que o anterior
        if ( this.samplesWindow.length > 0 && this.samplesWindow[this.samplesWindow.length-1][TIME] > sample[TIME] ) {
            throw "Novo sample é mais antigo do que série atual" ;
        }

        // Atualiza a lista com um clone (evita que modificações externas alterem a soma)
        this.samplesWindow.push([sample[TIME], sample[VALUE]]) ;  // Cria uma cópia

        // Define o tempo de corte
        var minSampleTime = sample[TIME] - this.windowInMs ;

        // Remove todas os samples menores do que minSampleTime e atualiza a soma
        while( this.samplesWindow.length > 0 && this.samplesWindow[0][TIME] < minSampleTime) {
            this.samplesWindow.shift() ;
        }

        var area = 0 ;
        for( var i = this.samplesWindow.length-1; i > 0; i-- ) {
            area += this.areaSincePrevious(i) ;
        }
        area += (this.samplesWindow[0][TIME] - minSampleTime) * this.samplesWindow[0][VALUE] ;

        return area / this.windowInMs ;
    }
}

// OBS: a implementação deste filtro deve estar sincronizada com TagPIFilterKalman.groovy
function KalmanFilter(processVariance, measureVariance) {
    this.xhatminus = 0 ;
    this.minusP = 0 ;
    this.paramK = 0 ;
    this.paramP = 0 ;
    this.lastValue = null ;
    this.coldStartTotal = 0 ;
    this.lastDate = null ;

    this.processVariance = processVariance ;
    this.measureVariance = measureVariance ;

    var kalmanMaxToleratedTimeBetweenSample = 1000*60*2 ;
    var totalSamplesColdStart = 5 ;

    this.checkStateAndResetIfNeeded = function(sample) {
         if(this.lastDate != null) {
             var diffTime = sample[TIME] - this.lastDate ;
             // Checa se o novo sample é mais antigo do que a ultima data
             if (this.lastDate > sample[TIME]) {
                 throw "Novo sample é mais antigo do que série atual" ;
             }

             if (diffTime > kalmanMaxToleratedTimeBetweenSample) {
                 this.paramP = 0
                 this.minusP = null
                 this.xhatminus = null
                 this.paramK = null
                 this.lastDate = sample[TIME]
                 this.lastValue = sample[VALUE]
                 this.coldStartTotal = 0
                 return true
             } else {
                 return false
             }
         } else {
             return false
         }
    }

    this.apply = function(sample) {
        if(!this.checkStateAndResetIfNeeded(sample)) {
            if (this.coldStartTotal == null || this.coldStartTotal < totalSamplesColdStart || this.lastDate == null) {
                var increaseAverageAmounts = (this.coldStartTotal?this.coldStartTotal:0) + 1
                this.lastDate = sample[TIME]
                this.lastValue = ((this.lastValue?this.lastValue:0) * ((increaseAverageAmounts-1)/(increaseAverageAmounts))) + (sample[VALUE]/increaseAverageAmounts)
                this.coldStartTotal = increaseAverageAmounts
            } else {
                // time update
                this.xhatminus = this.lastValue
                this.minusP = this.paramP + this.processVariance

                // measurement update
                this.paramK = this.minusP / (this.minusP + this.measureVariance)
                var xhatValue = this.xhatminus + this.paramK * (sample[VALUE] - this.xhatminus)
                this.lastDate = sample[TIME]
                this.lastValue = xhatValue
                this.paramP = (1 - this.paramK) * this.minusP
            }
        }

        return this.lastValue
    }
}

// OBS: a implementação deste filtro deve estar sincronizada com TagPIFilterMinMax.groovy
function MinMaxFilter(min, max) {
    this.min = min ;
    this.max = max ;

    this.apply = function(sample) {
        if (this.max != null && sample[VALUE] > this.max) {
            throw "Valor sample está acima do máximo" ;
        }
        if (this.min != null && sample[VALUE] < this.min) {
            throw "Valor sample está abaixo do mínimo" ;
        }

        return sample[VALUE]
    }
}