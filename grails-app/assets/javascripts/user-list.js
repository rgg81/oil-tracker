paapApp.controller("userListController", ['$scope','$http','$filter', 'ngTableParams', function($scope, $http, $filter, ngTableParams) {
	$scope.tags = null
	$scope.checkbox = []
	$scope.init = function(data) {
		console.log(data)
		for (var i in data) {
			$scope.checkbox[data[i].username] = false;
		}
		console.log($scope.checkbox)
		$scope.tags = data;
		$scope.userTable = new ngTableParams({
	        page: 1,            // show first page
	        count: 10,           // count per page
	        filter: {
	        	username: '',
	        	name: '',
	        },
	        sorting: {
        		name: ''     // initial sorting
    		}
	    }, {
	        total: data.length, // length of data
	        getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;
                orderedData = params.filter ?
                        $filter('filter')(orderedData, params.filter()) :
                        orderedData;

                $scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve($scope.users);
        	}
	    })
	}
	$scope.submit = function() {
		var userList = []
		for (var i in $scope.checkbox) {
			var user = $scope.checkbox[i];
			if (user == true)
				$("#user-list").append('<input type="hidden" name="users" value="'+i+'">')		
		}
	}

			/*$scope.tags = data;
			$scope.tagTable = new ngTableParams({
		        page: 1,            // show first page
		        count: 10,           // count per page
		        filter: {
		        	name: '',
		        	formula: '',
		        	type: '',
		        	location: ''
		        },
		        sorting: {
            		name: 'asc'     // initial sorting
        		}
		    }, {
		        total: data.length, // length of data
		        getData: function($defer, params) {
	                // use build-in angular filter
	                var orderedData = params.sorting ?
	                        $filter('orderBy')(data, params.orderBy()) :
	                        data;
	                orderedData = params.filter ?
	                        $filter('filter')(orderedData, params.filter()) :
	                        orderedData;

	                $scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

	                params.total(orderedData.length); // set total for recalc pagination
	                $defer.resolve($scope.users);
            	}
		    })*/
}])