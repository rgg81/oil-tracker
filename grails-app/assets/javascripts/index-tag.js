paapApp.controller("indexTagController", ['$scope','$http','$filter', 'ngTableParams', function($scope, $http, $filter, ngTableParams) {
	$scope.tags = null
	$http.get("/painelep/admin/TagPI/tagSort").
		success(function(data, status, headers, config) {
			$scope.tags = data;
			$scope.tagTable = new ngTableParams({
		        page: 1,            // show first page
		        count: 10,           // count per page
		        filter: {
		        	name: '',
		        	formula: '',
		        	type: '',
		        	location: ''
		        },
		        sorting: {
            		name: 'asc'     // initial sorting
        		}
		    }, {
		        total: data.length, // length of data
		        getData: function($defer, params) {
	                // use build-in angular filter
	                var orderedData = params.sorting ?
	                        $filter('orderBy')(data, params.orderBy()) :
	                        data;
	                orderedData = params.filter ?
	                        $filter('filter')(orderedData, params.filter()) :
	                        orderedData;

	                $scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

	                params.total(orderedData.length); // set total for recalc pagination
	                $defer.resolve($scope.users);
            	}
		    })
		}).error(function(data, status, headers, config) {

		});

	/*$scope.tagTable = new ngTableParams({
		page: 1,
		count: 10
	}, {
		filterDelay: 300
	})*/
}])