paapApp.service('$syncTimeout', function () {
    var syncObjects = {} ;

    function syncTimeout(name, timeout, callback) {
        if ( syncObjects[name] ) {
            syncObjects[name].push(callback) ;
        } else {
            syncObjects[name] = [callback] ;
            setTimeout(function() {
                for( var i in syncObjects[name] ) {
                    syncObjects[name][i]() ;
                }
                delete syncObjects[name] ;
            }, timeout) ;
        }
    }

    return syncTimeout;
});

paapApp.service('$syncCallback', function () {
    var syncObjects = {} ;
    var syncCount = {} ;

    function syncCallback(name, callback) {
        if(typeof(callback) == "undefined") {
            // Add syncCount
            if(typeof(syncCount[name]) == "undefined") {
                syncCount[name] = 1 ;
                syncObjects[name] = [] ;
            } else {
                syncCount[name]++ ;
            }
        } else {
            // Remove syncCount
            syncObjects[name].push(callback) ;
            syncCount[name]-- ;

            if(syncCount[name] == 0) {
                for( var i in syncObjects[name] ) {
                    syncObjects[name][i]() ;
                }
                delete syncCount[name] ;
                delete syncObjects[name] ;
            }
        }
    }

    return syncCallback;
});