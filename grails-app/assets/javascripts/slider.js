var slideInterval = 6155;
var slideLoop = null;
var mouseTimeout = null;
var isFullScreen = false;
var fullScreenF11 = false;

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function() {
    if (interval = getParameterByName("interval"))
        slideInterval = interval
});

 function whichTransitionEvent(){
  var t,
      el = document.createElement("fakeelement");

  var transitions = {
    "transition"      : "transitionend",
    "OTransition"     : "oTransitionEnd",
    "MozTransition"   : "transitionend",
    "WebkitTransition": "webkitTransitionEnd"
  };

  for (t in transitions){
    if (el.style[t] !== undefined){
      return transitions[t];
    }
  }
}

var transitionEvent = whichTransitionEvent();

function setFlexOrder(element,value) {
    element.css("-webkit-box-ordinal-group",value);
    element.css("-moz-box-ordinal-group",value);
    element.css("-ms-flex-order",value);
    element.css("-webkit-order",value);
    element.css("order",value);
    return element;
}
function getFlexOrder(element) {
    if (element.css("order")) return element.css("order");
    else if (element.css("-webkit-order",value)) return element.css("-webkit-order",value);
    else if (element.css("-ms-flex-order",value)) return element.css("-ms-flex-order",value);
    else if (element.css("-moz-box-ordinal-group",value)) return element.css("-moz-box-ordinal-group",value);
    else if (element.css("-webkit-box-ordinal-group",value)) return element.css("-webkit-box-ordinal-group",value);
}
function clearAllIntervals() {
    if (slideLoop) {
        clearInterval(slideLoop);
        slideLoop = null;
    }
    if (mouseTimeout) {
        clearInterval(mouseTimeout);
        mouseTimeout = null;
    }
}

//Method used in the setInterval loops, applies to all .slider divs
function slideNext() {
    var sliders = $(".slider");
    var l = sliders.length;
        var body = $("body");
    var screenLimit = body.data("screenLimit");
    var sliderWidths = []
    for (var i = 0; i < l; i++ ) {
        sliderWidths.push($(sliders[i]).width())
    };

    for (var i = 0; i < l; i++ ) {
        slide($(sliders[i]),screenLimit,sliderWidths[i]);
    };

    animateStart = Date.now() ;
}

function highlightIndicators(chart,enteringFullScreen) {
    var order = 1;
    if (enteringFullScreen)
        order = 0;
    if (chart.attr("data-order") == order)
        $("#indicator-" + chart.attr("id")).addClass("highlight");
    else
        $("#indicator-" + chart.attr("id")).removeClass("highlight");

}

//Main method, called to calculate and slide the charts
function slide(slider,screenLimit,sliderWidth,initializing) {

    if (sliderWidth >= screenLimit) {
        if (initializing)
            $(slider).parent().children(".slider-indicator").children().show();
        else {
            // Slide 1 chart to left
            slideNumCharts(slider, 1)
        }
    } else {
        $(slider).parent().children(".slider-indicator").children().hide();
    } 
}

function slideNumCharts(slider, numChartsToSlide) {
    var charts = slider.children();
    var numCharts = charts.length;

    var body = $("body");
    var translateMove = numChartsToSlide * body.data("chartWidth");

    slider.addClass("slider-animated");
    slider.css("transform","translate3d(-"+translateMove+"px,0,0)");


    for (var i = 0; i < numCharts; i++) {
        var chart = charts[i];
        highlightIndicators($(chart))
    }

    slider.one(transitionEvent,function(event) {
        for (var i = 0; i < numCharts; i++) {
            var chart = $(charts[i]);
            var currentOrder = $(chart).attr("data-order") - numChartsToSlide;
            if (currentOrder < 0)
                currentOrder += numCharts;
            setFlexOrder($(chart),currentOrder).attr("data-order",currentOrder);
            if (body.data("chartWidth")*currentOrder > body.data("screenLimit"))
                if (!$(chart).hasClass("chart-hide")) 
                    $(chart).addClass("chart-hide")
            else
                if ($(chart).hasClass("chart-hide"))
                    $(chart).removeClass("chart-hide")
        }                                
        slider.removeClass("slider-animated");
        slider.css("transform","translate3d(0,0,0)");
        slider.trigger("sliderAnimationEnded");
    });
}

//Puts the body element into fullscreen mode by adding the 'fullscreen' class
function addFullScreenClassToBody() {
    $("body").addClass("fullscreen") ;
}



function initSlide() {
        calculateSliderDimensions();
        clearAllIntervals();
        slideLoop = setInterval(slideNext,slideInterval);
}

function stopSlide() {
    clearAllIntervals();
}

//Resizes the display when using the "TV Mode" button
function resizeDisplay() {
    d = document.documentElement;
    if(d.requestFullscreen) {
        d.requestFullscreen();
    } else if(d.mozRequestFullScreen) {
        d.mozRequestFullScreen();
    } else if(d.webkitRequestFullscreen) {
        d.webkitRequestFullscreen();
    } else if(d.msRequestFullscreen) {
        d.msRequestFullscreen();
    }
}
/*Initializes the fullscreen's funcionalities: 
    - sets the status flag to indicate that fullscreen mode is on
    - calls the method that sets the body with the fullscreen class
    - starts the slider interval
    - enables the fullscreen button
    - resizes the window due to highchart's bugs
*/
function initFullScreen() {
    isFullScreen = true;
    addFullScreenClassToBody();
    document.getElementById("tv-toggle").className = document.getElementById("tv-toggle").className + " ativo";
    $(window).resize();
    initSlide();
    var body = $("body");
    var screenLimit = body.data("screenLimit");
    $(".chart").each(function(k,chart) {
        highlightIndicators($(chart),true);
    });
    var sliders = $(".slider");
    var l = sliders.length;
    var sliderWidths = []
    for (var i = 0; i < l; i++ ) {
        sliderWidths.push($(sliders[i]).width())
    };
    sliders.each(function(k,slider) {
        slide($(slider),screenLimit,sliderWidths[k],true);
    });
    $(".chart")
          .mouseenter(function() {
            stopSlide();
          })
          .mouseleave(function() {
            initSlide();
          });
}

//Exits fullscreen mode, stops the funcionalities started by initFullScreen, resizes due to highchart's bugs
function exitFullScreen() {
    stopSlide() ;
    if(document.exitFullscreen) {
        document.exitFullscreen();
    } if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
    isFullScreen = false;
    $("body").removeClass("fullscreen");
    if ($(".slider"))
        $(".slider").removeAttr('style');
    document.getElementById("tv-toggle").className = document.getElementById("tv-toggle").className.replace(" ativo", "");
    $(window).resize();

    $(".chart").unbind("mouseenter").unbind("mouseleave") ;
}

//Simple method just to do the appropriate behaviour when clicking on "TV Mode" button
function toggleFullScreen(caller) {
   if (document.fullscreenElement || 
       document.mozFullScreenElement || 
       document.webkitFullscreenElement || 
       document.msFullscreenElement ) {
        exitFullScreen();
    }
   else        
        resizeDisplay();
        initFullScreen();
}


//Remove the close button, which is useless when the fullscreen is initialized by pressing F11
function removeCloseButtonWhenFullscreenKeyPressed(element) {
    element.css("display","none");
}

//Re-adds the close button when closing fullscreen pressing F11
function addCloseButtonWhenFullscreenKeyPressed(button) {
    button.attr('style', function(i, style)
    {
        if (style)
            return style.replace(/display[^;]+;?/g, '');
    });
}

function calculateSliderDimensions() {
    var slider = $(".slider").first();
    var chartWidth = slider.children(".chart").first().width();
    var screenLimit = (slider.parent().width() + chartWidth);
    var body = $("body");
    body.data("chartWidth",chartWidth);
    body.data("screenLimit",screenLimit);
}
//Detect and add the appropriate behaviours when the user uses F11 (code 122) to toggle the fullscreen mode or exits using ESC (code 27)
$(document).keyup(function(e){
    if(e.which==122){
        if (isFullScreen) {
            fullScreenF11 = false;            
            exitFullScreen();
            addCloseButtonWhenFullscreenKeyPressed($(".fechar"));
        } else {
            fullScreenF11 = true;  
            initFullScreen();
            removeCloseButtonWhenFullscreenKeyPressed($(".fullscreen .fechar"));
        }
        
    }
    if (e.which==27 && fullScreenF11 === false){
        if (isFullScreen) {
            exitFullScreen();
            addCloseButtonWhenFullscreenKeyPressed($(".fechar"));
        }
    }
    //return false;
});

var resizeTimer;
window.onresize=function() {
    if (resizeTimer){clearTimeout(resizeTimer);}
    resizeTimer = setTimeout(function(){
        //console.log(Math.max(document.documentElement.clientHeight, window.innerHeight || 0));
        if ($("body").hasClass("fullscreen")){
            calculateSliderDimensions();
            //initSlide();
        }
    },100);
};


