import grails.util.Environment

grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.project.war.file = "target/${appName}.war"

grails.project.fork = [
    // configure settings for compilation JVM, note that if you alter the Groovy version forked compilation is required
    //  compile: [maxMemory: 256, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
    //test: false,
    // configure settings for the test-app JVM, uses the daemon by default
    test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
    // configure settings for the run-app JVM
    run: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the run-war JVM
    war: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the Console UI JVM
    console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        mavenLocal()

        def envVariables = new ConfigSlurper().parse(new File("${userHome}/paap-env.groovy").toURI().toURL())
        def proxyconfig = {
            proxy 'inet-sys.gnet.petrobras.com.br', 8080, auth([username: "${envVariables.petrobras.proxy.user}", password:"${envVariables.petrobras.proxy.password}"])
        }

        grailsCentral proxyconfig
        mavenCentral proxyconfig

        mavenRepo "http://nexus.petrobras.com.br/nexus/content/groups/public/"
        mavenRepo "http://nexus.petrobras.com.br/nexus/content/repositories/releases/"
        mavenRepo "http://nexus.petrobras.com.br/nexus/content/repositories/snapshots/"

        mavenRepo ("https://oss.sonatype.org/content/groups/public/") {
            proxyconfig
        }
        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
        // runtime 'mysql:mysql-connector-java:5.1.29'
        // runtime 'org.postgresql:postgresql:9.3-1101-jdbc41'
        test "org.grails:grails-datastore-test-support:1.0.2-grails-2.4"
        compile 'org.grails:grails-datastore-gorm:3.1.5.RELEASE'
        
        def versionSecurity = '4.6.0.1'
        compile("br.com.petrobras.security:security-core:${versionSecurity}",
                "br.com.petrobras.security:security-command:${versionSecurity}",
                "br.com.petrobras.security:security-extension-web:${versionSecurity}",
                "br.com.petrobras.security:security-consume-api:${versionSecurity}",
                "br.com.petrobras.security:security-context-api:${versionSecurity}",
                "br.com.petrobras.security:security-consume-soap:${versionSecurity}",
                "br.com.petrobras.security:security-context-spring:${versionSecurity}",
                "br.com.petrobras.security:security-utility:${versionSecurity}",
                "br.com.petrobras.security:security-configuration:${versionSecurity}",
                "br.com.petrobras.security:security-annotation-aspectj:${versionSecurity}",
                "br.com.petrobras.security:security-exception:${versionSecurity}",
                "commons-codec:commons-codec:1.4") {
            // O Grails usa a versao 3.0 do Spring, por isto precisa desta exclusao
            excludes 'spring-beans', 'spring-context', 'spring-core', 'spring-web', 'spring-aop',
                    'aspectjweaver', 'log4j'
        }
        

        compile 'br.com.petrobras.security:security-shiro:4.3.2'

        compile ('org.apache.shiro:shiro-core:1.2.2',
                'org.apache.shiro:shiro-web:1.2.2',
                'org.apache.shiro:shiro-spring:1.2.2',
                'org.apache.shiro:shiro-ehcache:1.2.2',
                'org.apache.shiro:shiro-quartz:1.2.2') {
            excludes 'ejb', 'jsf-api', 'servlet-api', 'jsp-api', 'jstl', 'jms',
                    'connector-api', 'ehcache-core', 'slf4j-api', 'commons-logging', 'quartz'
        }

		bundle('javax.websocket:javax.websocket-api:1.1') {
            // This line is necessary for deployment to Tomcat, since
            // Tomcat comes with its own version of javax.websocket-api.
            export = false
        }
		compile group: 'org.quartz-scheduler', name: 'quartz', version: '2.2.1'
		compile group: 'org.quartz-scheduler', name: 'quartz-jobs', version: '2.2.1'
        compile group: 'net.objecthunter', name: 'exp4j', version: '0.4.4'
		compile "commons-codec:commons-codec:1.10"
        compile 'commons-lang:commons-lang:2.6'

        compile 'org.apache.activemq:activemq-core:5.7.0'
        test("org.seleniumhq.selenium:selenium-firefox-driver:2.45.0")
        test "org.gebish:geb-spock:0.10.0"
        test "org.gebish:geb-junit4:0.10.0"

        provided "ojdbc:ojdbc:14"

        compile 'org.apache.poi:poi:3.11'
        compile 'org.apache.poi:poi-ooxml:3.11'
        compile 'org.apache.poi:ooxml-schemas:1.1'
        compile 'org.apache.commons:commons-collections4:4.0'
    }

    plugins {

        test ":geb:0.10.0"
        test ":plastic-criteria:1.2"

        test ":code-coverage:2.0.3-3"

        // plugins for the build system only
        build ":tomcat:7.0.55"

        // plugins for the compile step
        compile ":scaffolding:2.1.2"
        compile ':cache:1.1.8'
        compile ":asset-pipeline:2.5.1"
        compile ":cookie:1.4"
        //compile ":angular-annotate-asset-pipeline:2.1.0"

        // plugins needed at runtime but not for compilation
        runtime ":hibernate4:4.3.5.5" // or ":hibernate:3.6.10.18"
        runtime ":database-migration:1.4.0"
        runtime ":jquery:1.11.1"

        compile (":shiro:1.2.1")

        test ":plastic-criteria:1.5.1"

        compile ":spring-mobile:1.1.3"

        // Uncomment these to enable additional asset-pipeline capabilities
        //compile ":sass-asset-pipeline:1.9.0"
        //compile ":less-asset-pipeline:1.10.0"
        //compile ":coffee-asset-pipeline:1.8.0"
        //compile ":handlebars-asset-pipeline:1.3.0.3"
    }
}

coverage {
   exclusions = ['**/*changelog*','**/*teste*','**/*my-changelog*','**/*SecurityFilters*']
}