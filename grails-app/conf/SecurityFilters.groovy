// http://grails.org/doc/2.4.4/guide/theWebLayer.html#filters
class SecurityFilters {
    def filters = {
        authenticated(controller: '*', action: '*') {
            before = {
                if (!controllerName) return true

                // Access control by convention.
                try {
                    accessControl()
                } catch (NullPointerException npe) {
                    // Treat bogus CA implementation :(
                    log.error("Erro do CA ao processar verificacao de seguranca: invalidando secao", npe)
                    session.invalidate()
                    request.redirect(controller: "auth", action: "login")
                    return false
                } catch (e) {
                    log.error("Erro ao processar verificacao de seguranca", e)
                    return false
                }
            }
        }
    }
}