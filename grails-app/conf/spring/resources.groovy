import br.com.petrobras.paap.PIDataService
import br.com.petrobras.paap.UepService
import br.com.petrobras.paap.EventService
import br.com.petrobras.paap.ShiroService
import br.com.petrobras.paap.WSMessageService
import br.com.petrobras.paap.ws.EPProductionJMSListener
import br.com.petrobras.paap.ws.EPProductionWS
import grails.util.Environment
import org.springframework.jndi.JndiObjectFactoryBean
import org.springframework.jndi.JndiTemplate

// Place your Spring DSL code here
beans = {
    cacheManager(org.apache.shiro.cache.ehcache.EhCacheManager)

    petrobrasSecurityRealm(br.com.petrobras.security.shiro.PetrobrasSecurityRealm) {
        cacheManager = ref("cacheManager")
    }

    if ( Environment.current == Environment.PRODUCTION ) {
        jmsConnectionFactory(JndiObjectFactoryBean) {
            jndiName = 'weblogic.jms.ConnectionFactory'
        }

        jmsTopicNewPiData(JndiObjectFactoryBean) {
            jndiName = 'jms/new-pi-data'
        }

        jmsQueueResetTag(JndiObjectFactoryBean) {
            jndiName = 'jms/reset-tag'
        }

        jmsQueueResetTagDone(JndiObjectFactoryBean) {
            jndiName = 'jms/reset-tag-done'
        }
        
    } else {
        jndiTemplate(JndiTemplate) {
            environment = [
                "java.naming.factory.initial" : "org.apache.activemq.jndi.ActiveMQInitialContextFactory",
                "java.naming.provider.url": "vm://localhost",
                "topic.jms/new-pi-data": "newPIData",
                "queue.jms/reset-tag": "resetTag",
                "queue.jms/reset-tag-done": "resetTagDone"
            ]
        }

        jmsConnectionFactory(JndiObjectFactoryBean) {
            jndiTemplate = ref(jndiTemplate)
            jndiName = 'ConnectionFactory'
        }

        jmsTopicNewPiData(JndiObjectFactoryBean) {
            jndiTemplate = ref(jndiTemplate)
            jndiName = 'jms/new-pi-data'
        }

        jmsQueueResetTag(JndiObjectFactoryBean) {
            jndiTemplate = ref(jndiTemplate)
            jndiName = 'jms/reset-tag'
        }

        jmsQueueResetTagDone(JndiObjectFactoryBean) {
            jndiTemplate = ref(jndiTemplate)
            jndiName = 'jms/reset-tag-done'
        }
    }
	
	if(Environment.current != Environment.TEST ){
		println "Configuring log4j ..."
		log4jConfigurer(org.springframework.beans.factory.config.MethodInvokingFactoryBean)
		{
			targetClass = "org.springframework.util.Log4jConfigurer"
			targetMethod = "initLogging"
			arguments = '${log4jpath}'
		}
	}
	
	localeResolver(org.springframework.web.servlet.i18n.SessionLocaleResolver) {
		defaultLocale = new Locale("pt","BR")
		java.util.Locale.setDefault(defaultLocale)
	}

    wsInstance(EPProductionWS) 

    piDataService(PIDataService)

    eventService(EventService)

    wsMessageService(WSMessageService) { bean ->
        jmsConnectionFactory = ref(jmsConnectionFactory)
        jmsTopicNewPiData = ref(jmsTopicNewPiData)
        piDataService = ref(piDataService)
        eventService = ref(eventService)
    }

    jmsListenerInstance(EPProductionJMSListener) { bean ->
        jmsConnectionFactory = ref(jmsConnectionFactory)
        jmsTopicNewPiData = ref(jmsTopicNewPiData)
    }

    eventService(EventService) {bean ->
        shiroService = ref('shiroService')
    }
}