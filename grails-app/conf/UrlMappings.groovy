class UrlMappings {

	static mappings = {
        "/detail/$id" (controller: "detail", action: "show")

        "/PIData/lastSamplesUep/$id?(.$format)?/$interval?" (controller: "PIData", action: "lastSamplesUep")

        group("/admin") {
            "/$controller/$action?/$id?(.$format)?"{
                namespace = "admin"
            } 
        }

        group("/api") {
            "/UEP/$action?/$id?(.$format)?"{
                namespace = "api"
                controller = "UEPApi"
            } 
        }

        "/$controller/$action?/$id?(.$format)?" {

        }

        "/"(controller: "home")
        "/unauthorized"(view: "unauthorized")
		"/index.gsp" (controller: "home")
        "/show/$action/$id" (controller: "home")
        "/show/$action" (controller: "home")
        "500"(view:'/error')
	}
}
