dataSource {
    pooled = true
    jmxExport = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
    dialect = "br.com.petrobras.paap.dbdialect.ImprovedH2Dialect"
}

def localDevDs = null
try {
    def envVariables = new ConfigSlurper().parse(new File("${userHome}/paap-env.groovy").toURI().toURL())
    localDevDs = envVariables?.paap?.localDev?.dataSource
} catch (e) {
    e.printStackTrace()
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
//    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
    singleSession = true // configure OSIV singleSession mode
    flush.mode = 'manual' // OSIV session flush mode outside of transactional context
    jdbc.use_get_generated_keys = true
}

// environment specific settings
environments {
    development {
        if (localDevDs) {
            print "Configurando localDevDs..."
            dataSource = localDevDs
        } else {
            dataSource {
                dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
                url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
            }
        }
    }
    desenvolvimento {
        dataSource {
            dbCreate = ""
            username = "paap"
            password = "dE3rO#qasL"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "br.com.petrobras.paap.PaapOracle10gDialect"
            url = "jdbc:oracle:thin:@//nestor.petrobras.com.br:1521/prtlcrpd"
            logSql = true
        }
    }

    desenvolvimentoapp {
        dataSource {
            dbCreate = ""
            username = "paap_aplicacao"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "br.com.petrobras.paap.PaapOracle10gDialect"
            url = "jdbc:oracle:thin:@//nestor.petrobras.com.br:1521/prtlcrpd"
            logSql = true
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
        }
    }
    teste {
        dataSource {
            dbCreate = ""
            username = "paap"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "br.com.petrobras.paap.PaapOracle10gDialect"
            url = "jdbc:oracle:thin:@//glaucius.petrobras.com.br:1521/prtlcrpt"
            logSql = true
        }
    }

    testeapp {
        dataSource {
            dbCreate = ""
            username = "paap_aplicacao"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "br.com.petrobras.paap.PaapOracle10gDialect"
            url = "jdbc:oracle:thin:@//glaucius.petrobras.com.br:1521/prtlcrpt"
            logSql = true
        }
    }
    homologacao {
        dataSource {
            dbCreate = ""
            username = "paap"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "br.com.petrobras.paap.PaapOracle10gDialect"
            url = "jdbc:oracle:thin:@//vipprtlcrpha.petrobras.com.br:1521/prtlcrph.petrobras.com.br"
            logSql = true
        }
    }

    homologacaoapp {
        dataSource {
            dbCreate = ""
            username = "paap_aplicacao"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "br.com.petrobras.paap.PaapOracle10gDialect"
            url = "jdbc:oracle:thin:@//vipprtlcrpha.petrobras.com.br:1521/prtlcrph.petrobras.com.br"
            logSql = true
        }
    }

    producao {
        dataSource {
            dbCreate = ""
            username = "paap"
            password = 'LkdP$3gY'
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "br.com.petrobras.paap.PaapOracle10gDialect"
            url = "jdbc:oracle:thin:@//vipprtlcrppa.petrobras.com.br:1521/prtlcrpp.petrobras.com.br"
        }
    }

    production {
        dataSource {
            dbCreate = ""
            jndiName = "paapDS"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "br.com.petrobras.paap.PaapOracle10gDialect"
        }
    }
}
