import br.com.petrobras.paap.*
import br.com.petrobras.paap.job.PaapScheduler
import grails.converters.JSON
import grails.util.Environment
import grails.validation.ValidationException
import org.quartz.*
import org.quartz.impl.StdSchedulerFactory
import org.apache.log4j.Logger
import static org.quartz.JobBuilder.newJob
import static org.quartz.SimpleScheduleBuilder.simpleSchedule
import static org.quartz.TriggerBuilder.newTrigger

class BootStrap {

    private Scheduler scheduler;
    private static final Logger log = Logger.getLogger(BootStrap.class);

    def init = { servletContext ->
        log.info("Iniciando aplicacao Grails...")
        // Declara a função "truncate" para data
        Date.metaClass.truncateToSeconds = { seconds -> new Date( delegate.time - delegate.time % (1000*seconds) ) } 

        // Configura marsheller de Date no JSON
        JSON.registerObjectMarshaller(Date) {
            return it?.getTime()
         }

        def addFilters = { tagPIInstance, filtersSpec ->
            filtersSpec?.eachWithIndex { filterSpec, idx ->
                def tagFilter
                switch (filterSpec.kind.toLowerCase()) {
                    case "kalman":
                        tagFilter = new TagPIFilterKalman(
                                filterSpec.processVariance,
                                filterSpec.measureVariance,
                                idx)
                        break;
                    case "average":
                        tagFilter = new TagPIFilterMovingAverage(
                                Math.round(filterSpec.windowInMinutes * 1000 * 60),
                                idx)
                        break;
                    case "minMax":
                        tagFilter 
                    default:
                        throw new IllegalArgumentException("Filter spec not found: '${filterSpec.kind}'")
                }
                tagPIInstance.addToFilters(tagFilter)
            }
            tagPIInstance
        }
        OnlineUser.executeUpdate('delete from OnlineUser')
        
        switch(Environment.current) {
            case Environment.DEVELOPMENT:
                try {
                    // Faz bootstrap da UO-RIO
                    // --> Tags:

                    if(System.getProperty('mode.init') == 'mock') {
                        for(i in 0..10){
                            new UEP(name: "P-${i} ES", uo: 'UO-ES', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: "Tag P-${i} ES", formula: '7.00 * 1000', tagType: TagPIType.CALCULATED).save()).save();
                        }
                        for(i in 0..10){
                            new UEP(name: "P-${i} BS", uo: 'UO-BS', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: "Tag P-${i} BS", formula: '20.00 * 1000', tagType: TagPIType.CALCULATED).save()).save();
                        }
                        for(i in 0..10){
                            new UEP(name: "P-${i} RIO", uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: "Tag P-${i} RIO", formula: '30.00 * 1000', tagType: TagPIType.CALCULATED).save()).save();
                        }
                        for(i in 0..10){
                            new UEP(name: "P-${i} BC", uo: 'UO-BC', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: "Tag P-${i} BC", formula: '40.00 * 1000 ', tagType: TagPIType.CALCULATED).save()).save();
                        }

                    }else if(System.getProperty('mode.init') == 'light') {

                        addFilters(new TagPI(name: 'P15-DV_FI-552501_PV', formula: 'P15-DV_FI-552501_PV', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P15-DV_FI-552501_PV_CAN1', formula: 'P15-DV_FI-552501_PV_CAN1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'P15-DV_FI-552501_PV_CAN2', formula: 'P15-DV_FI-552501_PV_CAN2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()

                       new UEP(name: 'P-15', uo: 'UO-BS', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço', wi: 100.0,
                                tag: new TagPI(name: 'Tag P-15', formula: '(\'P15-DV_FI-552501_PV\' + \'P15-DV_FI-552501_PV_CAN1\' + \'P15-DV_FI-552501_PV_CAN2\') * 0.78 * 6.2898 * 24', tagType: TagPIType.CALCULATED).save()).save();


                        addFilters(new TagPI(name: 'P43_FIT-1223326B', formula: 'P43_FIT-1223326B', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P43_FQIT-1223324B', formula: 'P43_FQIT-1223324B', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P43_FQIT-1223326A', formula: 'P43_FQIT-1223326A', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P43_FQIT-1223327', formula: 'P43_FQIT-1223327', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        new UEP(name: 'P-43', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço', wi: 85.0,
                                tag: new TagPI(name: 'Tag P-43', formula: '(\'P43_FQIT-1223327\' + \'P43_FQIT-1223324B\' + \'P43_FIT-1223326B\' + \'P43_FQIT-1223326A\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();

                        new UEP(name: 'P-43 + P-15', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço', wi: 100.0,
                                tag: new TagPI(name: 'Tag P-43 + P-15', formula: '(\'P15-DV_FI-552501_PV\' + \'P15-DV_FI-552501_PV_CAN1\' + \'P15-DV_FI-552501_PV_CAN2\' + \'P43_FQIT-1223327\' + \'P43_FQIT-1223324B\' + \'P43_FIT-1223326B\' + \'P43_FQIT-1223326A\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();


                        new TagPI(name: 'P08-CALC_VAZAO_EXP_OLEO', formula: 'P08-CALC_VAZAO_EXP_OLEO', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P08_LAB_BSW_OEXP', formula: 'P08_LAB_BSW_OEXP', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()

                        new UEP(name: 'P-08', uo: 'UO-BC', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço', wi: 25.0,
                                tag: new TagPI(name: 'P-08', formula: '(\'P08-CALC_VAZAO_EXP_OLEO\') * (1 - (\'P08_LAB_BSW_OEXP\'/100))', tagType: TagPIType.CALCULATED).save()).save();

                        new TagPI(name: 'P07-CALC_VAZAO_EXP_OLEO', formula: 'P07-CALC_VAZAO_EXP_OLEO', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P07-DV_FI-552501_PV_CAN1', formula: 'P07-DV_FI-552501_PV_CAN1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P07_LAB_BSW_OEXP', formula: 'P07_LAB_BSW_OEXP', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()

                        new UEP(name: 'P-07', uo: 'UO-BC', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço', wi: 100.0,
                                tag: new TagPI(name: 'P-07', formula: '(\'P07-CALC_VAZAO_EXP_OLEO\'+\'P07-DV_FI-552501_PV_CAN1\')*6.2898*(1-(\'P07_LAB_BSW_OEXP\'/100))', tagType: TagPIType.CALCULATED).save()).save();

                        new TagPI(name: 'P18-CALC_VAZAO_EXP_OLEO', formula: 'P18-CALC_VAZAO_EXP_OLEO', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P-18-DV_FI-552501_PV_CAN1', formula: 'P18-DV_FI-552501_PV_CAN1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P18-DV_FI-552501_PV_CAN2', formula: 'P18-DV_FI-552501_PV_CAN2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P-18-DV_FI-552502_PV', formula: 'P18-DV_FI-552502_PV', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P18-DV_FI-552502_PV_CAN1', formula: 'P18-DV_FI-552502_PV_CAN1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P18-DV_FI-552502_PV_CAN2', formula: 'P18-DV_FI-552502_PV_CAN2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P18_LAB_BSW_OEXP', formula: 'P18_LAB_BSW_OEXP', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()

                        new UEP(name: 'P-18', uo: 'UO-BC', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço', wi: 100.0,
                                tag: new TagPI(name: 'P-18', formula: '(\'P18-CALC_VAZAO_EXP_OLEO\'+\'P18-DV_FI-552501_PV_CAN1\'+\'P18-DV_FI-552501_PV_CAN2\'+\'P18-DV_FI-552502_PV\'+\'P18-DV_FI-552502_PV_CAN1\'+\'P18-DV_FI-552502_PV_CAN2\')*6.2898*(1-(\'P18_LAB_BSW_OEXP\'/100))', tagType: TagPIType.CALCULATED).save()).save();


                        new TagPI(name: 'CST_301084_SVC_0127_04A', formula: 'CST_301084_SVC_0127_04A', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save()

                        new TagPI(name: 'CST_301084_SVC_0127_04B', formula: 'CST_301084_SVC_0127_04B', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save()


                        new UEP(name: 'FPSO-CST', uo: 'UO-BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço', wi: 65.0,
                                tag: new TagPI(name: 'Tag FPSO-CST', formula: '(\'CST_301084_SVC_0127_04A\' + \'CST_301084_SVC_0127_04B\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save()

                        new TagPI(name: 'P26-INSQL_FQY1223007A_VL', formula: 'P26-INSQL_FQY1223007A_VL', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'P26-INSQL_FQY1223007B_VL', formula: 'P26-INSQL_FQY1223007B_VL', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new UEP(name: 'P26', uo: 'UO-BC', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P26', formula: '(\'P26-INSQL_FQY1223007A_VL\' + \'P26-INSQL_FQY1223007B_VL\')*6.2898*24*10', tagType: TagPIType.CALCULATED).save()).save()

                        new TagPI(name: 'PCE1-INSQL_FQY1223503_VL', formula: 'PCE1-INSQL_FQY1223503_VL', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new TagPI(name: 'PCE1-INSQL_FQY1223506_VL', formula: 'PCE1-INSQL_FQY1223506_VL', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BC).save()
                        new UEP(name: 'PCE1', uo: 'UO-BC', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço', wi: 100.0,
                                tag: new TagPI(name: 'Tag PCE1', formula: '(\'PCE1-INSQL_FQY1223503_VL\' + \'PCE1-INSQL_FQY1223506_VL\')*6.2898*24*10*3', tagType: TagPIType.CALCULATED).save()).save()

                    } else {
                        addFilters(new TagPI(name: 'FPNIT_FI-1135-04A', formula: 'FPNIT_FI-1135-04A', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'FPNIT_FI-1135-04B', formula: 'FPNIT_FI-1135-04B', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P40_FIT-1223007A2', formula: 'P40_FIT-1223007A2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P40_FIT-1223007B2', formula: 'P40_FIT-1223007B2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P40_FT-1223007A', formula: 'P40_FT-1223007A', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P40_FT-1223007B', formula: 'P40_FT-1223007B', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P43_FIT-1223326B', formula: 'P43_FIT-1223326B', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P43_FQIT-1223324B', formula: 'P43_FQIT-1223324B', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P43_FQIT-1223326A', formula: 'P43_FQIT-1223326A', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P43_FQIT-1223327', formula: 'P43_FQIT-1223327', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P48_FQIT-1223324A', formula: 'P48_FQIT-1223324A', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P48_FQIT-1223324B', formula: 'P48_FQIT-1223324B', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P48_FQIT-1223326A', formula: 'P48_FQIT-1223326A', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P48_FQIT-1223326B', formula: 'P48_FQIT-1223326B', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P50_FT-1223016C', formula: 'P50_FT-1223016C', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P50_FT-1223017C', formula: 'P50_FT-1223017C', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P50_FT-1223018C', formula: 'P50_FT-1223018C', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P50_FT-1223019C', formula: 'P50_FT-1223019C', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P51_FIT-1223016', formula: 'P51_FIT-1223016', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P51_FIT-1223017', formula: 'P51_FIT-1223017', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P51_FIT-1223018', formula: 'P51_FIT-1223018', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P52_FI-1223008', formula: 'P52_FI-1223008', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P52_FI-1223009', formula: 'P52_FI-1223009', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P52_FI-1223010', formula: 'P52_FI-1223010', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P53_FQI-1223016', formula: 'P53_FQI-1223016', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P53_FQI-1223017', formula: 'P53_FQI-1223017', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P53_FQI-1223018', formula: 'P53_FQI-1223018', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P54_FQT-1223016', formula: 'P54_FQT-1223016', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P54_FQT-1223017', formula: 'P54_FQT-1223017', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P54_FQT-1223018', formula: 'P54_FQT-1223018', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P55_T65-FT-0001', formula: 'P55_T65-FT-0001', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P55_T65-FT-0002', formula: 'P55_T65-FT-0002', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P55_T65-FT-1000', formula: 'P55_T65-FT-1000', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P56_FIT-1223016', formula: 'P56_FIT-1223016', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P56_FIT-1223017', formula: 'P56_FIT-1223017', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P56_FIT-1223018', formula: 'P56_FIT-1223018', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P62_FQI-1212001-VZBC', formula: 'P62_FQI-1212001-VZBC', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_RIO),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()

                        // --> UEPs:
                        new UEP(name: 'P-40', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-40', formula: '(\'P40_FT-1223007A\' + \'P40_FT-1223007B\' - \'P40_FIT-1223007A2\' - \'P40_FIT-1223007B2\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-43', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-43', formula: '(\'P43_FQIT-1223327\' + \'P43_FQIT-1223324B\' + \'P43_FIT-1223326B\' + \'P43_FQIT-1223326A\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-48', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-48', formula: '(\'P48_FQIT-1223324A\' + \'P48_FQIT-1223324B\' + \'P48_FQIT-1223326A\' + \'P48_FQIT-1223326B\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-50', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-50', formula: '(\'P50_FT-1223016C\' + \'P50_FT-1223017C\' + \'P50_FT-1223018C\' + \'P50_FT-1223019C\') * 6.2898 * 24 * 0.90', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-51', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-51', formula: '(\'P51_FIT-1223016\' + \'P51_FIT-1223017\' + \'P51_FIT-1223018\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-52', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-52', formula: '(\'P52_FI-1223008\' + \'P52_FI-1223009\' + \'P52_FI-1223010\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-53', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-53', formula: '(\'P53_FQI-1223016\' + \'P53_FQI-1223017\' + \'P53_FQI-1223018\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-54', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-54', formula: '(\'P54_FQT-1223016\' + \'P54_FQT-1223017\' + \'P54_FQT-1223018\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-55', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-55', formula: '(\'P55_T65-FT-0001\' + \'P55_T65-FT-0002\' + \'P55_T65-FT-1000\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-56', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-56', formula: '(\'P56_FIT-1223016\' + \'P56_FIT-1223017\' + \'P56_FIT-1223018\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'P-62', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-62', formula: '(\'P62_FQI-1212001-VZBC\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        new UEP(name: 'FPSO-NIT', uo: 'UO-RIO', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-NIT', formula: '(\'FPNIT_FI-1135-04B\' + \'FPNIT_FI-1135-04A\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();

                        // Faz bootstrap da UO-BS
                        // --> Tags:
                        addFilters(new TagPI(name: 'CAR_301086_FQI_1160_01A', formula: 'CAR_301086_FQI_1160_01A', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1.5],
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'kalman', measureVariance: 2000, processVariance: 20]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CAR_301086_FQI_1160_01B', formula: 'CAR_301086_FQI_1160_01B', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1.5],
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'kalman', measureVariance: 2000, processVariance: 20]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CAR_301086_FQI_1160_01C', formula: 'CAR_301086_FQI_1160_01C', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1.5],
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'kalman', measureVariance: 2000, processVariance: 20]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CAR_301086_FQI_1160_01D', formula: 'CAR_301086_FQI_1160_01D', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1.5],
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'kalman', measureVariance: 2000, processVariance: 20]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'CST_301084_SVC_0127_04A', formula: 'CST_301084_SVC_0127_04A', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'kalman', measureVariance: 100000, processVariance: 25],
                                        [kind: 'kalman', measureVariance: 1000, processVariance: 5]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'CST_301084_SVC_0127_04B', formula: 'CST_301084_SVC_0127_04B', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'kalman', measureVariance: 100000, processVariance: 25],
                                        [kind: 'kalman', measureVariance: 1000, processVariance: 5]
                                ]
                        ).save()
                        //                new TagPI(name: 'CSV_301085_21FIS5405', formula: 'CSV_301085_21FIS5405', tagType: TagPIType.ACCUMULATOR).save()
                        addFilters(new TagPI(name: 'DYPR_30100E_FT_5266A_GSVOL', formula: 'DYPR_30100E_FT_5266A_GSVOL', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 5000, processVariance: 50]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'DYPR_30100E_FT_5266B_GSVOL', formula: 'DYPR_30100E_FT_5266B_GSVOL', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 5000, processVariance: 50]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'CSP_30100A_1198_FT_001_AS', formula: 'CSP_30100A_1198_FT_001_AS', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 1000, processVariance: 75]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CSP_30100A_1198_FT_001_BS', formula: 'CSP_30100A_1198_FT_001_BS', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 2000, processVariance: 50]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CSP_30100A_1198_FT_001_CS', formula: 'CSP_30100A_1198_FT_001_CS', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 1000, processVariance: 75]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'CP_30100C_T62_FT_1108_VALUE', formula: 'CP_30100C_T62_FT_1108_VALUE', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 2000, processVariance: 50]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CP_30100C_T62_FT_1103_VALUE', formula: 'CP_30100C_T62_FT_1103_VALUE', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 2000, processVariance: 50]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'MLZ_FIVF1223009A', formula: 'MLZ_FIVF1223009A', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'kalman', measureVariance: 50000, processVariance: 75]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'MLZ_FIVF1223009B', formula: 'MLZ_FIVF1223009B', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'kalman', measureVariance: 50000, processVariance: 75]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'PMXL1_3A1701_1231_FT_001A', formula: 'PMXL1_3A1701_1231_FT_001A', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 50000, processVariance: 100]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'PMXL1_3A1701_1231_FT_001B', formula: 'PMXL1_3A1701_1231_FT_001B', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 50000, processVariance: 100]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'CIB_30100K_T62_FT_1103', formula: 'CIB_30100K_T62_FT_1103', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 50000, processVariance: 100],
                                        [kind: 'average', windowInMinutes: 1]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CIB_30100K_T62_FT_1108', formula: 'CIB_30100K_T62_FT_1108', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'kalman', measureVariance: 50000, processVariance: 100],
                                        [kind: 'average', windowInMinutes: 1]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'CMB_30100L_1198_FI_001_AG', formula: 'CMB_30100L_1198_FI_001_AG', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CMB_30100L_1198_FI_001_BG', formula: 'CMB_30100L_1198_FI_001_BG', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CMB_30100L_1198_FI_001_CG', formula: 'CMB_30100L_1198_FI_001_CG', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'kalman', measureVariance: 1000000, processVariance: 10]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'CMB_30100L_1198_FI_001_DG', formula: 'CMB_30100L_1198_FI_001_DG', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'average', windowInMinutes: 2],
                                        [kind: 'kalman', measureVariance: 1000000, processVariance: 10]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'CIT_30100B_927_FT_001_A_B_C_D_CALC', formula: 'CIT_30100B_927_FT_001_A_B_C_D_CALC', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS),
                                [
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()

                        // --> UEP's:
                        new UEP(name: 'FPSO Ilhabela', uo: 'UO-BS', department: 'PRESAL', asset: 'PRESAL', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO Ilhabela', formula: '(\'CIB_30100K_T62_FT_1103\' + \'CIB_30100K_T62_FT_1108\') * 6.2898 * 24 * 0.45', tagType: TagPIType.CALCULATED).save()).save();

                        new UEP(name: 'FPSO-DYPR', uo: 'UO-BS', department: 'PRESAL', asset: 'PRESAL', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-DYPR', formula: '(\'DYPR_30100E_FT_5266A_GSVOL\' + \'DYPR_30100E_FT_5266B_GSVOL\') * 6.2898 * 24 * 0.65', tagType: TagPIType.CALCULATED).save()).save();

                        new UEP(name: 'FPSO Mangaratiba', uo: 'UO-BS', department: 'PRESAL', asset: 'PRESAL', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO Mangaratiba', formula: '(\'CMB_30100L_1198_FI_001_AG\' + \'CMB_30100L_1198_FI_001_BG\' + \'CMB_30100L_1198_FI_001_CG\' + \'CMB_30100L_1198_FI_001_DG\') * 6.2898 * 24 * 0.65', tagType: TagPIType.CALCULATED).save()).save();
                        //
                        new UEP(name: 'FPSO-CPY', uo: 'UO-BS', department: 'PRESAL', asset: 'PRESAL', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-CPY', formula: '(\'CP_30100C_T62_FT_1108_VALUE\' + \'CP_30100C_T62_FT_1103_VALUE\') * 6.2898 * 24 * 0.65', tagType: TagPIType.CALCULATED).save()).save()
                        //
                        new UEP(name: 'FPSO-CSP', uo: 'UO-BS', department: 'PRESAL', asset: 'PRESAL', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-CSP', formula: '(\'CSP_30100A_1198_FT_001_AS\' + \'CSP_30100A_1198_FT_001_BS\' + \'CSP_30100A_1198_FT_001_CS\') * 6.2898 * 24 * 0.45', tagType: TagPIType.CALCULATED).save()).save();
                        //
                        //                new UEP( name: 'FPSO-CSV', uo: 'UO-BS', department: 'PRESAL', asset: 'PRESAL', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                        //                    tag: new TagPI(name: 'Tag FPSO-CSV', formula: '(CSV_301085_21FIS5405) * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save() ).save() ;
                        //
                        //                new UEP( name: 'FPSO-CAR', uo: 'UO-BS', department: 'SSE', asset: 'ATP-C', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                        //                    tag: new TagPI(name: 'Tag FPSO-CAR', formula: '(CAR_301086_FQI_1160_01A + CAR_301086_FQI_1160_01B + CAR_301086_FQI_1160_01C + CAR_301086_FQI_1160_01D) * 6.2898 * 24 * 0.65', tagType: TagPIType.CALCULATED).save() ).save() ;
                        //

                        new UEP(name: 'FPSO-CST', uo: 'UO-BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-CST', formula: '(\'CST_301084_SVC_0127_04A\' + \'CST_301084_SVC_0127_04B\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save()
                        //
                        new UEP(name: 'Merluza Condensado', uo: 'UO-BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag PMLZ-1', formula: '(\'MLZ_FIVF1223009A\' + \'MLZ_FIVF1223009B\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save();
                        //
                        new UEP(name: 'Mexilhão Condensado', uo: 'UO-BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag PMXL', formula: '(\'PMXL1_3A1701_1231_FT_001A\' + \'PMXL1_3A1701_1231_FT_001B\') * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save()

                        new UEP(name: 'FPSO-CIT', uo: 'UO-BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'FPSO_CIT', formula: '\'CIT_30100B_927_FT_001_A_B_C_D_CALC\' * 1', tagType: TagPIType.CALCULATED).save()).save()



                        // Faz bootstrap da UO-ES
                        // --> Tags:
                        addFilters(new TagPI(name: 'prodin_cvit_producao_oleo', formula: 'prodin_cvit_producao_oleo', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES),
                                [
                                        [kind: 'kalman', measureVariance: 20000.0, processVariance: 40]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'PRODIN_CAPX_PRODUCAO_OLEO', formula: 'PRODIN_CAPX_PRODUCAO_OLEO', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES),
                                [
                                        [kind: 'kalman', measureVariance: 10000.0, processVariance: 50.0]
                                ]).save()
                        addFilters(new TagPI(name: 'PRODIN_PPER1_PRODUCAO_OLEO', formula: 'PRODIN_PPER1_PRODUCAO_OLEO', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES),
                                [
                                        [kind: 'average', windowInMinutes: 1],
                                        [kind: 'average', windowInMinutes: 7],
                                        [kind: 'average', windowInMinutes: 5]
                                ]).save()
                        addFilters(new TagPI(name: 'PRODIN_FCDA_PRODUCAO_OLEO', formula: 'PRODIN_FCDA_PRODUCAO_OLEO', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES),
                                [
                                        [kind: 'kalman', measureVariance: 2000.0, processVariance: 50.0]
                                ]).save()
                        new TagPI(name: 'prodin_cdsm_producao_oleo', formula: 'prodin_cdsm_producao_oleo', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES).save()
                        new TagPI(name: 'PRODIN_P57_PRODUCAO_OLEO', formula: 'PRODIN_P57_PRODUCAO_OLEO', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES).save()
                        new TagPI(name: 'PRODIN_P58_PRODUCAO_OLEO', formula: 'PRODIN_P58_PRODUCAO_OLEO', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES).save()

//                    --> UEP's:
                        new UEP(name: 'FPSO-CVIT', uo: 'UO-ES', department: 'SSE', asset: 'ATP-GLF', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-CVIT', formula: '\'prodin_cvit_producao_oleo\' * 1.00', tagType: TagPIType.CALCULATED).save()).save()

                        new UEP(name: 'FPSO-CDSM', uo: 'UO-ES', department: 'SSE', asset: 'ATP-GLF', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-CDSM', formula: '\'prodin_cdsm_producao_oleo\' * 0.78', tagType: TagPIType.CALCULATED).save()).save()

                        new UEP(name: 'PPER-1', uo: 'UO-ES', department: 'SSE', asset: 'ATP-GLF', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag PPER-1', formula: '\'PRODIN_PPER1_PRODUCAO_OLEO\' * 6.2898 * 24 * 1.00', tagType: TagPIType.CALCULATED).save()).save()

                        new UEP(name: 'P-57', uo: 'UO-ES', department: 'SSE', asset: 'ATP-JBT-CHT', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-57', formula: '\'PRODIN_P57_PRODUCAO_OLEO\' * 1.00', tagType: TagPIType.CALCULATED).save()).save()

                        new UEP(name: 'P-58', uo: 'UO-ES', department: 'PDP', asset: 'PDP', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-58', formula: '\'PRODIN_P58_PRODUCAO_OLEO\' * 1.00', tagType: TagPIType.CALCULATED).save()).save()

                        new UEP(name: 'FPSO Norte Capixaba', uo: 'UO-ES', department: 'SSE', asset: 'ATP-NC', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-CAPX', formula: '\'PRODIN_CAPX_PRODUCAO_OLEO\' * 1.00', tagType: TagPIType.CALCULATED).save()).save()

                        new UEP(name: 'FPSO-CDAN', uo: 'UO-ES', department: 'SSE', asset: 'ATP-JBT-CHT', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag FPSO-CDAN', formula: '\'PRODIN_FCDA_PRODUCAO_OLEO\' * 1.00', tagType: TagPIType.CALCULATED).save()).save()


                        // UO-BC
                        addFilters(new TagPI(name: 'P15-DV_FI-552501_PV', formula: 'P15-DV_FI-552501_PV', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC),
                                [
                                        [kind: 'kalman', measureVariance: 2000.0, processVariance: 50.0],
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()
                        addFilters(new TagPI(name: 'P15-DV_FI-552501_PV_CAN1', formula: 'P15-DV_FI-552501_PV_CAN1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC),
                                [
                                        [kind: 'kalman', measureVariance: 2000.0, processVariance: 50.0],
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()

                        addFilters(new TagPI(name: 'P15-DV_FI-552501_PV_CAN2', formula: 'P15-DV_FI-552501_PV_CAN2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BC),
                                [
                                        [kind: 'kalman', measureVariance: 2000.0, processVariance: 50.0],
                                        [kind: 'average', windowInMinutes: 2]
                                ]
                        ).save()

                        new UEP(name: 'P-15', uo: 'UO-BS', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-15', formula: '(\'P15-DV_FI-552501_PV\' + \'P15-DV_FI-552501_PV_CAN1\' + \'P15-DV_FI-552501_PV_CAN2\') * 0.78 * 6.2898', tagType: TagPIType.CALCULATED).save()).save();
    
                    }

                    ['UP2H','UQ4E','UQ4N','UPN1'].each { user ->
                        new UserPermission(attribute: 'uo', value: 'UO-BS', username: user).save(flush:true)
                        new UserPermission(attribute: 'uo', value: 'UO-ES', username: user).save(flush:true)
                        new UserPermission(attribute: 'uo', value: 'UO-BC', username: user).save(flush:true)
                        new UserPermission(attribute: 'uo', value: 'UO-RIO', username: user).save(flush:true)
                    }

                } catch (ValidationException e) {
                    log.warn("Erro ao inicializar dados no bootstrap (esta mensagem é esperada caso o banco seja persistênte entre execuções): ${e}")
                }
                break

        }

        try {
            if(Environment.current != Environment.TEST){
                log.info("Iniciando o job...")
                def elapsedTime = 1000*30
                JobDetail job = newJob(PaapScheduler.class).withIdentity("job1", "group1").build();
                Trigger trigger = newTrigger().withIdentity("trigger1", "group1").startAt(new Date(new Date().getTime())).withSchedule(simpleSchedule().withIntervalInMilliseconds(elapsedTime).repeatForever()).build();
                SchedulerFactory sf
                if (Environment.current == Environment.PRODUCTION) sf = new StdSchedulerFactory('production.quartz.properties') else sf = new StdSchedulerFactory()
                scheduler = sf.getScheduler();
                scheduler.deleteJob(new JobKey("job1", "group1"));
                scheduler.scheduleJob(job, trigger);
                scheduler.start();
                log.info("Job iniciado com sucesso")
            }

        } catch (Exception e) {
            log.error("Erro ao iniciar job", e)
        }
    }
    def destroy = {
        try {
            if(Environment.current != Environment.TEST){
                log.info("Removendo o job...")
                scheduler.shutdown();
                scheduler.deleteJob(new JobKey("job1", "group1"));
                log.info("Job removido com sucesso")
            }
        } catch (Exception e) {
            log.error("Erro ao encerrar job", e)
        }
    }

}
