<%@ page import="br.com.petrobras.paap.UserPermission" %>

<table ng-table="userTable" class="table" show-filter="true">
	<tr ng-repeat="user in $data">
		<td>
			<input type="hidden" name="{{user.username}}" value="{{user.name}}" id="{{user.username}}">
			<input type="hidden" name="_users">
			<input type="checkbox" name="users" value="{{user.username}}" id="{{user.username}}" ng-model="checkbox[user.username]">
		</td>
		<td class="col-sm-2" data-title="'${message(code:"user.username.label")}'" filter="{ 'username': 'text'}" sortable="'username'">
		    <a href="/painelep/admin/userPermission/edit?username={{user.username}}">{{user.username}}</a>
		</td>
		<td class="col-sm-10" data-title="'${message(code:"user.name.label")}'" filter="{ 'name': 'text'}" sortable="'name'">
		    <a href="/painelep/admin/userPermission/edit?username={{user.username}}">{{user.name}}</a>
		</td>
	</tr>
	
</table>