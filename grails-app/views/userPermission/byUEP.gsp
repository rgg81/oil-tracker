<%@ page import="br.com.petrobras.paap.UserPermission" %>
<%@ page import="grails.converters.*" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'userPermission.label', default: 'UserPermission')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
		<asset:javascript src="user-list.js"/>
		<asset:javascript src="ng-table.min.js"/>
		<asset:stylesheet src="ng-table.min.css"/>		
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="UserPermission.attribute.edit.label" args="[displayText]" /></h2>
		</div>

		<div id="edit-userPermission" class="container" role="main" ng-controller="userListController" ng-init="init(${users as JSON})">
			<div class="col-md-9 admin-full-page">
				<g:if test="${flash.message}">
					<div class="alert alert-info" role="status">
						${flash.message}
						<ul>
							<g:each var="user" in="${flash.users}">
								<li>${user.username} - ${user.name}</li>
							</g:each>
						</ul>
					</div>
				</g:if>
				<g:if test="${flash.error}">
					<div class="alert alert-danger" role="status">
						${flash.error}
					</div>
				</g:if>
				<g:form action="addPermission" namespace="admin" method="POST" class="form-horizontal center-block col-sm-8 ">
					<div class="required form-group">
						<div class="col-sm-4">
							<g:textField name="username" placeholder="Chave" required="" maxlength="15" class="form-control" value=""/>
						</div>
						<div class="col-sm-4">
							<g:submitButton name="Conceder permissão" class="btn btn-primary"></g:submitButton>
						</div>
					</div>
					<g:hiddenField name="attribute" value="${attribute}"/>
					<g:hiddenField name="value" value="${value}"/>
				</g:form>
				<g:form action="updateAttribute" ng-submit="submit()" name="user-list" namespace="admin" method="PUT" class="form-horizontal center-block col-sm-12 ">
					<g:render template="attributeForm"/>
					<g:submitButton name="updateAttribute" class="btn btn-danger" value="Retirar permissão dos usuários selecionados" />
					<g:hiddenField name="attribute" value="${attribute}"/>
					<g:hiddenField name="value" value="${value}"/>
				</g:form>
			</div>
		</div>
	</body>
</html>
