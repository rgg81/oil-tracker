<%@ page import="br.com.petrobras.paap.UserPermission" %>

<g:each var="user" in="${permissions}">
	<shiroPermissions:hasManagerAccess username="${user.username}">
		<span class="label label-warning"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> <g:message code="UserPermission.manager.has.access.warning"/></span>
	</shiroPermissions:hasManagerAccess>
	<h3>${user.name} - ${user.username}</h3>
	<div class="container-fluid" style="margin-top: 1px">
	<g:hiddenField name="username" value="${user.username}" />
	<g:each in="${uos}" status="i" var="uo">
		<div id="uo-${uo.name}" class="container-fluid uo-permission ${user.permissions?.uo?.contains(uo.name) ? 'uo-selected' : ''}">
		<h3 onclick="toggleSelectUO('${uo.name}')" style="cursor: default"><div id="selection-${uo.name}" class="uo-checkbox ${user.permissions?.uo?.contains(uo.name) ? 'checked' : 'unchecked'}"></div>${uo.name}</h3>		
		<g:checkBox name="${uo.name}" value="${user.permissions?.uo?.contains(uo.name)}" class="hidden-checkbox"/>
		<div class="row">
			<g:each in="${uo.values}" var="uep">				
				<div class="col-md-1 admin-uep-permission-card ${user.permissions?.uo?.contains(uep.uo) ? 'unchangeable' : ''} ${user.permissions?.id?.contains(uep.id.toString()) ? 'selected' : ''}" id="div-${uep.id}" onclick="select(${uep.id})" title="${uep.name}">
					<div class="admin-uep-card-name ellipsis">${uep.name}</div>
					<span class="admin-uep-card-department ellipsis">${uep.department}</span>							
					<g:checkBox name="uep-${uep.id.toString()}" id="checkbox-${uep.id}" value="${user.permissions?.uo?.contains(uep.uo) && !user.permissions?.id?.contains(uep.id.toString()) ? false : user.permissions?.id?.contains(uep.id.toString()) ? true: false}" class="hidden-checkbox"/>
				</div>
			</g:each>
		</div>
		</div>
	</g:each>
	</div>

</g:each>

<asset:javascript src="typeahead.js"/>
<asset:javascript src="typeahead-rest-directive.js"/>
