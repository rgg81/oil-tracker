
<%@ page import="br.com.petrobras.paap.UserPermission" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<title><g:message code="UserPermission.permissions.label"/></title>
		<asset:javascript src="typeahead.js"/>
		<asset:javascript src="typeahead-rest-directive.js"/>
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="UserPermission.permissions.label"/></h2>
		</div>
		<div class="container">
			<div id="list-userPermission" class="col-md-12" class="content scaffold-list" role="main">

				<g:if test="${flash.message}">
					<div class="alert alert-info" role="status">
						${flash.message}
					</div>
				</g:if>
				<g:if test="${flash.error}">
					<div class="alert alert-danger" role="status">
						${flash.error}
					</div>
				</g:if>

				<g:form url="[namespace:'admin', action:'edit']"  class="form-horizontal center-block col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							Procurar por usuário
						</div>
						<div class="panel-body">
							<div class="required form-group">
								<div class="col-sm-8">
									<g:textField name="username" placeholder="Chave" required="" maxlength="15" class="form-control" value=""/>
								</div>
								<div class="col-sm-4">
									<g:submitButton name="Verificar" class="btn btn-primary"></g:submitButton>
								</div>
							</div>
						</div>
					</div>
				</g:form>

				<g:form url="[namespace:'admin', action:'editAttribute']"  class="form-horizontal center-block col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							Procurar por UO
						</div>
						<div class="panel-body">
							<div class="required form-group">
								<div class="col-sm-8">
									<g:hiddenField name="attribute" value="uo"/>
									<g:select class="form-control" name="value" from="${uos}"></g:select>
								</div>
								<div class="col-sm-4">
									<g:submitButton name="Verificar" class="btn btn-primary"></g:submitButton>
								</div>
							</div>
						</div>
					</div>
				</g:form>

				<g:form url="[namespace:'admin', action:'byUEP']"  class="form-horizontal center-block col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							Procurar por UEP
						</div>
						<div class="panel-body">
							<div class="required form-group">
								<div class="col-sm-8">
									<g:hiddenField name="attribute" value="name"/>
									<g:textField name="value" required="" class="form-control typeahead" autocomplete autocomplete-url="/painelep/api/UEP/names"/>
								</div>
								<div class="col-sm-4">
									<g:submitButton name="Verificar" class="btn btn-primary"></g:submitButton>
								</div>
							</div>
						</div>
					</div>
				</g:form>

			</div>
		</div>
	</body>
	
</html>
