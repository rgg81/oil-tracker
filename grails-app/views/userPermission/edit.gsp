<%@ page import="br.com.petrobras.paap.UserPermission" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'userPermission.label', default: 'UserPermission')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
		<asset:javascript src="edit-permissions.js"/>
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="UserPermission.permissions.label"/></h2>
		</div>

		<div id="edit-userPermission" class="container-fluid" role="main">
			<g:if test="${flash.message}">
				<div class="alert alert-info" role="status">
					${flash.message}
				</div>
			</g:if>
			<g:form url="[namespace:'admin',action:'update']" method="PUT"  class="form-horizontal center-block col-sm-12 ">
				<g:render template="form" model="['permissions':permissions]"/>
				<div class="col-md-7 pull-right" style="background: #fafafa; padding-bottom: 15px;margin-right: 15px; margin-bottom: 15px">
					<h3>Legenda</h3>
					<div class="col-md-12">
						<div class="col-md-2 admin-uep-permission-card selected">
							<div class="admin-uep-card-name ellipsis">UEP</div>
							<span class="admin-uep-card-department ellipsis">&nbsp;</span>							
						</div>
						<div class="col-md-3" style="padding: 15px 0">
						UEP com permissão individual
						</div>
						<div class="col-md-2 admin-uep-permission-card">
							<div class="admin-uep-card-name ellipsis">UEP</div>
							<span class="admin-uep-card-department ellipsis">&nbsp;</span>							
						</div>
						<div class="col-md-3" style="padding: 15px 0">
						UEP sem permissão individual
						</div>
					</div>
					<div class="col-md-12" style="padding-top: 5px; ">
						<div class="uo-permission uo-selected col-md-2" style="margin-left: 5px; padding: 0 1em 1em 1em">
						<h3 style="cursor: default"><div class="uo-checkbox checked"></div>UO</h3>	
						</div>
						<div class="col-md-3" style="margin-left: 5px; padding: 15px 0">
							Permissão de acesso à todas UEPs da UO
						</div>
						<div class="uo-permission col-md-2" style="margin-left: 5px; padding: 0 1em 1em 1em">
						<h3 style="cursor: default"><div class="uo-checkbox unchecked"></div>UO</h3>	
						</div>
						<div class="col-md-3" style="margin-left: 5px; padding: 15px 0">
							Sem permissão de acesso à UO (não inviabiliza permissões individuais por UEP)
						</div>
					</div>
				</div>
				<div class="col-md-4">
				<g:actionSubmit class="btn btn-primary" action="update" namespace="admin" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</div>

			</g:form>
		</div>
	</body>
</html>
