
<%@ page import="br.com.petrobras.paap.UserPermission" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'userPermission.label', default: 'UserPermission')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body class="admin">
		<nav class="navbar navbar-default">
			<div class="container-fluid" role="navigation">
				<ul class="nav navbar-nav">
					<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
					<li><g:link class="list" action="index" namespace="admin"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
					<li><g:link class="create" action="create" namespace="admin"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</ul>
			</div>
		</nav>

		<div id="show-userPermission" class="container" role="main">
			<h2><g:message code="default.show.label" args="[entityName]" /></h2>

			<g:if test="${flash.message}">
				<div class="alert alert-info" role="status">
					${flash.message}
				</div>
			</g:if>

			<table class="table">
			
				<g:if test="${userPermissionInstance?.attribute}">
				<tr>
					<td>
						<span id="attribute-label" class="property-label bold"><g:message code="userPermission.attribute.label" default="Attribute" /></span>
					</td>
					<td>
					
						<span class="property-value" aria-labelledby="attribute-label"><g:fieldValue bean="${userPermissionInstance}" field="attribute"/></span>
					
					</td>
				</tr>
				</g:if>
			
				<g:if test="${userPermissionInstance?.username}">
				<tr>
					<td>
						<span id="username-label" class="property-label bold"><g:message code="userPermission.username.label" default="Username" /></span>
					</td>
					<td>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${userPermissionInstance}" field="username"/></span>
					
					</td>
				</tr>
				</g:if>
			
				<g:if test="${userPermissionInstance?.value}">
				<tr>
					<td>
						<span id="value-label" class="property-label bold"><g:message code="userPermission.value.label" default="Value" /></span>
					</td>
					<td>
					
						<span class="property-value" aria-labelledby="value-label"><g:fieldValue bean="${userPermissionInstance}" field="value"/></span>
					
					</td>
				</tr>
				</g:if>
			
			</table>
			<g:form url="[resource:userPermissionInstance, namespace:'admin', action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="btn btn-primary" action="edit" resource="${userPermissionInstance}" namespace="admin"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="btn btn-danger" action="delete" namespace="admin" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
