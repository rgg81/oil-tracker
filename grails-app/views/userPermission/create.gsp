<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'userPermission.label', default: 'UserPermission')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body class="admin">
		<nav class="navbar navbar-default">
			<div class="container-fluid" role="navigation">
				<ul class="nav navbar-nav">
					<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
					<li><g:link class="list" action="index" namespace="admin"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				</ul>
			</div>
		</nav>
		<div id="create-userPermission" class="container" role="main" ng-controller="permissionController"  >
			<h2><g:message code="default.create.label" args="[entityName]" /></h2>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${userPermissionInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${userPermissionInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>

			<g:form url="[resource:userPermissionInstance, namespace:'admin', action:'save']"  class="form-horizontal center-block col-sm-8">
				<g:render template="form"/>
				<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
			</g:form>
		</div>
	</body>

</html>


