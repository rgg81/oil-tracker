<!-- Modal -->
<div ng-controller="reloadController" ng-init="init(${UEPInstance ? UEPInstance.tag.id : TagInstance.id},${UEPInstance ? UEPInstance.id : -1})" id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
	    <form action="/painelep/admin/${UEPInstance ? 'UEP' : 'TagPI'}/reload/${UEPInstance ? UEPInstance.id : TagInstance.id}" method="post">
	    	<div class="modal-header">
	    		<g:if  test="${UEPInstance}">
	    			<h4 class="modal-title">Recarregar uep <strong>${UEPInstance.name}</strong></h4>
	    		</g:if>
	    		<g:else>
	    			<h4 class="modal-title">Recarregar uep <strong>${TagInstance.name}</strong></h4>
	    		</g:else>
	    	</div>
	      <div class="modal-body">
				<div ng-show="childrenTags.length > 0" class="panel panel-default">
				 	<div class="panel-heading">Tags afetadas</span></div>
					<div class="panel-body">
						<input type="hidden" name="reloadTags" value="${UEPInstance ? UEPInstance.tag.id : TagInstance.id}">
					   	<div class="checkbox" ng-repeat="tag in childrenTags">
						  <label>
						    <input ng-model="checkbox[tag.id]" ng-disabled="uReloading" ng-change="reloadRelatedUeps()" type="checkbox" name="reloadTags" value="{{tag.id}}">{{tag.name}}<br>
						  </label>
						</div>
				  	</div>
				</div>
				<div ng-show="relatedUeps.length > 0" class="panel panel-danger">
					<div class="panel-heading">UEPs afetadas</div>
					<div class="panel-body">
						{{relatedUepsString}}
						<!-input ng-repeat="uep in relatedUeps" type="hidden" name="reloadTags" value="{{uep.tag.id}}"-->			
					</div>
				</div>
	      </div>
	      <div class="modal-footer">
	      	
			<button type="submit" ng-disabled="cReloading || uReloading" type="button" class='btn btn-danger'>
				<span ng-show="cReloading || uReloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
				${message(code: 'default.button.confirm.label', default: 'Confirmar')}
			</button>
	        <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
	      </div>
	    </form>
    </div>
  </div>
</div>