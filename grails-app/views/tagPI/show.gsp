<%@ page import="grails.converters.JSON" %>
<%@ page import="br.com.petrobras.paap.TagPI" %>
<%@ page import="br.com.petrobras.paap.ReloadStatus" %>

<%@ page import="br.com.petrobras.paap.TagPIType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'tagPI.label', default: 'TagPI')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="default.show.label" args="[entityName]" /></h2>
		</div>
		<div id="show-tagPI" class="container admin-full-page" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
				<br>
			</g:if>
			<g:if test="${errorcode}">
				<div class="message" role="status">${message(code: errorcode,args:[tagPIInstance.name])}</div>
				<ul>
					<g:each status="i" in="${tagPIInstance.possibleParentTags}" var="calc">
					<li><g:link controller="TagPI" namespace="admin" action="show" id="${calc.id}">${calc.name}</g:link></li>
					</g:each>
				</ul>
				
				<br>
			</g:if>

			<table class="table">			
			
				<g:if test="${tagPIInstance?.name}">
				<tr>
					<td><span id="name-label" class="property-label"><g:message code="tagPI.name.label" default="Name" /></span></td>
					
					<td><span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${tagPIInstance}" field="name"/></span></td>
					
				</g:if>
			
				<g:if test="${tagPIInstance?.formula}">
				<tr>
					<td><span id="formula-label" class="property-label"><g:message code="tagPI.formula.label" default="Formula" /></span></td>
					
					<td><span class="property-value" aria-labelledby="formula-label">${tagFormula.encodeAsRaw()}</span></td>
					
				</g:if>
			
				<g:if test="${tagPIInstance?.tagType}">
				<tr>
					<td><span id="tagType-label" class="property-label"><g:message code="tagPI.tagType.label" default="Tag Type" /></span></td>
					
					<td><span class="property-value" aria-labelledby="tagType-label"><g:fieldValue bean="${tagPIInstance}" field="tagType"/></span></td>
					
				</g:if>

                <g:if test="${tagPIInstance?.tagLocation}">
                    <tr>
                        <td><span id="tagType-label" class="property-label"><g:message code="tagPI.tagLocation.label" default="Tag Location" /></span></td>

                        <td><span class="property-value" aria-labelledby="tagLocation-label"><g:fieldValue bean="${tagPIInstance}" field="tagLocation"/></span></td>

                </g:if>
                <g:if test="${tagPIInstance?.filters}">
                    <g:set var="filtersSpec" value="${tagPIInstance.filtersSpec()}" />
                    <g:each status="i" in="${filtersSpec}" var="filterSpec">
                        <tr>
                            <g:if test="${i == 0}">
                                <td rowspan="${filtersSpec.size()}"><span id="filters-label" class="property-label"><g:message code="tagPI.filters.label" default="Filters" /></span></td>
                            </g:if>
                            <td><span class="property-value" aria-labelledby="filters-label">${filterSpec}</span></td>
                        </tr>
                    </g:each>
                </g:if>
			</table>
		
				<fieldset class="buttons form-buttons">
					<g:form  url="[resource:tagPIInstance, namespace:'admin', action:'delete']" method="DELETE">
						<g:link class="btn btn-primary" action="edit" namespace="admin" resource="${tagPIInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					
						<g:actionSubmit class="btn btn-danger" action="delete" namespace="admin" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					</g:form>
					<g:if test="${tagPIInstance.tagType == TagPIType.CALCULATED }">
						<g:if test="${tagPIInstance.reloading || tagPIInstance.reloadStatus == ReloadStatus.QUEUE}">
							<button class="btn btn-warning" type="button" ng-disabled="true"/>${
								tagPIInstance.reloadStatus == ReloadStatus.DONE ? 
								message(code: 'default.button.reloading.PROCESSING' ):
								message(code: 'default.button.reloading.' + tagPIInstance.reloadStatus )}</button>
						</g:if>
						<g:else>
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">${
								tagPIInstance.reloadStatus == ReloadStatus.FAILED ? 
								message(code: 'default.button.reloading.RETRY') : 
								message(code: 'default.button.reloading.' + tagPIInstance.reloadStatus )}</button>
						</g:else>
					</g:if>
				<button class="btn btn-sm btn-link" type="button" onclick="$('#events').toggle()">Events</button>

				</fieldset>
			
		</div>
		<div class="container" style="padding: 20px">
			<table class="table" id="events" style="display: ${params.showEvents == 'true' ? 'block' : 'none'}">
				<tr><th style="width: 100px">Level</th><th style="width: 130px">Date</th><th>Message</th></tr>
				<g:each var="event" in = "${tagPIInstance?.events}">
					<tr>
						<td><g:fieldValue bean="${event}" field="eventType"/></td>
						<td><g:formatDate format="dd/MM HH:mm:ss" date="${event.date}"/></td>
						<td><g:fieldValue bean="${event}" field="msg"/></td>
					</tr>
				</g:each>
			</table>
			<ul>
			</ul>
		</div>

		<g:render template="../reload-modal" model="['TagInstance': tagPIInstance]"/>
		<asset:javascript src="reload-uep-tag.js"/>
	</body>
</html>
