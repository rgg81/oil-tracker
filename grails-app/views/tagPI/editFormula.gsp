<%@ page import="br.com.petrobras.paap.TagPI" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'tagPI.label', default: 'TagPI')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body class="admin">
		<div id="edit-tagPI" class="container admin-full-page" role="main">
			<h2><g:message code="default.edit.label" args="[entityName]" /></h2>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${tagPIInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${tagPIInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>

			<g:form url="[namespace:'admin', action:'updateFormula', params: [tagId:tagPIInstance.id]]" method="PUT" class="center-block col-sm-12" >
				<g:render template="formulaForm"/>

				<g:actionSubmit class="btn btn-primary" action="updateFormula" namespace="admin" value="${message(code: 'default.button.update.label', default: 'Update')}" />

			</g:form>
		</div>
	</body>
</html>
