
<%@ page import="br.com.petrobras.paap.TagPI" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'tagPI.label', default: 'TagPI')}" />
		<title><g:message code="default.list.fem.label" args="['Tags']" /></title>
		<asset:javascript src="index-tag.js"/>
		<asset:javascript src="ng-table.min.js"/>
		<asset:stylesheet src="ng-table.min.css"/>
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="default.list.fem.label" args="['Tags']" /></h2>
		</div>
		<div class="container-fluid admin-content-fluid">
			<div id="list-tagPI" class="col-md-12" role="main" ng-controller="indexTagController">
				<g:if test="${flash.message}">
					<div class="message" role="status">${flash.message}</div>
				</g:if>
				<span ng-hide="tags" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
				<table ng-show="tags" ng-table="tagTable" class="table" show-filter="true">
			        <tr ng-repeat="tag in $data">
			           <td data-title="'${message(code:"tagPI.name.label")}'" filter="{ 'name': 'text'}" sortable="'name'">
			                <a href="/painelep/admin/tagPI/show/{{tag.id}}">{{tag.name}}</a>
			            </td>
			            <td data-title="'${message(code:"tagPI.formula.label")}'" filter="{ 'formula': 'text' }" sortable="'formula'">
			                <a href="/painelep/admin/tagPI/show/{{tag.id}}">{{tag.formula}}</a>
			            </td>
			            <td data-title="'${message(code:"tagPI.tagType.label")}'" filter="{ 'type': 'text' }" sortable="'text'">
			                <a href="/painelep/admin/tagPI/show/{{tag.id}}">{{tag.type}}</a>
			            </td>
			            <td data-title="'${message(code:"tagPI.tagLocation.label")}'" filter="{ 'location': 'text' }" sortable="'location'">
			                <a href="/painelep/admin/tagPI/show/{{tag.id}}">{{tag.location}}</a>
			            </td>
			            <td data-title="'${message(code:"tagPI.filters.label")}'" sortable="'filters'">
			                <a href="/painelep/admin/tagPI/show/{{tag.id}}">{{tag.filters}}</a>
			            </td>
			        </tr>
				</table>
				
			</div>
		</div>
	</body>
</html>
