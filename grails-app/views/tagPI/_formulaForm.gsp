<%@ page import="br.com.petrobras.paap.TagPI" %>
<%@ page import="br.com.petrobras.paap.TagPIType" %>
<%@ page import="br.com.petrobras.paap.TagPILocation" %>
<%@ page import="grails.converters.JSON" %>

<asset:javascript src="edit-tag.js"/>

<!-- Form horizontal -->
<div ng-controller="testChartController"
     ng-init='init("preview-chart", "${tagPIInstance?.formula}", "${tagPIInstance?.tagType?.name() ?: TagPIType.SNAPSHOT.name()}", "${tagPIInstance?.tagLocation?.name() ?: TagPILocation.UO_ES}", ${tagPIInstance.filtersSpec() as JSON})'>
    <div class="form-horizontal">
        <input type="hidden" name="filtersSpec" value="{{selectedFilters}}" />

        <div class="fieldcontain ${hasErrors(bean: tagPIInstance, field: 'name', 'error')} required form-group">
            <label for="name" class="col-sm-2">
                <g:message code="tagPI.name.label" default="Name" />
                <span class="required-indicator">*</span>
            </label>
            <div class="col-sm-8">
                ${tagPIInstance?.name}
            </div>

        </div>

        <div class="fieldcontain ${hasErrors(bean: tagPIInstance, field: 'formula', 'error')} required form-group">
            <label for="formula" class="col-sm-2">
                <g:message code="tagPI.formula.label" default="Formula" />
                <span class="required-indicator">*</span>
            </label>
            <div class="col-sm-8">
                <g:textField name="formula" required="" class="form-control" value="${tagPIInstance?.formula}" ng-model="formula"/>
            </div>
            <!--div class="col-sm-2"><input type="button" name="update" class="btn btn-secondary" value="Verificar" id="update-chart" ng-click="checkFormula()"></div-->


        </div>
    </div>

    <!-- Filtros -> form inline -->
    <div class="panel panel-default form-tag-filtros">
        <div class="panel-heading"><label>Filtros</label></div>

        <div class="panel-body">
            <div class="form-inline form-tag-filtro-linha" ng-repeat="selectedFilter in selectedFilters">
                <div class="form-group">
                    <select class="form-control" ng-change="updateFilter(selectedFilter)" ng-model="selectedFilter.kind">
                        <option value="average">Média Móvel</option>
                        <option value="kalman">Kalman Filter</option>
                        <option value="minmax">Min Max</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" ng-model="selectedFilter.enabled" ng-change="updateChartWithCachedData()">Habilitar
                        </label>
                    </div>
                </div>
                <div class="form-group" ng-repeat="(propertyName, propertyValue) in selectedFilter.properties">
                    <label class="label-input-number">{{labels[selectedFilter.kind][propertyName]}}</label>
                    <input type="number" class="form-control text-right" min="0" ng-model="selectedFilter.properties[propertyName]" ng-change="updateChartWithCachedData()">
                </div>
                <div class="form-group pull-right">
                    <button type="button" ng-click="removeFilter(selectedFilter)" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash"></span></button>
                    <button type="button" ng-click="upFilter($index)" ng-disabled="$first" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-up"></span></button>
                    <button type="button" ng-click="downFilter($index)" ng-disabled="$last" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-down"></span></button>
                </div>
            </div>

            <div class="form-group form-tag-filtro-linha text-center">
                <button type="button" ng-click="addFilter()" class="btn"><span class="glyphicon glyphicon-plus"></span> Adicionar</button>
            </div>
            <div class="form-inline form-tag-filtro-linha">
                <label>Data:</label>
                <input class="form-control"  type="date" ng-model="dt" max="<g:formatDate format="yyyy-MM-dd" date="${new Date()}"/>" ng-required="false" />
                <input type="button" name="update" class="btn btn-secondary" value="Simular Gráfico" id="update-chart" ng-click="updateChart()">
            </div>
            <div class="form-group form-tag-filtro-linha">
                <div id="preview-chart">
                </div>
            </div>
        </div>
    </div>
</div>