 <!-- Modal -->
 <div ng-controller="reloadUOController" ng-init="init('${uepsUo.name}',${uepsUo.values*.id},'modal${uepsUo.name}')" id="modal${uepsUo.name}" class="modal fade" role="dialog">
  <div class="modal-dialog modal-uo">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <div class="row">
                <h4 class="modal-title col-md-5">
                    <strong>{{uoname}}</strong>
                </h4>
            </div>
        </div>
        <div class="modal-body">
            <div ng-show="data.length > 0" >
                <div class="row" ng-repeat="row in rows">
                    <div class="col-md-4" ng-repeat="uep in row">
                        <div ng-class="uepClass(uep)">
                            <div class="panel-heading">
                                <div class="pull-right">
                                    <span ng-show="onqueue(uep)" class="label label-warning">Fila</span>
                                    <span ng-show="onreloading(uep)" class="label label-danger">Recarregando</span>
                                    <span ng-show="onfailed(uep)" class="label label-error">Erro</span>
                                </div>
                                <a href="#" ng-click="openClose(uep)">
                                    <strong>{{uep.name}}</strong>
                                </a>
                            </div>
                            <div class="panel-body" id="reload-detail-{{uep.id}}">
                                <!--a ng-show="uep" href="#" ng-click="markGroup(uep,true)">todos</a>
                                <a ng-show="uep" href="#" ng-click="markGroup(uep,false)">limpar</a-->
                                <div class="checkbox" ng-repeat="tag in uep.tags">
                                  <label>
                                    <input id="{{uep.id + 'x' + tag.id}}" ng-model="checkbox[uep.id][tag.id]" ng-disabled="reloading || tag.reloadStatus == 'QUEUE' || tag.reloading || (onlycalc && tag.type!='CALCULATED')" type="checkbox" name="reloadTags" value="{{tag.id}}">{{tag.name}}
                                    <span ng-show="tag.reloadStatus == 'QUEUE' " class="label label-warning">Fila</span>
                                    <span ng-show="tag.reloading" class="label label-danger">Recarregando</span>
                                    <span ng-show="tag.reloadStatus == 'FAILED' " class="label label-error">Erro</span>
                                    <br>
                                  </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <!--button ng-click="checkonlycalc()" class="btn btn-default pull-left">Selecionar Apenas tags calculadas</button-->
            <button ng-click="reload()" ng-disabled="reloading || !anyTagToReload()" type="button" class='btn btn-danger'>
                <span ng-show="reloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                ${message(code: 'default.button.confirm.label', default: 'Regerar UEPS')}
            </button>
            <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
        </div>
        
    </div>
  </div>
</div>