<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Painel Online de Produção"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
	</head>
    <g:set var="admin" value="${true}"/>
    <shiro:lacksRole name='${ grailsApplication.config.app.role.admin }'>
        <g:set var="admin" value="${false}"/>
    </shiro:lacksRole>
	<body ng-app="paapApp" class="${pageProperty(name: 'body.class') + (admin ? '' : ' uep-admin')}">


    <!-- header -->
            <div class="container-fluid topo" ng-controller="headerController">
                <div class="titulo">
                    <h1><a href="${createLink(uri:"/", params: (params.view == 'card' ? [view:'card'] : ''))}">Painel Online de Produção</a></h1>
                    <span class="traco hidden-xs">—</span>
                    <span class="pull-right subtitulo hidden-xs">E&P-SSE/PG/PLC</span>
                    <span class="h1data hidden-sm hidden-md hidden-lg">
                        <span class="pull-right subtitulo hidden-md">E&P-SSE/PG/PLC</span>
                    </span>
                </div>


                <div class="logo-petrobras pull-right" title="Logo Petrobras"></div>                
                <div class="usuario pull-right"><greetedUser:showName/><br>Sessões em tempo real: <onlineUsers:count/></div>

            </div>        
        <div class="container-fluid separador">
            <div class="row">
                <div class="admin-sidebar col-sm-2">
                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <span class="visible-xs navbar-brand">Administração</span>
                            </div>
                            <shiro:hasRole name="${ grailsApplication.config.app.role.admin }">
                            <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="submenu">
                                        <span class="submenu-title">UEPs</span>
                                        <ul class="list-unstyled submenu-link-list">
                                            <li class="submenu-item"><span class="glyphicon glyphicon glyphicon-th-list" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="UEP" action="index" namespace="admin"><g:message code="default.list.fem.label" args="['UEPs']" /></g:link></li>
                                            <li class="submenu-item"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 10px"></span><g:link class="create admin-menu-link" controller="UEP" action="create" namespace="admin"><g:message code="default.new.fem.label" args="['UEP']" /></g:link></li>
                                        </ul>
                                    </li>

                                    <li class="submenu">
                                        <span class="submenu-title">Tags</span>
                                        <ul class="list-unstyled submenu-link-list">
                                            <li class="submenu-item"><span class="glyphicon glyphicon-th-list" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="TagPI" action="index" namespace="admin"><g:message code="default.list.fem.label" args="['Tags']" /></g:link></li>
                                            <li class="submenu-item"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 10px"></span><g:link class="create admin-menu-link" controller="TagPI" action="create" namespace="admin"><g:message code="default.new.fem.label" args="['Tag']" /></g:link></li>
                                        </ul>
                                    </li>

                                    <li class="submenu">
                                        <span class="submenu-title">Administração</span>
                                        <ul class="list-unstyled submenu-link-list" style="">
                                            <li><span class="glyphicon glyphicon-eye-open" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="UserPermission" action="index" namespace="admin"><g:message code="UserPermission.permissions.label"/></g:link></li>
                                            <li><span class="glyphicon glyphicon-lock" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="UserPermission" action="adminPermission" namespace="admin"><g:message code="UserPermission.admin.label"/></g:link></li>
                                            <li><span class="glyphicon glyphicon-cog" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="UserPermission" action="managerPermission" namespace="admin"><g:message code="UserPermission.manager.label"/></g:link></li>
                                            <li><span class="glyphicon glyphicon-time" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="ProcessStatus" action="index" namespace="admin"><g:message code="processStatus.process.status.label"/></g:link></li>
                                        </ul>
                                    </li>

                                    <li class="submenu">
                                        <span class="submenu-title"><g:message code="sitopBdo.label"/></span>
                                        <ul class="list-unstyled submenu-link-list" style="">
                                            <li><span class="glyphicon glyphicon-th-list" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="SitopBdo" action="index" namespace="admin"><g:message code="default.list.label" args="[message(code: 'sitopBdo.label', default: '')]"/></g:link></li>
                                            <li><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="SitopBdo" action="upload" namespace="admin"><g:message code="default.new.label" args="[message(code: 'sitopBdo.label', default: '')]"/></g:link></li>
                                            <li><span class="glyphicon glyphicon-download-alt" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="SitopBdo" action="exportLastExcel" namespace="admin"><g:message code="sitopBdo.saveLastExcel.label"/></g:link></li>
                                            <li><span class="glyphicon glyphicon-calendar" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="SitopBdo" action="exportExcel" namespace="admin" params="[weekly:true]"><g:message code="sitopBdo.exportLastWeekToExcel.label"/></g:link></li>
                                            <li><span class="glyphicon glyphicon-download" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="SitopBdo" action="exportExcel" namespace="admin"><g:message code="sitopBdo.exportAllToExcel.label"/></g:link></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!--/.nav-collapse -->
                            </shiro:hasRole>
                            <shiro:lacksRole name='${ grailsApplication.config.app.role.admin }'>
                            <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="submenu">
                                        <span class="submenu-title">UEPs</span>
                                        <ul class="list-unstyled submenu-link-list">
                                            <li class="submenu-item"><span class="glyphicon glyphicon glyphicon-th-list" aria-hidden="true" style="margin-right: 10px"></span><g:link class="list admin-menu-link" controller="UEP" action="index" namespace="admin"><g:message code="default.list.fem.label" args="['UEPs']" /></g:link></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            </shiro:lacksRole>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 admin-content">
                    <g:layoutBody/>
                </div>
            </div>
        </div>
    </body>
</html>
