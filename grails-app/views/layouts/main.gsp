<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Painel Online de Produção"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
	</head>
	<body ng-app="paapApp" class="${pageProperty(name: 'body.class')}">
    <!-- header -->
    <div class="container-fluid topo" ng-controller="headerController">
        <div class="titulo">
            <h1><a href="${createLink(uri:"/", params: (params.view == 'card' ? [view:'card'] : ''))}">Painel Online de Produção</a></h1>
            <span class="traco hidden-xs">—</span>
            <span class="pull-right subtitulo hidden-xs">E&P-SSE/PG/PLC</span>
            <span class="h1data hidden-sm hidden-md hidden-lg"  ng-show="newestDataDate != null">{{ newestDataDate | date:"HH:mm:ss"}} <span class="online-mark" ng-class="{'on': online, 'animate': initialLoading}"></span>
            <span class="pull-right subtitulo hidden-md">E&P-SSE/PG/PLC</span>
            </span>
        </div>

        <ul class="funcionalidades">
            <shiro:hasRole name="${ grailsApplication.config.app.role.admin }">
                <!--<li class="hidden-xs"><a href="#" class="f-status"><span class="sr-only">Notificações</span></a></li>-->
                <!-- li><a href="#" class="f-alerta"><span class="sr-only">Alertas</span></a></li -->
                <li class="dropdown hidden-xs"><a href="#" class="dropdown-toggle f-alerta" data-toggle="dropdown" ng-class="{ativo : (events.length > 0)}"><span class="sr-only">Notificações</span></a>
                    <ul class="dropdown-menu noticebar-menu" role="menu">
                        <li class="nav-header pull-left">Histórico de Notificações</li>
                        <li class="noticebar">
                            <a ng-href="${createLink(namespace: 'admin',controller: 'tagPI', action: 'show')}/{{event.tag}}?showEvents=true" class="noticebar-item" ng-repeat="event in events">
                                <span class="noticebar-item-date">{{event.date | date:"dd/MM/yyyy HH:mm"}}</span>
                                <span class="noticebar-item-tag">{{event.tag}}</span>
                                <span class="noticebar-item-msg">{{event.msg}}</span>

                            </a>
                        </li>
                    </ul>
                </li>
            </shiro:hasRole>
            <g:if test="${!UEPInstance}">
            <li>
                <g:if test="${params.view != "card"}">
                    <a href="${createLink(controller:controllerName, action:actionName, id: params.id, params: [view:'card'])}" class="f-grafico"><span class="sr-only">Modo cartões</span></a>
                </g:if>
                <g:else>
                    <a href="${createLink(controller: controllerName, action: actionName, id: params.id, params: [view:'chart'])}" class="f-grafico ativo "><span class="sr-only">Modo gráficos</span></a>
                </g:else>
            </li>
            <li class="hidden-xs"><a href="#" onclick="toggleFullScreen()" class="f-tv" id="tv-toggle"><span class="sr-only">Modo TV</span></a></li>
            <li class="dropdown"><a href="#" class="dropdown-toggle f-filtro ativo" data-toggle="dropdown"><span class="sr-only">Filtro</span></a>
                <ul class="dropdown-menu filtro-mobile" role="menu">
                    <li><a href="${createLink(controller: 'home', action: 'uo', params: (params.view == 'card' ? [view:'card'] : ''))}" class="filtro ${((filterKey == "uo") && (!filterValue)) || ((!filterKey) && (!filterValue)) ? 'ativo' : ''}">Todas UOs</a></li>
                    <g:each var="uo" in="${uos}">
                        <li><a href="${createLink(controller: 'home', action: 'uo', id: uo, params: (params.view == 'card' ? [view:'card'] : ''))}" class="filtro ${filterValue == uo ? 'ativo' : ''}">${uo}</a></li>
                    </g:each>
                    <li><a href="${createLink(controller: 'home', action: 'department', params: (params.view == 'card' ? [view:'card'] : ''))}" class="filtro ${((filterKey == "department") && (!filterValue)) ? 'ativo' : ''}">
                    Todas GEs</a></li>
                    <g:each var="department" in="${departments}">
                        <li><a href="${createLink(controller: 'home', action: 'department', id: department, params: (params.view == 'card' ? [view:'card'] : ''))}" class="filtro ${filterValue == department ? 'ativo' : ''}">${department}</a></li>
                    </g:each>
                </ul>
            </li>
            </g:if>
            <li class="hidden-xs-360"><a href="#" ng-click="toggleWI100()" class="f-wi-100" ng-class="{'ativo': wi100}" id="wi-100-toggle"><span class="sr-only">WI 100%</span></a></li>
            <!--li class="hidden-xs"><a href="#" class="f-config"><span class="sr-only">Configurações</span></a></li-->
        </ul>
				<div class="logo-petrobras pull-right" title="Logo Petrobras"></div>
                <g:set var="permissions" value="${grailsApplication.config.app.role.admin}"/>
				<shiro:hasRole in="${[grailsApplication.config.app.role.admin,grailsApplication.config.app.role.manager]}">
					<div class="usuario pull-right">
                        <a href="${createLink(controller: 'admin', action: 'index')}">
                            <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                            <span class="hidden-sm hidden-xs">Administração</span>
                            <span class="visible-sm-inline visible-xs-inline">Adm.</span>
                        </a>
                    </div>
				</shiro:hasRole>
        <div class="hidden-xs tit-data" ng-show="newestDataDate != null" ><span  class="online-mark" ng-class="{'on': online, 'animate': initialLoading}"></span>{{newestDataDate | date:"HH:mm:ss"}}</div>
				<!--div class="usuario pull-right"><greetedUser:showName/></div-->
    </div>
    <!-- fim do header -->

    <g:layoutBody/>

    <!-- footer -->
    <div class="container-fluid">
        <div class="rodape">Copyright &copy; Petrobras. Todos os direitos reservados.<div class="pull-right">* Todos os valores em barris por dia.</div></div>
    </div>
    <!-- fim do footer -->

    </body>
</html>
