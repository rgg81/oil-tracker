<%@ page import="br.com.petrobras.paap.ProcessStatus" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'processStatus.label', default: 'ProcessStatus')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body class="admin">

		<nav class="navbar navbar-default">
			<div class="container-fluid" role="navigation">
				<ul class="nav navbar-nav">
					<li><g:link class="list" action="index" namespace="admin"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
					<li><g:link class="chart" action="chart" namespace="admin"><g:message code="processStatus.chart.label" /></g:link></li>
				</ul>
			</div>
		</nav>
		<div class="admin-title">
			<h2><g:message code="processStatus.chart.label" args="[entityName]" /></h2>
		</div>
		<div id="list-processStatus" class="content scaffold-list" role="main">
			<div class="">
				<div id="chart"></div>
				<button name="older" id="btn-older" class="btn">Older</button>
				<button name="newer" id="btn-newer" class="btn">Newer</button>
			</div>
		</div>

		<g:javascript>
			var chart = null ;
			var currPage = 1 ;

			var nextColor = 0 ;
			var colors = ["#DDDF0D", "#55BF3B", "#DF5353", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
				"#55BF3B", "#DF5353", "#7798BF", "#aaeeee"] ;
			var colorByType = {} ;

			function getPage(page) {
				if(page > 0) {
					currPage = page ;
					chart.showLoading() ;
					$.getJSON( "${createLink(action: 'allStatus', namespace: 'admin')}", {page: page}, function( data ) {
						// Remove series
						while(chart.series.length > 0) chart.series[0].remove(true);

						// Add new series
						for(var type in data) {
							var values = data[type] ;

							var color = colorByType[type] ;
							if(!color){
								color = colors[nextColor++] ;
								colorByType[type] = color ;
							}

							chart.addSeries({
								name:type,
								data: values,
								color:color
							} );
						}

						chart.hideLoading() ;
						chart.redraw() ;
					}) ;
				}
			}

			$("#btn-older").click(function(){
				getPage(currPage+1);
			});
			$("#btn-newer").click(function(){
				getPage(currPage-1);
			});


			$(document).ready(function() {
				Highcharts.setOptions({
					global: {
						useUTC: false
					}
				});

				var options = {
					chart: {
						renderTo: 'chart',
						zoomType: 'xy',
						height: 600
					},
					legend: {
						enabled: true,
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'middle'
					},
					credits: false,
					title: {
						text: 'Process Status'
					},
					yAxis: {
						title: {
							text: 'Duração (ms)'
						},
						gridLineWidth: 1,
						min: 0
					},
					xAxis: {
						type: 'datetime',
						gridLineWidth: 1,
						title: {
							text: null
						}
					},
					series: [{
						data: [],
						allowPointSelect: true,
						lineWidth: 1,
						id: 'primary'
					}]
				} ;

				chart = new Highcharts.Chart(options);

				getPage(1) ;
			});

		</g:javascript>
	</body>
</html>
