<%@ page import="br.com.petrobras.paap.ProcessStatus" %>
<!DOCTYPE html>
<html xmlns:g="http://www.w3.org/1999/html">
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'processStatus.label', default: 'ProcessStatus')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body class="admin">

		<nav class="navbar navbar-default">
			<div class="container-fluid" role="navigation">
				<ul class="nav navbar-nav">
					<li><g:link class="list" action="index" namespace="admin"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
					<li><g:link class="chart" action="chart" namespace="admin"><g:message code="processStatus.chart.label" /></g:link></li>
				</ul>
			</div>
		</nav>
		<div class="admin-title">
			<h2><g:message code="default.list.label" args="[entityName]" /></h2>
		</div>

		<div id="list-processStatus" class="content scaffold-list" role="main">
			<div class="container">
			<div id="list-tagPI" class="col-md-12" role="main">

				<!-- Average of times -->
				<a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapse-avg-times" aria-expanded="false" aria-controls="collapse-avg-times">
					Show average time by type
				</a>
				<div class="collapse" id="collapse-avg-times">
					<h3>Average time by type</h3>
					<table class="table">
						<tr>
							<th>Class</th>
							<th>Average Time</th>
						</tr>
						<g:each in="${meanTimePerType.sort{-it.value}}" var="meanTime">
							<tr>
								<td>
									<g:link class="list" action="index" namespace="admin" params="${[type: meanTime.key]}">
										${meanTime.key.substring(meanTime.key.lastIndexOf('.')+1)}
									</g:link>
								</td>
								<td style="text-align: right">
									<g:formatNumber number="${meanTime.value}" format="#,###.0" /> ms
								</td>
							</tr>
						</g:each>
					</table>
				</div>

			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>

			<!-- Content -->
			<table class="table">
			<thead>
					<tr>
					
						<g:sortableColumn property="startTime" title="${message(code: 'processStatus.startTime.label', default: 'Start Time')}" />
						<g:sortableColumn property="endTime" title="${message(code: 'processStatus.endTime.label', default: 'End Time')}" />
						<g:sortableColumn property="elapsedTime" title="${message(code: 'processStatus.elapsedTime.label', default: 'Duration')}" />
						<th>% of average</th>
						<g:sortableColumn property="type" title="${message(code: 'processStatus.type.label', default: 'Type')}" />
						<g:sortableColumn property="status" title="${message(code: 'processStatus.status.label', default: 'Status')}" />
						<g:sortableColumn property="msg" title="${message(code: 'processStatus.msg.label', default: 'Msg')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${processStatusInstanceList}" status="i" var="processStatusInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${fieldValue(bean: processStatusInstance, field: "formattedStartTime")}</td>
						<td>
							<g:if test="${processStatusInstance.endTime > 0}">
								${fieldValue(bean: processStatusInstance, field: "formattedEndTime")}
							</g:if>
							<g:else>
								N/A
							</g:else>
						</td>
						<td style="text-align: right">
						<g:if test="${processStatusInstance.endTime > 0}">
							${fieldValue(bean: processStatusInstance, field: "elapsedTime")}ms
							<g:set var="percentOfMean" value="${processStatusInstance.elapsedTime * 100 / meanTimePerType[processStatusInstance.type]}" />
							<g:if test="${percentOfMean < 50}">
								<g:set var="valueColor" value="#76FFFF" />
							</g:if>
							<g:elseif test="${percentOfMean < 120}">
								<g:set var="valueColor" value="#AEFAAE" />
							</g:elseif>
							<g:elseif test="${percentOfMean < 180}">
								<g:set var="valueColor" value="#FFFF93" />
							</g:elseif>
							<g:else>
								<g:set var="valueColor" value="#FF8A8A" />
							</g:else>
						</g:if>
						<g:else>
							${fieldValue(bean: processStatusInstance, field: "formattedDuration")}
						</g:else>
						</td>
						<td style="background-color: ${valueColor}; text-align: right">
							<g:if test="${percentOfMean}">
								<g:formatNumber number="${percentOfMean}" format="#" />%
							</g:if>
							<g:else>
								N/A
							</g:else>
						</td>
						<td>
							<g:if test="${params.type}">
								${processStatusInstance.type.substring(processStatusInstance.type.lastIndexOf('.')+1)}
							</g:if>
							<g:else>
								<g:link class="list" action="index" namespace="admin" params="${[type: processStatusInstance.type]}">
									${processStatusInstance.type.substring(processStatusInstance.type.lastIndexOf('.')+1)}
								</g:link>
							</g:else>
						</td>
						<td>${fieldValue(bean: processStatusInstance, field: "status")}</td>
						<td>${fieldValue(bean: processStatusInstance, field: "msg")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${processStatusInstanceCount ?: 0}" namespace="admin" params="${[type: params.type]}" />
			</div>
		</div>
		</div>
	</body>
</html>
