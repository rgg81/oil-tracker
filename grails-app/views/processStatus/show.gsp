
<%@ page import="br.com.petrobras.paap.ProcessStatus" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'processStatus.label', default: 'ProcessStatus')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-processStatus" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index" namespace="admin"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create" namespace="admin"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-processStatus" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list processStatus">
			
				<g:if test="${processStatusInstance?.startTime}">
				<li class="fieldcontain">
					<span id="startTime-label" class="property-label"><g:message code="processStatus.startTime.label" default="Start Time" /></span>
					
						<span class="property-value" aria-labelledby="startTime-label"><g:fieldValue bean="${processStatusInstance}" field="startTime"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${processStatusInstance?.endTime}">
				<li class="fieldcontain">
					<span id="endTime-label" class="property-label"><g:message code="processStatus.endTime.label" default="End Time" /></span>
					
						<span class="property-value" aria-labelledby="endTime-label"><g:fieldValue bean="${processStatusInstance}" field="endTime"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${processStatusInstance?.type}">
				<li class="fieldcontain">
					<span id="type-label" class="property-label"><g:message code="processStatus.type.label" default="Type" /></span>
					
						<span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${processStatusInstance}" field="type"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${processStatusInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="processStatus.status.label" default="Status" /></span>
					
						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${processStatusInstance}" field="status"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${processStatusInstance?.msg}">
				<li class="fieldcontain">
					<span id="msg-label" class="property-label"><g:message code="processStatus.msg.label" default="Msg" /></span>
					
						<span class="property-value" aria-labelledby="msg-label"><g:fieldValue bean="${processStatusInstance}" field="msg"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:processStatusInstance, action:'delete', namespace: 'admin']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${processStatusInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
