<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main">
  <title>Painel Online de Produção</title>
  <asset:javascript src="home-controllers.js"/>
</head>
<body>


    <!-- conteudo -->
    <div class="container-fluid separador" ng-controller="webSocketController" ng-init="init()" >

        <div class="fundo-sse container-fluid separador">
            <div class="tit-area-mobile hidden-sm hidden-md hidden-lg">${total != null ? total : "Total"}</div>
            <div class="hidden-xs col-sm-2 col-md-1 col-lg-1"><p class="sse">${total != null ? total : "Total"}</p></div>
            <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 text-center">
                <span class="tit-preliminar">{{sumOfAllPreliminary() | loadingNumber : 0}}</span>
                <p class="tit-label" ng-show="sitopDate">preliminar<span class="hidden-xs"> em {{bdoDate | date:"dd/MM/yyyy"}}</span></p>
            </div>
            <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 text-center">
                <span class="tit-previsto">{{sumOfAllExpected() | loadingNumber : 0}}</span>
                <p class="tit-label" ng-show="sitopDate">previsto<span class="hidden-xs"> em {{sitopDate | date:"dd/MM/yyyy"}}</span></p>
            </div>
            <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 text-center">
                <span class="tit-vazao">{{sum(sumBy)  | loadingNumber : 0}}</span>
                <p class="tit-label">vazão<span class="hidden-xs"> instantânea</span></p>
            </div>
            <div class="hidden-xs hidden-sm col-md-2 col-lg-2 text-center">
                <p class="tit-data">{{newestDataDate | date:"HH:mm:ss"}}</p>
                <p class="tit-status"><span class="online-mark" ng-class="{'on': online}"></span><span class="hidden-xs">{{online?'online':'offline'}}</span></p>
            </div>
            <div class="fechar"><a href="#" onclick="exitFullScreen()"><img src="/painelep/assets/ico-fechar.svg" alt="Fechar Modo TV" /></a></div>
        </div>

        <g:each var="uepsUo" in="${result}">
            <g:set var="uoName" value="${uepsUo.name}" />
            <g:set var="ueps" value="${uepsUo.values}" />
            <!-- div class="fundo-uo" -->
                <div class="container-fluid separador tit-uo-mobile nomargin hidden-sm hidden-md hidden-lg">${uoName}</div>
                <div class="fundo-uo container-fluid separador">
                    <div class="hidden-xs col-sm-2 col-md-1 col-lg-1"><p class="sse">${uoName}</p></div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 text-center"><span class="tit-preliminar">{{sumOfPreliminaryBy["${uoName}"] | loadingNumber : 0}}</span><p class="tit-label hidden-xs" ng-show="sitopDate">preliminar em {{bdoDate | date:"dd/MM/yyyy"}}</p></div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 text-center"><span class="tit-previsto">{{sumOfExpectedBy["${uoName}"] | loadingNumber : 0}}</span><p class="tit-label hidden-xs" ng-show="sitopDate">previsto em {{sitopDate | date:"dd/MM/yyyy"}}</p></div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 text-center"><span class="tit-vazao">{{sumBy["${uoName}"] ? sumBy["${uoName}"] : lastSumBy["${uoName}"] | loadingNumber : 0}}</span><p class="tit-label hidden-xs">vazão instantânea</p></div>
                    <div class="hidden-xs hidden-sm col-md-2 col-lg-2"></div>
                </div>
                <div class="fundo-miolo container-fluid separador">
                    <g:if test="${!(params.view == "card")}">
                        <div class="slider-wrapper">
                            <div class="slider" id="${uoName}">
                                <g:each var="uep" in="${ueps}">
                                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 chart ">
                                        <div ng-controller="chartController" ng-init="init(${uep.tag.id}, ${uep.id},'${uep.name}', '${uoName}', false)" class="grafico-uep">
                                            <div class="header-uep">
                                                <div class="tit-uep pull-left">
                                                    <a href="${createLink(controller: 'detail', action: 'show', id: uep.id)}">${uep.name}</a>
                                                </div>

                                                <div class="dropdown pull-right">
                                                    <a href="#" class="dropdown-toggle config-uep pull-right" data-toggle="dropdown"></a>
                                                    <ul class="dropdown-menu noticebar-menu" role="menu">
                                                        <li>
                                                            <a href="${createLink(controller: 'UEP', action: 'show', id: uep.id)}" class="noticebar-item">Configurar</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="valor-uep">
                                                    {{lastReceivedValue | loadingNumber : 0}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </div>
                            <div class="slider-indicator">
                            </div>
                        </div>
                    </g:if>
                    <g:else>
                        <g:each var="uep" in="${ueps}">
                            <div class="card" ng-controller="cardController" ng-init="init(${uep.tag.id}, ${uep.id}, '${uoName}')">
                                    <div class="card-uep"><a href="${createLink(controller: 'detail', action: 'show', id: uep.id)}">${uep.name}</a></div>
                                    <div class="card-previsto">{{expected | loadingNumber : 0}}</div><div class="card-vazao">{{lastReceivedValue | loadingNumber : 0}}</div>

                            </div>
                        </g:each>
                    </g:else>


                    <!--<g:each var="uep" in="${ueps}">
            
                    </g:each>-->
                </div>
            <!-- /div -->
        </g:each>
    </div>
    <!-- fim do conteudo -->
</body>
</html>