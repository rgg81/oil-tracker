<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Painel Online de Produção</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>		
        <style>
        @media screen and (min-width: 767px) {
            .login-container {
                text-align: right;
                float: right;  
                font-size: 3em;              
            }
            .container.login-main {
                margin-top: 25vh;
            }
            .login-form {
                float: right;
            }
            .error, .info {
                text-align: center;
                color: red;
                font-size: 0.9em;
                padding-bottom: 1em;
            }
            .info { 
                color: yellow;
            }

        }
        @media screen and (min-width: 320px) and (max-width: 767px) {
             @-ms-viewport { width: 320px; }
            .login-container {
                font-size: 3em;
                text-align: center;
            }
            .login {
                display: table;
                margin: 0 auto;
            }
            .login-form {
                max-width: 300px;
                margin: 0 auto;
            }
            .error, .info {
                text-align: center;
                color: red;
                font-size: 0.8em;
                padding-bottom: 1em;
            }
            .info { 
                color: yellow;
            }

        }
        </style>
	</head>
	<body>
    <!-- header -->
    <div class="container-fluid topo" style="margin-bottom: 2em">

        <div class="logo-petrobras pull-right" title="Logo Petrobras"></div>

    </div>
    <!-- fim do header -->
    <div class="container login-main">
        <div class="col-md-8 login"  >
                <div class="login-container"><h1><a href="/painelep">Painel Online de<br> Produção</a></h1></div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Não tem autorização para acessar o sistema.</p>
            </div>
        </div>
        </div>

    </div>
    

    </body>
</html>
