<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main">
  <title>Painel Online de Produção</title>
  <asset:javascript src="home-controllers.js"/>
</head>
<body>


    <!-- conteudo -->
    <div class="container-fluid separador" ng-controller="webSocketController" ng-init="init('${result*.values*.id.flatten().join(',')}', ${params.view == 'card'})" >

        <div class="fundo-sse container-fluid separador">
            <div class="tit-area-mobile hidden-sm hidden-md hidden-lg">${total != null ? total : "E&P-SSE"}</div>
            <div class="hidden-xs col-sm-2 col-md-2 col-lg-2"><p class="sse">${total != null ? total : "E&P-SSE"}</p></div>
            <div ng-cloak class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center">
                <span class="tit-preliminar">{{properties.get('preliminary') | loadingNumber : 0}}<!--span class="snapshot hidden-sm hidden-xs">{{properties.get('previousSnapshotValue') | loadingNumber : 0}}</span--></span>
                <p class="tit-label" ng-show="sitopDate">BDO<span class="hidden-xs"> (preliminar) - {{bdoDate | date:"dd/MM/yyyy"}}</span></p>
            </div>
            <div ng-cloak class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center">
                <span class="tit-previsto">{{properties.get('expected') | loadingNumber : 0}}<span class="snapshot hidden-sm hidden-xs">{{properties.get('currentSnapshotValue') | loadingNumber : 0}}</span></span>
                <p class="tit-label" ng-show="sitopDate">SITOP<span class="hidden-xs"> (previsto) - {{sitopDate | date:"dd/MM/yyyy"}}</span></p>
            </div>
            <div ng-cloak class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center">
                <span class="tit-24h">{{properties.get('avg24') | loadingNumber : 0}}</span>
                <p class="tit-label"><span class="hidden-xs">média </span>24h</p>
            </div>
            <div ng-cloak class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center">
                <span class="tit-vazao">{{properties.get('lastReceivedValue') | loadingNumber : 0}}</span>
                <p class="tit-label">vazão<span class="hidden-xs"> instantânea</span></p>
            </div>
            <div ng-cloak class="hidden-xs col-sm-2 col-md-2 col-lg-2 text-center">
                <div class="tit-data"><span class="online-mark" ng-class="{'on': online, 'animate': initialLoading}"></span>{{newestDataDate | date:"HH:mm:ss"}}</div>
            </div>
            <div class="fechar"><a href="#" onclick="exitFullScreen()"><img src="/painelep/assets/ico-fechar.svg" alt="Fechar Modo TV" /></a></div>
        </div>
        <g:each var="uepsUo" in="${result}">
            <g:set var="uoName" value="${uepsUo.name}" />
            <g:set var="ueps" value="${uepsUo.values}" />
            <div ng-controller="uoController">
                <div ng-init="uoName = '${uoName}'" class="container-fluid separador tit-uo-mobile nomargin hidden-sm hidden-md hidden-lg">${uoName}</div>
                <div class="fundo-uo container-fluid separador">
                    <div class="hidden-xs col-sm-2 col-md-2 col-lg-2"><p class="sse">${uoName}</p></div>
                    <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center"><span class="tit-preliminar badge">{{properties.get('preliminary') | loadingNumber : 0}}<!--span class="snapshot hidden-sm hidden-xs">{{properties.get('previousSnapshotValue') | loadingNumber : 0}}</span--></span><p class="tit-label hidden-xs">BDO (preliminar) <span ng-show="bdoDate"> - {{bdoDate | date:"dd/MM/yyyy"}}</span></p></div>
                    <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center"><span class="tit-previsto badge">{{properties.get('expected') | loadingNumber : 0}}<span class="snapshot hidden-sm hidden-xs">{{properties.get('currentSnapshotValue') | loadingNumber : 0}}</span></span><p class="tit-label hidden-xs">SITOP (previsto) <span ng-show="sitopDate"> - {{sitopDate | date:"dd/MM/yyyy"}}</span></p></div>
                    <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center"><span class="tit-24h badge">{{properties.get('avg24') | loadingNumber : 0}}</span><p class="tit-label hidden-xs">média 24h</p></div>
                    <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center"><span class="tit-vazao badge">{{properties.get('lastReceivedValue') | loadingNumber : 0}}</span><p class="tit-label hidden-xs">vazão instantânea</p></div>
                    <div class="hidden-xs col-sm-2 col-md-2 col-lg-2">
                        <shiro:hasRole name="${ grailsApplication.config.app.role.admin }">
                        <div class="dropdown pull-right">
                            <a href="#" class="dropdown-toggle config-uep pull-right" data-toggle="dropdown"></a>
                            <ul class="dropdown-menu noticebar-menu" role="menu">
                                <li>
                                    <a href="" data-toggle="modal" data-target="#modal${uepsUo.name}" class="noticebar-item">Recarregar 24h</a>
                                </li>
                            </ul>
                        </div>
                        <g:render template="../reload-uo-modal" model="['uepsUo': uepsUo]"/>
                        </shiro:hasRole>
                    </div>
                </div>
                <div class="fundo-miolo container-fluid separador">
                    <g:if test="${!(params.view == "card")}">
                        <div class="slider-wrapper">
                            <div class="slider" id="${uoName}">
                                <g:each var="uep" in="${ueps}" status="idx">
                                    <div ng-cloak class="col-xs-12 col-sm-4 col-md-3 col-lg-2 chart " data-order="${idx}" id="uepid${uep.id}">
                                        <div ng-controller="chartController" ng-class="[stoppedClass(),alertClass()]" ng-init="init(${uep.id},'${uep.name}', '${uoName}', false, false, ${uep.wiWithDefault()})" class="grafico-uep">
                                            <div class="header-uep">
                                                <span title="Vazão abaixo de {{cutoff}} barril/dia" ng-show="uepStopped()" class="pull-left glyphicon glyphicon-ban-circle stopped-uep"></span>
                                                <div class="tit-uep pull-left">                                                    
                                                    <a href="${createLink(controller: 'detail', action: 'show', id: uep.id)}">${uep.name}</a>
                                                </div>

                                                <shiro:hasAnyRole in="${[grailsApplication.config.app.role.admin,grailsApplication.config.app.role.manager]}">
                                                <div class="dropdown pull-right">
                                                    <a href="#" class="dropdown-toggle config-uep pull-right" data-toggle="dropdown"></a>
                                                    <ul class="dropdown-menu noticebar-menu" role="menu">
                                                        <li>
                                                            <shiro:hasRole name="${ grailsApplication.config.app.role.admin }">
                                                                <a href="${createLink(controller: 'UEP', action: 'show', namespace: 'admin',id: uep.id)}" class="noticebar-item">Configurar</a>
                                                            </shiro:hasRole>
                                                            <shiro:lacksRole name="${ grailsApplication.config.app.role.admin }">
                                                                <a href="${createLink(controller: 'TagPI', action: 'show', namespace: 'admin',id: uep.tag.id)}" class="noticebar-item">Configurar</a>
                                                            </shiro:lacksRole>
                                                        </li>
                                                    </ul>
                                                </div>
                                                </shiro:hasAnyRole>
                                                <div class="valor-uep">
                                                    {{properties.get('avg24') | loadingNumber : 0}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </div>
                            <div class="slider-indicator">
                                <g:each var="uep" in="${ueps}" status="idx">
                                    <div id="indicator-uepid${uep.id}"></div>
                                </g:each>
                            </div>
                        </div>
                    </g:if>
                    <g:else>
                        <g:each var="uep" in="${ueps}">
                            <div ng-cloak class="card" ng-class="[stoppedClass(),alertClass()]" ng-controller="cardController" ng-init="init(${uep.id}, '${uoName}', ${uep.wiWithDefault()})">
                                    <div class="card-uep">
                                        <span title="Vazão abaixo de {{cutoff}} barril/dia" ng-show="uepStopped()" class="glyphicon glyphicon-ban-circle stopped-uep"></span>
                                        <span ng-show="isReloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span><a href="${createLink(controller: 'detail', action: 'show', id: uep.id)}">${uep.name}</a></div>
                                    <div ng-hide="isReloading" class="card-preliminar">{{properties.get('preliminary') | loadingNumber : 0}}</div>
                                    <div ng-hide="isReloading" class="card-24h">{{properties.get('avg24') | loadingNumber : 0}}</div>
                                    <div ng-hide="isReloading" class="separador"></div>
                                    <div ng-hide="isReloading" class="card-previsto">{{properties.get('expected') | loadingNumber : 0}}</div>
                                    <div ng-hide="isReloading" class="card-vazao">{{properties.get('lastReceivedValue') | loadingNumber : 0}}</div>
                            </div>
                        </g:each>
                    </g:else>


                    <!--<g:each var="uep" in="${ueps}">
            
                    </g:each>-->
                </div>
            </div>
        </g:each>
        <div class="global-legend pull-right">* Todos os valores em barris por dia.</div>
    <!-- Modal de mensagem de erro -->
    <!-- Modal -->
        <div class="modal fade" id="error-message-modal" tabindex="-1" role="dialog" aria-labelledby="errorMessage">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="error-message-label"><span class="black-font">Offline</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info">
                            O sistema está offline. Isso pode ter acontecido devido a alguma atualização da aplicação.<br>
                            Tente recarregar a página para voltar ao normal.<br>
                            Se a aplicação não voltar ao normal abra um 881 para a mesa ME-N4-SITES_E_PORTAIS-IST-II-SP.<br>
                            Obrigado.
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" onclick="location.reload(true);" class="btn btn-primary">Recarregar página</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fim do conteudo -->

    <asset:javascript src="reload-uo.js"/>
</body>
</html>