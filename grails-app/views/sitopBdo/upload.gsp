<%@ page import="grails.converters.JSON" %>
<%@ page import="br.com.petrobras.paap.SitopBdo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'sitopBdo.label', default: 'SitopBdo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="default.new.label" args="[entityName]" /></h2>
		</div>
		<div class="container-fluid">
			<div class="col-md-9 admin-full-page">
				<h3><g:message code="default.upload.label" args="[entityName]" /></h3>
				<p class="admin-line"></p>
				<div id="upload-SitopBdo" role="main">
					<g:if test="${flash.error}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span> ${flash.error}
						</div>
					</g:if>

		            <g:uploadForm action="upload" namespace="admin" class="form-horizontal center-block col-sm-12">

						<div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'uo', 'error')} required form-group" >
							<label for="uo" class="col-sm-2">
								<g:message code="sitopBdo.sitopDate" default="SITOP Date" />
								<span class="required-indicator">*</span>
							</label>
							<div class="col-sm-4" id="uo-prefetch">
								<input name="date" value="<g:formatDate format='dd/MM/yyyy' date="${date}"/>" placeholder="dd/mm/yyyy" class="form-control"  />
							</div>
						</div>


						<div class="fieldcontain required form-group" >
							<label for="uo" class="col-sm-2">
								<g:message code="sitopBdo.file.label" default="File"/>
								<span class="required-indicator">*</span>
							</label>
							<div class="col-sm-8" id="uo-prefetch">
								<span class="btn btn-default btn-file">
									<g:message code="default.button.browse.label" default="Browse"/>
									<input type="file" name="myFile" onchange='$("#upload-file-info").html($(this).val().replace("C:\\fakepath\\", ""));' />
									<span class='label label-info' id="upload-file-info"></span>
								</span>
							</div>
						</div>

						<div class="alert alert-info" role="alert">
							O arquivo deve deve cumprir os seguintes requisitos:
							<ul>
								<li>Formato <strong>.xlsm</strong> ou <strong>.xlsx</strong>;</li>
								<li>Planilha com nome <strong>Relatório Final</strong>;</li>
								<li>Coluna <strong>C</strong>, com o nome da UEP;</li>
								<li>Coluna <strong>D</strong>, com o nome da UEP exatamente como cadastrado no POP;</li>
								<li>Coluna <strong>Q</strong>, com os dados do SITOP;</li>
								<li>Coluna <strong>AA</strong>, com os dados do BDO;</li>
								<li>Os dados serão lidos a partir da linha 4 até a coluna C conter uma célula vazia ou com conteúdo igual a "Total";</li>
								<li>A data do SITOP e BDO presente no arquivo será ignorada. Apenas a data do campo
									<strong><g:message code="SitopBdo.sitopDate" default="SITOP Date" /></strong> será considerada;</li>
								<li>A data do BDO será considerada como o dia anterior ao SITOP;</li>
							</ul>
						</div>

						<input type="submit" class="btn btn-primary" value="${message(code: 'default.button.upload.label', default: 'Upload')}" />

		            </g:uploadForm>
				</div>
			</div>
		</div>
	</body>
</html>
