
<%@ page import="br.com.petrobras.paap.SitopBdo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'sitopBdo.label', default: 'SitopBdo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="default.list.label" args="[entityName]" /></h2>
		</div>
		<div class="container-fluid">
			<div class="col-md-8 admin-full-page">
				<div id="list-sitopBdo" class="col-md-12" class="content scaffold-list" role="main">
					<g:if test="${flash.message}">
						<div class="alert alert-info" role="status">
							${flash.message}
						</div>
					</g:if>
					<table class="table">
					<thead>
							<tr>

								<th class="text-center"><g:message code="uep.department.label" default="UO" /></th>
								<th class="text-center"><g:message code="uep.uo.label" default="Department" /></th>
								<th class="text-center"><g:message code="sitopBdo.uep.label" default="UEP" /></th>
								<th class="text-center"><g:message code="sitopBdo.sitopDate.label" default="Sitop Date" /></th>
								<th class="text-center"><g:message code="sitopBdo.expected.label" default="Expected" /></th>
								<th class="text-center"><g:message code="sitopBdo.preliminary.label" default="Preliminary" /></th>
								<th class="text-center"><g:message code="snapshot24h.value.label" default="Snapshot 24h" /></th>

							</tr>
						</thead>
						<tbody>
						<g:each in="${sitopBdoInstanceList}" status="i" var="sitopBdoInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

								<td>${sitopBdoInstance.uepDepartment}</td>
								<td>${sitopBdoInstance.uepUo}</td>
								<td><g:link action="show" namespace="admin" controller="UEP" id="${sitopBdoInstance.uepId}">${sitopBdoInstance.uepName}</g:link></td>
								<td class="text-right"><g:formatDate date="${sitopBdoInstance.sitopDate}" format="dd/MM/yyyy" /></td>
								<td class="text-right"><g:formatNumber number="${sitopBdoInstance.expected}" format="###,##0.0" /></td>
								<td class="text-right"><g:formatNumber number="${sitopBdoInstance.preliminary}" format="###,##0.0" /></td>
								<td class="text-right"><g:formatNumber number="${sitopBdoInstance.snapshot24h}" format="###,##0.0" /></td>

							</tr>
						</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate total="${sitopBdoInstanceCount ?: 0}" max="20" namespace="admin" />
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
