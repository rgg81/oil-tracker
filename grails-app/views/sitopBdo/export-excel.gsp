<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
	</head>
	<body>
		<table>
		<thead>
				<tr>
					<th><g:message code="uep.department.label" default="UO" /></th>
					<th><g:message code="uep.uo.label" default="Department" /></th>
					<th><g:message code="sitopBdo.uep.label" default="UEP" /></th>
					<th><g:message code="sitopBdo.sitopDate.label" default="Sitop Date" /></th>
					<th><g:message code="sitopBdo.expected.label" default="Expected" /></th>
					<th><g:message code="sitopBdo.preliminary.label" default="Preliminary" /></th>
					<th><g:message code="snapshot24h.value.label" default="Snapshot 24h" /></th>
				</tr>
			</thead>
			<tbody>
			<g:each in="${sitopBdoInstanceList}" var="sitopBdoInstance">
				<tr>
					<td>${sitopBdoInstance.uepDepartment}</td>
					<td>${sitopBdoInstance.uepUo}</td>
					<td>${sitopBdoInstance.uepName}</td>
					<td><g:formatDate date="${sitopBdoInstance.sitopDate}" format="dd/MM/yyyy" /></td>
					<td><g:formatNumber number="${sitopBdoInstance.expected}" format="0.0" /></td>
					<td><g:formatNumber number="${sitopBdoInstance.preliminary}" format="0.0" /></td>
					<td><g:formatNumber number="${sitopBdoInstance.snapshot24h}" format="0.0" /></td>
				</tr>
			</g:each>
			</tbody>
		</table>
	</body>
</html>
