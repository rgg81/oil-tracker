<%@ page import="br.com.petrobras.paap.SitopBdo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'sitopBdo.label', default: 'SitopBdo')}" />
		<title><g:message code="default.list.fem.label" args="['SitopBdo']" /></title>
	</head>
	<body class="admin">
		<nav class="navbar navbar-default">
			<div class="container-fluid" role="navigation">
				<ul class="nav navbar-nav">
					<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
					<li><g:link class="list" action="index" namespace="admin"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				</ul>
			</div>
		</nav>
		<div class="container">
		<div id="list-sitopBdo" class="col-md-12" role="main">
			<h2><g:message code="default.create.label" args="[entityName]" /></h2>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message} </div>
			</g:if>
			<g:if test="${duplicates}">
				<div class="alert alert-danger" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					<span class="sr-only"><g:message code="default.error"/></span> <g:message code="sitopBdo.error.duplicated.ueps"/>
					<ul>
						<g:each var="dupe" in="${duplicates}">
							<li>${dupe.key}: <g:message code="sitopBdo.error.times.duplicated" args="[dupe.value]"/></li>
						</g:each>
					</ul>					
				</div>
			</g:if>
			<g:else>
				<div class="alert alert-info" role="alert">
				Planilha importada com sucesso
				${sitopBdos.findAll {it.uep !=null}.size()}
				<ul>
					<li>${sitopBdos.size()} UEPs na planilha</li>
					<li>${sitopBdos.findAll {it.uep != null}.size()} UEPs encontradas no sistema</li>
					<li>${sitopBdos.findAll {it.uep == null}.size()} UEPs não encontradas no sistema</li>
				</ul>
				</div>
			</g:else>	

			<g:form action="saveMultiple" namespace="admin">
			<table class="table">
			<thead>
					<tr>
						
						<th>#</th>

						<th><g:message code="sitopBdo.sheet.uep.label"/></th>

						<th><g:message code="sitopBdo.system.uep.label"/></th>

						<th><g:message code="default.date.label" args="['SITOP']"/></th>

						<th><g:message code="sitopBdo.expected.label"/></th>

						<th><g:message code="sitopBdo.preliminary.label"/></th>

					</tr>
				</thead>
				<tbody>
				<g:each in="${sitopBdos}" status="i" var="sitopBdoInstance">
					<tr class="${sitopBdos[i].uep == null ? 'warning' : duplicates[sitopBdos[i].uep.name] != null ? 'danger' : ''}">

						<td>${i+1}</td>

						<td>${sheetUepNames[i]}</td>

						<td>
							<g:hiddenField name="sitopBdos[$i].uep" value="${sitopBdoInstance.uep?.id}"/>
							<g:if test="${sitopBdos[i].uep}">
								${sitopBdos[i].uep}
							</g:if>
							<g:else>
								<g:message code="sitopBdo.warning.uepnotfound" />
							</g:else>
						</td>

						<td>
							<g:hiddenField name="sitopBdos[$i].sitopDate" value="${sitopBdoInstance.sitopDate.format('dd/MM/yyyy')}" />
							<g:formatDate format="dd/MM/yyyy" date="${sitopBdoInstance.sitopDate}"/>
						</td>

						<td style="text-align: right">
							<g:textField name="sitopdisplay" value="${fieldValue(bean: sitopBdoInstance, field: 'expected')}" class="text-right btn-disabled" disabled="true"/>
							<g:hiddenField name="sitopBdos[$i].expected" value="${fieldValue(bean: sitopBdoInstance, field: 'expected')}"/>
						</td>

						<td style="text-align: right">
							<g:textField name="bodisplay" value="${fieldValue(bean: sitopBdoInstance, field: 'preliminary')}" class="text-right" disabled="true"/>
							<g:hiddenField name="sitopBdos[$i].preliminary" value="${fieldValue(bean: sitopBdoInstance, field: 'preliminary')}"/>
						</td>

					</tr>
				</g:each>
				</tbody>
			</table>
				<g:if test="${!duplicates}">
					<g:actionSubmit class="btn btn-primary" action="saveMultiple" namespace="admin" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</g:if>
				<g:else>
					<button disabled="true" class="btn btn-disabled"><g:message code="sitopBdo.error.create.button.disabled"/></button>
				</g:else>
			</g:form>
		</div>
		</div>
	</body>
</html>
