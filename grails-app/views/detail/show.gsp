<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title>${UEPInstance.name}</title>
    <asset:javascript src="home-controllers.js"/>
</head>
<body>


<!-- conteudo -->
<div class="container-fluid separador detail" ng-controller="webSocketController" ng-init="init('${UEPInstance.id}', false)" >
    <div ng-controller="chartController" ng-init="init( ${UEPInstance.id},'${UEPInstance.name}', '${UEPInstance.uo}', true,true,${UEPInstance.wiWithDefault()})" >
        <div class="fundo-uo container-fluid separador">
            <div class="col-xs-3 text-center">
                <span class="tit-preliminar badge">{{properties.get('preliminary') | loadingNumber : 0}}</span>
                <p class="tit-label hidden-xs">BDO (preliminar) <span ng-show="bdoDate"> - {{bdoDate | date:"dd/MM/yyyy"}}</span></p>
            </div>
            <div class="col-xs-3 text-center">
                <span class="tit-previsto badge">{{properties.get('expected') | loadingNumber : 0}}<span class="snapshot hidden-sm hidden-xs">{{properties.get('currentSnapshotValue') | loadingNumber : 0}}</span></span>
                <p class="tit-label hidden-xs">SITOP (previsto) <span ng-show="sitopDate"> - {{sitopDate | date:"dd/MM/yyyy"}}</span></p>
            </div>
            <div class="col-xs-3 text-center">
                <span class="tit-24h badge">{{properties.get('avg24') | loadingNumber : 0}}</span>
                <p class="tit-label hidden-xs">média 24h</p>
            </div>
            <div class="col-xs-3 text-center">
                <span class="tit-vazao badge">{{properties.get('lastReceivedValue') | loadingNumber : 0}}</span>
                <p class="tit-label hidden-xs">vazão instantânea</p>
            </div>
        </div>
        <div class="fundo-miolo container-fluid separador">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chart">
                <div class="grafico-uep" ng-class="alertClass()" >
                    <div class="header-uep">
                        <div class="tit-uep pull-left">
                            <span title="Vazão abaixo de {{cutoff}} barril/dia" ng-show="uepStopped()" class="glyphicon glyphicon-ban-circle stopped-uep hidden-sm hidden-md hidden-lg"></span>
                            ${UEPInstance.name}
                        </div>
                        <div class="dropdown pull-right">
                            <a href="#" class="dropdown-toggle config-uep pull-right" data-toggle="dropdown"></a>
                            <ul class="dropdown-menu noticebar-menu" role="menu">
                                <li class="noticebar-li-group"><a ng-click="changeTimeOption('24')" href="#" class="noticebar-item" ng-class="{ 'noticebar-active':(timeOption == '24') }">24h</a></li>
                                <li class="noticebar-li-group"><a ng-click="changeTimeOption('48')" href="#" class="noticebar-item" ng-class="{ 'noticebar-active':(timeOption == '48') }">48h</a></li>
                                <li><a ng-click="changeTimeOption('72')" href="#" class="noticebar-item" ng-class="{ 'noticebar-active':(timeOption == '72') }">72h</a></li>
                                <shiro:hasAnyRole in="${[grailsApplication.config.app.role.admin,grailsApplication.config.app.role.manager]}">
                                    <li>
                                    <shiro:hasRole name="${grailsApplication.config.app.role.admin}">
                                        <a href="${createLink(namespace:'admin',controller: 'UEP', action: 'show', id: UEPInstance.id)}" class="hidden-xs noticebar-item">configurar</a></li>
                                    </shiro:hasRole>
                                    <shiro:lacksRole name="${grailsApplication.config.app.role.admin}">
                                        <a href="${createLink(namespace:'admin',controller: 'TagPI', action: 'show', id: UEPInstance.tag.id)}" class="hidden-xs noticebar-item">configurar</a></li>
                                    </shiro:lacksRole>
                                </shiro:hasAnyRole>
                                <li><a ng-click="download()" href="#" class="noticebar-item">salvar imagem do gráfico</a></li>
                            </ul>
                        </div>
                        <div class="valor-uep">
                            {{properties.get('avg24') | loadingNumber : 0}}
                        </div>
                    </div>
                    <div class="chart-container"></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">   
                <a href="https://webep.ep.petrobras.com.br" target="_blank">
                  <g:img dir="images" file="webep.png" class="web-app-link"/>
                </a>         
                <a href="http://wlpprd001.ep.petrobras.com.br/GipWeb/gipPortal.portal?_nfpb=true&_nfxr=false&_pageLabel=gipPortal_portal_page_97&_nfls=false#wlp_gipPortal_portal_page_97" target="_blank">
                  <g:img dir="images" file="gip.png" class="gip-link"/>
                </a>
                <a href="http://www.un-bc.petrobras.com.br/aplicativo/li04-sitop/consulta/generica/frameConsGer.asp" target="_blank">
                  <g:img dir="images" file="sitop.png" class="gip-link"/>
                </a>
            </div>
        </div>

    </div>
</div>
<!-- fim do conteudo -->
</body>
</html>
