
<%@ page import="br.com.petrobras.paap.UEP" %>
<%@ page import="br.com.petrobras.paap.ReloadStatus" %>
<%@ page import="br.com.petrobras.paap.TagPIType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'UEP.label', default: 'UEP')}" />
		<title><g:message code="default.details.of.label" args="[UEPInstance.name]" /></title>
	</head>
	<body class="admin">
		<div class="admin-title">
			<g:if test="${UEPInstance?.name}">
			<h2><g:message code="default.details.of.fem.label" args="[entityName]" /></h2>
			</g:if>
		</div>
		<div id="show-UEP" class="container-fluid" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<div class="row">
				<div class="col-md-9 admin-full-page">
					<h3>${UEPInstance?.name}</h3>
					<table class="table">
					
						<g:if test="${UEPInstance?.name}">
						<tr>
							<td><span id="name-label" class="property-label bold"><g:message code="UEP.name.label" default="Name" /></span></td>
							
							<td><span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${UEPInstance}" field="name"/></span></td>					
						</tr>
						</g:if>
					
						<g:if test="${UEPInstance?.uo}">
						<tr>
							<td><span id="uo-label" class="property-label"><g:message code="UEP.uo.label" default="Uo" /></span></td>
							
							<td><span class="property-value" aria-labelledby="uo-label"><g:fieldValue bean="${UEPInstance}" field="uo"/></span></td>
							
						</tr>
						</g:if>
					
						<g:if test="${UEPInstance?.department}">
						<tr>
							<td><span id="department-label" class="property-label"><g:message code="UEP.department.label" default="Department" /></span></td>
							
							<td><span class="property-value" aria-labelledby="department-label"><g:fieldValue bean="${UEPInstance}" field="department"/></span></td>
							
						</tr>
						</g:if>

						<tr>
							<td><span id="asset-label" class="property-label"><g:message code="UEP.wi.label" default="WI" /></span></td>

							<td><span class="property-value" aria-labelledby="asset-label">
								<g:if test="${UEPInstance?.wi}">
									<g:fieldValue bean="${UEPInstance}" field="wi"/>%
								</g:if>
								<g:else>
									<i>null</i> (100%)
								</g:else>
							</span></td>

						</tr>

						<g:if test="${UEPInstance?.asset}">
						<tr>
							<td><span id="asset-label" class="property-label"><g:message code="UEP.asset.label" default="Asset" /></span></td>
							
							<td><span class="property-value" aria-labelledby="asset-label"><g:fieldValue bean="${UEPInstance}" field="asset"/></span></td>
							
						</tr>
						</g:if>
					
						<g:if test="${UEPInstance?.oceanCurrent}">
						<tr>
							<td><span id="oceanCurrent-label" class="property-label"><g:message code="UEP.oceanCurrent.label" default="Ocean Current" /></span></td>
							
							<td><span class="property-value" aria-labelledby="oceanCurrent-label"><g:fieldValue bean="${UEPInstance}" field="oceanCurrent"/></span></td>
							
						</tr>
						</g:if>
					
						<g:if test="${UEPInstance?.oilWell}">
						<tr>
							<td><span id="oilWell-label" class="property-label"><g:message code="UEP.oilWell.label" default="Oil Well" /></span></td>
							
							<td><span class="property-value" aria-labelledby="oilWell-label"><g:fieldValue bean="${UEPInstance}" field="oilWell"/></span></td>
							
						</tr>
						</g:if>
					
						<g:if test="${UEPInstance?.tag}">
						<tr>
							<td><span id="tag-label" class="property-label"><g:message code="UEP.tag.label" default="Tag" /></span></td>
							
							<td><span class="property-value" aria-labelledby="tag-label"><g:link controller="tagPI" action="show" id="${UEPInstance?.tag?.id}" namespace="admin">${UEPInstance?.tag?.name.encodeAsHTML()}</g:link></span></td>
							
						</tr>
						</g:if>


						<tr>
							<td>
								<span id="alert-label" class="property-label"><g:message code="UEP.alert.label" default="Alert" /></span>
							</td>

							<td>
								<span class="property-value" aria-labelledby="oilWell-label">
									<g:if test="${UEPInstance?.hasAlert()}">
										Short Window: <g:fieldValue bean="${UEPInstance}" field="shortWindow"/><br/>
										Long Window: <g:fieldValue bean="${UEPInstance}" field="longWindow"/><br/>
										Percent: <g:fieldValue bean="${UEPInstance}" field="alertPercent"/>%<br/>
									</g:if>
									<g:else>
										<em>No alert defined</em>
									</g:else>
								</span>
							</td>
						</tr>
					</table>

					<fieldset class="buttons form-buttons pull-right">
						<g:form url="[resource:UEPInstance, namespace:'admin', action:'delete']" method="DELETE">
							<g:link class="btn btn-primary" action="edit" namespace="admin" resource="${UEPInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>

							<g:actionSubmit class="btn btn-danger" action="delete" namespace="admin" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</g:form>
						<g:if test="${UEPInstance.tag.tagType == TagPIType.CALCULATED }">
						<g:if test="${UEPInstance.tag.reloading || UEPInstance.tag.reloadStatus == ReloadStatus.QUEUE}">
							<button class="btn btn-warning" type="button" ng-disabled="true"/>${
								UEPInstance.tag.reloadStatus == ReloadStatus.DONE ? 
								message(code: 'default.button.reloading.PROCESSING' ):
								message(code: 'default.button.reloading.' + UEPInstance.tag.reloadStatus )}</button>
						</g:if>
						<g:else>
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">${
								UEPInstance.tag.reloadStatus == ReloadStatus.FAILED ? 
								message(code: 'default.button.reloading.RETRY') : 
								message(code: 'default.button.reloading.' + UEPInstance.tag.reloadStatus )}</button>
						</g:else>
						</g:if>
					</fieldset>	
				</div>
			</div>
		</div>

		<g:render template="../reload-modal" model="['UEPInstance': UEPInstance]"/>

		<asset:javascript src="reload-uep-tag.js"/>
	</body>
</html>
