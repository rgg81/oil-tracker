<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'UEP.label', default: 'UEP')}" />
		<title><g:message code="default.new.fem.label" args="[entityName]" /></title>
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="default.new.fem.label" args="[entityName]" /></h2>
		</div>
		<div id="create-UEP" class="container-fluid" role="main">
			<div class="col-md-10 admin-full-page">
				<h3>Dados da UEP</h3>
				<p class="admin-line"></p>
				<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
				</g:if>
				<g:hasErrors bean="${UEPInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${UEPInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</g:hasErrors>
				<g:form url="[resource:UEPInstance, namespace:'admin', action:'save']" class="center-block col-sm-12" >
						<g:render template="form"/>
						<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</g:form>
			</div>
		</div>
	</body>
</html>
