<%@ page import="br.com.petrobras.paap.UEP" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'UEP.label', default: 'UEP')}" />
		<title><g:message code="default.edit.label" args="[UEPInstance?.name]" /></title>
	</head>
	<body class="admin">
		<div class="admin-title">
			<g:if test="${UEPInstance?.name}">
			<h2><g:message code="default.edit.label" args="[entityName]" /></h2>
			</g:if>
		</div>
		<div id="edit-UEP" class="container-fluid" role="main">
			<div class="col-md-10 admin-full-page">
				<h3>${UEPInstance?.name}</h3>
				<p class="admin-line"></p>
				<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
				</g:if>
				<g:hasErrors bean="${UEPInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${UEPInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</g:hasErrors>
				<g:form url="[resource:UEPInstance, namespace:'admin', action:'update']" method="PUT" class="center-block col-sm-12" >
					<g:hiddenField name="version" value="${UEPInstance?.version}" />
						<g:render template="form"/>
						<g:actionSubmit class="btn btn-primary" action="update" namespace="admin" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</g:form>
			</div>
		</div>
	</body>
</html>
