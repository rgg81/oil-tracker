
<%@ page import="br.com.petrobras.paap.UEP" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'UEP.label', default: 'UEP')}" />
		<title><g:message code="default.list.fem.label" args="['UEPs']" /></title>
	</head>
	<body class="admin">
		<div class="admin-title">
			<h2><g:message code="default.list.fem.label" args="['UEPs']" /></h2>
		</div>
		<div class="container-fluid">
		<div id="list-UEP" class="col-md-12" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
				<g:each in="${uos}" status="i" var="uo">
					<h3>${uo.name}</h3>
					<div class="row">
					<g:each in="${uo.values}" var="uep">
						<div class="col-md-1 admin-uep-card" title="${uep.name}">
							<g:link class="admin-uep-card-tag-link ellipsis" controller="TagPI" namespace="admin" action="show" id="${uep.tag.id}">
								<div class="admin-uep-card-name ellipsis">${uep.name}</div>
								<span class="admin-uep-card-department ellipsis">${uep.department}</span>
							</g:link>
							<g:link class="admin-uep-card-tag-link ellipsis" controller="TagPI" namespace="admin" action="show" id="${uep.tag.id}"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> ${uep.tag.name}</g:link>
						</div>
					</g:each>
					</div>
				</g:each>

		</div>
		</div>
	</body>
</html>
