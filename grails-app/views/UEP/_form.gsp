<%@ page import="br.com.petrobras.paap.UEP" %>

<div ng-controller="uepController" ng-init='init("preview-chart", "${UEPInstance?.shortWindow}", "${UEPInstance?.longWindow}", "${UEPInstance?.alertPercent}", "${UEPInstance?.tag?.id}")'>
    <div class="form-horizontal">
        <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'name', 'error')} required form-group">
            <label for="name" class="col-sm-2">
                <g:message code="UEP.name.label" default="Name"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-sm-10">
                <g:textField name="name" required="" class="form-control" value="${UEPInstance?.name}"/>
            </div>
        </div>

        <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'uo', 'error')} required form-group">
            <label for="uo" class="col-sm-2">
                <g:message code="UEP.uo.label" default="Uo"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-sm-10" id="uo-prefetch">
                <g:textField name="uo" required="" id="uo" class="form-control typeahead" value="${UEPInstance?.uo}" autocomplete autocomplete-url="/painelep/api/UEP/uos"/>
            </div>
        </div>

        <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'department', 'error')} required form-group">
            <label for="department" class="col-sm-2">
                <g:message code="UEP.department.label" default="Department"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-sm-10">
                <g:textField name="department" required="" id="department" class="form-control typeahead" value="${UEPInstance?.department}" autocomplete autocomplete-url="/painelep/api/UEP/departments"/>
            </div>
        </div>

        <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'wi', 'error')} required form-group">
            <label for="wi" class="col-sm-2">
                <g:message code="UEP.wi.label" default="WI (%)"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-sm-2">
                <g:textField name="wi" id="wi" class="form-control" value="${formatNumber(number:UEPInstance?.wi, maxFractionDigits:10)}"/>
            </div>
        </div>

        <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'asset', 'error')} required form-group">
            <label for="asset" class="col-sm-2">
                <g:message code="UEP.asset.label" default="Asset"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-sm-10">
                <g:textField name="asset" id="asset" required="" class="form-control" value="${UEPInstance?.asset}" autocomplete autocomplete-url="/painelep/api/UEP/assets"/>
            </div>
        </div>

        <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'oceanCurrent', 'error')} required form-group">
            <label for="oceanCurrent" class="col-sm-2">
                <g:message code="UEP.oceanCurrent.label" default="Ocean Current"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-sm-10">
                <g:textField name="oceanCurrent" id="oceanCurrent" required="" class="form-control" value="${UEPInstance?.oceanCurrent}" autocomplete autocomplete-url="/painelep/api/UEP/oceanCurrents"/>
            </div>
        </div>

        <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'oilWell', 'error')} required form-group">
            <label for="oilWell" class="col-sm-2">
                <g:message code="UEP.oilWell.label" default="Oil Well"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-sm-10">
                <g:textField name="oilWell" id="oilWell" required="" class="form-control" value="${UEPInstance?.oilWell}" autocomplete autocomplete-url="/painelep/api/UEP/oilWells"/>
            </div>

        </div>

        <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'tag', 'error')} required form-group">
            <label for="tag" class="col-sm-2">
                <g:message code="UEP.tag.label" default="Tag"/>
                <span class="required-indicator">*</span>
            </label>

            <div class="col-sm-10">
                <g:select id="tag" name="tag.id" from="${br.com.petrobras.paap.TagPI.findAllByTagType(br.com.petrobras.paap.TagPIType.CALCULATED,[sort:'name'])}" ng-model="tagId" ng-change="reloadGraph()"
                          noSelection="${['0':'Select One...']}" optionKey="id" optionValue="name" required="" value="${UEPInstance?.tag?.id}" class="form-control"/>
            </div>

        </div>
    </div>


    <!-- Filtros -> form inline -->
    <div class="panel panel-default">
        <div class="panel-heading"><label>Alerts</label></div>

        <div class="panel-body">
            <div class="form-inline">
              <div class="col-md-11">
                <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'shortWindow', 'error')} form-group col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <g:message code="UEP.shortWindow.label" default="Short window"/>
                        </div>
                        <g:field name="shortWindow" id="shortWindow" type="number" ng-model="shortWindow" ng-change="updateBands()" min="1"
                                 class="form-control" value="${UEPInstance?.shortWindow}" placeholder="Ex: 5"/>
                    </div>
                </div>
                <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'longWindow', 'error')} form-group col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <g:message code="UEP.longWindow.label" default="Long window"/>
                        </div>
                        <g:field name="longWindow" id="longWindow" type="number" ng-model="longWindow" ng-change="updateBands()" min="10" step="10"
                                 class="form-control" value="${UEPInstance?.longWindow}" placeholder="Ex: 120"/>
                    </div>
                </div>
                <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'alertPercent', 'error')} form-group col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <g:message code="UEP.alertPercent.label" default="Percent"/>
                        </div>
                        <g:field name="alertPercent" id="alertPercent" type="number" ng-model="alertPercent" ng-change="updateBands()" min="1"
                                 class="form-control" value="${UEPInstance?.alertPercent}" placeholder="Ex: 25"/>
                    </div>
                </div>
              </div>
              <div class="fieldcontain ${hasErrors(bean: UEPInstance, field: 'alertPercent', 'error')} form-group col-md-1">
                  <button type="button" class="btn" ng-click="clearAlert()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
              </div>
            </div>

            <div class="form-group">
                <div id="preview-chart">
                </div>
            </div>
        </div>
    </div>
</div>

<asset:javascript src="typeahead.js"/>
<asset:javascript src="typeahead-rest-directive.js"/>
<asset:javascript src="edit-uep.js"/>
