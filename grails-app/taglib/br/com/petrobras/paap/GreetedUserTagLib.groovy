package br.com.petrobras.paap
import org.apache.shiro.SecurityUtils

class GreetedUserTagLib {
    //static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "greetedUser"

    def showName = { attrs ->
    	def currentUser = SecurityUtils.subject
    	if (currentUser) out << currentUser?.principals?.oneByType(br.com.petrobras.security.model.User.class).shortName
    }

}
