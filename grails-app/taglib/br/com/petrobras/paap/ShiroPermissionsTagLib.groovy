package br.com.petrobras.paap
import org.apache.shiro.SecurityUtils

class ShiroPermissionsTagLib {
    static defaultEncodeAs = [taglib:'text']

    static namespace = "shiroPermissions"

    def shiroService

    def hasManagerAccess = { attrs,body ->
    	if (shiroService.hasManagerAccess(attrs.username)) 
    		out << body()
    }

}
