package br.com.petrobras.paap

class OnlineUsersTagLib {
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static namespace = "onlineUsers"

    def count = { attrs ->
    	out << OnlineUser.count()
    }
}
