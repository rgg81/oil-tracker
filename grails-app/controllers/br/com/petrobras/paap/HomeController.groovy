package br.com.petrobras.paap

import grails.converters.JSON
import javax.servlet.http.Cookie

class HomeController {
    def uepService
    def shiroService

    def index() {    	
    	def result = null
    	def id = PaapCookie.getCookie("filterValue")
        if (PaapCookie.getCookie("filterKey") == "department") { 
        	result = applyFilters("department","uo",id)
            if (result.result?.size() > 1) 
                result.result = reorderDepartments(result.result)   
        }
        else {
        	result = applyFilters("uo","department",id)
            if(id != null && result.result?.size() > 1){
                result.result = reorderDepartments(result.result)          
            }
        }

		// View default on mobile devices: card
		if (params.view == null && isMobile() ) {
			params.view = 'card'
		}

        def ueps = uepService.getUEPsWithPermissionForUser(shiroService.getLoggedUser())
        session["ueps"] = ueps
		session["isAdmin"] = shiroService.isAdmin()
    
    	render (view: "/index", model: result, params: params)
    }

    def uo(String id) {
		def result = applyFilters("uo","department",id)
		if (id != null && result.result?.size() > 1) 
			result.result = reorderDepartments(result.result)	    
    	render (view: "/index", model: result)
    }

    def department(String id) {
    	def result = applyFilters("department","uo",id)
    	if (result.result?.size() > 1) 
    		result.result = reorderDepartments(result.result)
    	render (view: "/index", model: result)
    }

    def clearCookies() {
    	PaapCookie.deleteCookie("filterKey")
    	PaapCookie.deleteCookie("filterValue")
    	PaapCookie.deleteCookie("groupKey")
    }

    def applyFilters(String filterKey, String groupKey, String filterValue) {
    	clearCookies()
    	def total = null
    	def result = null
    	def filters = [:]
    	def uos = uepService.getListOfRegisteredAttributes("uo")
        def departments = uepService.getListOfRegisteredAttributes("department")
        if (departments?.size() > 1)
        	departments = reorderDepartments(departments.collect { [name:it]}).collect {it.name}

        PaapCookie.setCookie("filterKey",filterKey)
        PaapCookie.setCookie("groupKey",groupKey)
        if (filterValue)
        	PaapCookie.setCookie("filterValue",filterValue)

   		filters["filterKey"] = filterKey 
    	filters["groupKey"] = groupKey

    	if (filterValue) {
    		if (uos.contains(filterValue) || departments.contains(filterValue)) {
	    		total = filterValue 
	    		filters["filterValue"] = filterValue
	    	} else {
	    		clearCookies()
	    		redirect(uri:"/")
	    	}
    	}
    	
    	result = uepService.filterUEP(filters)
    	[result: result, total: total, filterKey: filterKey, filterValue: filterValue, uos: uos.sort{UEP.getUOPosition(it)}, departments:departments ]
    }

    def reorderDepartments(departmentArray) {
    	def lastItem = departmentArray?.size()-1
	    def sseIndex = departmentArray.findIndexOf { it.name == UEP.getFirstDepartment() }
	    departmentArray[0,sseIndex] = departmentArray[sseIndex,0]
	    departmentArray[1..lastItem] = departmentArray[1..lastItem].sort{ it.name }
	    return departmentArray
    }


}
