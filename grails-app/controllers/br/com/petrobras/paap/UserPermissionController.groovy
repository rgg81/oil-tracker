package br.com.petrobras.paap

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import br.com.petrobras.security.exception.*
import grails.converters.*

@Transactional(readOnly = true)
class UserPermissionController {

    static allowedMethods = [save: "POST", delete: "DELETE"]
    def uepService
    def userPermissionService
    def shiroService

    static namespace = 'admin'

    def findUser() {
        render userPermissionService.findUser(params.username) as JSON
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def uos = uepService.getListOfRegisteredAttributes("uo")
        respond UserPermission.list(params), model:[userPermissionInstanceCount: UserPermission.count(), uos: uos]
    }

    def show(UserPermission userPermissionInstance) {
        respond userPermissionInstance
    }

    def edit(UserPermission perm) {
        def userNotFound = false
        try {
            def result = userPermissionService.findUser(params.username?.toUpperCase())
            def query = [filterValue: null, filterKey: null ]
            def uos = uepService.getAllUEPs(query)           
            if (result) {            
                render(view: "edit", model: [permissions:result, uos: uos])                
            } else
                userNotFound = true
        } catch (Exception e) {
            log.error("UEPController error:", e)
            userNotFound = true
        } finally {
            if (userNotFound) {
                def uos = uepService.getListOfRegisteredAttributes("uo")
                flash.error = message(code:"UserPermission.user.not.found")
                render(view: "index",model: [uos: uos])
            }
        }
    }

    def editAttribute() {
        def users = userPermissionService.findUsersByAttribute(params.attribute,params.value)
        render(view: "editAttribute", model: [attribute: params.attribute, value: params.value, users: users, usersJson: users as JSON, userCount:users.size()])
    }

    def byUEP() {
        def uep = null
        if (params.attribute == "name") {
            uep = UEP.findByName(params.value)
            if (!uep) {
                flash.error = message(code: 'UserPermission.uep.not.found', args: [params.value]).decodeHTML()
                def uos = uepService.getListOfRegisteredAttributes("uo")
                redirect (controller: "userPermission", action: "index")
            } 
        } else if (params.attribute == "id") {
            uep = UEP.get(params.value)
        }
        def users = userPermissionService.findUsersByAttribute("id",uep?.id?.toString())
        render(view: "byUEP", model: [displayText: uep?.name, attribute: "id", value: uep?.id, users: users, userCount:users.size()])
    }

    @Transactional
    def update() {  
        def userPermissions = userPermissionService.updateUserPermissions(params)
        def userRole = userPermissionService.updateUserRolePermissions(params.username)


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UserPermission.permission.label', default: 'UserPermission'), params.username])
                redirect (controller:"userPermission", action:"edit", params:[username: params.username], model:[permissions: userPermissions, access: userRole])
            }
        }
    }

    @Transactional
    def updateAttribute() {
        if (params.users instanceof String)
            params.users = [params.users]
        def removedUsers = userPermissionService.removeUserPermissionsBatch(params)
        request.withFormat {
            form multipartForm {
                if (removedUsers) {
                  flash.message = message(code: 'default.deleted.female.plural.message', args: [message(code: 'UserPermission.permissions.label', default: 'UserPermission')])
                  flash.users = removedUsers
                }
                def action = "editAttribute"
                def displayText = params.value
                if (params.attribute == "id") {
                    displayText = UEP.get(params.value)?.name
                    action = "byUEP"
                }
                redirect (controller:"userPermission", action:action, params:[displayText: displayText, attribute: params.attribute, value: params.value])
            }
        }
    }

    @Transactional
    def addPermission() {
        def returnMessage = null
        def error = null
        def result = userPermissionService.addPermission(params)
        def action = "editAttribute"
        def displayText = params.value
        if (params.attribute == "id") {
            displayText = UEP.get(params.value)?.name
            action = "byUEP"
        }
        if (result == null)
            error = message(code:message(code:"UserPermission.user.not.found"))
        else if (result.success == true)
            returnMessage = message(code:"UserPermission.permission.granted.user", args:[result.username,result.name]).decodeHTML()
        else if (result.success == false)
            error = message(code:"UserPermissions.error.user.already.has.permission", args:[result.username,result.name, displayText]).decodeHTML()
        request.withFormat {
            form multipartForm {
                flash.message = returnMessage
                flash.error = error                
                redirect (controller:"userPermission", action: action, params:[displayText: displayText, attribute: params.attribute, value: params.value])
            }
        }
    }

    def adminPermission() {
        def users = shiroService.admins()
        def admins = []
        users.each { username ->
            admins.add([username: username, name:shiroService.userDetails(username.toUpperCase()).name])
        }
        respond admins, model: [users: admins, usersJson: admins as JSON]
    }

    def managerPermission() {
        def users = shiroService.managers()
        def managers = []
        users.each { username ->
            managers.add([username: username, name:shiroService.userDetails(username.toUpperCase()).name])
        }
        respond managers, model: [users: managers, usersJson: managers as JSON]
    }

    @Transactional
    def addAdmin() {
        def username = params.username.toUpperCase()
        def error = null
        def returnMessage = null
        def result = userPermissionService.addAdmin(username)

        if (result) {
            if (result.adminAcess) 
                returnMessage = message(code:'UserPermission.admin.added', args: [result.name] ).decodeHTML()
            else if (!result.adminAcess)
                error = message(code:"UserPermission.admin.added.error")
        }
        else     
            error = message(code:"UserPermission.user.not.found")


        request.withFormat {
          form multipartForm {
              flash.message = returnMessage
              flash.error = error
              redirect (controller:"userPermission", action:"adminPermission", params:[attribute: params.attribute, value: params.value])
          }
        }
    }

    @Transactional
    def addManager() {
        def username = params.username.toUpperCase()
        def error = null
        def returnMessage = null
        def result = userPermissionService.addManager(username)

        if (result) {
            if (result.managerAcess) 
                returnMessage = message(code:'UserPermission.manager.added', args: [result.name] ).decodeHTML()
            else if (!result.managerAcess)
                error = message(code:"UserPermission.manager.added.error")
        }
        else     
            error = message(code:"UserPermission.user.not.found")


        request.withFormat {
          form multipartForm {
              flash.message = returnMessage
              flash.error = error
              redirect (controller:"userPermission", action:"managerPermission", params:[attribute: params.attribute, value: params.value])
          }
        }
    }

    @Transactional
    def removeAdmins() {
        def removedUsers = []
        if (params.users instanceof String)
            params.users = [params.users]
        
        removedUsers = userPermissionService.removeAdmins(params)

        request.withFormat {
            form multipartForm {
                if (removedUsers) {
                    flash.message = message(code: 'default.deleted.plural.message', args: [message(code: 'UserPermission.admin.label', default: 'Administrators')])
                    flash.users = removedUsers
                }
                redirect (controller:"userPermission", action:"adminPermission", params:[attribute: params.attribute, value: params.value])
            }
        }
    }

    @Transactional
    def removeManagers() {
        def removedUsers = []
        if (params.users instanceof String)
            params.users = [params.users]
        
        removedUsers = userPermissionService.removeManagers(params)

        request.withFormat {
            form multipartForm {
                if (removedUsers) {
                    flash.message = message(code: 'default.deleted.plural.message', args: [message(code: 'UserPermission.manager.label', default: 'Managers')])
                    flash.users = removedUsers
                }
                redirect (controller:"userPermission", action:"managerPermission", params:[attribute: params.attribute, value: params.value])
            }
        }
    }

    @Transactional
    def delete(UserPermission userPermissionInstance) {

        if (userPermissionInstance == null) {
            notFound()
            return
        }

        userPermissionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UserPermission.label', default: 'UserPermission'), userPermissionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userPermission.label', default: 'UserPermission'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
