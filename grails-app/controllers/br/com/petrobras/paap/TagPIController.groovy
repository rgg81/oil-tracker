package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesInterpolator
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.time.TimeCategory

import static org.springframework.http.HttpStatus.*

class TagPIController {
    ReadPIService readPIService
    def grailsApplication
    def tagPIService
    def uepService
    def PIDataService
    def processTagService
    def shiroService

    static namespace = 'admin'
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", reset:"PUT"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def tags = tagPIService.tagFilter("CALCULATED")
        respond TagPI.list(params), model:[tagPIInstanceCount: TagPI.count()]
    }

    def show(TagPI tagPIInstance) {
        if (!tagPIService.hasAccessToTag(tagPIInstance)) {
            redirect(url: g.createLink(uri:'/unauthorized'));
            return 
        }
        else {
            // Tenta pegar a tag por nome, caso o 'default' venha null
            if (tagPIInstance == null && params.id) {
                tagPIInstance = TagPI.findByNameOrFormula(params.id, params.id)
            }
            def reloading = tagPIInstance ? [reloading:tagPIService.isReloading(tagPIInstance)] : []

            if (!shiroService.isAdmin() && tagPIInstance)
                respond tagPIInstance, view: "showFormula"
            respond tagPIInstance, model: [tagFormula: formulaWithLinks(tagPIInstance)] + reloading
        }      
    }

    def formulaWithLinks(tagPIInstance){
        if(tagPIInstance?.tagType == TagPIType.CALCULATED) {
            def currFormula = tagPIInstance?.formula

            if (currFormula) {
                TagPI.findAllByTagTypeOrTagTypeOrTagType(TagPIType.SNAPSHOT,TagPIType.ACCUMULATOR,TagPIType.SPACED_SNAPSHOT).each{ tag ->
                    currFormula = currFormula.replaceAll("'"+tag.name+"'", "<a href='${tag.id}'>'${tag.name}'</a>")
                }
            }
            currFormula
        } else {
            tagPIInstance?.formula
        }
    }

    def create() {
        respond new TagPI(params)
    }




    def save(TagPI tagPIInstance) {
        //println params
        if (tagPIInstance == null) {
            notFound()
            return
        }

        if(tagPIInstance.tagType == TagPIType.CALCULATED){
            tagPIInstance.tagLocation = null
        }

        if (!tagPIInstance.hasErrors()) {
            def result = validation(params)
            if (result['error']) {
                flash.error = true
                flash.message = result.error
                respond tagPIInstance.errors, view:'create'
                return
            }
        }

        try {
            tagPIService.update(tagPIInstance, params?.filtersSpec)
        }
        catch(Exception e) {
            respond tagPIInstance.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tagPI.label', default: 'TagPI'), tagPIInstance.id])
                redirect tagPIInstance
            }
            '*' { respond tagPIInstance, [status: CREATED] }
        }
    }

    def edit(TagPI tagPIInstance) {
        if (!tagPIService.hasAccessToTag(tagPIInstance)) 
            redirect(url: g.createLink(uri:'/unauthorized')); 
        else if (!shiroService.isAdmin())
            respond tagPIInstance, view: "editFormula"
        else
            respond tagPIInstance
    }

    def updateFormula(Long tagId) {        
        def formula = params.formula
        def tagPIInstance = TagPI.get(tagId)
        if (tagPIInstance == null) {
            notFound()
            return
        }
        else if (!tagPIService.hasAccessToTag(tagPIInstance)) 
            redirect(url: g.createLink(uri:'/unauthorized')); 
        else {
            tagPIInstance.formula = formula
            
            try {
                tagPIService.update(tagPIInstance, params?.filtersSpec)
            }
            catch(Exception e) {
                flash.error = true
                respond tagPIInstance.errors, view:'editFormula'                
            }

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'TagPI.label', default: 'TagPI'), tagPIInstance.id])
                    redirect tagPIInstance                    
                }
                '*'{ respond tagPIInstance, [status: OK] }
            }
        }

    }

    def update(TagPI tagPIInstance) {
        if (tagPIInstance == null) {
            notFound()
            return
        }

        if(tagPIInstance.tagType == TagPIType.CALCULATED){
            tagPIInstance.tagLocation = null
        }

        if (!tagPIInstance.hasErrors()) {
            def result = validation(params)
            if (!(result instanceof ArrayList) && result['error']) {
                flash.error = true
                flash.message = result.error
                respond tagPIInstance.errors, view:'edit'
                return
            }
        }

        try {
            tagPIService.update(tagPIInstance, params?.filtersSpec)
        }
        catch(Exception e) {
            respond tagPIInstance.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TagPI.label', default: 'TagPI'), tagPIInstance.id])
                redirect tagPIInstance
            }
            '*'{ respond tagPIInstance, [status: OK] }
        }
    }

    def delete(TagPI tagPIInstance) {

        if (tagPIInstance == null) {
            notFound()
            return
        }

        try {
            tagPIService.delete(tagPIInstance)
        }catch(Exception e) {
            log.error("TagPIController error", e)
            if(e.cause.class == TagPIReferenceException) {
                
                tagPIInstance = TagPI.get(tagPIInstance.id)  //doc https://github.com/grails/grails-doc/commit/253a59050e12b69b84084e985f28e3a0eca39d62
                def reloading = tagPIInstance ? [reloading:tagPIService.isReloading(tagPIInstance)] : []
                render view:'show', model:[errorcode:'tag.reference.error', tagPIInstance:tagPIInstance , tagFormula: formulaWithLinks(tagPIInstance)] + reloading

            }else{

                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'TagPI.label', default: 'TagPI'), tagPIInstance.id])
                redirect action:'show', method:"GET", id:tagPIInstance.id

            }
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TagPI.label', default: 'TagPI'), tagPIInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tagPI.label', default: 'TagPI'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    protected def validation(params) {
        def formula = params.formula
        def tagType = TagPIType.valueOf(params.tagType)
        def tagLocation = params.tagLocation ? TagPILocation.valueOf(params.tagLocation) : null
        def dateParam = params.date ? Date.parse('yyyy-MM-dd', "${params.date}") : new Date()
        def result = [:]
        try {
            def aDayInMs = TimeCategory.getDay(1).toMilliseconds()
            // Adiciona +1 dia ao parâmetro, para que o range final seja dentro do dia especificado
            dateParam = new Date(dateParam.getTime() + aDayInMs)

            def now = new Date()
            def toDate = dateParam.after(now) ? now : dateParam
            def fromDate = new Date(toDate.getTime() - TimeCategory.getDay(1).toMilliseconds())


            if(tagType == TagPIType.CALCULATED) {
                // Tags não calculadas
                def tagsPI = TagPI.findAllByTagTypeOrTagTypeOrTagType(TagPIType.SNAPSHOT,TagPIType.ACCUMULATOR,TagPIType.SPACED_SNAPSHOT)
                def usedTagsFormulas = tagsPI*.formula as Set

                // Evita uso de tags não presentes na fórmula
                usedTagsFormulas = usedTagsFormulas.findAll { formula.indexOf(it) >= 0 }
                tagsPI = tagsPI.findAll { usedTagsFormulas.contains(it.formula) }

                TagPIExpression expression = new TagPIExpressionBuilder(formula).variables(usedTagsFormulas).build()

                //Expression expression = new ExpressionBuilder(formula).variables(usedTagsFormulas).build()

                def samplesByTag = [:]
                tagsPI.each { tagPI ->
                    def tagSamples = Sample
                            .findAllByTagAndDateBetween(tagPI, fromDate, toDate, [sort: 'date'])
                            .collect { new TimeSeriesSample(time: it.date, value: it.value) }

                    samplesByTag[tagPI.formula] = new TimeSeriesInterpolator(tagSamples as Set)
                }

                result['samples'] = []
                def currentDate = fromDate.getTime()
                long interval = TimeCategory.getMinutes(2).toMilliseconds()
                while (currentDate < toDate.getTime()) {
                    usedTagsFormulas.each { String f ->
                        //TODO duplicated code!!
                        def interpolatedValue = samplesByTag[f].getSampleAt(new Date(currentDate))
                        expression.setVariable(f, interpolatedValue?: 0 as Double)
                    }

                    double value = expression.evaluate()

                    result['samples'] << [currentDate, value]

                    currentDate = currentDate + interval
                }
            } else {
                // Pega valores direto do PI
                def samples = readPIService.readDataFromTagInTimePeriodReturningError(formula, tagLocation, fromDate, toDate)

                if (tagType == TagPIType.ACCUMULATOR) {
                    samples = processTagService.transformAccumulatedSamples(samples,null)
                }
                result['samples'] = samples.collect{ [it.time.getTime(), it.value] }
            }
            result

        } catch (Exception e) {
            log.error("Error while processing formula: ${formula}", e)
            result = [error: e.getMessage()]

            result
        }
    }

    def calculate() {
        def result = validation(params)
        if (result.error) {
            response.status = 500
        }
        render result  as JSON
    }

    def checkFormula() {
        def tagsFound = []
        def indexes = [:] as TreeMap
        def tags = TagPI.findAll()
        /*tags.each { tag ->
            def index = -1
            params.formula.toUpperCase().findAll(tag.name.toUpperCase()) { match ->
                    index = params.formula.toUpperCase().indexOf(match, index+1)
                    tagsFound.add([id:tag.id,name:tag.name, start: index, end: index + tag.name.length()-1])
                    if (!indexes[index])
                        indexes[index] = []
                    indexes[index].add(tag.name);

            }
        }*/
        try{
            TagPIExpression expression = new TagPIExpressionBuilder(params.formula).variables(tags*.formula as Set).build()
        }catch (Exception e){
            log.error("TagPIController error", e)
            def json = [message:'Não foi possivel realizar parse da formula : ${params.formula}',ex:e.message]
            throw e
            render(text:json as JSON,status:400)
        }
        def json = [message:"ok"]
        render (text:json as JSON, status:200)
    }


    def tags() {
        def tagType = TagPIType.valueOf(params.tagType)
        def tags = TagPI.createCriteria().list {
            eq("tagType",tagType)
            projections {
                 property("id","id")
                 property("name","name")
            }
            
        }
        render tags as JSON
    }

    def tagSort() {
        def tags = TagPI.findAll().collect { tag ->
            [id: tag.id, name: tag.name, formula: tag.formula, type: tag.tagType.toString(), location: tag.tagLocation ? tag.tagLocation.toString() : "", filters: tag.filters.size() ]
        }
        render tags as JSON
    }

    def childrentags(TagPI tag){
        render tag.possibleChildrenTags.collect{ [id:it.id,name:it.name]} as JSON
    }

    def relatedUeps(){
        
        if(!params.ids || params.ids == ""){
            render status: BAD_REQUEST
            return
        }

        if(params.ids instanceof String){
            params.ids = [params.ids]
        }


        def tags = params.ids.collect{
            TagPI.get(it.toLong())
        }

        render uepService.findRelatedUepsFromTags(tags) as JSON
    }

    def reload(TagPI tagpi){
        def reloadTags = params.list('reloadTags')

         try {
            if(reloadTags.isEmpty()){
                render status: 400
                return
            }
            reloadTags = reloadTags.collect{TagPI.get(it)}
            tagPIService.reloadtags(reloadTags)
        }catch(Exception e) {
            log.error "Error no reload das tags ${reloadTags}", e
            flash.message = message(code: 'default.not.resetted.message', args: [message(code: 'TagPI.label', default: 'TagPI'), tagpi.id])
            respond tagpi.errors, view:'show'
            return
        }        

        flash.message = message(code: 'default.resetted.message', args: [message(code: 'TagPI.label', default: 'TagPI'), tagpi.id])
        redirect action:"show", method:"GET", id:tagpi.id        
    }
}
