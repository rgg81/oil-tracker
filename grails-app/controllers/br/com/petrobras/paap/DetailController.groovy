package br.com.petrobras.paap

class DetailController extends UEPSecurityController {
	
	def uepService

    def show(String id) {
    	withUepAccess(id) { uep ->
        	respond uep
        }
    }
}
