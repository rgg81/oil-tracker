package br.com.petrobras.paap

import grails.converters.JSON

import java.nio.file.AccessDeniedException

import static grails.async.Promises.task
import static grails.async.Promises.waitAll

class PIDataController extends UEPSecurityController{

    PIDataService piDataService
    EventService eventService
    UepService uepService


    private static final def MINUTES_IN_A_DAY = 60 * 24



    def lastSamplesUep(String id, Integer interval) {
        Integer intervalValue = interval / 24

        def uep = UEP.get(id)
        def result = [
                highSamples: (piDataService.lastsamplesfortag(uep.tag, 60, 'all')),
                lowSamples : (piDataService.lastsamplesfortag(uep.tag, 60 * 24 * intervalValue, 'medium'))
        ]

        render result as JSON
    }


    def lastsamplestag(String id) {
        render piDataService.lastsamplesfortag(TagPI.get(id), MINUTES_IN_A_DAY, 'all') as JSON
    }

    def lastsamples(String id) {
        withUepAccess(id) { uep ->
            def minutes = params.minutes != null ? Integer.parseInt(params.minutes) : MINUTES_IN_A_DAY
            def resolution = params.resolution ?: 'low'
            def result = piDataService.lastsamplesfortag(uep.tag, minutes, resolution)

            render result as JSON
        }
    }



    def uepInitialData(String id) {
        boolean loadAllSamples = !(params.nosamples == 'true')

        withUepAccess(id) { uep ->
            def result ;
            if (uep.tag.reloading || uep.tag.reloadStatus == ReloadStatus.FAILED) {
                result = ['error': uep.tag.reloading ? 'reloading' : 'reloadfail']
            } else {
                def uepId = id.toLong()

                def sitopBdo = SitopBdo.findByUep(uep, [sort: 'sitopDate', order: 'desc'])

                result = [
                        highSamples: (loadAllSamples ? piDataService.lastsamplesfortag(uep.tag, 60, 'all') : []),
                        lowSamples : (loadAllSamples ? piDataService.lastsamplesfortag(uep.tag, 60 * 24, 'medium') : []),
                        avg24: piDataService.average24hours(uepId),
                        expected: sitopBdo?.expected,
                        preliminary: sitopBdo?.preliminary,
                        sitopDate: sitopBdo?.sitopDate,
                        alert: piDataService.alert(uepId),
                        lastReceivedValue: uepService.lastValue(uep)
                ] + uepService.snapShotAverage24hours(uepId)

            }

            render result as JSON
        }
    }

    def uepsInitialData(String ids) {
        def splitedIds = ids?.split(",")
        def finalResult = [:]

        boolean loadAllSamples = !(params.nosamples == 'true')

        def tasks = []
        for(id in splitedIds) {
            def uep = UEP.get(id)
            if (uep) {
                boolean hasPermission = uepService.hasPermission(uep)

                if(hasPermission) {
                    if (uep.tag.reloading || uep.tag.reloadStatus == ReloadStatus.FAILED) {
                        finalResult[id] = ['error': uep.tag.reloading ? 'reloading' : 'reloadfail']
                    } else {
                        def uepId = id.toLong()

                        tasks << task {
                            def rr
                            TagPI.withTransaction(readOnly: true) { t ->
                                def sitopBdo = SitopBdo.findByUep(uep, [sort: 'sitopDate', order: 'desc'])

                                rr = [(uepId), ([
                                        highSamples: (loadAllSamples ? piDataService.lastsamplesfortag(uep.tag, 60, 'all') : []),
                                        lowSamples : (loadAllSamples ? piDataService.lastsamplesfortag(uep.tag, 60 * 24, 'medium') : []),
                                        avg24      : piDataService.average24hours(uepId),
                                        expected   : sitopBdo?.expected,
                                        preliminary: sitopBdo?.preliminary,
                                        sitopDate  : sitopBdo?.sitopDate,
                                        alert      : piDataService.alert(uepId),
                                        lastReceivedValue: uepService.lastValue(uep)
                                ] + uepService.snapShotAverage24hours(uepId)) ]
                            }

                            rr
                        }
                    }
                }
            }
        }

        finalResult += waitAll(tasks).collectEntries { [it[0], it[1]] }

        render finalResult as JSON
    }

    def lastTimes() {
        def ueps = uepService.getUEPsWithPermission()
        def values = ueps?.collectEntries { uep ->
            def aSample = Sample.findByTag(uep.tag, [sort:'date', order: 'desc'])
            [(uep.id):aSample?.date]
        }
        render values as JSON
    }

	def average24hours(String id) {
        withUepAccess(id) {
		  def result = [avg: piDataService.average24hours(id.toLong())]
          render result as JSON
        }
	}

    def sitopBdo(String id) {
        withUepAccess(id){ uep ->
            def sitopBdo = SitopBdo.findByUep(uep, [sort: 'sitopDate', order: 'desc'])
            def result = sitopBdo ? [expected: sitopBdo.expected, preliminary: sitopBdo.preliminary, sitopDate: sitopBdo.sitopDate] : null
            render result as JSON
        }
    }

    def snapshotAverage24hours(String id) {
        withUepAccess(id) {
            def result = uepService.snapShotAverage24hours(id.toLong())
            render result as JSON
        }
    }

    def alert(String id) {
        withUepAccess(id) {
            def result = [alert: piDataService.alert(id.toLong())]
            render result as JSON
        }
    }

    def events() {
        def result
        try {
            result = [events: eventService.eventsWarnings()]
        } catch (AccessDeniedException e) {
            // Access denied (user is not admin)
            result = [error: 'User must be admin']
        }
        render result as JSON
    }

    def now() {
        new Date()
    }
}
