package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesDownsampler
import grails.converters.JSON
import groovy.time.TimeCategory

class ProcessStatusController {

    static namespace = 'admin'
    private final static PAGE_SIZE_HOURS = 6

    def index() {
    	params.max = 50
        def meanTimePerType = ProcessStatus.createCriteria().list {
            ne 'endTime', 0L
            projections{
                groupProperty("type")
                avg('elapsedTime')
            }
        }.collectEntries {
            [it[0], it[1]]  // [type, value]
        }

        def result, count
        if (params.type) {
            result = ProcessStatus.findAllByType(params.type, params)
            count = ProcessStatus.countByType(params.type)
        } else {
            result = ProcessStatus.list(params)
            count = ProcessStatus.count()
        }

        respond result, model:[processStatusInstanceCount: count, meanTimePerType: meanTimePerType]
    }

    def chart() {
        respond view:'processStatus/graph.gsp', model:[page: 1]
    }

    def allStatus() {
        def result = [:] as TreeMap
        Date fromDate, toDate, now = new Date()
        def page = params.page ? Integer.valueOf(params.page) : 1
        use (TimeCategory) {
            fromDate = now - (page*PAGE_SIZE_HOURS).hour
            toDate = fromDate + (PAGE_SIZE_HOURS).hour
        }

        ProcessStatus.findAllByStartTimeBetween(fromDate.time, toDate.time, [sort: 'startTime', order: 'asc']).each { ProcessStatus status ->
            def type = status.type.substring(status.type.lastIndexOf('.')+1)
            def listForType = result[type]
            if (!listForType) {
                result[type] = listForType = [] as LinkedList
            }

            if (status.endTime > status.startTime) {
                listForType << ([status.endTime, status.elapsedTime] as Number[])
            }
        }

        result = result.collectEntries { String key, List<Number[]> values ->
            [key, TimeSeriesDownsampler.largestTriangleThreeBucketsTimeArray(values, 1000) ]
        }

        render result as JSON
    }
}
