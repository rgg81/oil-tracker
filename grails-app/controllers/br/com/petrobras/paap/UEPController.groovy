package br.com.petrobras.paap



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.converters.*

@Transactional
class UEPController {

    static namespace = 'admin'

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", reload: "POST",reloadUEPs:'POST']
    def uepService
    def tagPIService
    def shiroService

    def index(Integer max) {
        def uos = [:]
        if (shiroService.isAdmin()) {
            params.max = Math.min(max ?: 10, 100)
            def query = [filterValue: null, filterKey: null ]
            uos = uepService.getAllUEPs(query)
        } else if (shiroService.isManager()) {
            def ueps = uepService.getUEPsWithPermission()
            def query = [filterValue: null, filterKey: null ]
            uos = uepService.getAndGroupUEPS(ueps,query)         
        }
        respond uos, model:[uos: uos]
    }

    def show(UEP UEPInstance) {
        if (UEPInstance == null) {
            notFound()
            return
        }
        def relatedUeps = uepService.findRelatedUeps(UEPInstance)
        def model = UEPInstance.tag ? [reloading:tagPIService.isReloading(UEPInstance.tag)] : []
        respond UEPInstance, model : model
    }


    def create() {
        respond new UEP(params)
    }

    def save(UEP UEPInstance) {
        if (UEPInstance == null) {
            notFound()
            return
        }

        if (UEPInstance.hasErrors()) {
            respond UEPInstance.errors, view:'create'
            return
        }

        try {
            UEPInstance = uepService.save(UEPInstance)
            //UEPInstance.save flush:true
        }
        catch(Exception e) {
            log.error("UEPController error:", e)
            respond UEPInstance.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'UEP.label', default: 'UEP'), UEPInstance.id])
                redirect UEPInstance
            }
            '*' { respond UEPInstance, [status: CREATED] }
        }
    }

    def edit(UEP UEPInstance) {
        respond UEPInstance
    }

    def update(UEP UEPInstance) {
        if (UEPInstance == null) {
            notFound()
            return
        }

        if (UEPInstance.hasErrors()) {
            respond UEPInstance.errors, view:'edit'
            return
        }


        try {
            uepService.save(UEPInstance)
            //UEPInstance.save flush:true
        }
        catch(Exception e) {
            log.error("UEPController error:", e)
            respond UEPInstance.errors, view:'edit'
            return
        }
        

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UEP.label', default: 'UEP'), UEPInstance.id])
                redirect UEPInstance
            }
            '*'{ respond UEPInstance, [status: OK] }
        }
    }

    def delete(UEP UEPInstance) {

        if (UEPInstance == null) {
            notFound()
            return
        }

        try{
            uepService.delete(UEPInstance)
            //UEPInstance.delete flush:true
        }
        catch(Exception e) {
            log.error("UEPController error:", e)
            respond UEPInstance.errors, view:'show'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UEP.label', default: 'UEP'), UEPInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

     def reload(UEP UEPInstance){
        def reloadTags = params.list('reloadTags')

        try {
            if(reloadTags.isEmpty()){
                render status: 400
                return
            }
            reloadTags = reloadTags.collect{TagPI.get(it)}
            tagPIService.reloadtags(reloadTags)
        }catch(Exception e) {
            log.error "Error no reload das tags ${reloadTags}", e
            flash.message = message(code: 'default.not.resetted.message', args: [message(code: 'UEP.label', default: 'UEP'), UEPInstance.id])
            respond UEPInstance.errors, view:'show'
            return
        }        

        flash.message = message(code: 'default.resetted.message', args: [message(code: 'UEP.label', default: 'UEP'), UEPInstance.id])
        redirect action:"show", method:"GET", id:UEPInstance.id

    }

    def reloadUEPS(){
        def ueps = request.JSON
        try {
            def tags = ueps.flatten().collect{
                def tag = TagPI.get(it)
                if(!tag) throw new IllegalStateException("bad tag id")
                tag
            }

            if(tags.size() > 0){
                tagPIService.reloadtags(tags)
            }

            render status: 200
        }catch(IllegalStateException e){
            log.debug "bad request : ${ueps}", e
            render status: 400
        }catch(Exception e ){
            log.error "Error no reload das ueps ${ueps}", e
            render status: 500
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'UEP.label', default: 'UEP'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def getAllOnlineUEPs() {
        def ueps = UEP.findAll().collect {
            [id: it.id, tagId: it.tag.id, name: it.name]
        }
        render ueps as JSON
    }


    def childrenTagsFromUEPS(){
        def ueps = params.list('ueps')

        ueps = ueps.inject([]){result,id ->
            def uep = UEP.get(id)
            if(uep)
                result + uep
        }

        ueps = ueps.collect{ uep ->
            def  tags =[uep.tag]
            tags += uep.tag.possibleChildrenTags
            [id:(uep.id), name: (uep.name),tags : tags.collect{ [id:it.id,name:it.name, type: it.tagType.name() ,reloading:it.reloading, reloadStatus: it.reloadStatus.name()]}]
        }

        render ueps as JSON
    }
}
