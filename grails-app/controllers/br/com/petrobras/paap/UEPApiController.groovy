package br.com.petrobras.paap

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.converters.*

@Transactional(readOnly = true)
class UEPApiController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def uepService
    static namespace = "api"

    def getAllOnlineUEPs() {
        def ueps = uepService.getUEPsWithPermission().collect {
            [id: it.id, tagId: it.tag.id, name: it.name]
        }
        render ueps as JSON
    }

    def uos() {
        def uos = uepService.getListOfRegisteredAttributes("uo")
        uos.sort{UEP.getUOPosition(it)}
        render uos as JSON
    }

    def oilWells() {
        def oilWells = uepService.getListOfRegisteredAttributes("oilWell")
        render oilWells as JSON
    }

    def oceanCurrents() {
        def oceanCurrents = uepService.getListOfRegisteredAttributes("oceanCurrent")
        render oceanCurrents as JSON
    }

    def assets() {
        def assets = uepService.getListOfRegisteredAttributes("asset")
        render assets as JSON
    }

    def departments() {
        def departments = uepService.getListOfRegisteredAttributes("department")
        departments.sort()
        render departments as JSON
    }

    def names() {
        def names = uepService.getListOfRegisteredAttributes("name")
        names.sort()
        render names as JSON
    }
}
