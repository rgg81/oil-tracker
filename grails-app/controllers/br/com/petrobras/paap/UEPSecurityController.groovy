package br.com.petrobras.paap

import grails.converters.JSON
import javax.servlet.http.HttpServletResponse

class UEPSecurityController{

    private def withUepAccess(id,Closure closure){
        uepExists(id){ uep -> 

            if (!uepService.hasPermission(uep)) {
                log.debug "Unauthorized access!"
                render text: [message: "Unauthorized access!"] as JSON, status: HttpServletResponse.SC_FORBIDDEN, 
                        contentType: "text/json"  
                return
            }
            closure(uep)
        }
    }

    private def uepExists(id,Closure closure){
        def uep = UEP.get(id)
        if (!uep) {
            log.debug "No Content!"
            render text: [message: "No Content!"] as JSON, status: HttpServletResponse.SC_NO_CONTENT, 
                    contentType: "text/json"  
            return
        }
        closure(uep)
    }
	
}