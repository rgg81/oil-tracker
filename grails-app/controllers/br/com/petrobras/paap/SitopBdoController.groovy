package br.com.petrobras.paap

import br.com.petrobras.paap.ws.WSMessageType
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.shiro.web.servlet.ShiroHttpServletRequest
import org.springframework.web.multipart.MultipartFile

class SitopBdoController {

    WSMessageService wsMessageService
    static namespace = "admin"
    def dataSource
    SitopBdoService sitopBdoService

    def index(){
        def offset = Integer.parseInt(params.offset?: '0')
        final def max = 20

        def queryResults = sitopBdoService.sitopBdo(max ,offset)

        render view : 'index', model:[sitopBdoInstanceList: queryResults, sitopBdoInstanceCount: SitopBdo.count(), offset: offset, max: max]
    }

    def exportExcel(){
        boolean weekly = (params.weekly == "true")

        def queryResults = sitopBdoService.lastSitopBdo(weekly)

        header 'Content-Disposition', "inline; filename=sitop-bdo.xls"
        render view : 'export-excel', model:[sitopBdoInstanceList: queryResults], contentType: 'application/vnd.ms-excel'
    }

    private tmpFileName() {
        grailsApplication.config.sitopBdoFileLocation + '.tmp'
    }

    def upload() {
        try {
            if (request instanceof ShiroHttpServletRequest) {
                render(view: 'upload', model:[date:new Date()])
                return
            }

            def sitopDate
            try{
                sitopDate = Date.parse("dd/MM/yyyy", params.date)
                if(sitopDate.getTime() - new Date(0).getTime() < 0 ){
                    throw new Exception("Data sitop apenas após 1970.")
                }

            }catch(Exception e){
                flash.error = message(code: "sitopBdo.invalidDate",default:"Invalid Date.")
                render(view: 'upload', model:[date:new Date()])
                return
            }

            MultipartFile f = request.getFile('myFile')
            if (!f || f.empty) { 
                flash.error = message(code: "sitopBdo.invalidFile",default:'File cannot be empty.')
                render(view: 'upload', model:[date:sitopDate])
                return
            }

            // Remove previously temporary file
            File tmpFile = new File(tmpFileName())
            if (tmpFile.exists()) {
                log.info("Removing previously created tmp file: '${tmpFile}'")
                tmpFile.delete()
            }

            // Create temporary file
            f.transferTo(tmpFile)

            log.debug("Created tmp file: '${tmpFile}")

            XSSFWorkbook workbook = new XSSFWorkbook(tmpFile)
            XSSFSheet sheet = workbook.getSheet("Relatório Final");

            int uepCol = new CellReference("C").getCol();
            int paapUepCol = new CellReference("D").getCol();
            int sitopCol = new CellReference("Q").getCol();
            int bdoCol = new CellReference("AA").getCol();

            int firstValueRow = 3;

            def sitopBdos = []
            def sheetUepNames = []
            def ueps = UEP.list(sort:"name")

            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            try {
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();

                    if (row.getRowNum() >= firstValueRow) {
                        Cell uepCell = row.getCell(uepCol);
                        String uepName = uepCell.getStringCellValue();

                        if (uepName.isEmpty() || uepName.toLowerCase().trim().equals("total")) {
                            break;
                        }

                        String paapUepName = row.getCell(paapUepCol).getStringCellValue();
                        Double sitopValue = getCellValue(row.getCell(sitopCol));
                        Double bdoValue = getCellValue(row.getCell(bdoCol));

                        sheetUepNames << uepName
                        SitopBdo sitopBdo = new SitopBdo(sitopDate: sitopDate, expected: sitopValue, preliminary: bdoValue, uep: findUepFromName(ueps, paapUepName))
                        sitopBdos << sitopBdo
                    }
                }
            } catch(IllegalStateException e) {
                workbook.close()
                tmpFile.delete()

                log.info("Removing tmp file due to error: ${tmpFile}")

                // Erro ao processar planilha
                throw new IllegalStateException("Erro ao processar planilha: '${e.message}'.", e);
            }
            def duplicates = sitopBdos.countBy { it.uep }.findAll { it.value > 1 && it.key}
                                        .collectEntries { 
                                            if (it.key)
                                                [(it.key.name):it.value]
                                        }

            render(view: 'editupload', model: [ueps: ueps, sitopBdos: sitopBdos, sheetUepNames: sheetUepNames, duplicates: duplicates])
        } catch(e) {
            log.error("Erro no upload da planilha de SITOP e BDO", e)
            flash.error = e.message
            render(view: 'upload')
        }
    }

    def saveMultiple(SitopBdoList newItems) {
        int savedCount = 0
        newItems.sitopBdos
            .findAll{ it.uep != null }
            // Pega do banco caso seja um update...
            .each { unsavedSitopBdo ->                
                def savedSitopBdo = SitopBdo.findBySitopDateAndUep(unsavedSitopBdo.sitopDate, unsavedSitopBdo.uep)
                if(savedSitopBdo) {
                    savedSitopBdo.expected = unsavedSitopBdo.expected
                    savedSitopBdo.preliminary = unsavedSitopBdo.preliminary
                    savedSitopBdo.save(flush:true)
                } else {
                    unsavedSitopBdo.save(flush:true)
                }
                savedCount++
            }

        wsMessageService.sendEvent(WSMessageType.UPDATE_SITOP_BDO)
        flash.message = "${savedCount} SITOPs e BDOs cadastrados com sucesso"

        // Transfer file to final one
        File tmpUploadedFile = new File(tmpFileName())
        if (tmpUploadedFile.exists()) {
            log.info("Moving tmp file from '${tmpUploadedFile}' to ${grailsApplication.config.sitopBdoFileLocation}")
            // Remove older file
            new File(grailsApplication.config.sitopBdoFileLocation).delete()

            if( tmpUploadedFile.renameTo( grailsApplication.config.sitopBdoFileLocation ) == false ) {
                log.error("Error while moving tmp file")
            }

        } else {
            log.error("Temporary file was not found")
        }

        redirect(action: 'index')
    }

    def exportLastExcel() {
        def sitopBdoFile = new File(grailsApplication.config.sitopBdoFileLocation)

        if (sitopBdoFile.exists()) {
            render file : sitopBdoFile, fileName: 'last-sitop-bdo.xls', contentType: 'application/vnd.ms-excel'
        } else {
            response.status = 404

            render text:"File not found"
        }

    }

    private def findUepFromName(ueps, uepName) {
        return ueps.find { it.name.toUpperCase() == uepName.toUpperCase() }
    }

    private def getCellValue(Cell cell) {
        if (cell.cellType  == Cell.CELL_TYPE_NUMERIC) {
            return cell.numericCellValue;
        } else {
            return null ;
        }
    }
}
