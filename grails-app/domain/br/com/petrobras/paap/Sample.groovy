package br.com.petrobras.paap

class Sample {
	
	Date date
	Double value

	static belongsTo = [tag:TagPI]

    static mapping = {
        date column: 'sample_date'
    }
    static constraints = {
        tag unique: 'date'
    }
}
