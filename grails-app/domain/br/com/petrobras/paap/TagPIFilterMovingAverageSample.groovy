package br.com.petrobras.paap

class TagPIFilterMovingAverageSample implements Comparable {

    Date time
    Double value

    static belongsTo = [movingAverage: TagPIFilterMovingAverage]

    static mapping = {
        table 'tagpifilter_moving_avg_sample'
        time column: 'sample_time'
    }

    static constraints = {
        time    nullable: false
        value   nullable: false
    }

    int compareTo(obj) {
        time.compareTo(obj.time)
    }
}