package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesSample
import groovy.time.TimeCategory

// OBS: a implementação deste filtro deve estar sincronizada com edig-tag.js
class TagPIFilterKalman extends TagPIFilter
{
    Double xhatminus
    Double minusP
    Double paramK
    Double paramP = 0
    Date lastDate
    Double lastValue

    Double processVariance
    Double measureVariance
    Integer coldStartTotal = 0

    static def kalmanMaxToleratedTimeBetweenSample = 1000*60*2
    static def totalSamplesColdStart = 5

    public TagPIFilterKalman(double processVariance, double measureVariance, int filterOrder)
    {
        this.processVariance = processVariance
        this.measureVariance = measureVariance
        this.filterOrder = filterOrder
    }

    static constraints = {
        processVariance nullable: false
        measureVariance nullable: false

        paramP          nullable: false

        xhatminus       nullable: true
        minusP          nullable: true
        paramK          nullable: true
        lastDate        nullable: true
        lastValue       nullable: true
        coldStartTotal  nullable: true
    }

    private Boolean checkStateAndResetIfNeeded(TimeSeriesSample sample) throws TagPIFilterException {
        if(lastDate != null) {
            def diffTime = TimeCategory.minus(sample.time, lastDate).toMilliseconds()
            // Checa se o novo sample é mais antigo do que a ultima data
            if (lastDate > sample.time) {
                throw new TagPIFilterException("Novo sample (${sample}) é mais antigo do que a ultima data: (${lastDate})")
            }
            if (diffTime > kalmanMaxToleratedTimeBetweenSample) {
                log.info("resetting kalman filter tag ${tag}")
                paramP = 0
                minusP = null
                xhatminus = null
                paramK = null
                lastDate = sample.time
                lastValue = sample.value
                coldStartTotal = 0
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }

    @Override
    // Ported from http://wiki.scipy.org/Cookbook/KalmanFiltering
    public Double addAndGetResult(TimeSeriesSample sample) throws TagPIFilterException {
        if(!checkStateAndResetIfNeeded(sample)) {
            if (coldStartTotal == null || coldStartTotal < totalSamplesColdStart || lastDate == null) {
                def increaseAverageAmounts = (coldStartTotal?:0) + 1
                lastDate = sample.time
                lastValue = ((lastValue?:0) * ((increaseAverageAmounts-1)/(increaseAverageAmounts))) + (sample.value/increaseAverageAmounts)
                coldStartTotal = increaseAverageAmounts
            } else {
                // time update
                xhatminus = lastValue
                minusP = paramP + processVariance

                // measurement update
                paramK = minusP / (minusP + measureVariance)
                def xhatValue = xhatminus + paramK * (sample.value - xhatminus)
                lastDate = sample.time
                lastValue = xhatValue
                paramP = (1 - paramK) * minusP
            }
        }

        return lastValue
    }

    @Override
    public void resetState() throws TagPIFilterException {
        paramP = 0
        minusP = null
        xhatminus = null
        paramK = null
        lastDate = null
        lastValue = null
        coldStartTotal = 0
    }
}
