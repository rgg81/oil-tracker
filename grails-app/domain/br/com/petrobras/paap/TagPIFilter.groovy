package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesSample

abstract class TagPIFilter {

    Integer filterOrder

    static belongsTo = [tag:TagPI]

    static constraints = {
        filterOrder   nullable: false, unique: 'tag'
    }

    public abstract Double addAndGetResult(TimeSeriesSample sample) throws TagPIFilterException ;
    public abstract void resetState() throws TagPIFilterException ;
}

