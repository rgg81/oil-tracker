package br.com.petrobras.paap

class OnlineUser {
	String login
	String sessionId

    static constraints = {
    }
    static mapping = {
	    table 'Online_User'
    	id generator: 'assigned', name: "sessionId", type: 'string'
  	}
}
