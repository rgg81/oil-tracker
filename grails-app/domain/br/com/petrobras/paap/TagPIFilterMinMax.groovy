package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesSample

/**
 * Created by uq4e on 18/05/15.
 */
class TagPIFilterMinMax extends TagPIFilter {
    Double min
    Double max

    static constraints = {
        min nullable: true
        max nullable: true
    }

    public TagPIFilterMinMax(Double min, Double max, int filterOrder)
    {
        this.min = min
        this.max = max
        this.filterOrder = filterOrder
    }

    @Override
    public Double addAndGetResult(TimeSeriesSample sample) throws TagPIFilterException {
        if (max != null && sample.value > max) {
            throw new TagPIFilterException("Valor ${sample.value} está acima do máximo ${max}")
        }
        if (min != null && sample.value < min) {
            throw new TagPIFilterException("Valor ${sample.value} está abaixo do mínimo ${min}")
        }

        return sample.value
    }
    public void resetState() throws TagPIFilterException {
    
    }
}
