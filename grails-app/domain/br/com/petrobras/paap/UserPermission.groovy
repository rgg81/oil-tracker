package br.com.petrobras.paap
import org.codehaus.groovy.grails.commons.DefaultGrailsDomainClass

class UserPermission {
	String attribute
	String value
	String username

	static def validAttributes = ["uo","department","id"]

    static constraints = {
    	attribute(inList: validAttributes)
    	username(unique:['attribute','value'])
    }
}
