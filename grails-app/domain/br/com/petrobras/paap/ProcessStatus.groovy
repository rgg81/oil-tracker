package br.com.petrobras.paap
import java.text.SimpleDateFormat
class ProcessStatus {

	long startTime
	long endTime
	String type
	String status
	String msg
    long elapsedTime

    static SimpleDateFormat formatter = new SimpleDateFormat('dd-MM-yyyy HH:mm:ss', new Locale("pt", "BR"))

    static constraints = {
    	startTime nullable: false
    	endTime nullable: true
    	type nullable: false
    	status nullable: false
    	msg nullable:true
    }
    static mapping = {
        sort startTime:"desc"
        msg type: "text"
        elapsedTime formula: "END_TIME - START_TIME"
    }

    String getFormattedStartTime(){
        return formatter.format(new Date(startTime))
    }

    String getFormattedEndTime(){
        return formatter.format(new Date(endTime))
    }

    String getFormattedDuration(){
        return (endTime ? endTime :  new Date().getTime()) - startTime + "ms" 
    }
}
