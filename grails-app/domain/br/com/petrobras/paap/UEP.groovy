package br.com.petrobras.paap

class UEP {

	String name
	String uo
	String department
	String asset
	String oceanCurrent
	String oilWell
	Double wi

	// Fields for alert calculation:
	Integer shortWindow
	Integer longWindow
	Double alertPercent

	static def uo_order = ['UO-ES' : 0,'UO-BC' : 1,'UO-RIO': 2,'UO-BS': 3]
	static def firstDepartment = "SSE"
	static def possibleFilters = ["uo","department","asset"]

	TagPI tag

	static hasMany = [
		sitopBdos:SitopBdo,
		events:EventUEP
	]

    static constraints = {
    	name 			blank: false
    	uo 				blank: false
    	department 		blank: false
    	asset 			blank: false
    	oceanCurrent 	blank: false
    	oilWell 		blank: false
		shortWindow 	nullable: true
		longWindow 		nullable: true
		alertPercent 	nullable: true
		wi 				nullable: true
		tag 			nullable: false, unique: true , validator: { val ->
            if(val.tagType != TagPIType.CALCULATED){
         		return ['onlyCalculated']
            }
            return true
        }
	}

	boolean hasAlert() {
		return shortWindow != null && longWindow != null && alertPercent != null
	}

	String toString() {
		return name
	}

	static getUOPosition(String uo){
		return uo_order[uo] != null ? uo_order[uo] : 0		
	}

	static getFirstDepartment() {
		return firstDepartment
	}

	static canFilter(String filterParam) {
		return possibleFilters.contains(filterParam)
	}

	def wiWithDefault() {
		if (wi) {
			wi
		} else {
			100.0
		}
	}
}
