package br.com.petrobras.paap

class SnapshotSamples24h {
	
	Date date
	Double value

	static belongsTo = [tag:TagPI]

    static mapping = {
        date column: 'sample_date'
    }
    static constraints = {
        tag unique: 'date'
    }

    static def findByTagAndDay(TagPI tag , Date date){
        def beginDay = new Date(date.time)
        beginDay.clearTime()
        def endDay = new Date(year:date.year, month: date.month, date: date.date, hours : 23, minutes:59, seconds:59)

        def ret  = SnapshotSamples24h.withCriteria{
            eq 'tag',tag
            between 'date', beginDay, endDay
            order 'date', 'desc'
        }
        return (ret && !ret.isEmpty() ? ret.first() : [] )
    }
}
