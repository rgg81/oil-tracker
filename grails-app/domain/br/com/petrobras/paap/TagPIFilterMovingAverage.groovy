package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesSample

// OBS: a implementação deste filtro deve estar sincronizada com edig-tag.js
class TagPIFilterMovingAverage extends TagPIFilter
{
    Long windowInMs
    SortedSet<TagPIFilterMovingAverageSample> samples

    public TagPIFilterMovingAverage(long windowInMs, int filterOrder)
    {
        this.windowInMs = windowInMs
        this.filterOrder = filterOrder
    }

    static hasMany = [
        samples: TagPIFilterMovingAverageSample
    ]

    static mapping = {
        samples sort: 'time', order: 'asc', lazy: false
    }

    static constraints = {
        windowInMs  nullable: false
    }

    /*
        This method consideres the samples as a "continuous" function and calculate the area under the window, since the last sample.
        More info: http://mathcentral.uregina.ca/QQ/database/QQ.09.00/esther2.html

        NOTE-1: If the first sample is inside the window, than its value is propagated linearly until the window
        NOTE-2: This funcion keeps one sample outside the window. This keeps the shape of the samples since the first sample inside the window
        NOTE-3: We consider the samples to be linearny conected and, therefore, we use the 'trapezoidArea' function to calculate the area between
            each two points
     */
    @Override
    public Double addAndGetResult(TimeSeriesSample sample) throws TagPIFilterException {
        // Define o tempo de corte
        Date minSampleTime = new Date(sample.time.getTime() - windowInMs)

        if (samples != null) {

            // Checa se o novo sample é mais antigo do que o anterior
            if (!samples.isEmpty() && samples.last().time > sample.time) {
                throw new TagPIFilterException("Novo sample (${sample}) é mais antigo do que série atual (${samples.last().time})")
            }

            // Remove todas samples menores do que minSampleTime, com exceção do primeiro
            // MSC-ONLY CODE!!!!!!!!!!!
            // * See NOTE-2
            TagPIFilterMovingAverageSample first;
            Iterator<TagPIFilterMovingAverageSample> iSamples ;
            while (samples.size() >= 2                                                       // At least 2 items
                && (first = (iSamples = samples.iterator()).next()).time < minSampleTime     // Analyzes and stores the first item
                && iSamples.next().time < minSampleTime )                                    // Analyzes the second item
            {
                this.removeFromSamples(first)
                first.delete()
            }
        }


        // Adiciona o novo sample à lista (ou modifica o último, caso o campo 'time' seja o mesmo)
        TagPIFilterMovingAverageSample lastSample = samples && !samples.isEmpty() ? samples.last() : null
        if (lastSample?.time == sample.time) {
            // Substitui o antigo pelo novo
            lastSample.value = sample.value

        } else {
            TagPIFilterMovingAverageSample newSample = new TagPIFilterMovingAverageSample(time: sample.time, value: sample.value)
            this.addToSamples(newSample)
        }


        def area = 0 ;
        if ( !samples?.isEmpty() ) {
            Iterator<TagPIFilterMovingAverageSample> iSamples = samples.iterator()

            TagPIFilterMovingAverageSample previous = iSamples.next()
            if ( minSampleTime < previous.time ) {
                // Rectangle tail (* see NOTE-1)
                area += (previous.time.time - minSampleTime.time) * previous.value ;
            }

            while( iSamples.hasNext() ) {
                TagPIFilterMovingAverageSample current = iSamples.next()

                if ( previous.time < minSampleTime ) {
                    // 'last' is outside minSampleTime, but 'current' is inside -> calculate only from midValue
                    // * See NOTE-2
                    def midValue = previous.value + (current.value - previous.value) * (minSampleTime.time - previous.time.time) / (current.time.time - previous.time.time)
                    area += trapezoidArea(midValue, current.value, minSampleTime, current.time)
                } else {
                    // 'last' and 'current' are inside minSampleTime range
                    area += trapezoidArea(previous.value, current.value, previous.time, current.time)
                }

                previous = current
            }
        }

        return area / this.windowInMs ;
    }

    private trapezoidArea(v1, v2, t1, t2) {
        return (v1 + v2) * (t2.time - t1.time) / 2 ;
    }

    public  void resetState() throws TagPIFilterException {
        this.samples*.delete()
        this.samples?.clear()
    }
}
