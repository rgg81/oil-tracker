package br.com.petrobras.paap

abstract class Event {

    Date date
    String msg
    EventType eventType

    static mapping = {
        date column: 'event_date', index: 'idx_event_date'
        msg type: "text"
    }

    static constraints = {
    }
}
