package br.com.petrobras.paap

import org.grails.databinding.BindingFormat

class SitopBdo {
    @BindingFormat('dd/MM/yyyy')
    Date sitopDate
    double expected
    double preliminary

    static belongsTo = [uep: UEP]

    static constraints = {
        uep unique: 'sitopDate'
    }
}
