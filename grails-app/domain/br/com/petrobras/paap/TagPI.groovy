package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesSample
import net.objecthunter.exp4j.ExpressionBuilder
import javax.persistence.FlushModeType
import org.hibernate.FlushMode


class TagPI {

	String name
	String formula
	TagPIType tagType
    TagPILocation tagLocation
    
    Boolean reloading
    ReloadStatus reloadStatus = ReloadStatus.DONE

	static hasMany = [
        samples:Sample,
        samples2m:SampleGroup2m,
        samples10m:SampleGroup10m,
        samples1h:SampleGroup1h,
        samples6h:SampleGroup6h,
        samples24h:SampleGroup24h,
        filters:TagPIFilter,
        events:EventTagPI,
        snapshot:SnapshotSamples24h
	]

    static mapping = {
        filters sort: 'filterOrder', order: 'asc'
        events lazy:true, sort: 'date', order: 'desc'
        reloading defaultValue: false
        reloadStatus defaultValue: ReloadStatus.DONE
    }

	static constraints = {
        name    unique: true
        reloading nullable:true
        reloadStatus nullable:false
        tagLocation blank:true, nullable: true, validator: { value, object ->
            // Tag não pode vir do PI (!= CALCULATED) e ter a localização nula
            if(object.tagType == TagPIType.CALCULATED){
                return value == null || TagPILocation.isValid(value)
            }else{
                return TagPILocation.isValid(value)
            }
        }

        formula validator: { value, object ->
            if(object.tagType == TagPIType.CALCULATED){
                def tags
                TagPI.withSession{ sessionObj ->
                    boolean isHibernate = (sessionObj instanceof org.hibernate.Session)
                    if (isHibernate) {
                        sessionObj.setFlushMode(FlushMode.MANUAL);
                    } else {
                        sessionObj.setFlushMode(FlushModeType.COMMIT)
                    }

                    tags = TagPI.findAllByTagTypeOrTagTypeOrTagType(TagPIType.SNAPSHOT,TagPIType.ACCUMULATOR,TagPIType.SPACED_SNAPSHOT)

                    if (isHibernate) {
                        sessionObj.setFlushMode(FlushMode.AUTO);
                    } else {
                        sessionObj.setFlushMode(FlushModeType.AUTO)
                    }

                }

                try {
                    new TagPIExpressionBuilder(value,tags).variables(tags*.formula as Set).build()
                } catch(Exception e) {
                    log.error("not valid formula:'${value}' in TagPI instance ", e)
                    return false
                }
            } else {
                def tag
                TagPI.withSession{ sessionObj ->
                    boolean isHibernate = (sessionObj instanceof org.hibernate.Session)
                    if (isHibernate) {
                        sessionObj.setFlushMode(FlushMode.MANUAL);
                    } else {
                        sessionObj.setFlushMode(FlushModeType.COMMIT)
                    }

                    tag = TagPI.findByFormula(value)

                    if (isHibernate) {
                        sessionObj.setFlushMode(FlushMode.AUTO);
                    } else {
                        sessionObj.setFlushMode(FlushModeType.AUTO)
                    }

                }
                if (tag && tag?.id != object?.id && tag?.tagType != TagPIType.CALCULATED)
                    return false
            }
            return true
        }
        
	}

    public double applyFilters(TimeSeriesSample sample) throws TagPIFilterException {
        TimeSeriesSample filteredSample = sample.clone()
        filters?.each { TagPIFilter filter ->
            filteredSample.value = filter.addAndGetResult(filteredSample)
        }

        return filteredSample.value
    }

    def filtersSpec() {
        filters.collect { filter ->
            if (filter instanceof TagPIFilterMovingAverage) {
                [
                    kind: 'average',
                    properties: [
                        windowInMinutes: filter.windowInMs / (60*1000)
                    ]
                ]
            } else if (filter instanceof TagPIFilterKalman) {
                [
                    kind: 'kalman',
                    properties: [
                        processVariance: filter.processVariance,
                        measureVariance: filter.measureVariance,
                    ]
                ]
            } else if (filter instanceof TagPIFilterMinMax) {
                [
                    kind: 'minmax',
                    properties: [
                        min: filter.min,
                        max: filter.max,
                    ]
                ]
            }
        }
    }

    def getPossibleChildrenTags(){
        if(this.tagType == TagPIType.CALCULATED){
            return TagPI.findAllByTagTypeOrTagTypeOrTagType(TagPIType.SNAPSHOT, TagPIType.ACCUMULATOR, TagPIType.SPACED_SNAPSHOT)
                .findAll{
                    this.formula.indexOf("'"+it.formula+"'") >= 0
                }
        }
        return []
    }

    def getPossibleParentTags(){
        if(this.tagType != TagPIType.CALCULATED){
            return TagPI.findAllByTagType(TagPIType.CALCULATED).findAll{it.formula.indexOf("'"+this.formula+"'") >= 0 }
        }
        return []
    }

    def getUep(){
        UEP.findByTag(this)
    }
    

	String toString() {
		return name
	}

    boolean hasFilter() {
        filters?.size()
    }

    static def findPiTags(isreloading = false){
        TagPI.withCriteria {
            or {
                eq("reloading", isreloading)
                    if(!isreloading)
                        isNull("reloading")
            }
            or {
                eq("tagType", TagPIType.SNAPSHOT)
                eq("tagType", TagPIType.ACCUMULATOR)
                eq("tagType", TagPIType.SPACED_SNAPSHOT)
            }
        }
    }


    static def findPiTagsFromLocation(tagLocation,isreloading = false){
        TagPI.withCriteria {
            or {
                eq("reloading", isreloading)
                    if(!isreloading)
                        isNull("reloading")
            }
            eq("tagLocation", tagLocation)
            or {
                eq("tagType", TagPIType.SNAPSHOT)
                eq("tagType", TagPIType.ACCUMULATOR)
                eq("tagType", TagPIType.SPACED_SNAPSHOT)
            }
        }
    }

    static def findCalculatedTags(isreloading = false){
        TagPI.withCriteria {
            or {
                eq("reloading", isreloading)
                    if(!isreloading)
                        isNull("reloading")
            }
            eq("tagType", TagPIType.CALCULATED)
        }
    }

    static def findAllTags(isreloading = false){
        TagPI.withCriteria {
            or {
                eq("reloading", isreloading)
                    if(!isreloading)
                        isNull("reloading")
            }
        }
    }
}
