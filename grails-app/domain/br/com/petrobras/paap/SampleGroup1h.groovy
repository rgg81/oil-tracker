package br.com.petrobras.paap

import groovy.time.TimeCategory
import groovy.time.TimeDuration

class SampleGroup1h implements SampleGroup {
    Date date
    Double mean
    Double max
    Double min
    Double variance
    Double num
    Double sum

	static belongsTo = [tag:TagPI]

    static constraints = {
    }
    static mapping = {
        date column: 'sample_date'
    }

    @Override
    TimeDuration getDuration() {
        use(TimeCategory) {
            return 1.hour
        }
    }
}
