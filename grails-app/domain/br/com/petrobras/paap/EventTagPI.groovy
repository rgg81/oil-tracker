package br.com.petrobras.paap

class EventTagPI extends Event {

    static belongsTo = [tag:TagPI]

    static mapping = {
        tag index: 'idx_event_date,idx_eventtagpi_tag'
    }

    static constraints = {
    }
}
