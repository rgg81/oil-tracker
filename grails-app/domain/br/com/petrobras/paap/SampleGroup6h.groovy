package br.com.petrobras.paap

import groovy.time.TimeCategory
import groovy.time.TimeDuration

class SampleGroup6h implements SampleGroup {
    Date date
    Double mean
    Double max
    Double min
    Double variance
    Double num
    Double sum

	static belongsTo = [tag:TagPI]

    static constraints = {
    }

    @Override
    TimeDuration getDuration() {
        use(TimeCategory) {
            return 6.hours
        }
    }
    static mapping = {
        date column: 'sample_date'
    }
}