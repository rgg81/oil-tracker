package br.com.petrobras.paap

class PIDataService {

	private static final LOW_PRODUCTION = 1000
	def uepService

	private static final def MS_IN_A_MINUTE = 1000 * 60
	private static final def MINUTES_IN_A_DAY = 60 * 24
	private static final def MS_IN_3_DAY = MS_IN_A_MINUTE * MINUTES_IN_A_DAY * 3

	def now() {
		new Date()
	}

	def averageLastMinutes(long uepId, long minutes) {
		TagPI tag = UEP.get(uepId).tag
		if (tag.reloading) {
			return null
		}
		def tagId = tag.id
		return averageLastMinutesTag(tagId, minutes)

	}

	def averageLastMinutesTag(def tagId, long minutes,Date lastSampleTime = null) {
		def avg = Sample.createCriteria().list {
			and {
				eq 'tag', TagPI.get(tagId)
				gt 'date', new Date((lastSampleTime?:now()).getTime() - 1000*60*minutes)
			}
			projections{
				avg('value')
			}
		}

		return avg[0]
	}

	def average24hours(uep) {
		return averageLastMinutes(uep, 24 * 60)
	}

	def sumAverage24hours() {
		def ueps = uepService.getUEPsWithPermission()

		def avgs = Sample.createCriteria().list {
       		between 'date', new Date(now().getTime() - 1000*60*60*24), now()
    		projections{
				avg('value')
				groupProperty("tag")
			}		
		}

		def sum = avgs.inject(0d) { acc, avg ->
			acc + (ueps.grep({it.tag.id == avg[1].id}) ? avg[0] : 0d)
		}

		return sum
	}

	def alert(long uepId) {
		def hasAlert = null

		UEP uep = UEP.get(uepId)

		// Trata reloading
		if (uep.tag.reloading) {
			return null
		}

		//		def uepData = UEP.executeQuery("select shortWindow, longWindow, alertPercent, tag.id from UEP where id=?", [uepId])[0]
		def shortWindow = uep.shortWindow
		def longWindow = uep.longWindow
		def alertPercent = uep.alertPercent
		def tagId = uep.tag.id
		def lastSampleTime = lastSample(uep.tag)?.date

		if (uep.hasAlert()) {
			def longAvgValue = averageLastMinutesTag(tagId, longWindow,lastSampleTime)

			if (longAvgValue > LOW_PRODUCTION) {
				def shortAvgValue = averageLastMinutesTag(tagId, shortWindow,lastSampleTime)

				if (shortAvgValue) {
					def rate = (shortAvgValue - longAvgValue) / longAvgValue * 100

					if (rate > alertPercent) {
						// Positive alert
						hasAlert = 1
					} else if (rate < -alertPercent) {
						// Negative alert
						hasAlert = -1
					}
				}
			}
		}
		

		return hasAlert
	}

	def deleteAllSamplesFromTag(tag,date=null) {
		def and = '' 
		def params = [tag: tag]

		if(date != null){
			and = ' and s.date <= :date'
			params['date'] = date
		}

		Sample.executeUpdate("delete Sample s where s.tag = :tag" + and, params)
		SampleGroup2m.executeUpdate("delete SampleGroup2m s where s.tag = :tag" + and, params)
		SampleGroup10m.executeUpdate("delete SampleGroup10m s where s.tag = :tag" + and, params)
		SampleGroup1h.executeUpdate("delete SampleGroup1h s where s.tag = :tag" + and, params)
		SampleGroup6h.executeUpdate("delete SampleGroup6h s where s.tag = :tag" + and, params)
		SampleGroup24h.executeUpdate("delete SampleGroup24h s where s.tag = :tag" + and, params)
	}

	def lastSample(tag){
		Sample.findByTag(tag, [sort:'date', order: 'desc'])
	}

	def deleteSamplesFromNonReloadingTagsUntil(date=null) {
		def and = ''
		def params = [:]

		if(date != null){
			and = ' and s.date <= :date'
			params['date'] = date
		}

		Sample.executeUpdate("delete Sample s where tag.id not in (select id from TagPI where reloading = true)" + and, params)
		SampleGroup2m.executeUpdate("delete SampleGroup2m s where tag.id not in (select id from TagPI where reloading = true)" + and, params)
		SampleGroup10m.executeUpdate("delete SampleGroup10m s where tag.id not in (select id from TagPI where reloading = true)" + and, params)
		SampleGroup1h.executeUpdate("delete SampleGroup1h s where tag.id not in (select id from TagPI where reloading = true)" + and, params)
		SampleGroup6h.executeUpdate("delete SampleGroup6h s where tag.id not in (select id from TagPI where reloading = true)" + and, params)
		SampleGroup24h.executeUpdate("delete SampleGroup24h s where tag.id not in (select id from TagPI where reloading = true)" + and, params)
	}

	def lastsamplesfortag(TagPI tag, minutes, resolution) {
		// Check for tag being reload
		if (tag.reloading || tag.reloadStatus == ReloadStatus.FAILED) {
			def reloadingResult = ['error': tag.reloading ? 'reloading' : 'reloadfail']

			return reloadingResult
		} else {
			def rangeInMs = minutes * MS_IN_A_MINUTE
			if(rangeInMs > MS_IN_3_DAY || rangeInMs < 0) {
				rangeInMs = MS_IN_3_DAY
			}
			// resolution is one of 'all', 'high', 'medium' or 'low' -> Sample, SampleGroup2m, SampleGroup10m, SampleGroup1h

			def toDate = now()
			def fromDate = new Date(toDate.time - rangeInMs)

			def result = []
			switch (resolution) {
				case 'all':
					Sample.findAllByTagAndDateBetween(tag, fromDate, toDate, [sort: 'date', order: 'asc']).each {
						result << [it.date.getTime(), it.value]
					}
					break ;
				case 'high':
					SampleGroup2m.findAllByTagAndDateBetween(tag, fromDate, toDate, [sort: 'date', order: 'asc']).each {
						result << [it.date.getTime(), it.mean]
					}
					break ;
				case 'medium':
					SampleGroup10m.findAllByTagAndDateBetween(tag, fromDate, toDate, [sort: 'date', order: 'asc']).each {
						result << [it.date.getTime(), it.mean]
					}
					break ;
				case 'low':
					SampleGroup1h.findAllByTagAndDateBetween(tag, fromDate, toDate, [sort: 'date', order: 'asc']).each {
						result << [it.date.getTime(), it.mean]
					}
					break ;
			}

			return result
		}
	}
}
