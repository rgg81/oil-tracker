package br.com.petrobras.paap

import grails.transaction.Transactional
import groovy.json.JsonSlurper
import grails.converters.JSON
import javax.jms.*
import org.apache.log4j.Logger


class TagPIService {

	static transactional = false

    def reloadQueueService

    def PIDataService
    def uepService

	@Transactional
    def update(TagPI tagPIInstance,String filtersSpecParam = null) {
    	try{
            if(filtersSpecParam){
        	   updateFiltersFromRequest(tagPIInstance, filtersSpecParam, true)
            }
        	tagPIInstance.save flush:true
        }catch(Exception e){
        	log.error("update tagpi(${tagPIInstance}) failed: ${e.message}", e)
        	throw new RuntimeException("update tagpi(${tagPIInstance}) failed: ${e.message}", e)
        }
    }
	@Transactional
    def delete(TagPI tagPIInstance){
    	try{
            def parents = tagPIInstance.possibleParentTags
            if(parents != [])
                throw new TagPIReferenceException("Cannot delete tag, it is still referenced in calculated tags: ${parents}")
    		tagPIInstance.delete flush:true
    	}catch(Exception e){
    		log.error("delete tagpi(${tagPIInstance}) failed: ${e.message}", e)
        	throw new RuntimeException("delete tagpi(${tagPIInstance}) failed: ${e.message}", e)
        }
    }

    @Transactional
    def resetFilters(TagPI tagPIInstance){
        try{
           tagPIInstance.filters.each{ f ->
                f.resetState()
           }
           tagPIInstance.save()
        }catch(Exception e){
            log.error("resetFilters tagpi(${tagPIInstance}) failed: ${e.message}", e)
            throw new RuntimeException("resetFilters tagpi(${tagPIInstance}) failed: ${e.message}", e)
        }
    }

    @Transactional
    def resetTag(TagPI tagPIInstance){
        try{
            PIDataService.deleteAllSamplesFromTag(tagPIInstance)
            resetFilters(tagPIInstance)
            tagPIInstance.save()
        }catch(Exception e){
            log.error("resetTag tagpi(${tagPIInstance}) failed: ${e.message}", e)
            throw new RuntimeException("resetTag tagpi(${tagPIInstance}) failed: ${e.message}", e)
        }
    }

    def isReloading(TagPI tagPIInstance){
        return (tagPIInstance.reloading?: false)
    }

    @Transactional
    def reloadtags(List<TagPI> tags){
        try{
            if(tags == null)
                throw new IllegalArgumentException("tags cannot be null")

            tags.collect{ tag ->
                if(tag.reloading || tag.reloadStatus == ReloadStatus.QUEUE || tag.reloadStatus == ReloadStatus.PROCESSING)
                    throw new IllegalArgumentException("tags invalid state")
            }
            if(tags.size() > 0)
                reloadQueueService.pushToStartQueue(tags as Set)

            tags.each{ tag ->
                tag.reloadStatus = ReloadStatus.QUEUE
                tag.save flush:true
            }
        }catch(Exception e){
            log.error("reloadtags tags(${tags}) failed: ${e.message}", e)
            throw new RuntimeException("reloadtags tags(${tags}) failed: ${e.message}", e)
        }
    }
    
    private void updateFiltersFromRequest(TagPI tagPIInstance, String filtersSpecParam, boolean persist) {
        //println "upadteFiltersFromRequest: ${params}"

        // Remove os filtros anteriores
        tagPIInstance.filters*.delete()
        tagPIInstance.filters?.clear()

        // Salva com flush para evitar erros ao se adicionar os novos filtros
        if( persist ) {
            tagPIInstance.save flush: true
        }

        // Cria os filtros novos
        if (filtersSpecParam?.length() > 0 ) {
            def filtersSpec = new JsonSlurper().parseText(filtersSpecParam)
            //println "filtersSpec = ${filtersSpec}"
            filtersSpec?.findAll{ it.enabled }.eachWithIndex { filterSpec, idx ->
                //println "idx = ${idx}"
                def tagFilter
                switch (filterSpec.kind.toLowerCase()) {
                    case "kalman":
                        tagFilter = new TagPIFilterKalman(
                                filterSpec.properties.processVariance,
                                filterSpec.properties.measureVariance,
                                idx)
                        break;
                    case "average":
                        tagFilter = new TagPIFilterMovingAverage(
                                Math.round(filterSpec.properties.windowInMinutes * 1000 * 60),
                                idx)
                        break;
                    case "minmax":
                        tagFilter = new TagPIFilterMinMax(
                                filterSpec.properties.min,
                                filterSpec.properties.max,
                                idx)
                        break;
                    default:
                        throw new IllegalArgumentException("Filter spec not found: '${filterSpec.kind}'")
                }
                tagPIInstance.addToFilters(tagFilter)
            }
        }
    }

    def tagFilter(tagSelect) {
        def result = null
        if (TagPILocation.checkTag(tagSelect)) {
            return TagPI.findAllByTagLocation(tagSelect)
        }
        else if (tagSelect == "CALCULATED") {
            return TagPI.findAllByTagType(tagSelect)

        } else {

        }
    }

    def hasAccessToTag(TagPI tagPIInstance) {
        return uepService.hasPermission(UEP.findByTag(tagPIInstance))
    }
}
