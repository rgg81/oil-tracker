package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesSample
import grails.transaction.Transactional
import org.apache.commons.lang.StringEscapeUtils
import org.xml.sax.SAXParseException

import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat

@Transactional
class ReadPIService {
	static final serviceUrl = 'https://wsconsultapi.petrobras.com.br/wsconsultapi.asmx/';
	static final numberFormat = NumberFormat.getInstance(Locale.getInstance("pt", "BR", "en"))
    static final TimeZone saoPauloTZ = TimeZone.getTimeZone("America/Sao_Paulo")
    static final long MS_IN_AN_HOUR = 1000 * 60 * 60

	def grailsApplication

	SimpleDateFormat webserviceDateParser = new SimpleDateFormat('yyyy-MM-dd\'T\'HH:mm:ss', new Locale("pt", "BR"))

    private def locationConfig(TagPILocation location) {
        grailsApplication.config.paap.servicepi[location.name()]
    }

	def exception = { c ->
		try{
			c.call()
		} catch (Exception e){
			log.error("Falha ao recuperar tag",e)
			[]
		}
	}

    def readDataFromTagInTimePeriodReturningError(String tag, TagPILocation location, Date initDate, Date endDate) {
        boolean fixDst = isDst()
        initDate = fixDstDateParameter(initDate, fixDst)
        endDate = fixDstDateParameter(endDate, fixDst)
        def config = locationConfig(location)
        def url = "${serviceUrl}GetCompressedData?tags=${tag}&userKey=${config.user}&userPass=${config.password}&dataHoraInicial=${webserviceDateParser.format(initDate)}&dataHoraFinal=${webserviceDateParser.format(endDate)}&modo=1"
        log.debug("URL service for one tag:${url}")
        parseUrl(url)	// Retorna apenas uma tag
    }

    def readDataFromTagInTimePeriod(String tag, TagPILocation location, Date initDate, Date endDate) {
    	exception {
            readDataFromTagInTimePeriodReturningError(tag, location, initDate, endDate)
		}
    }

    def readDataFromTagsInTimePeriodReturningError(List<String> tags, TagPILocation location, Date initDate, Date endDate) {
        boolean fixDst = isDst()
        initDate = fixDstDateParameter(initDate, fixDst)
        endDate = fixDstDateParameter(endDate, fixDst)
        def config = locationConfig(location)
        def paramsTags = {if(tags.size() > 1) tags.tail().inject("tags=${tags.head()}") {acum, element -> "${acum}&tags=${element}"} else "tags=${tags.head()}"}.call()
        def params = "${paramsTags}&userKey=${config.user}&userPass=${config.password}&dataHoraInicial=${webserviceDateParser.format(initDate)}&dataHoraFinal=${webserviceDateParser.format(endDate)}&modo=1"
        def url = "${serviceUrl}GetCompressedData"
        log.debug("URL service:${url}")
        parseUrlArrayTags(url,params)
    }

    def readDataFromTagsInTimePeriod(List<String> tags, TagPILocation location, Date initDate, Date endDate) {
        exception {
            readDataFromTagsInTimePeriodReturningError(tags, location, initDate, endDate)
        }
    }

    def readDataFromTagInTimePeriodInclusive(String tag, TagPILocation location, Date initDate, Date endDate) {
    	exception {
            boolean fixDst = isDst()
            initDate = fixDstDateParameter(initDate, fixDst)
            endDate = fixDstDateParameter(endDate, fixDst)
            def config = locationConfig(location)
			def url = "${serviceUrl}GetCompressedData?tags=${tag}&userKey=${config.user}&userPass=${config.password}&dataHoraInicial=${webserviceDateParser.format(initDate)}&dataHoraFinal=${webserviceDateParser.format(endDate)}&modo=1"
			parseUrl(url)	// Retorna apenas uma tag
		}
    }

    def readLastDataFromTag(String tag, TagPILocation location) {
        def config = locationConfig(location)         
        def url = "${serviceUrl}GetCurrentValue?tags=${tag}&userKey=${config.user}&userPass=${config.password}&modo=0"
        log.debug("URL service:${url}")
        parseUrl(url)
    }

	/*
	@Deprecated
	def readDataFromTagInTimePeriodWithAverage(String tag, TagPILocation location, Date initDate, Date endDate) {
		exception{
            def config = locationConfig(location)
			def url = "${serviceUrl}GetTagCalculation?tag=${tag}&userKey=${config.user}&userPass=${config.password}&dataHoraInicial=${webserviceDateParser.format(initDate)}&dataHoraFinal=${webserviceDateParser.format(endDate)}&operacao=media"
            def xml = xmlParser().parse("${url}")
			xml?.Propriedades?.Propriedade?.collect {
	            [media: valueNodeTag(it)]
			}.head()
		}
	}
	*/

    private def valueNodeTag = { aValue ->
        try {
            numberFormat.parse(aValue.Valor?.text())
        } catch (ParseException e) {
            if(aValue.Valor?.text() != "No Data"){
                log.error("Error parsing value from tag value:${aValue.Valor?.text()}")
            }
            Double.NaN
        }
    }

    private def fixDstDateParameter(Date aDate, boolean fixDst) {
        if (fixDst) {
            new Date(aDate.time - MS_IN_AN_HOUR)
        } else {
            aDate
        }
    }

    private def dateNodeTag(aDate, fixDst) {
        Date result = webserviceDateParser.parse(aDate.Data?.text())
        if(fixDst) {
            result.setTime(result.time + MS_IN_AN_HOUR)
        }
        
        return result
    }
    
    def isDst() {
        return saoPauloTZ.inDaylightTime( new Date() );
    }

    private def parseUrlArrayTags(String url,String params) {
        def res = [:]
        def dataRetrieve = false
        int numExecutions = 0
        boolean fixDst = isDst()

        def maxRetryService = grailsApplication.config.paap.servicepi.maxRetryService

        while (!dataRetrieve && numExecutions < maxRetryService) {
            numExecutions++
            def strXml = "N/A"
            try {
                // Prepara stream do XML
                HttpURLConnection httpCon = createHttpConnection(url)
                httpCon.setRequestMethod("POST");
                httpCon.setDoOutput(true)

                OutputStream os = httpCon.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(params)
                writer.flush()
                writer.close()
                os.close()

                InputStream stream
                boolean hasError = (httpCon.getResponseCode() >= 400)
                if (hasError) {
                    stream = httpCon.getErrorStream();
                } else {
                    stream = httpCon.getInputStream();
                }

                // Transforma stream num Reader de UTF-8
                Reader reader = new BufferedReader(new InputStreamReader(stream, "utf-8"))

                // Trata erro
                if(hasError) {
                    strXml = StringEscapeUtils.unescapeHtml(reader.text)
                    throw new IOException(strXml)
                }

                // Faz o parse (ou colocando todo o texto na memória ou via Reader)
                def xml
                if ( log.isTraceEnabled() ) {
                    // Baixa todo o XML e joga no LOG antes de fazer o parse
                    strXml = reader.text
                    log.trace(strXml)
                    xml = xmlParser().parseText(strXml)
                } else {
                    // Faz o parse sem armazenar na memória (direto do Reader)
                    xml = xmlParser().parse(reader)
                }

                xml?.Tag.each { tag ->
                    res.put(tag.Nome.text(), tag.Propriedades?.Propriedade?.findAll{!Double.isNaN(valueNodeTag(it))}.collect { propriedade ->
                        new TimeSeriesSample(time: dateNodeTag(propriedade, fixDst), value: valueNodeTag(propriedade))
                    } as Set)
                }
                dataRetrieve = true
            } catch (IOException e) {
                if( numExecutions < maxRetryService ) {
                    log.error("Erro ao obter dados do PI. Tentando novamente em 2s (${numExecutions} de ${maxRetryService})... (${e})")
                    Thread.sleep(2000)
                } else {
                    // Desiste da execução deste request
                    throw new IOException("Número máximo de tentativas de obter o dados do PI", e)
                }
            } catch (SAXParseException saxException) {
                log.error("Erro while parsing xml", saxException)
                log.error(strXml)
            }
        }
        return res
    }

	private def parseUrl(String url) {
		def res = []
		def dataRetrieve = false
		int numExecutions = 0
        boolean fixDst = isDst()

        def maxRetryService = grailsApplication.config.paap.servicepi.maxRetryService

        while (!dataRetrieve && numExecutions < maxRetryService) {
            numExecutions++
            def strXml = "N/A"
            try {
				// Prepara stream do XML
                HttpURLConnection httpCon = createHttpConnection(url)
                InputStream stream
                boolean hasError = (httpCon.getResponseCode() >= 400)
                if (hasError) {
                    stream = httpCon.getErrorStream();
                } else {
                    stream = httpCon.getInputStream();
                }

				// Transforma stream num Reader de UTF-8
                Reader reader = new BufferedReader(new InputStreamReader(stream, "utf-8"))

				// Trata erro
                if(hasError) {
                    strXml = StringEscapeUtils.unescapeHtml(reader.text)
                    throw new IOException(strXml)
                }

				// Faz o parse (ou colocando todo o texto na memória ou via Reader)
				def xml
                if ( log.isTraceEnabled() ) {
					// Baixa todo o XML e joga no LOG antes de fazer o parse
					strXml = reader.text
                    log.trace(strXml)
					xml = xmlParser().parseText(strXml)
                } else {
					// Faz o parse sem armazenar na memória (direto do Reader)
					xml = xmlParser().parse(reader)
				}

				xml?.Tag.each { tag ->
                    // Trata erro de tag inválida como exceção
                    def tagName = tag.Nome.text().trim()
                    if( tagName.startsWith("Exception:") ) {
                        throw new Exception(tagName.substring(10).trim())
                    }
					tag.Propriedades?.Propriedade?.collect { propriedade ->
						def v = valueNodeTag(propriedade)
						if ( !Double.isNaN(v) ) {
							res.push(new TimeSeriesSample(time: dateNodeTag(propriedade, fixDst), value: v))
						}
					}
				}
				dataRetrieve = true
			} catch (IOException e) {
				if( numExecutions < maxRetryService ) {
					log.error("Erro ao obter dados do PI. Tentando novamente em 2s (${numExecutions} de ${maxRetryService})... (${e})")
					Thread.sleep(2000)
				} else {
					// Desiste da execução deste request
					throw new IOException("Número máximo de tentativas de obter o dados do PI", e)
				}
			} catch (SAXParseException saxException) {
                log.error("Erro while parsing xml", saxException)
                log.error(strXml)
            }
		}
		return res as Set
	}

	def createHttpConnection(url) {
		URL location = new URL(url);
        HttpURLConnection httpCon = (HttpURLConnection) location.openConnection();
        log.debug("Configuring connectTimeout with ${grailsApplication.config.paap.socket.connectTimeout} and readTimeout with ${grailsApplication.config.paap.socket.readTimeout}")
        httpCon.setConnectTimeout(grailsApplication.config.paap.socket.connectTimeout)
        httpCon.setReadTimeout(grailsApplication.config.paap.socket.readTimeout)

		return httpCon
	}

    def xmlParser() {
        new XmlSlurper(false,false);

    }


	
	/*def readDataFromTagInCurrentPeriod(def tags) {
		if( tags?.size() > 0 ) {
			def tagsParams = "tags=" + tags.join("&tags=")
			def url = "${serviceUrl}GetCurrentValue?${tagsParams}&userKey=${grailsApplication.config.paap.servicepi.user}&userPass=${grailsApplication.config.paap.servicepi.password}"
			parseUrl(url)   // Result is in the same order of tags
		} else {
			[]
		}
	}*/
}
