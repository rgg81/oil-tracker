package br.com.petrobras.paap

import grails.transaction.Transactional
import br.com.petrobras.paap.timeseries.TimeSeriesSample

class ProcessTagService {

	def readPIService
    def grailsApplication

    def readDataFromLocation(TagPILocation tagLocation,Date begin, Date end){
        log.debug("end:${end}, begin:${begin}")
        def tags = TagPI.findPiTagsFromLocation(tagLocation)
        if (tags.isEmpty()) {
            return [:]
        }
        else {
            long startTime = System.currentTimeMillis()
            try {
                def samples = readPIService.readDataFromTagsInTimePeriodReturningError(tags*.formula, tagLocation, begin, end)
                long endTime = System.currentTimeMillis()
                log.debug("Carregado tags for type '${tagLocation}' do PI entre ${begin} e ${end} (em ${endTime-startTime}ms)...")
                return (samples?:[:])
            } catch (Exception e) {
                def thisMoment = new Date()
                tags.each { tagPI ->
                    new EventTagPI(date: thisMoment, tag:tagPI, eventType:EventType.WARNING,
                            msg: "PI Error: '${e.message}'. Cause: '${e.cause?.message}'").save()
                }

                return [:]
            }
        }
    }


    def transformAccumulatedSamples(accumulatedSamples,tagPI){
        def samples = []
        def lastSample = accumulatedSamples[0]
        for( def sample : accumulatedSamples ) {

            def diffTime = sample.time.getTime() - lastSample.time.getTime()
            if( diffTime >= grailsApplication.config.paap.processdata.MIN_MS_BETWEEN_SAMPLES ) {
                // Calcula o valor em valor/hora
                def diffValue = sample.value - lastSample.value

                // Ignora valores negativos (momento em que a Tag acumulada reinicia)
                if (diffValue >= 0) {
                    def snapshotValue = (diffValue / diffTime) * grailsApplication.config.paap.processdata.MS_IN_AN_HOUR
                    samples << new TimeSeriesSample(time: sample.time, value: snapshotValue)
                }else {
                    if(tagPI)
                        new EventTagPI(date: new Date(), tag:tagPI, eventType:EventType.INFO,
                            msg: "Negative value=${diffValue} for Acc. tag between ${sample.time} and ${lastSample.time}").save()
                }

                lastSample = sample
            } else {
                if(tagPI)
                    new EventTagPI(date: new Date(), tag:tagPI, eventType:EventType.INFO,
                        msg: "diffTime=${diffTime} on samples were shorter than ${grailsApplication.config.paap.processdata.MIN_MS_BETWEEN_SAMPLES}").save()
            }
        }
        return samples
    }

    def readLastValueFromLocation(TagPILocation tagLocation){        
        def tags = TagPI.findPiTagsFromLocation(tagLocation)
        if (tags.isEmpty()) {
            return [:]
        }
        else {
            long startTime = System.currentTimeMillis()
            try {
                def samples = readPIService.readDataFromTagsInTimePeriodReturningError(tags*.formula, tagLocation, begin, end)
                long endTime = System.currentTimeMillis()
                log.debug("Carregado tags for type '${tagLocation}' do PI entre ${begin} e ${end} (em ${endTime-startTime}ms)...")
                return (samples?:[:])
            } catch (Exception e) {
                def thisMoment = new Date()
                tags.each { tagPI ->
                    new EventTagPI(date: thisMoment, tag:tagPI, eventType:EventType.WARNING,
                            msg: "PI Error: '${e.message}'. Cause: '${e.cause?.message}'").save()
                }

                return [:]
            }
        }
    }

}
