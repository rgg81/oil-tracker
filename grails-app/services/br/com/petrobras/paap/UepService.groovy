package br.com.petrobras.paap

import grails.transaction.Transactional
import groovy.time.TimeCategory

class UepService {

    def shiroService

	def getListOfRegisteredAttributes(attribute) {
   		def result = UEP.createCriteria().list {
            projections {
                distinct(attribute)
            }
        }
    }

    def getUEPsWithPermissionForUser(user) {
        def userPermission = UserPermission.findAllByUsername(user).collect{
            [attribute: it.attribute, value: it.value]
        }
        def result = []
        def ueps = UEP.findAll()
        if (shiroService.isAdmin())
            return ueps
        userPermission.each() { permission ->
            ueps.each() { uep ->
                if (uep.(permission.attribute).toString() == permission.value) {
                    if (!result.contains(uep))
                        result.add(uep)
                }
            }
        }
        return result
    }

    def getUEPsWithPermission() {
        def user = shiroService.getLoggedUser()
        return getUEPsWithPermissionForUser(user)
    }

    boolean hasPermission(uep) {
        boolean hasPermission = false
        def user = shiroService.getLoggedUser()
        def userPermission = UserPermission.findAllByUsername(user).collect {
            [attribute: it.attribute, value: it.value]
        }

        if (shiroService.isAdmin())
            return true
        userPermission.each() { permission ->
            //Has to convert to string, the ID attribute is a long
            if (uep?.(permission.attribute).toString() == permission.value) {
                hasPermission = true
            }
        }
        return hasPermission
    }

    def snapShotAverage24hours(id) {
        UEP uep = UEP.get(id)

        def result = [:]

        if (uep ) {
            // Sitop/BDO
            def sitopBdo = SitopBdo.findByUep(UEP.get(id), [sort: 'sitopDate', order: 'desc'])
            if(sitopBdo){
                // Last 24h snapshots
                SnapshotSamples24h currentSnapshot = SnapshotSamples24h.findByTagAndDay(uep.tag,sitopBdo?.sitopDate)
                result.currentSnapshotValue = currentSnapshot?.value
                result.currentSnapshotDate = currentSnapshot?.date

                // 24 hours before
                def previousDate = new Date(sitopBdo?.sitopDate.time - 24 * 60 * 60 * 1000)

                SnapshotSamples24h previousSnapshot = previousDate ? SnapshotSamples24h.findByTagAndDay(uep.tag, previousDate) : null

                result.previousSnapshotValue = previousSnapshot?.value
                result.previousSnapshotDate = previousSnapshot?.date
            }
        }

        return result
    }

    def filterUEP(params) {
        def ueps = getUEPsWithPermission()
        def result = getAndGroupUEPS(ueps,params)
        return result
    }

    def getAllUEPs(params) {
        def ueps = UEP.findAll()
        def result = getAndGroupUEPS(ueps,params)
        return result
    }

    def getAndGroupUEPS(ueps,params) {
        def result = null
        if (params.filterValue && UEP.canFilter(params.filterKey)) {
            if (params.groupKey && UEP.canFilter(params.groupKey)) {
                ueps = ueps.findAll{it.(params.filterKey)==(params.filterValue)}
                    .groupBy({ uep ->
                        uep.(params.filterKey)})
                        .collect{ k, v ->
                            def values = v.groupBy({it.(params.groupKey)})
                                .collect{k2,v2 ->
                                    [name: k2, values:v2.sort{it.name}]
                                }
                            [name:k, values: params.groupKey == 'uo' ? values.sort{UEP.getUOPosition(it.name)} : values.sort{it.name}]
                        }
                if (ueps.values)
                    result = ueps.values.first()
            }
        }
        else {
             if(!params.filterKey || !UEP.canFilter(params.filterKey)){
                params.filterKey = 'uo'
            }
            ueps = ueps.findAll().groupBy({ uep -> uep.(params.filterKey)}).collect{k,v->
                [name:k, values: v.sort{it.name}]
            }

            result = params.filterKey == 'uo' ? ueps.sort{UEP.getUOPosition(it.name)} : ueps.sort{it.name}
        }
        return result
    }

    def findRelatedUeps(uep){
        def  children = uep.tag.possibleChildrenTags
        return findRelatedUepsFromTags(children).findAll{ it != uep }
    }

    def findRelatedUepsFromTags(childrenTags){
        return (childrenTags*.possibleParentTags.flatten()).collect{UEP.findByTag(it)}.findAll{it != null} as Set
    }

    @Transactional
    def delete(uep){
        uep.delete flush:true
    }

    @Transactional
    def save(uep){
        uep.save(flush:true)
    }

    def lastValue(UEP uep) {
        def tag = uep.tag
        if (tag.reloading) {
            return null
        }
        def sample = Sample.findByTag(tag, [sort:'date', order: 'desc'])
        return sample?.value != null ? Math.round(sample.value) : null
    }
}
