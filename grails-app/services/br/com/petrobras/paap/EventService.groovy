package br.com.petrobras.paap

import java.nio.file.AccessDeniedException

/**
 * Created by uq4e on 01/06/15.
 */
class EventService {
    def shiroService

    def eventsWarnings() throws AccessDeniedException {
        if ( shiroService.isAdmin() ) {
            return unsafeEventsWarnings()
        } else {
            throw new AccessDeniedException("User is not admin")
        }
    }

    // Runs without checking for admin user
    def unsafeEventsWarnings() {
        def warningEvents = EventTagPI.executeQuery("""
            from EventTagPI e1
            where e1.date = (select max(e2.date) from EventTagPI e2 where e1.tag = e2.tag and e2.eventType=?)
            and e1.eventType=?
            order by date desc""", [EventType.WARNING, EventType.WARNING])
        return warningEvents.collect {
            [tag: it.tag.formula, msg: it.msg, date: it.date]
        }
    }

}
