package br.com.petrobras.paap

import grails.transaction.Transactional


@Transactional
class UserPermissionService {

	def shiroService
	def uepService

   	def findUser(String login) {
		def result = []
	     try {
    		def users = [login]  //tirei o tokenize, por enquanto só um mesmo
    		users.each() { user ->
		    	def userDetails = shiroService.userDetails(user)
				def permissions = UserPermission.findAllByUsername(user).groupBy {
				    permission -> permission.attribute
				}.collectEntries { k, v->
				    [(k):v.(value)]
				}
				result.add([username: user.toUpperCase(), name: userDetails.name,permissions:permissions])
			}
        } catch(IllegalArgumentException e) {
            log.info e.message, e
        }
		return result
    }

    def findUsersByAttribute(String attribute, String value) {
		def result = []
		try {
			def users = UserPermission.findAllByAttributeAndValue(attribute,value)

			users.each() { user ->
					def userDetails = shiroService.userDetails(user.username)
					result.add([username: user.username.toUpperCase(), name: userDetails.name])
			}
		} catch(IllegalArgumentException  e) {
          	log.info e.message, e
      	}
		return result
  	}

  	def updateUserPermissions(params) {
        def username = params.username.toUpperCase()
        updateUserUoPermissions(params)
        updateUserUEPPermissions(params)
        return UserPermission.findAllByUsername(username).value
  	}

    def updateUserUoPermissions(params) {
        def uos = uepService.getListOfRegisteredAttributes("uo")
        def username = params.username.toUpperCase()
        uos.each { uo ->
            def userPermission = new UserPermission(username:username, attribute: "uo", value: uo)
            if (!UserPermission.find(userPermission)) {
                if (params[uo] =="on") {
                    userPermission.save flush:true
                }
            } else {
                if (params[uo] != "on")
                    UserPermission.find(userPermission).delete flush:true
            }
        }
    }

    def updateUserUEPPermissions(params) {
        def username = params.username.toUpperCase()
        def ueps = UEP.findAll()
        ueps.each { uep ->
            def userPermission = new UserPermission(username:username, attribute: "id", value: uep.id)
            if (!UserPermission.find(userPermission)) {
                if (params["uep-"+uep.id] =="on") {
                    userPermission.save flush:true
                }
            } else {
                if (params["uep-"+uep.id] != "on")
                    UserPermission.find(userPermission).delete flush:true
            }
        }
    }

  	def updateUserRolePermissions(username) {
  		if (shiroService.userDetails(username)) {
	  		def permissions = UserPermission.findAllByUsername(username)
	  		def result = [:]
	        if (permissions) {
	            if (!shiroService.hasAccess(username)) {
	                shiroService.grantAccess(username)                
	            }
	            result["hasAccess"] = true
	        }
	        else {
	            if (shiroService.hasAccess(username)) {
	                shiroService.revokeAccess(username)
	            }
	            result["hasAccess"] = false
	        }
	        result["notFound"] = false
	        return result
    	} else {
    		return [hasAccess: false, notFound: true]
    	}
  	}

  	def removeUserPermissionsBatch(params) {
  		def removedUsers = []
		  params.users.each { user ->
            def username = user.toUpperCase()
            def userPermission = new UserPermission(username:username, attribute: params.attribute, value: params.value)
            if (UserPermission.find(userPermission)) {
            	UserPermission.find(userPermission)?.delete(flush:true)
            	removedUsers.add([username:username, name: shiroService.userDetails(username.toUpperCase()).name])
            	updateUserRolePermissions(username)
        	}
        }
        return removedUsers
  	}

  	def addPermission(params) {
  		try {  			
        	def username = params.username.toUpperCase()
			def userDetails = shiroService.userDetails(username)
	        def userPermission = new UserPermission(username:username, attribute: params.attribute, value: params.value)

	        if (!UserPermission.find(userPermission)) {
	            userPermission.save flush:true
	            updateUserRolePermissions(username)
	            return [success:true, username: username, name:userDetails.name]
	        } else {
	        	return [success:false, username: username, name:userDetails.name]
	        }
	    } catch(IllegalArgumentException  e) {
          	log.info e.message, e
      	} 
  	}

  	def addAdmin(username) {
  		try {
            def userDetails = shiroService.userDetails(username)
            def name = userDetails.name
            if (!shiroService.hasAdminAccess(username)) {
                shiroService.grantAdminAccess(username)
            }
            if (!shiroService.hasAdminAccess(username))
            	return [adminAcess: false, username: username, name: name]
            else 
            	return [adminAcess: true, username: username, name: name]        
        } catch(IllegalArgumentException  e) {
            log.info e.message, e
        }
    }

    def addManager(username) {
      try {
            def userDetails = shiroService.userDetails(username)
            def name = userDetails.name
            if (!shiroService.hasManagerAccess(username)) {
                shiroService.grantManagerAccess(username)
            }
            if (!shiroService.hasManagerAccess(username))
              return [managerAcess: false, username: username, name: name]
            else 
              return [managerAcess: true, username: username, name: name]        
        } catch(IllegalArgumentException  e) {
            log.info e.message, e
        }
    }

    def removeAdmins(params) {
    	def removedUsers = []
     	params.users.each { user ->
            def username = user.toUpperCase()
            if (shiroService.hasAdminAccess(username)) {
                shiroService.revokeAdminAccess(username)
                removedUsers.add([username:username, name: shiroService.userDetails(username.toUpperCase()).name]);
            }
        }
        return removedUsers
    }

    def removeManagers(params) {
      def removedUsers = []
      params.users.each { user ->
            def username = user.toUpperCase()
            if (shiroService.hasManagerAccess(username)) {
                shiroService.revokeManagerAccess(username)
                removedUsers.add([username:username, name: shiroService.userDetails(username.toUpperCase()).name]);
            }
        }
        return removedUsers
    }
}
