package br.com.petrobras.paap

import grails.converters.JSON

import javax.jms.TextMessage
import javax.jms.TopicConnection
import javax.jms.TopicPublisher
import javax.jms.TopicSession

import br.com.petrobras.paap.ws.WSMessageType
import br.com.petrobras.paap.ws.WSMessage

/**
 * Created by uq4e on 16/06/15.
 */
class WSMessageService {
    def jmsConnectionFactory
    def jmsTopicNewPiData
    def piDataService
    def eventService

    private def sendMessage(WSMessage wsmessage) {

        // Inicia sessão JMS
        TopicConnection con = jmsConnectionFactory.createTopicConnection()

        try {
            TopicSession session = con.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE)
            TopicPublisher publisher = session.createPublisher(jmsTopicNewPiData)
            TextMessage message = session.createTextMessage();

            def json = wsmessage.build() as JSON

            message.setText(json.toString())
            publisher.publish(message)
        } finally {
            con.close()
        }
    }

    def sendEvent(WSMessageType type, eventData = null) {
        sendMessage(new WSMessage(piDataService,eventService).type(type).data(eventData))
    }

}
