package br.com.petrobras.paap

import grails.transaction.Transactional
import org.apache.shiro.SecurityUtils
import br.com.petrobras.security.ISecurityContext
import br.com.petrobras.security.management.access.IRoleManager
import br.com.petrobras.security.exception.*

@Transactional
class ShiroService {

    def grailsApplication

    boolean isAuthenticated(){
        return SecurityUtils?.subject?.isAuthenticated()
    }

    String getLoggedUser(){
        def user = userInfo
        log.info "******** loggedUser, chave = ${user?.login}"
        return user?.login?.toUpperCase()
    }

    private getUserInfo() {
        def currentUser = SecurityUtils.subject
        return currentUser?.principals?.oneByType(br.com.petrobras.security.model.User.class)
    }

    boolean isAdmin() {
        return SecurityUtils?.subject?.hasRole(grailsApplication.config.app.role.admin)
    }

    boolean isManager() {
        return SecurityUtils?.subject?.hasRole(grailsApplication.config.app.role.manager)
    }

    def userDetails(String username) {
        try {
            return ISecurityContext.context.userManager.find(username)
        } catch(ObjectNotFoundException e) {
            log.info e.message, e
            throw new IllegalArgumentException("Usuário ${username} não encontrado.", e)
        } catch(Exception e) {
            log.info e.message, e
            throw new RuntimeException("Erro ao obter dados do usuário de chave ${username}. ", e)
        }
    }

    boolean hasAccess(username) {
        return ISecurityContext.context.roleAuthorizer.isAuthorizedToUser(username,grailsApplication.config.app.role.paapUser);
    }

    def grantAccess(String username) {
        return ISecurityContext.context.userRoleAuthorizationManager.grant(username, grailsApplication.config.app.role.paapUser)
    }

    def revokeAccess(String username) {
        return ISecurityContext.context.userRoleAuthorizationManager.revoke(username, grailsApplication.config.app.role.paapUser)
    }

    def grantAdminAccess(String username) {
      return ISecurityContext.context.userRoleAuthorizationManager.grant(username, grailsApplication.config.app.role.admin)
    }

    def revokeAdminAccess(String username) {
        return ISecurityContext.context.userRoleAuthorizationManager.revoke(username, grailsApplication.config.app.role.admin)
    }

    boolean hasAdminAccess(username) {
      return ISecurityContext.context.roleAuthorizer.isAuthorizedToUser(username,grailsApplication.config.app.role.admin);
    }

    def grantManagerAccess(String username) {
      return ISecurityContext.context.userRoleAuthorizationManager.grant(username, grailsApplication.config.app.role.manager)
    }

    def revokeManagerAccess(String username) {
        return ISecurityContext.context.userRoleAuthorizationManager.revoke(username, grailsApplication.config.app.role.manager)
    }

    boolean hasManagerAccess(username) {
      return ISecurityContext.context.roleAuthorizer.isAuthorizedToUser(username,grailsApplication.config.app.role.manager);
    }

    def admins() {
        def role = ISecurityContext.context.roleManager.find(grailsApplication.config.app.role.admin)
        def users = ISecurityContext.context.userRoleAuthorizationManager.findAllWithRole(role).user.login
        return users
    }

    def managers() {
        def role = ISecurityContext.context.roleManager.find(grailsApplication.config.app.role.manager)
        def users = ISecurityContext.context.userRoleAuthorizationManager.findAllWithRole(role).user.login
        return users
    }


}
