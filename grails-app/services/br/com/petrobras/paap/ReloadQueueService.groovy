package br.com.petrobras.paap

import grails.converters.JSON
import groovy.json.JsonSlurper

import javax.jms.*

class ReloadQueueService {

    static final int receiveTimeout = 10

    def jmsConnectionFactory
    def jmsQueueResetTag
    def jmsQueueResetTagDone

    def browse() {
        def tags = [:]
        // Inicia sessão JMS
        QueueConnection con = jmsConnectionFactory.createQueueConnection()
        try {
            log.debug "verify tag is reloading"
            con.start();
            QueueSession session = con.createQueueSession(false, Session.AUTO_ACKNOWLEDGE)
            QueueBrowser browser = session.createBrowser(jmsQueueResetTag)

            Enumeration msgs = browser.getEnumeration()
            while (msgs.hasMoreElements()) {
                Message message = (Message)msgs.nextElement()
                Map jsonMap = new JsonSlurper().parseText(message.text)
                jsonMap = jsonMap.inject( [:] ) { map, v -> map[v.key.toLong()] = v.value ; map }

                log.debug "json ${jsonMap}"
                tags = tags + jsonMap
            }
        }finally {
            con.close()
        }
        log.debug "tags in queue : ${tags}"
        tags
    }

     private def push(queue,Set<TagPI> tags){
        // Inicia sessão JMS
        QueueConnection con = jmsConnectionFactory.createQueueConnection()

        try {
            QueueSession session = con.createQueueSession(false, Session.AUTO_ACKNOWLEDGE)
            MessageProducer producer = session.createProducer(queue);
            TextMessage message = session.createTextMessage()
            def json = tags.collect{it.id} as JSON
           
            message.setText(json.toString())
            log.debug("Sending message to reload queue ${json.toString()}")
            producer.send(message)
        } catch(Exception e ){
            log.error("notifyReloadingTag tags(${tags}) failed: ${e.message}", e)
            throw new RuntimeException("notifyReloadingTag tags(${tags}) failed: ${e.message}", e)
        }finally {
            con.close()
        }
    }

    private def read(queue,Closure c,threshold=999999){
        def tags = []

        // Inicia sessão JMS
        QueueConnection con 
        QueueSession session
        try {
            con = jmsConnectionFactory.createQueueConnection()

            log.debug "starting receiving tag reset messages"
            con.start();

            session = con.createQueueSession(true, Session.SESSION_TRANSACTED)
            MessageConsumer consumer = session.createConsumer(queue)
            
            Message message = consumer.receive(receiveTimeout)
            while(message != null){
                log.debug "message received : " + message.text
                List<Long> ids = new JsonSlurper().parseText(message.text)
                if(ids == null || ids.isEmpty()){
                    throw new IllegalStateException("wrong message from queue : ${message.text}")
                }               

                tags = tags + ids
                if(tags.size() >= threshold)
                    break;
                
                message = consumer.receive(receiveTimeout)
            }

            c.call(tags)
            session.commit()
        }catch(Exception e ){
            session.rollback()
            log.error("rolling back read jms, tags ${tags}",e)
        }finally {
            if(con!=null)
                con.close()
        }
    }
    def pushToStartQueue(Set<TagPI> tags){
        push(jmsQueueResetTag,tags)
    }

    def readFromStartQueueAndDo(Closure c){
        return read(jmsQueueResetTag, c, 30)
    }

    def pushToEndQueue(Set<TagPI> tags){
        push(jmsQueueResetTagDone,tags)
    }

    def readFromEndQueueAndDo(Closure c){
        return read(jmsQueueResetTagDone,c)
    }
}
