package br.com.petrobras.paap

import groovy.time.TimeCategory

/**
 * Created by uq4e on 02/09/15.
 */
class SitopBdoService {

    def lastSitopBdo(boolean weekly) {
        use(TimeCategory) {
            queryWithParams('WHERE sb.sitopDate >= :since', [since: new java.sql.Date((weekly ? 1.week.ago : 2.months.ago).time)])
        }
    }

    def sitopBdo(max, offset) {
        queryWithParams('', [max: max, offset: offset])
    }

    private queryWithParams(whereCondition, sqlParams) {
        String query = """
            SELECT sb.uep.department, sb.uep.uo, sb.uep.name, sb.uep.id, sb.sitopDate, sb.expected, sb.preliminary,
                (SELECT value FROM SnapshotSamples24h s24h WHERE s24h.tag = sb.uep.tag AND sb.sitopDate = s24h.date)
            FROM SitopBdo sb
            ${whereCondition}
            ORDER BY sb.sitopDate DESC, sb.uep.department, sb.uep.uo, sb.uep.name
        """


        SitopBdo.executeQuery(query, sqlParams).collect {
            [
                uepDepartment: it[0],
                uepUo: it[1],
                uepName: it[2],
                uepId: it[3],
                sitopDate: it[4],
                expected: it[5],
                preliminary: it[6],
                snapshot24h: it[7]
            ]
        }
    }
}
