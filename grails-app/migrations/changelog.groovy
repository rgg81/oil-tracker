databaseChangeLog = {

    changeSet(author: "roberto (generated)", id: "1426619877079-1") {
        createTable(tableName: "sample") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "samplePK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "sample_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "value", type: "double precision") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-2") {
        createTable(tableName: "sample_group10m") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sample_group10mPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "sample_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "max", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "mean", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "min", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "num", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "sum", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "variance", type: "double precision") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-3") {
        createTable(tableName: "sample_group1h") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sample_group1hPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "sample_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "max", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "mean", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "min", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "num", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "sum", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "variance", type: "double precision") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-4") {
        createTable(tableName: "sample_group24h") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sample_group24hPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "sample_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "max", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "mean", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "min", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "num", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "sum", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "variance", type: "double precision") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-5") {
        createTable(tableName: "sample_group2m") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sample_group2mPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "sample_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "max", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "mean", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "min", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "num", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "sum", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "variance", type: "double precision") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-6") {
        createTable(tableName: "sample_group6h") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sample_group6PK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "sample_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "max", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "mean", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "min", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "num", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "sum", type: "double precision") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "variance", type: "double precision") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-7") {
        createTable(tableName: "tagpi") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "tagpiPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "formula", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "tag_type", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-8") {
        createTable(tableName: "uep") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "uepPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "asset", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "department", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "ocean_current", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "oil_well", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "uo", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-16") {
        createIndex(indexName: "unique_tag_id", tableName: "sample", unique: "true") {
            column(name: "sample_date")

            column(name: "tag_id")
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-17") {
        createIndex(indexName: "name_uniq_1426619859609", tableName: "tagpi", unique: "true") {
            column(name: "name")
        }
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-9") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "sample", constraintName: "FK_ruaqqfihiuu7tpx8dvl63idh9", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-10") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "sample_group10m", constraintName: "FK_qoo1i6awy9mra0r0al2o83cm0", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-11") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "sample_group1h", constraintName: "FK_p2hsiu3hmkm9s9gmfr73gx1", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-12") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "sample_group24h", constraintName: "FK_mi3obvfggdlht69rcc1ml852k", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-13") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "sample_group2m", constraintName: "FK_666wvdvt2l6gdcc0orvdbcrif", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-14") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "sample_group6h", constraintName: "FK_iw9njbj7rws02860eln3rp3sj", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "roberto (generated)", id: "1426619877079-15") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "uep", constraintName: "FK_eljyme3lkgcplhsc387xj2vjl", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "up2h", id:"create-sequence") {
        sql("""
            CREATE SEQUENCE  "PAAP"."HIBERNATE_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2335601 CACHE 20 NOORDER  NOCYCLE ;
        """)
    }

    changeSet(author: "roberto", id: "grants") {
        sql("""
		grant all on SAMPLE to paap_aplicacao;
		grant all on SAMPLE_GROUP10M to paap_aplicacao;
		grant all on SAMPLE_GROUP1H to paap_aplicacao;
		grant all on SAMPLE_GROUP24H to paap_aplicacao;
		grant all on SAMPLE_GROUP2M to paap_aplicacao;
		grant all on SAMPLE_GROUP6H to paap_aplicacao;
		grant all on TAGPI to paap_aplicacao;
		grant all on UEP to paap_aplicacao;
		grant all on HIBERNATE_SEQUENCE to paap_aplicacao;
	  	""")
    }

    changeSet(author: "upn1", id: "create-all-synonym") {
        sql("""
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."SAMPLE" FOR "PAAP"."SAMPLE";
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."SAMPLE_GROUP10M" FOR "PAAP"."SAMPLE_GROUP10M";
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."SAMPLE_GROUP1H" FOR "PAAP"."SAMPLE_GROUP1H";
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."SAMPLE_GROUP24H" FOR "PAAP"."SAMPLE_GROUP24H";
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."SAMPLE_GROUP2M" FOR "PAAP"."SAMPLE_GROUP2M";
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."SAMPLE_GROUP6H" FOR "PAAP"."SAMPLE_GROUP6H";
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."TAGPI" FOR "PAAP"."TAGPI";
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."UEP" FOR "PAAP"."UEP";
		CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."HIBERNATE_SEQUENCE" FOR "PAAP"."HIBERNATE_SEQUENCE";
	  	""")
    }

    /***************************************************
     * Cria tabelas para Filtros
     ***************************************************/
    changeSet(author: "uq4e (generated)", id: "1428514991387-1") {
        createTable(tableName: "tagpifilter") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "tagpifilterPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "filter_order", type: "number(10,0)") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "class", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "samples_sum", type: "double precision")

            column(name: "window_in_ms", type: "number(19,0)")

            column(name: "last_date", type: "timestamp")

            column(name: "last_value", type: "double precision")

            column(name: "measure_variance", type: "double precision")

            column(name: "minusp", type: "double precision")

            column(name: "paramk", type: "double precision")

            column(name: "paramp", type: "double precision")

            column(name: "process_variance", type: "double precision")

            column(name: "xhatminus", type: "double precision")
        }
    }

    changeSet(author: "uq4e (generated)", id: "1428514991387-2") {
        createTable(tableName: "tagpifilter_moving_avg_sample") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "tagpifilter_mPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "moving_average_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "sample_time", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "value", type: "double precision") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "uq4e (generated)", id: "1428514991387-3") {
        createIndex(indexName: "unique_filter_order", tableName: "tagpifilter", unique: "true") {
            column(name: "tag_id")

            column(name: "filter_order")
        }
    }

    changeSet(author: "uq4e (generated)", id: "1428514991387-4") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "tagpifilter", constraintName: "FK_p8cyyqhgoanqml3oh8l75xqwd", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "uq4e (generated)", id: "1428514991387-5") {
        addForeignKeyConstraint(baseColumnNames: "moving_average_id", baseTableName: "tagpifilter_moving_avg_sample", constraintName: "FK_jqall0e25d1mgr3w4eritku6y", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpifilter", referencesUniqueColumn: "false")
    }

    changeSet(author: "up2h", id: "create-synonym-tagpi-filters") {
        sql("""
        grant all on TAGPIFILTER to paap_aplicacao;
        grant all on TAGPIFILTER_MOVING_AVG_SAMPLE to paap_aplicacao;
        CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."TAGPIFILTER" FOR "PAAP"."TAGPIFILTER";
        CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."TAGPIFILTER_MOVING_AVG_SAMPLE" FOR "PAAP"."TAGPIFILTER_MOVING_AVG_SAMPLE";
        """)
    }

    // Coluna de localização da tag
    changeSet(author: "uq4e (generated)", id: "1429135983041-1") {
        addColumn(tableName: "tagpi") {
            column(name: "tag_location", type: "varchar2(255 char)")
        }
    }

    // Corrige bug no double do oracle
    changeSet(author: "uq4e (generated)", id: "1429547146362-1") {
        modifyDataType(columnName: "VALUE", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-2") {
        modifyDataType(columnName: "MAX", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP10M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-3") {
        modifyDataType(columnName: "MEAN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP10M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-4") {
        modifyDataType(columnName: "MIN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP10M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-5") {
        modifyDataType(columnName: "NUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP10M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-6") {
        modifyDataType(columnName: "SUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP10M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-7") {
        modifyDataType(columnName: "VARIANCE", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP10M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-8") {
        modifyDataType(columnName: "MAX", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP1H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-9") {
        modifyDataType(columnName: "MEAN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP1H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-10") {
        modifyDataType(columnName: "MIN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP1H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-11") {
        modifyDataType(columnName: "NUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP1H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-12") {
        modifyDataType(columnName: "SUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP1H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-13") {
        modifyDataType(columnName: "VARIANCE", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP1H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-14") {
        modifyDataType(columnName: "MAX", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP24H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-15") {
        modifyDataType(columnName: "MEAN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP24H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-16") {
        modifyDataType(columnName: "MIN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP24H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-17") {
        modifyDataType(columnName: "NUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP24H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-18") {
        modifyDataType(columnName: "SUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP24H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-19") {
        modifyDataType(columnName: "VARIANCE", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP24H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-20") {
        modifyDataType(columnName: "MAX", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP2M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-21") {
        modifyDataType(columnName: "MEAN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP2M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-22") {
        modifyDataType(columnName: "MIN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP2M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-23") {
        modifyDataType(columnName: "NUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP2M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-24") {
        modifyDataType(columnName: "SUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP2M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-25") {
        modifyDataType(columnName: "VARIANCE", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP2M")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-26") {
        modifyDataType(columnName: "MAX", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP6H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-27") {
        modifyDataType(columnName: "MEAN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP6H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-28") {
        modifyDataType(columnName: "MIN", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP6H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-29") {
        modifyDataType(columnName: "NUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP6H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-30") {
        modifyDataType(columnName: "SUM", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP6H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-31") {
        modifyDataType(columnName: "VARIANCE", newDataType: "BINARY_DOUBLE", tableName: "SAMPLE_GROUP6H")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-32") {
        modifyDataType(columnName: "LAST_VALUE", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-33") {
        modifyDataType(columnName: "MEASURE_VARIANCE", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-34") {
        modifyDataType(columnName: "MINUSP", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-35") {
        modifyDataType(columnName: "PARAMK", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-36") {
        modifyDataType(columnName: "PARAMP", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-37") {
        modifyDataType(columnName: "PROCESS_VARIANCE", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-38") {
        modifyDataType(columnName: "SAMPLES_SUM", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-39") {
        modifyDataType(columnName: "XHATMINUS", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER")
    }

    changeSet(author: "uq4e (generated)", id: "1429547146362-40") {
        modifyDataType(columnName: "VALUE", newDataType: "BINARY_DOUBLE", tableName: "TAGPIFILTER_MOVING_AVG_SAMPLE")
    }

    changeSet(author: "up2h (generated)", id: "1430778359611-1") {
        createTable(tableName: "process_status") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "process_statuPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "end_time", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "msg", type: "varchar2(255 char)")

            column(name: "start_time", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "status", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "type", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "up2h", id: "create-synonym-process-status") {
        sql("""
        grant all on PROCESS_STATUS to paap_aplicacao;
        CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."PROCESS_STATUS" FOR "PAAP"."PROCESS_STATUS";
        """)
    }

    changeSet(author: "valente (generated)", id: "1431103178622-1") {
        createTable(tableName: "sitop_bdo") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sitop_bdoPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "expected", type: "BINARY_DOUBLE") {
                constraints(nullable: "false")
            }

            column(name: "preliminary", type: "BINARY_DOUBLE") {
                constraints(nullable: "false")
            }

            column(name: "sitop_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "uep_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "valente (generated)", id: "1431103178622-27") {
        createIndex(indexName: "unique_uep_id", tableName: "sitop_bdo", unique: "true") {
            column(name: "sitop_date")

            column(name: "uep_id")
        }
    }

    changeSet(author: "valente (generated)", id: "1431103178622-7") {
        addForeignKeyConstraint(baseColumnNames: "uep_id", baseTableName: "sitop_bdo", constraintName: "FK_ikb57jb1ne8599kslwsq4i01t", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "uep", referencesUniqueColumn: "false")
    }

    changeSet(author: "valente", id: "create-synonym-sitop-bdo") {
        sql("""
        grant all on SITOP_BDO to paap_aplicacao;
        CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."SITOP_BDO" FOR "PAAP"."SITOP_BDO";
        """)
    }


    changeSet(author: "upn1", id: "1431105988025-1") {
        createTable(tableName: "job_last_date_run") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "job_last_datePK")
            }
            column(name: "last_update_date", type: "timestamp") {
                constraints(nullable: "false")
            }
        }
    }


    changeSet(author: "upn1", id: "create-synonym-job_last_date_run") {
        sql("""
        grant all on JOB_LAST_DATE_RUN to paap_aplicacao;
        CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."JOB_LAST_DATE_RUN" FOR "PAAP"."JOB_LAST_DATE_RUN";
        """)
    }

    changeSet(author: "upn1", id: "1431115785535-1") {
        addColumn(tableName: "job_last_date_run") {
            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "uq4n", id: "Change.column.type.from.varchar.to.clob") {
        sql("""
        ALTER TABLE PROCESS_STATUS RENAME COLUMN MSG to MSG_OLD
        """)
        sql("""
        ALTER TABLE PROCESS_STATUS ADD (MSG CLOB)
        """)
        sql("""
        ALTER TABLE PROCESS_STATUS DROP COLUMN MSG_OLD 
        """)
    }

    changeSet(author: "upn1", id: "1431606448868-1") {
        addColumn(tableName: "tagpifilter") {
            column(name: "cold_start_total", type: "number(10,0)")
        }
    }

    changeSet(author: "valente (generated)", id: "1431977614641-1") {
    	addColumn(tableName: "tagpifilter") {
    		column(name: "max", type: "BINARY_DOUBLE")
    	}
    }

    changeSet(author: "valente (generated)", id: "1431977614641-2") {
    	addColumn(tableName: "tagpifilter") {
    		column(name: "min", type: "BINARY_DOUBLE")
    	}
    }
    changeSet(author: "valente (generated)", id: "1432325653915-26") {
        createIndex(indexName: "tag_id_uniq_1432325623851", tableName: "uep", unique: "true") {
                column(name: "tag_id")
        }
    }

    changeSet(author: "valente (generated)", id: "1432557807422-1") {
        addColumn(tableName: "uep") {
            column(name: "alert_percent", type: "BINARY_DOUBLE")
        }
    }

    changeSet(author: "valente (generated)", id: "1432557807422-2") {
        addColumn(tableName: "uep") {
            column(name: "long_window", type: "number(10,0)")
        }
    }

    changeSet(author: "valente (generated)", id: "1432557807422-3") {
        addColumn(tableName: "uep") {
            column(name: "short_window", type: "number(10,0)")
        }
    }

    /***************************************************
     * Cria tabelas para Eventos
     ***************************************************/
    changeSet(author: "uq4e (generated)", id: "1433181525720-1") {
        createTable(tableName: "event") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "eventPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "event_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "event_type", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "msg", type: "long") {
                constraints(nullable: "false")
            }

            column(name: "class", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)")

            column(name: "uep_id", type: "number(19,0)")
        }
    }

    changeSet(author: "uq4e (generated)", id: "1433181525720-29") {
        createSequence(sequenceName: "event_sequence")
    }

    changeSet(author: "uq4e (generated)", id: "1433181525720-8") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "event", constraintName: "FK_1861p7v3rmg0lv7ejmn1fy50h", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "uq4e (generated)", id: "1433181525720-9") {
        addForeignKeyConstraint(baseColumnNames: "uep_id", baseTableName: "event", constraintName: "FK_1ldbsi3u0xgqbjipul3fv9fny", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "uep", referencesUniqueColumn: "false")
    }

    changeSet(author: "uq4e", id: "create-synonym-event") {
        sql("""
        grant all on EVENT to paap_aplicacao;
        grant all on EVENT_SEQUENCE to paap_aplicacao;
        CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."EVENT" FOR "PAAP"."EVENT";
        CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."EVENT_SEQUENCE" FOR "PAAP"."EVENT_SEQUENCE";
        """)
    }

    changeSet(author: "uq4e (generated)", id: "1433363921745-26") {
        createIndex(indexName: "idx_event_date", tableName: "event") {
            column(name: "event_date")

            column(name: "tag_id")
        }
    }

    changeSet(author: "uq4e (generated)", id: "1433363921745-27") {
        createIndex(indexName: "idx_eventtagpi_tag", tableName: "event") {
            column(name: "tag_id")
        }
    }

    changeSet(author: "uq4e (generated)", id: "1433363921745-28") {
        createIndex(indexName: "idx_event_class_type", tableName: "event") {
            column(name: "class")
            column(name: "event_type")
        }
    }

    changeSet(author: "upn1", id: "change-column-size-tagpi-formula") {
        sql("""
        alter table tagpi modify (formula varchar2(1000));
        """)
    }
    
    changeSet(author: "valente (generated)", id: "1434735007853-1") {
            addColumn(tableName: "tagpi") {
                    column(name: "reloading", type: "number(1,0)")
            }
    }

    changeSet(author: "up2h (generated)", id: "1435265001421-1") {
		createTable(tableName: "user_permission") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "user_permissiPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "attribute", type: "varchar2(10 char)") {
				constraints(nullable: "false")
			}

			column(name: "username", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}

			column(name: "value", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "up2h (generated)", id: "1435265001421-28") {
		createIndex(indexName: "unique_username", tableName: "user_permission", unique: "true") {
			column(name: "value")

			column(name: "attribute")

			column(name: "username")
		}
	}

	changeSet(author: "up2h", id: "create-user_permission-synonym") {
        sql("""
        	grant all on USER_PERMISSION to paap_aplicacao;
			CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."USER_PERMISSION" FOR "PAAP"."USER_PERMISSION";
		""")
	}
    changeSet(author: "roberto (generated)", id: "1435346845564-1") {
        createTable(tableName: "snapshot_samples24h") {
            column(name: "id", type: "number(19,0)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "snapshot_sampPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "sample_date", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "tag_id", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "value", type: "BINARY_DOUBLE") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "roberto (generated)", id: "1435346845564-30") {
        createIndex(indexName: "unique_tag_id_snapshot", tableName: "snapshot_samples24h", unique: "true") {
            column(name: "sample_date")
            column(name: "tag_id")
        }
    }

    changeSet(author: "roberto (generated)", id: "1435346845564-8") {
        addForeignKeyConstraint(baseColumnNames: "tag_id", baseTableName: "snapshot_samples24h", constraintName: "FK_2ht5v7g42hi0bfruq5v91o0y8", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "tagpi", referencesUniqueColumn: "false")
    }

    changeSet(author: "roberto (generated)", id: "create-synonym-event") {
        sql("""
        grant all on SNAPSHOT_SAMPLES24H to paap_aplicacao;
        CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."SNAPSHOT_SAMPLES24H" FOR "PAAP"."SNAPSHOT_SAMPLES24H";
        """)
    }
    changeSet(author: "valente (generated)", id: "1436815867038-1") {
            addColumn(tableName: "tagpi") {
                    column(name: "waiting_to_reload", type: "number(1,0)")
            }
    }
    changeSet(author: "up2h (generated)", id: "1438280041104-1") {
        createTable(tableName: "Online_User") {
            column(name: "session_id", type: "varchar2(255 char)") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "Online_UserPK")
            }

            column(name: "version", type: "number(19,0)") {
                constraints(nullable: "false")
            }

            column(name: "login", type: "varchar2(255 char)") {
                constraints(nullable: "false")
            }
        }
    }
    changeSet(author: "up2h", id: "create-online_user-synonym") {
        sql("""
            grant all on ONLINE_USER to paap_aplicacao;
            CREATE OR REPLACE SYNONYM "PAAP_APLICACAO"."ONLINE_USER" FOR "PAAP"."ONLINE_USER";
        """)
    }

    // Remove coluna desnecessáriao para nova média móvel
    changeSet(author: "uq4e (generated)", id: "1438953978835-27") {
        dropColumn(columnName: "SAMPLES_SUM", tableName: "TAGPIFILTER")
    }

    // Adiciona o campo WI na UEP
    changeSet(author: "uq4e (generated)", id: "1441827489984-1") {
        addColumn(tableName: "uep") {
            column(name: "wi", type: "BINARY_DOUBLE")
        }
    }

    changeSet(author: "valente (generated)", id: "1442591608464-1") {
        sql("""
            ALTER TABLE tagpi ADD reload_status VARCHAR2(255 char) DEFAULT 'DONE' NOT NULL;
        """);
    }

    changeSet(author: "valente (generated)", id: "1442591608464-28") {
            dropColumn(columnName: "WAITING_TO_RELOAD", tableName: "TAGPI")
    }

}
