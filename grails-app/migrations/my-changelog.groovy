databaseChangeLog = {

	changeSet(author: "roberto (generated)", id: "1435855827581-1") {
		modifyDataType(columnName: "MSG", newDataType: "long", tableName: "PROCESS_STATUS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-2") {
		dropForeignKeyConstraint(baseTableName: "QRTZ_BLOB_TRIGGERS", baseTableSchemaName: "PAAP", constraintName: "QRTZ_BLOB_TRIG_TO_TRIG_FK")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-3") {
		dropForeignKeyConstraint(baseTableName: "QRTZ_CRON_TRIGGERS", baseTableSchemaName: "PAAP", constraintName: "QRTZ_CRON_TRIG_TO_TRIG_FK")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-4") {
		dropForeignKeyConstraint(baseTableName: "QRTZ_SIMPLE_TRIGGERS", baseTableSchemaName: "PAAP", constraintName: "QRTZ_SIMPLE_TRIG_TO_TRIG_FK")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-5") {
		dropForeignKeyConstraint(baseTableName: "QRTZ_SIMPROP_TRIGGERS", baseTableSchemaName: "PAAP", constraintName: "QRTZ_SIMPROP_TRIG_TO_TRIG_FK")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-6") {
		dropForeignKeyConstraint(baseTableName: "QRTZ_TRIGGERS", baseTableSchemaName: "PAAP", constraintName: "QRTZ_TRIGGER_TO_JOBS_FK")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-7") {
		dropIndex(indexName: "IDX_EVENT_CLASS_TYPE", tableName: "EVENT")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-8") {
		dropIndex(indexName: "IDX_QRTZ_FT_INST_JOB_REQ_RCVRY", tableName: "QRTZ_FIRED_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-9") {
		dropIndex(indexName: "IDX_QRTZ_FT_JG", tableName: "QRTZ_FIRED_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-10") {
		dropIndex(indexName: "IDX_QRTZ_FT_J_G", tableName: "QRTZ_FIRED_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-11") {
		dropIndex(indexName: "IDX_QRTZ_FT_TG", tableName: "QRTZ_FIRED_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-12") {
		dropIndex(indexName: "IDX_QRTZ_FT_TRIG_INST_NAME", tableName: "QRTZ_FIRED_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-13") {
		dropIndex(indexName: "IDX_QRTZ_FT_T_G", tableName: "QRTZ_FIRED_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-14") {
		dropIndex(indexName: "IDX_QRTZ_J_GRP", tableName: "QRTZ_JOB_DETAILS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-15") {
		dropIndex(indexName: "IDX_QRTZ_J_REQ_RECOVERY", tableName: "QRTZ_JOB_DETAILS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-16") {
		dropIndex(indexName: "IDX_QRTZ_T_C", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-17") {
		dropIndex(indexName: "IDX_QRTZ_T_G", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-18") {
		dropIndex(indexName: "IDX_QRTZ_T_JG", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-19") {
		dropIndex(indexName: "IDX_QRTZ_T_NEXT_FIRE_TIME", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-20") {
		dropIndex(indexName: "IDX_QRTZ_T_NFT_MISFIRE", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-21") {
		dropIndex(indexName: "IDX_QRTZ_T_NFT_ST", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-22") {
		dropIndex(indexName: "IDX_QRTZ_T_NFT_ST_MISFIRE", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-23") {
		dropIndex(indexName: "IDX_QRTZ_T_NFT_ST_MISFIRE_GRP", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-24") {
		dropIndex(indexName: "IDX_QRTZ_T_N_G_STATE", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-25") {
		dropIndex(indexName: "IDX_QRTZ_T_N_STATE", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-26") {
		dropIndex(indexName: "IDX_QRTZ_T_STATE", tableName: "QRTZ_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-27") {
		dropSequence(schemaName: "PAAP", sequenceName: "EVENT_SEQUENCE")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-28") {
		dropTable(tableName: "QRTZ_BLOB_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-29") {
		dropTable(tableName: "QRTZ_CALENDARS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-30") {
		dropTable(tableName: "QRTZ_CRON_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-31") {
		dropTable(tableName: "QRTZ_FIRED_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-32") {
		dropTable(tableName: "QRTZ_JOB_DETAILS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-33") {
		dropTable(tableName: "QRTZ_LOCKS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-34") {
		dropTable(tableName: "QRTZ_PAUSED_TRIGGER_GRPS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-35") {
		dropTable(tableName: "QRTZ_SCHEDULER_STATE")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-36") {
		dropTable(tableName: "QRTZ_SIMPLE_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-37") {
		dropTable(tableName: "QRTZ_SIMPROP_TRIGGERS")
	}

	changeSet(author: "roberto (generated)", id: "1435855827581-38") {
		dropTable(tableName: "QRTZ_TRIGGERS")
	}
}
