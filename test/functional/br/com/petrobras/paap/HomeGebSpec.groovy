package  br.com.petrobras.paap

import geb.spock.GebReportingSpec

import spock.lang.*


@Stepwise
class HomeGebSpec extends GebReportingSpec {
    
    def "go google"() {
        when:
        go "http://google.com/ncr"
        then:
        title == "Google"
    }
}