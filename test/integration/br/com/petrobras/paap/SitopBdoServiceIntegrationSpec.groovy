package br.com.petrobras.paap

import grails.test.spock.IntegrationSpec
import groovy.time.TimeCategory

class SitopBdoServiceIntegrationSpec extends IntegrationSpec {
    SitopBdoService sitopBdoService

    def setup() {
        println ">> SitopBdoServiceIntegrationSpec:\n${TagPI.list()}\n"

        // Tags não-calculadas
        new TagPI( name: "t1",
                formula:"t1",
                tagType: TagPIType.SNAPSHOT,
                tagLocation: TagPILocation.UO_BS).save(flush:true)
        new TagPI( name: "t2",
                formula:"t2",
                tagType: TagPIType.SNAPSHOT,
                tagLocation: TagPILocation.UO_BS).save(flush:true)

        // Tags calcualdas
        TagPI tag1 = new TagPI( name: "t1 calculated",
                formula:"'t1'",
                tagType: TagPIType.CALCULATED,
                tagLocation: TagPILocation.UO_BS).save(flush:true)
        TagPI tag2 = new TagPI( name: "t2 calculated",
                formula:"'t2'",
                tagType: TagPIType.CALCULATED,
                tagLocation: TagPILocation.UO_BS).save(flush:true)

        // UEP's
        UEP uep1 = new UEP(name: 'uep1', uo: 'uo1', department: 'department1', asset: 'asset1',
                oceanCurrent: 'oceanCurrent1', oilWell: 'oilWell1', tag: tag1).save(flush:true)
        UEP uep2 = new UEP(name: 'uep2', uo: 'uo2', department: 'department2', asset: 'asset2',
                oceanCurrent: 'oceanCurrent2', oilWell: 'oilWell2', tag: tag2).save(flush:true)

        def dtCurrent, dt6DaysAgo, dt8DaysAgo, dt2MonthsAgo, dt3MonthsAgo
        use(TimeCategory) {
            dtCurrent = 1.day.ago
            dt6DaysAgo = 6.days.ago
            dt8DaysAgo = 8.days.ago
            dt2MonthsAgo = 2.months.ago
            dt3MonthsAgo = 3.months.ago
        }

        new SitopBdo(sitopDate: dtCurrent,          uep: uep1, expected: 10070, preliminary: 9300).save(flush:true)
        new SitopBdo(sitopDate: dt6DaysAgo,   uep: uep1, expected: 10300, preliminary: 9040).save(flush:true)
        new SitopBdo(sitopDate: dt8DaysAgo,   uep: uep1, expected: 10040, preliminary: 9300).save(flush:true)
        new SitopBdo(sitopDate: dt2MonthsAgo, uep: uep1, expected: 10050, preliminary: 9300).save(flush:true)
        new SitopBdo(sitopDate: dt3MonthsAgo, uep: uep1, expected: 10300, preliminary: 9067).save(flush:true)

        new SitopBdo(sitopDate: dtCurrent,          uep: uep2, expected: 12200, preliminary: 11030).save(flush:true)
//        new SitopBdo(sitopDate: dt6DaysAgo,   uep: uep2, expected: 12005, preliminary: 11300).save(flush:true)
        new SitopBdo(sitopDate: dt8DaysAgo,   uep: uep2, expected: 12800, preliminary: 11040).save(flush:true)
        new SitopBdo(sitopDate: dt2MonthsAgo, uep: uep2, expected: 12002, preliminary: 11040).save(flush:true)
        new SitopBdo(sitopDate: dt3MonthsAgo, uep: uep2, expected: 12400, preliminary: 11200).save(flush:true)

        new SnapshotSamples24h(date: dtCurrent,          value: 10030, tag: tag1).save(flush:true)
        new SnapshotSamples24h(date: dt6DaysAgo,   value: 10670, tag: tag1).save(flush:true)
//        new SnapshotSamples24h(date: dt8DaysAgo,   value: 10070, tag: tag1).save(flush:true)
        new SnapshotSamples24h(date: dt2MonthsAgo, value: 10650, tag: tag1).save(flush:true)
        new SnapshotSamples24h(date: dt3MonthsAgo, value: 10470, tag: tag1).save(flush:true)

        new SnapshotSamples24h(date: dtCurrent,          value: 12670, tag: tag2).save(flush:true)
        new SnapshotSamples24h(date: dt6DaysAgo,   value: 12470, tag: tag2).save(flush:true)
        new SnapshotSamples24h(date: dt8DaysAgo,   value: 12043, tag: tag2).save(flush:true)
        new SnapshotSamples24h(date: dt2MonthsAgo, value: 12540, tag: tag2).save(flush:true)
        new SnapshotSamples24h(date: dt3MonthsAgo, value: 12570, tag: tag2).save(flush:true)
    }

    def "test lastSitopBdo"() {
        when: "requested weekly data"
            def weeklyData = sitopBdoService.lastSitopBdo(true)
        then: "last week returned"
            weeklyData.size() == 3

        when: "requested monthly data"
            def monthlyData = sitopBdoService.lastSitopBdo(false)
        then: "last week returned"
            monthlyData.size() == 7
    }

    def "test sitopBdo"() {
        when: "requested paged data"
           def data = sitopBdoService.sitopBdo(3, 2)
        then: "last week returned"
            data.size() == 3

        when: "requested last page"
            def lastPage = sitopBdoService.sitopBdo(3, 8)
        then: "last week returned"
            lastPage.size() == 1

    }
}
