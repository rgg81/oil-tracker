package br.com.petrobras.paap

import grails.test.spock.IntegrationSpec
import groovy.time.TimeCategory
import spock.lang.Shared

class PIDataServiceIntegrationSpec extends IntegrationSpec {

	def PIDataService

	@Shared
	TagPI t1, t2

    def setup() {
		println ">> PIDataServiceIntegrationSpec:\n${TagPI.list()}\n"
		t1 = new TagPI(name: "t1",formula:"t1", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
		t2 = new TagPI(name: "t2",formula:"t2", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS, reloading: true).save(flush:true,failOnError:true)
    }

    def cleanup() {
    }


	private createSampleGroup(clazz, date, value, tag) {
		clazz.newInstance(date: date, mean: value, tag: tag, min: value, max: value, sum: value, variance: 0, num: 1 )
	}


    void 'test delete tagpi all samples data'() {
    	
    	
    	setup:'creating tags, samples, grouped samples and filters'
    		def now = new Date()

	    	use ( TimeCategory ) {
	    		
	    		for( i in 1..100){
	    			new Sample(date: now - i.minute ,value: i * 1000 ,tag:t1).save()
	    			createSampleGroup(SampleGroup2m, now - i.minute , i * 1000, t1).save()
	    			createSampleGroup(SampleGroup10m, now - i.minute , i * 1000, t1).save()
	    			createSampleGroup(SampleGroup1h, now - i.minute , i * 1000, t1).save()
	    			createSampleGroup(SampleGroup6h, now - i.minute , i * 1000, t1).save()
	    			createSampleGroup(SampleGroup24h, now - i.minute , i * 1000, t1).save()
	    		}
				
			}

    	when:
	    	PIDataService.deleteAllSamplesFromTag(t1)

	    then:
	    	Sample.findAllByTag(t1).size() == 0
	    	SampleGroup2m.findAllByTag(t1).size() == 0
	    	SampleGroup10m.findAllByTag(t1).size() == 0
	    	SampleGroup1h.findAllByTag(t1).size() == 0
	    	SampleGroup6h.findAllByTag(t1).size() == 0
    }

    void 'test delete all samples data from non reloading tags'() {
		def minute = 60 * 1000
		setup:'creating tags, samples, grouped samples and filters'

			SampleGroupAggregator aggregatorT1 = new SampleGroupAggregator(SampleGroup2m.class, null)
			SampleGroupAggregator aggregatorT2 = new SampleGroupAggregator(SampleGroup2m.class, null)
			for( def i=1; i<=30; i++) {
				def time = new Date(i * minute)
				// Samples
				def s1 = new Sample(date: time, value: i, tag:t1).save()
				def s2 = new Sample(date: time, value: i, tag:t2).save()

				// Sample Groups
				aggregatorT1.update( SampleGroupSingle.createSingle(time, t1, s1.value) )
				aggregatorT2.update( SampleGroupSingle.createSingle(time, t2, s2.value) )
			}
			aggregatorT1.executeUpdates()
			aggregatorT2.executeUpdates()

		when:
			PIDataService.deleteSamplesFromNonReloadingTagsUntil(new Date(10 * minute))

		then:
			Sample.findAllByTag(t1).size() == 20
			SampleGroup2m.findAllByTag(t1).size() == 10

			Sample.findAllByTag(t2).size() == 30
			SampleGroup2m.findAllByTag(t2).size() == 16
	}
}
