package br.com.petrobras.paap

import br.com.petrobras.paap.job.ProcessData
import br.com.petrobras.paap.job.ProcessDataSnapshotSamples
import br.com.petrobras.paap.ws.WSMessageType
import grails.test.spock.IntegrationSpec
import groovy.time.TimeCategory

import java.lang.reflect.Field

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */

class ProcessDataSnapshotSamplesIntegrationSpec extends IntegrationSpec {

    def grailsApplication
    def setup() {

    }

    def cleanup() {
    }

    void "test snapshot"() {
        def tag1 = new TagPI(name: "TAGNAME",formula:"1 + 1", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true);
        def tag2 = new TagPI(name: "TAGNAME2",formula:"2 + 2", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true);

        def uep = new UEP(name: 'P-40', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag1).save(flush: true)

        def uep2 = new UEP(name: 'P-402', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag2).save(flush: true)

        def wsMessageService = Spy(WSMessageService)
        ProcessData p = new ProcessDataSnapshotSamples(wsMessageService, grailsApplication)
        Calendar today = p.clearTime(Calendar.getInstance())
        p.metaClass.HOUR_TO_GENERATE = { ->
            1
        }
        def date1 =  use(TimeCategory) {
            today.time - 1.hours
        }
        def date2 =  use(TimeCategory) {
            today.time - 2.hours
        }
        new Sample(tag:tag1, date: date1, value: 2d).save(flush: true)
        new Sample(tag:tag1, date: date2, value: 1d).save(flush: true)

        new Sample(tag:tag2, date: date1, value: 2d).save(flush: true)
        new Sample(tag:tag2, date: date2, value: 1d).save(flush: true)

        when:
            def data = p.processInternal([])
        then:
            1 * p.wsMessageService.sendEvent(WSMessageType.NEW_SNAPSHOT_24H, [
                    currentSnapshotDate: today.time,
                    previousSnapshotDate: use(TimeCategory) {
                        today.time - 24.hours
                    },
                    ueps: [(uep.id): 1.5, (uep2.id): 1.5]
            ]) >> null


    }
}
