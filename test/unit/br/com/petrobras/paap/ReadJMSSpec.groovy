package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import org.quartz.Scheduler
import org.quartz.JobDetail
import br.com.petrobras.paap.job.ReadJMS
import br.com.petrobras.paap.ws.WSMessageType
import grails.test.mixin.web.ControllerUnitTestMixin

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(ControllerUnitTestMixin)
@Mock([TagPI,UEP])
class ReadJMSSpec extends Specification {

	def reloadQueueService
    def scheduler
    def wsMessageService
    def jobdetail

    def t1,t2,calc,calcWithoutUep,uep

    def setup() {
    	reloadQueueService = Mock(ReloadQueueService)
    	scheduler = Mock(Scheduler)
    	wsMessageService = Mock(WSMessageService)
    	jobdetail = Mock(JobDetail)

    	t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
		t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
		calc = new TagPI(name: 'calc', formula: "'tag1'+'tag2'", tagType: TagPIType.CALCULATED).save(flush: true)
		calcWithoutUep = new TagPI(name: 'calcWithoutUep', formula: "('tag1'+'tag2')*0.5", tagType: TagPIType.CALCULATED).save(flush: true)
		uep = new UEP(name: 'P-15', uo: 'UO-BS', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',tag:calc ).save();
 
    }

    def cleanup() {
    }

	void "test reloading job alreary running"() {
		when:
			def process = new ReadJMS(reloadQueueService,wsMessageService,grailsApplication)
			process.scheduler = scheduler
    		process.processInternal()
    	then:
    		1 * scheduler.getJobDetail(_) >> jobdetail
    		0 * reloadQueueService.read()
	}

    void "test tags starting reloading"() {   	
    	when:
    		def process = new ReadJMS(reloadQueueService,wsMessageService,grailsApplication)
    		process.scheduler = scheduler
    		process.processInternal()
    	then:
    		1 * scheduler.getJobDetail(_) >> null
    		1 * reloadQueueService.readFromStartQueueAndDo(_) >> { Closure c -> c.call([(t1.id),(t2.id),(calc.id),(calcWithoutUep.id)]) }
            1 * reloadQueueService.readFromEndQueueAndDo(_) >>  { Closure c ->c.call([])}
    		1 * wsMessageService.sendEvent(WSMessageType.UEP_RELOAD_START,[uep.id])
    		1 * scheduler.scheduleJob(_, _)
            1 * scheduler.start()

    }

    void "test tags stopping reloading"() {
    	when:
    		def process = new ReadJMS(reloadQueueService,wsMessageService,grailsApplication)
    		process.scheduler = scheduler
    		process.processInternal()
    	then:
    		1 * scheduler.getJobDetail(_) >> null
            1 * reloadQueueService.readFromStartQueueAndDo(_) >>  { Closure c ->c.call([])}
    		1 * reloadQueueService.readFromEndQueueAndDo(_) >> { Closure c -> c.call([(t1.id),(t2.id),(calc.id),(calcWithoutUep.id)]) }
    		1 * wsMessageService.sendEvent(WSMessageType.UEP_RELOAD_FINISHED,[uep.id])
    		0 * scheduler.scheduleJob(_, _)
            0 * scheduler.start()
    }
}
