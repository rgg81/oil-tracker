package br.com.petrobras.paap

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.validation.ValidationException

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Sample)
@Mock([TagPI])
class SampleSpec extends ConstraintUnitSpec {


    void "test constraint"() {
        when: 'sample has two data with same date for one tag'
        def tag = getAnyValidTag().save()
        def date = new Date()
        def sample1 = new Sample(tag:tag, date: date, value: 1d)
        def sample2 = new Sample(tag:tag, date: date, value: 1d)
        mockForConstraintsTests(Sample, [sample1])
        sample2.save(flush: true)

        then: 'shoud throw an exception'
        thrown(ValidationException)

        when: 'sample has two data with diferent date for one tag'
        def date1 = new Date()
        def sample3 = new Sample(tag:tag, date: date1, value: 1d)
        sample3.save()

        then: 'should save'
        !sample3.hasErrors()

    }
}
