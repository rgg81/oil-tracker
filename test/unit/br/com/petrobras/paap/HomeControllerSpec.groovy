package br.com.petrobras.paap

import grails.test.mixin.TestFor
import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import javax.servlet.http.Cookie

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(HomeController)
@Mock([UepService])
class HomeControllerSpec extends Specification {
        def shiroService
        def uepServiceMock
    def setup() {
        shiroService = Mock(ShiroService)
        uepServiceMock = Mock(UepService)
        controller.uepService = uepServiceMock
        controller.shiroService = shiroService
		controller.metaClass.isMobile = { false }
    }

    def cleanup() {
		controller.metaClass.isMobile = null
    }


    void "Display as Total"() {

		    	
        when:"The index action is executed"
            controller.index()

        then:"The model should contain total as null, it should be ordered by the UEPs default order"
            1 * shiroService.getLoggedUser() >> 'uq4n'
            1 * uepServiceMock.getUEPsWithPermissionForUser('uq4n') >> ["UO-RIO","UO-BS",'UO-BC','UO-ES']
        	1 * uepServiceMock.getListOfRegisteredAttributes("uo") >> ["UO-RIO","UO-BS",'UO-BC','UO-ES']
    	   	1 * uepServiceMock.getListOfRegisteredAttributes("department") >> ["SSE","PDP","PRESAL"]
			1 * uepServiceMock.filterUEP(_) >> [[name:"UO-ES", values:["P-21","P-22"]],[name:"UO-BC", values:["P-11","P-12"]],[name:"UO-RIO", values:["P-51","P-92"]],[name:"UO-BS", values:["P-61","P-62"]]]
            model.total == null
            model.result*.name == ["UO-ES","UO-BC","UO-RIO","UO-BS"]
            /*println model.result*.name[0].class
            println ["UO-ES","UO-RIO","UO-BC","UO-BS"][0].class*/


        when:"The uo action is executed"
        	controller.uo()

        then:"The model should contain total as null, it should be ordered by the UEPs default order"
        	1 * uepServiceMock.getListOfRegisteredAttributes("uo") >> ["UO-RIO","UO-BS",'UO-BC','UO-ES']
    	   	1 * uepServiceMock.getListOfRegisteredAttributes("department") >> ["SSE","PDP","PRESAL"]
			1 * uepServiceMock.filterUEP(_) >> [[name:"UO-ES", values:["P-21","P-22"]],[name:"UO-BC", values:["P-11","P-12"]],[name:"UO-RIO", values:["P-51","P-92"]],[name:"UO-BS", values:["P-61","P-62"]]]
            model.total == null
            model.result*.name == ["UO-ES","UO-BC","UO-RIO","UO-BS"]
            /*println model.result*.name[0].class
            println ["UO-ES","UO-RIO","UO-BC","UO-BS"][0].class*/
 


    }
    void "Display Total as UO name"() {
    	given:
        	params.filterKey = "uo"
        	params.filterValue = "UO-RIO"
        	Cookie cookie = new Cookie("filterValue","UO-RIO")
        	cookie.path = "/"
        	request.cookies = [cookie].toArray()

        when:"The index action is executed"
            controller.index()

        then:"The model should contain total as UO-RIO"
            1 * shiroService.getLoggedUser() >> 'uq4n'
            1 * uepServiceMock.getUEPsWithPermissionForUser('uq4n') >> ["UO-RIO","UO-BS"]
        	1 * uepServiceMock.getListOfRegisteredAttributes("uo") >> ["UO-RIO","UO-BS"]
    	   	1 * uepServiceMock.getListOfRegisteredAttributes("department") >> ["SSE","PDP","PRESAL"]
			1 * uepServiceMock.filterUEP(_) >> [[name:"UO-RIO", values:["P-51","P-92"]]]
            model.total == "UO-RIO"
    }

    void "Display a single department"() {
    	given: 
    		params.id = "PRESAL"
    	   	def uepServiceMock = Mock(UepService)
    	   	controller.uepService = uepServiceMock     	
    	when:
    		controller.department("PRESAL")
    	then:
    		1 * uepServiceMock.getListOfRegisteredAttributes("uo") >> ["UO-RIO","UO-BS"]
    	   	1 * uepServiceMock.getListOfRegisteredAttributes("department") >> ["SSE","PDP","PRESAL"]
			1 * uepServiceMock.filterUEP(_) >> [[name:"UO-RIO", values:["P-51","P-92"]]] 

    		model.total == "PRESAL"
    		model.filterKey == "department"
    		model.filterValue == model.total
    }

    void "Display a UO with a single department"() {
    	given: 
    		params.id = "UO-RIO"
    	   	def uepServiceMock = Mock(UepService)
    	   	controller.uepService = uepServiceMock     	

    	
    	when:
    		controller.uo("UO-RIO")
    	then:
    	   	1 * uepServiceMock.getListOfRegisteredAttributes("uo") >> ["UO-RIO"]
    	   	1 * uepServiceMock.getListOfRegisteredAttributes("department") >> ["SSE"]
			1 * uepServiceMock.filterUEP(_) >> [[name:"SSE", values:["P-51","P-92"]]]   	

    		model.total == "UO-RIO"
    		model.filterKey == "uo"
    		model.filterValue == model.total
    }


}
