package br.com.petrobras.paap.ws

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.PIDataService
import br.com.petrobras.paap.EventService
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.UEP
import br.com.petrobras.paap.TagPIType
import br.com.petrobras.paap.TagPILocation
/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([UEP,TagPI])
class WSMessageSpec extends Specification {

    def piDataService
    def eventService

    def setup() {
    	piDataService = Mock(PIDataService)
    	eventService = Mock(EventService)
    }

    def cleanup() {

    }

    void "test newdata message"() {
    	setup:

	    	def t1 = new TagPI(name: "t1",formula:"t1", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
	        def t2 = new TagPI(name: "t2",formula:"t2", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
	        def f1 = new TagPI(name: "f(t1)",formula:"'t1' + 't1'", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)
	        def f2 = new TagPI(name: "f(t2)",formula:"'t2' + 't2'", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)
	        def uep1 = new UEP( name: 'Merluza Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
	                tag: f1 ).save(flush:true)
	        def uep2 = new UEP( name: 'Ilha Bela', uo: 'XX', department: 'XXX', asset: 'XX', oceanCurrent: 'east', oilWell: 'XXX',
	                tag: f2 ).save(flush:true)

    		def datain = [(f1):[
    			samples:[[time: 1, value: 101],[time: 2, value: 102]],
    			SampleGroup2m:[[time: 1, value: 10]],
    			SampleGroup10m:[[time: 1, value: 11]],
    			SampleGroup1h:[[time: 1, value: 12]],
    			SampleGroup6h:[[time: 1, value: 13]],
    			SampleGroup24h:[[time: 1, value: 14]]
    		]]

    		def dataout = [	(uep1.id) : [
    										'all' : [[1,101],[2,102]],
    										'high':[[1,10]],
    										'medium':[[1,11]],
    										'low':[[1,12]],
    										'very-low':[[1,13]],
    										'daily':[[1,14]],
    										'tagId':f1.id,
    										'24h_avg':100,
    										'alert': 1
    									],
    						(uep2.id) : ['tagId':f2.id, '24h_avg':200 ,'alert': -1],
    						events:[]]
		when:
    		def map = new WSMessage(piDataService,eventService).type(WSMessageType.NEW_DATA).data(datain).build()
    	then:
            1 * piDataService.average24hours(uep1.id) >> 100
            1 * piDataService.average24hours(uep2.id) >> 200
            1 * piDataService.alert(uep1.id) >> 1
            1 * piDataService.alert(uep2.id) >> -1
            1 * eventService.unsafeEventsWarnings() >> []

    		map.event == WSMessageType.NEW_DATA.typeName
    		map.data == dataout
    		map.data.events == []
    }

    void "test other types message"() {
    	when:
    		def map = new WSMessage(piDataService,eventService).type(WSMessageType.UPDATE_SITOP_BDO).data(["data"]).build()
    	then:
    		map.event == WSMessageType.UPDATE_SITOP_BDO.typeName
    		map.data == ["data"]

    	when:
    		map = new WSMessage(piDataService,eventService).type(WSMessageType.UPDATE_SITOP_BDO).build()
    	then:
    		map['data'] == null
    }
}
