package br.com.petrobras.paap.ws

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.UEP

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class EPProductionJMSListenerSpec extends Specification {
	

    def setup() {
    }

    def cleanup() {
    }

    void "test new data"() {
    	setup:
    		GroovyMock(WSClientService,global: true)

			def listener = new EPProductionJMSListener()

			def message = Mock(javax.jms.TextMessage)

			def uep1 = Mock(UEP)
			def uep2 = Mock(UEP)
			def uep3 = Mock(UEP)

			def session1 = Mock(javax.websocket.Session)
			def session2 = Mock(javax.websocket.Session)
			def session3 = Mock(javax.websocket.Session)
			def session4 = Mock(javax.websocket.Session)
			def session5 = Mock(javax.websocket.Session)

		when: "no data"
			listener.onMessage(message)
		then:
			2 * message.getText() >> '{}'
			1 * WSClientService.sendMessageToClients(_)

		when: "others events data"
			listener.onMessage(message)
		then:
			2 * message.getText() >> '{"event":"updateSitopBdo"}'
			1 * WSClientService.sendMessageToClients(_)

    	when: 
    		listener.onMessage(message)
    	then:
    		1 * message.getText() >> '{"event":"newdata","data":{"1":"fakedata","2":"fakedata","3":"fakedata","events":[{"date":10,"msg":"teste","tag":"tag1"},{"date":12,"msg":"teste2","tag":"tag2"}]}}'
    		2 * WSClientService.getClients() >> [session1,session2,session3,session4,session5] // 5 clients

    		2 * session1.getUserProperties() >> [httpsession:[ueps:[uep1,uep2,uep3],isAdmin:false]] // access to all ueps
    		2 * session2.getUserProperties() >> [httpsession:[ueps:[uep1,uep2],isAdmin:false]] // access to 2 ueps
    		2 * session3.getUserProperties() >> [httpsession:[ueps:[uep1],isAdmin:false]] // access to 1 ueps
    		2 * session4.getUserProperties() >> [httpsession:[ueps:[],isAdmin:false]] // no access
			2 * session5.getUserProperties() >>  [httpsession:[ueps:[uep1,uep2,uep3],isAdmin:true]] // access to all ueps

    		4 * uep1.getProperty('id') >> 1L
    		3 * uep2.getProperty('id') >> 2L
    		2 * uep3.getProperty('id') >> 3L

    		1 * WSClientService.sendMessageToClient(session1,'{"event":"newdata","data":{"1":"fakedata","2":"fakedata","3":"fakedata"}}')
    		1 * WSClientService.sendMessageToClient(session2,'{"event":"newdata","data":{"1":"fakedata","2":"fakedata"}}')
    		1 * WSClientService.sendMessageToClient(session3,'{"event":"newdata","data":{"1":"fakedata"}}')
    		1 * WSClientService.sendMessageToClient(session4,'{"event":"newdata","data":{}}')
			1 * WSClientService.sendMessageToClient(session5,'{"event":"newdata","data":{"1":"fakedata","2":"fakedata","3":"fakedata","events":[{"date":10,"msg":"teste","tag":"tag1"},{"date":12,"msg":"teste2","tag":"tag2"}]}}')
    }
}