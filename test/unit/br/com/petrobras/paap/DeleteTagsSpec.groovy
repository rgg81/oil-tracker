package br.com.petrobras.paap

import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.job.DeleteTags

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI,Sample,Event,EventTagPI])
class DeleteTagsSpec extends Specification {


    def setup() {
    }

    def cleanup() {
    }

    void "test delete tags"() {
        setup:
            TagPI.metaClass.static.withNewSession = {Closure c -> c.call() }

            def tags =  [   't1': [[10,100],[20,102],[30,104],[40,108]],
                            't2': [[10, 50],[20, 52],[30,54],[40,58]],
                            't3': [],
                            't4': [[10,0.0001]]]

            tags.each{ key, value ->
                def t = new TagPI(name: key,formula:key, tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
                value.each {
                    new Sample(date: new Date((it[0] * 1000).toLong()), value: it[1], tag: t).save()
                }
            }
            def service = Mock(TagPIService)
            def p = new DeleteTags(service, grailsApplication)
        when:
            def result = p.processInternal(TagPI.findAll())
        then:
            4 * service.resetTag(_)



    }
}
