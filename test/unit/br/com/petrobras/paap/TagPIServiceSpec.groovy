package br.com.petrobras.paap

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TagPIService)
@Mock([TagPI,TagPIFilter,TagPIFilterKalman, TagPIFilterMinMax, TagPIFilterMovingAverage, TagPIFilterMovingAverage, TagPIFilterMovingAverageSample,UEP,UserPermission])
class TagPIServiceSpec extends Specification {

    def t1,t2,calc
    def setup() {
        service.uepService = Mock(UepService)
        t1 = new TagPI( name: "tag1",
                            formula:"tag1", 
                            tagType: TagPIType.SNAPSHOT, 
                            tagLocation: TagPILocation.UO_BS).save(flush:true)
        t2 = new TagPI( name: "tag2",
                           formula:"tag2", 
                            tagType: TagPIType.SNAPSHOT, 
                            tagLocation: TagPILocation.UO_BS).save(flush:true)  

    }

    def cleanup() {
    }


    void "test delete tag pi with references should fail"() {
        calc = new TagPI( name: "tag calc",
                        formula:"'tag1' + 'tag2'", 
                        tagType: TagPIType.CALCULATED, 
                        tagLocation: TagPILocation.UO_BS).save(flush:true)     
        when:
            service.delete(t1)
        then:
            def e = thrown(RuntimeException)
            e.cause.class == TagPIReferenceException
            
    }

    void "test delete tag pi without references should succeed"() {
        when:
            service.delete(t1)
        then:
            notThrown(RuntimeException)
    }

    void "test save tag pi with filters"(filters, result) {
    	when:
		def r = service.update(t1,filters)
    	
    	then:
    	r.name == t1.name
    	r.filters[0].class == result[0]
    	r.filters[0].properties?.subMap(result[1].keySet() as String[]) == result[1]
   	

    	where:
    	filters | result
		'[{"kind":"kalman","properties":{"processVariance":100,"measureVariance":100000},"enabled":true}]'  | [TagPIFilterKalman, [processVariance:100.0,measureVariance:100000.0]]
		'[{"kind":"average","properties":{"windowInMinutes":5},"enabled":true}]'  | [TagPIFilterMovingAverage, [windowInMs:5000*60]]
		'[{"kind":"minmax","enabled":true,"properties":{"min":3,"max":1000}}]'  | [TagPIFilterMinMax, [min:3,max:1000]]
    }

    def 'test reset filters'(){
        setup:
        def filters = [ new TagPIFilterKalman(10,10, 0),
                        new TagPIFilterMovingAverage(10000,1),
                        new TagPIFilterMinMax(1,50000,2)]
        filters.each{ f->            
            t1.addToFilters(f)
        }        

        when:
            service.resetFilters(t1)
        
        then:

            t1.filters[0].paramP == 0
            t1.filters[0].minusP == null
            t1.filters[0].xhatminus == null
            t1.filters[0].paramK == null
            t1.filters[0].lastDate == null
            t1.filters[0].lastValue == null
            t1.filters[0].coldStartTotal == 0

            t1.filters[1].samples  == null
            //samples_sum=0, PARAMP=0, LAST_DATE=null, LAST_VALUE=null, MINUSP=null, PARAMK=null, XHATMINUS=null, COLD_START_TOTAL=0;
            // check state resets

    }

    def 'test reload tags'(){
        setup:
            calc = new TagPI( name: "tag calc",
                    formula:"'tag1' + 'tag2'", 
                    tagType: TagPIType.CALCULATED, 
                    tagLocation: TagPILocation.UO_BS).save(flush:true)     

            def tags = [ t1,t2, calc] as ArrayList
            def set = tags as Set

            def reloadQueueService = Mock(ReloadQueueService)

            service.reloadQueueService = reloadQueueService
        
        when:
            service.reloadtags(tags)

        then:
            1 * reloadQueueService.pushToStartQueue(set)
            t1.reloadStatus == ReloadStatus.QUEUE
            t2.reloadStatus == ReloadStatus.QUEUE
            calc.reloadStatus == ReloadStatus.QUEUE

        when:
            service.reloadtags([] as ArrayList)

        then:
            0 * reloadQueueService.push(_,_)

        when:
            service.reloadtags(null)

        then:
            0 * reloadQueueService.push(_,_)
            thrown(RuntimeException)
    }

    def 'test if user has access to tag'() {
        setup:
            def tag1 = new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save(flush:true)
            def uep = new UEP(id: 1, name: 'P-41', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                    tag: tag1).save(flush:true);
            def tag2 = new TagPI(name: 'Tag P-42', formula: '1+1', tagType: TagPIType.CALCULATED).save(flush:true)
            def uep2 = new UEP(name: 'P-42', uo: 'UO-BS', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                    tag: tag2).save(flush:true);
            def tag3 = new TagPI(name: 'Tag P-43', formula: '1+1', tagType: TagPIType.CALCULATED).save(flush:true)
            def uep3 = new UEP(name: 'P-43', uo: 'UO-ES', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                    tag: tag3).save(flush:true);
            new UserPermission(attribute: 'id', value: '1', username: 'XBTF').save(flush:true)
            new UserPermission(attribute: 'uo', value: 'UO-BS', username: 'XBTF').save(flush:true)
            1 * service.uepService.hasPermission(UEP.findByTag(TagPI.findByName(tag))) >> hasPermission
        expect:
            UEP.findByTag(TagPI.findByName(tag)).name == uepName
            service.hasAccessToTag(TagPI.findByName(tag)) == hasPermission
        where:
            tag        | uepName    | hasPermission
            "Tag P-41" | "P-41" | true
            "Tag P-42" | "P-42" | true
            "Tag P-43" | "P-43" | false
    }
}
