package br.com.petrobras.paap

import grails.test.MockUtils
import grails.test.mixin.TestFor
import org.apache.commons.logging.Log
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ReadPIService)
class ReadPIServiceSpec extends Specification {

    def setup() {
        grailsApplication.config.paap.servicepi.maxRetryService = 1

        service.metaClass.isDst = { -> false }
    }

    def cleanup() {
    }

    private void mockServiceForHttpReturn(int httpCode, boolean isError, String strXml) {
        service.metaClass.createHttpConnection = { url ->
            def httpMock = Spy(HttpURLConnection)
            httpMock.getResponseCode() >> httpCode
            httpMock.getOutputStream() >> new ByteArrayOutputStream()

            if (isError) {
                httpMock.getErrorStream() >> new ByteArrayInputStream(strXml.getBytes("utf-8"))
            } else {
                httpMock.getInputStream() >> new ByteArrayInputStream(strXml.getBytes("utf-8"))
            }

            httpMock
        }
    }

    private void mockServiceForHttpReturn(String strXml) {
        mockServiceForHttpReturn(200, false, strXml)
    }

    void 'test read from pi'() {
        mockServiceForHttpReturn('''<?xml version="1.0" encoding="utf-8"?>
<ArrayOfTag xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.ti.petrobras.com.br/">
  <Tag>
    <Nome>CIB_30100K_T62_FT_1103_FAKE</Nome>
    <Propriedades>
      <Propriedade>
        <Valor>201,392852783203</Valor>
        <Data>2015-03-09T00:00:00</Data>
      </Propriedade>
      <Propriedade>
        <Valor>201,400772094727</Valor>
        <Data>2015-03-09T06:43:59</Data>
      </Propriedade>
    </Propriedades>
  </Tag>
</ArrayOfTag>''')
        
        setup:
        service.metaClass.isDst = { -> true }

        when:
        def res = service.readDataFromTagInTimePeriod("CIB_30100K_T62_FT_1103_FAKE", TagPILocation.UO_BS, new Date(new Date().getTime() - 1000 * 60 * 60 * 24), new Date())

        then:
        res[0].value == 201.392852783203
        res[1].value == 201.400772094727
        service.webserviceDateParser.format(res[0].time) == '2015-03-09T01:00:00'
        service.webserviceDateParser.format(res[1].time) == '2015-03-09T07:43:59'

        cleanup:
        service.metaClass.isDst = { -> false }

    }

    void 'test read from pi during dst'() {
        mockServiceForHttpReturn('''<?xml version="1.0" encoding="utf-8"?>
<ArrayOfTag xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.ti.petrobras.com.br/">
  <Tag>
    <Nome>CIB_30100K_T62_FT_1103_FAKE</Nome>
    <Propriedades>
      <Propriedade>
        <Valor>201,392852783203</Valor>
        <Data>2015-03-09T00:00:00</Data>
      </Propriedade>
      <Propriedade>
        <Valor>201,400772094727</Valor>
        <Data>2015-03-09T06:43:59</Data>
      </Propriedade>
    </Propriedades>
  </Tag>
</ArrayOfTag>''')

        when:
        def res = service.readDataFromTagInTimePeriod("CIB_30100K_T62_FT_1103_FAKE", TagPILocation.UO_BS, new Date(new Date().getTime() - 1000 * 60 * 60 * 24), new Date())

        then:
        res[0].value == 201.392852783203
        res[1].value == 201.400772094727
        service.webserviceDateParser.format(res[0].time) == '2015-03-09T00:00:00'
        service.webserviceDateParser.format(res[1].time) == '2015-03-09T06:43:59'

    }

    void 'test read from pi multiple tags'() {
        mockServiceForHttpReturn('''<?xml version="1.0" encoding="utf-8"?>
<ArrayOfTag xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.ti.petrobras.com.br/">
  <Tag>
    <Nome>CIB_30100K_T62_FT_1103_FAKE</Nome>
    <Propriedades>
      <Propriedade>
        <Valor>201,392852783203</Valor>
        <Data>2015-03-09T00:00:00</Data>
      </Propriedade>
      <Propriedade>
        <Valor>201,400772094727</Valor>
        <Data>2015-03-09T06:43:59</Data>
      </Propriedade>
    </Propriedades>
  </Tag>
  <Tag>
    <Nome>CIB_30100K_T62_FT_1104_FAKE</Nome>
    <Propriedades>
      <Propriedade>
        <Valor>207,392852783203</Valor>
        <Data>2015-03-10T00:00:00</Data>
      </Propriedade>
      <Propriedade>
        <Valor>209,400772094727</Valor>
        <Data>2015-03-10T06:43:59</Data>
      </Propriedade>
    </Propriedades>
  </Tag>
  <Tag>
    <Nome>CIB_30100K_T62_FT_1105_FAKE</Nome>
    <Propriedades>
      <Propriedade>
        <Valor>101,392852783203</Valor>
        <Data>2015-03-11T00:00:00</Data>
      </Propriedade>
      <Propriedade>
        <Valor>107,400772094727</Valor>
        <Data>2015-03-11T06:43:59</Data>
      </Propriedade>
    </Propriedades>
  </Tag>
</ArrayOfTag>''')

        def res = service.readDataFromTagsInTimePeriod(["CIB_30100K_T62_FT_1103_FAKE","CIB_30100K_T62_FT_1104_FAKE","CIB_30100K_T62_FT_1105_FAKE"], TagPILocation.UO_BS, new Date(new Date().getTime() - 1000 * 60 * 60 * 24), new Date())
        expect:
        res['CIB_30100K_T62_FT_1103_FAKE'][0].value == 201.392852783203
        res['CIB_30100K_T62_FT_1103_FAKE'][1].value == 201.400772094727
        service.webserviceDateParser.format(res['CIB_30100K_T62_FT_1103_FAKE'][0].time) == '2015-03-09T00:00:00'
        service.webserviceDateParser.format(res['CIB_30100K_T62_FT_1103_FAKE'][1].time) == '2015-03-09T06:43:59'

        res['CIB_30100K_T62_FT_1104_FAKE'][0].value == 207.392852783203
        res['CIB_30100K_T62_FT_1104_FAKE'][1].value == 209.400772094727
        service.webserviceDateParser.format(res['CIB_30100K_T62_FT_1104_FAKE'][0].time) == '2015-03-10T00:00:00'
        service.webserviceDateParser.format(res['CIB_30100K_T62_FT_1104_FAKE'][1].time) == '2015-03-10T06:43:59'

        res['CIB_30100K_T62_FT_1105_FAKE'][0].value == 101.392852783203
        res['CIB_30100K_T62_FT_1105_FAKE'][1].value == 107.400772094727
        service.webserviceDateParser.format(res['CIB_30100K_T62_FT_1105_FAKE'][0].time) == '2015-03-11T00:00:00'
        service.webserviceDateParser.format(res['CIB_30100K_T62_FT_1105_FAKE'][1].time) == '2015-03-11T06:43:59'

    }

    /*
    @Deprecated
    void 'test read from pi with average'() {
        service.metaClass.xmlParser = {
            def xml = Mock(XmlSlurper)
            xml.parse(_) >> new XmlSlurper().parseText('''<?xml version="1.0" encoding="utf-8"?>
<Tag xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.ti.petrobras.com.br/">
  <Nome>('CIB_30100K_T62_FT_1108','09-Mar-15 00:00:00','10-Mar-15 00:00:00')</Nome>
  <Propriedades>
    <Propriedade>
      <Valor>198,137704635987</Valor>
      <Data>2015-03-09T00:00:00</Data>
    </Propriedade>
    <Propriedade>
      <Valor>198,137704635987</Valor>
      <Data>2015-03-10T00:00:00</Data>
    </Propriedade>
  </Propriedades>
</Tag>''')
            xml
        }

		def res = service.readDataFromTagInTimePeriodWithAverage("CIB_30100K_T62_FT_1103", TagPILocation.UO_BS, new Date(new Date().getTime() - 1000*60*60*24), new Date())
		expect:
			res.media == 198.137704635987

    }
    */

    void 'test read from pi with no data'() {
        mockServiceForHttpReturn('''<?xml version="1.0" encoding="utf-8"?>
<ArrayOfTag xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.ti.petrobras.com.br/">
<Tag>
  <Nome>CIB_30100K_T62_FT_1103_FAKE</Nome>
    <Propriedades>
      <Propriedade>
        <Valor>7320890</Valor>
        <Data>2015-03-10T18:13:15</Data>
      </Propriedade>
      <Propriedade>
        <Valor>7320896</Valor>
        <Data>2015-03-10T18:13:15</Data>
      </Propriedade>
      <Propriedade>
        <Valor>No Data</Valor>
        <Data>2015-03-10T18:13:32</Data>
      </Propriedade>
      <Propriedade>
        <Valor>No Data</Valor>
        <Data>2015-03-10T18:13:49</Data>
      </Propriedade>
    </Propriedades>
</Tag>
</ArrayOfTag>''')

        def res = service.readDataFromTagInTimePeriod("CIB_30100K_T62_FT_1103_FAKE", TagPILocation.UO_BS, new Date(new Date().getTime() - 1000 * 60 * 60 * 24), new Date())

        expect:
        res.size() == 2
        res[0].value == 7320890
        res[1].value == 7320896
    }

    void 'test pi error'() {
        mockServiceForHttpReturn(500, true, '''N&#227;o foi poss&#237;vel verificar se o C&#243;digo do Sistema &#233; v&#225;lido.
Hashtable insert failed. Load factor too high. The most common cause is multiple threads writing to the Hashtable simultaneously.''')

        when:
            service.parseUrl("not important")
        then:
            def e = thrown(IOException)
            e.cause instanceof IOException
            e.cause.message == '''Não foi possível verificar se o Código do Sistema é válido.
Hashtable insert failed. Load factor too high. The most common cause is multiple threads writing to the Hashtable simultaneously.'''
    }

    void 'test read from invalid pi tag'() {
        mockServiceForHttpReturn('''<?xml version="1.0" encoding="utf-8"?>
<ArrayOfTag xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.ti.petrobras.com.br/">
  <Tag>
    <Nome>Exception: CSV_3085_21FIS5405 : Tentativa de acessar tag[CSV_3085_21FIS5405] inexistente no servidor [SBS00AS25]</Nome>
    <Propriedades>
      <Propriedade>
        <Valor>Exception: Tentativa de acessar tag[CSV_3085_21FIS5405] inexistente no servidor [SBS00AS25]</Valor>
        <Data>2015-05-27T15:08:36.4338987-03:00</Data>
      </Propriedade>
    </Propriedades>
  </Tag>
</ArrayOfTag>''')

        when: 'Calling the ignore error version'
            def res = service.readDataFromTagInTimePeriod("CSV_3085_21FIS5405", TagPILocation.UO_BS, new Date(new Date().getTime() - 1000 * 60 * 60 * 24), new Date())
        then: 'No data and no exception is thrown'
            res.size() == 0

        when: 'Calling the thow error version'
            service.readDataFromTagInTimePeriodReturningError("CSV_3085_21FIS5405", TagPILocation.UO_BS, new Date(new Date().getTime() - 1000 * 60 * 60 * 24), new Date())
        then: 'No data and no exception is thrown'
            def e = thrown(Exception)
            e.message == "CSV_3085_21FIS5405 : Tentativa de acessar tag[CSV_3085_21FIS5405] inexistente no servidor [SBS00AS25]"
    }

    void 'test trace log'() {
        def strXml = '<any-valid-xml></any-valid-xml>'

        def mockLog = Mock(Log)
        mockLog.isTraceEnabled() >> true

        service.log = mockLog

        mockServiceForHttpReturn(200, false, strXml)

        when:
            service.parseUrl("not important")
        then:
            1 * mockLog.trace(strXml)
    }
}