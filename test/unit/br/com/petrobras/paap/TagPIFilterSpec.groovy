package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.timeseries.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI,TagPIFilter,TagPIFilterKalman, TagPIFilterMinMax, TagPIFilterMovingAverage, TagPIFilterMovingAverage, TagPIFilterMovingAverageSample])
class TagPIFilterSpec extends Specification {

	def tag

    def setup() {
    	 tag = new TagPI( name: "t1 name",
                          formula:"t1 formula", 
                          tagType: TagPIType.SNAPSHOT, 
                          tagLocation: TagPILocation.UO_BS).save(flush:true)
    }

    def cleanup() {
    }

    void "test reset Kalman Filter"() {
    	setup: 'setting filter'
    		TagPIFilter kalman = new TagPIFilterKalman(10, 10, 1)
    		tag.addToFilters(kalman)
    		
    	when: 'adding sample'
    		for(i in 1..100){
				kalman.addAndGetResult(new TimeSeriesSample(time: new Date(1000 + i), value: 10 + i))
			}

			

		then: 'state parameters changed'
			kalman.paramP != 0
            kalman.minusP != null
            kalman.xhatminus != null
            kalman.paramK != null
            kalman.lastDate != null
            kalman.lastValue != null
            kalman.coldStartTotal != 0

    	when: 'resetting state'
    		kalman.resetState()
    		kalman.save()

    	then: 'state parameters resetted'
    		kalman.paramP == 0
            kalman.minusP == null
            kalman.xhatminus == null
            kalman.paramK == null
            kalman.lastDate == null
            kalman.lastValue == null
            kalman.coldStartTotal == 0
    }


    void "test reset max min Filter"() {
    	setup: 'setting filter'
    		TagPIFilter maxMin = new TagPIFilterMinMax(0, 1000000, 1)
    		tag.addToFilters(maxMin)
    		
    	
    	when: 'adding sample and resetting'
			maxMin.addAndGetResult(new TimeSeriesSample(time: new Date(1000), value: 10))
    		maxMin.resetState()
    		maxMin.save()

    	then: 'configuration stays the same'
			maxMin.min == 0
            maxMin.max == 1000000

    }

    void "test reset average Filter"() {
    	setup: 'setting filter'
    		TagPIFilter average = new TagPIFilterMovingAverage(10,1)
    		tag.addToFilters(average)
    		
    	when: 'adding sample'
			average.addAndGetResult(new TimeSeriesSample(time: new Date(1000), value: 10))

		then: 'state parameters changed'
    		println "average.samples: " + average.samples
    		println "average.samples.class: " + average.samples?.class
    		average.samples.size() != 0
            
    	when: 'resetting state'
    		average.resetState()
    		average = average.save()

    	then: 'state parameters resetted'
			average.samples.size() == 0
            
    }
}
