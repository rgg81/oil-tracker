package br.com.petrobras.paap

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(UserPermissionService)
@Mock([TagPI,UEP,UserPermission])
class UserPermissionServiceSpec extends Specification {

	def shiroServiceMock = Mock(ShiroService)
	def uepServiceMock = Mock(UepService)
	def validUsers = ["UP2H","UQ4N","UPN1"]

    def setup() {
    	service.shiroService = shiroServiceMock
    	service.uepService = uepServiceMock
    	new UEP(name: 'P-40', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
	                            tag: new TagPI(name: 'Tag P-40', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
	    new UEP(name: 'P-41', uo: 'UO-ES', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
	                            tag: new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
	    new UEP(name: 'P-42', uo: 'UO-BC', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
	                            tag: new TagPI(name: 'Tag P-42', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
	    new UEP(name: 'P-43', uo: 'UO-BS', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
	                            tag: new TagPI(name: 'Tag P-43', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
	    new UEP(name: 'P-44', uo: 'UO-TEST', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
	                            tag: new TagPI(name: 'Tag P-44', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();

	   	new UserPermission(attribute: 'uo', value: 'UO-BS', username: 'UP2H').save()
       	new UserPermission(attribute: 'uo', value: 'UO-BC', username: 'UP2H').save()
        new UserPermission(attribute: 'uo', value: 'UO-ES', username: 'UP2H').save()
        new UserPermission(attribute: 'uo', value: 'UO-RIO', username: 'UP2H').save()
        new UserPermission(attribute: 'id', value: 1, username: 'UP2H').save()

       	new UserPermission(attribute: 'uo', value: 'UO-BS', username: 'UQ4N').save()
  		new UserPermission(attribute: 'uo', value: 'UO-BC', username: 'UQ4N').save()
      	new UserPermission(attribute: 'uo', value: 'UO-ES', username: 'UQ4N').save()

      	
    }

    def cleanup() {
    }

    void "test find user with permissions"() {  
    	if (username in validUsers) 	
       		1 * shiroServiceMock.userDetails(_) >> [name:"Epaminondas"]
       	else
       		1 * shiroServiceMock.userDetails(_) >> { throw new IllegalArgumentException() }

       	def user = service.findUser(username)
        expect:
        	 user.permissions.uo == expected
        where:
        	username | expected								| description
        	"UP2H"   | [["UO-BS","UO-BC","UO-ES","UO-RIO"]]	| "User with permission to see all UEPs"
        	"UQ4N"   | [["UO-BS","UO-BC","UO-ES"]]			| "User with permission to see some UEPs"
        	"UPN1"   | [null]								| "User without permissions"
        	"INVALID"| []									| "User does not exist"
    }

    void "test find users by a specific permission attribute"() {
   		
    	n * shiroServiceMock.userDetails(_) >> [name:"Epaminondas"]
    	
    	def users = service.findUsersByAttribute("uo",uoname)

    	expect:
    		users == expected

    	where:
    		uoname   | n | expected 																	  | description
    		"UO-BS"  | 2 | [[username:"UP2H", name:"Epaminondas"], [username:"UQ4N", name:"Epaminondas"]] | "Finds two users with access to UO-BS"
    		"UO-RIO" | 1 | [[username:"UP2H", name:"Epaminondas"]]										  | "Finds only one user with access to UO-RIO"
    		"UO-TEST"| 0 | []																			  | "No users with access to UO-TEST"
    }

    void "Test update user permissions"() {

        new UEP(id:1, name: 'P-40', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-40', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
        new UEP(id:2, name: 'P-41', uo: 'UO-ES', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();

        new UserPermission(attribute: 'uo', value: 'UO-BS', username: 'UPN1').save()
       	new UserPermission(attribute: 'uo', value: 'UO-BC', username: 'UPN1').save()
        new UserPermission(attribute: 'uo', value: 'UO-ES', username: 'UPN1').save()

        new UserPermission(attribute: 'uo', value: 'UO-BS', username: 'HACM').save()
       	new UserPermission(attribute: 'uo', value: 'UO-BC', username: 'HACM').save()
        new UserPermission(attribute: 'uo', value: 'UO-ES', username: 'HACM').save()
        new UserPermission(attribute: 'uo', value: 'UO-RIO', username: 'HACM').save()

        new UserPermission(attribute: 'uo', value: 'UO-RIO', username: 'UP2Y').save()

        new UserPermission(attribute: 'id', value: 2, username: 'UP2I')

        1 * uepServiceMock.getListOfRegisteredAttributes(_) >> ["UO-ES","UO-BC","UO-RIO","UO-BS"]

    	def params = [:]
    	params.username = username
    	permissionList.each { permission -> 
    		params[permission] = "on"
    	}
    	service.updateUserPermissions(params)
    	
    	expect:
    		UserPermission.findAllByUsername(username).value == output

    	where:
    		username  | permissionList     					| output                             | description
    		"UPN1"    |	["UO-BS","UO-BC","UO-ES","UO-RIO"]  | ["UO-BS","UO-BC","UO-ES","UO-RIO"] | "Giving access to one more UO" 
    		"UQ4N"    |	["UO-BS","UO-BC","UO-ES","UO-RIO"]  | ["UO-BS","UO-BC","UO-ES","UO-RIO"] | "Giving access to one more UO and granting the user role"
    		"UP2H"    | ["UO-BS","UO-BC","UO-ES","uep-1"]   | ["UO-BS","UO-BC","UO-ES","1"]      | "Revoking access to one UO"
    		"HACM"    | ["UO-BS","UO-BC","UO-ES"]           | ["UO-BS","UO-BC","UO-ES"]          | "Revoking access to one UO and grating the user role"
    		"UP2Y"    | []								    | []                                 | "Revoking access to any UO or UEP and revoking the user role"
    		"UP2I"    | ["uep-1","uep-2"]                   | ["1","2"]                          | "Granting access to one UEP"  
            "INVALID" |	["UO-BC"]         					| ["UO-BC"]                          | "Invalid user"
    }

    void "Test paapUser role grants and revokes"() {
    	if (username != "INVALID")
    		1 * shiroServiceMock.userDetails(username) >> true
    	else
    		1 * shiroServiceMock.userDetails(username) >> false
    	
    	if (username == "UP2H") {
    		1 * shiroServiceMock.hasAccess(username) >> false
    		1 * shiroServiceMock.grantAccess(username)  >> true
    	}
    	else if (username == "UQ4N")
    	 	1 * shiroServiceMock.hasAccess(username) >> true
    	else if (username == "UQ4E")
    		1 * shiroServiceMock.hasAccess(username) >> false
    	else if (username == "UPN1") {
    		1 * shiroServiceMock.hasAccess(username) >> true
    		1 * shiroServiceMock.revokeAccess(username) >> true
    	}


    	def user = service.updateUserRolePermissions(username)
    	expect:
    		user["hasAccess"] == expected
    		user["notFound"] == notFound

    	where:
    		username  | expected | notFound | description
    		"UP2H"    | true	 | false    | "User has permission but doesn't have the role"
    		"UQ4N"    | true     | false    | "User has permission and role"
    		"UQ4E"    | false    | false    | "User doesn't permission and doesn't have the role"
    		"UPN1"    | false    | false    | "User doesn't permission and has the role"
    		"INVALID" | false    | true     | "Invalid user"

    }

    def "Test remove user permissions in batch"() {
        (_..4) * shiroServiceMock.userDetails(_) >> [name: "Epaminondas"]

	   	def executions = users.size()
	   	def params = [:]
	   	new UserPermission(attribute: 'uo', value: 'UO-BC', username: 'UPN1').save()

	   	users.each { user ->
	   		params[user] = "Epaminondas"
	   	}
	   	params.users = users
	   	params.attribute = "uo"
	   	params.value = uo

	   	def removedUsers = service.removeUserPermissionsBatch(params)

	   	expect:
	   		removedUsers == expected
	   		users.each { user ->
	   			assert UserPermission.findByUsernameAndValue(user,uo) == null
	   		}

  		where:
  			users    	    | uo       | expected 																	     | description
  			["UP2H"]   		| "UO-ES"  | [[username:"UP2H", name: "Epaminondas"]]		  							     | "Revoke permission of a single user"
  			["UPN1","UQ4N"] | "UO-BC"  | [[username:"UPN1", name: "Epaminondas"],[username:"UQ4N", name: "Epaminondas"]] | "Revoke permission of multiple users"
    		["UP2K"]	    | "UO-ES"  | []                                                                              | "User doesn't have the specified permission"
    }

    def "Test add permission"() {

    	if (username == "INVALID")
    		1 * shiroServiceMock.userDetails(_) >> { throw new IllegalArgumentException() }
    	else if (username == "UPN1")
    		2 * shiroServiceMock.userDetails(_) >> [name:"Epaminondas"]
    	else 
    		1 * shiroServiceMock.userDetails(_) >> [name:"Epaminondas"]

    	def params = [username: username, attribute: "uo", value: uo]

    	def result = service.addPermission(params)

    	expect:
    		result == expected
    	where:
    		username  | uo       | expected
    		"UPN1"    | "UO-RIO" | [success:true, username: "UPN1", name:"Epaminondas"]
    		"UP2H"	  | "UO-RIO" | [success:false, username: "UP2H", name: "Epaminondas"]
    		"INVALID" | "UO-RIO" | null
    }

    def "Test give admin access"() {
    	if (!notFound) {
    		1 * shiroServiceMock.userDetails(_) >> [username: username, name: "Epaminondas"]
    		1 * shiroServiceMock.hasAdminAccess(_) >> false
    		if (mockError) {    			
    			1 * shiroServiceMock.grantAdminAccess(_) >> false
    			1 * shiroServiceMock.hasAdminAccess(_) >> false
    		} else {
    			1 * shiroServiceMock.grantAdminAccess(_) >> true
    			1 * shiroServiceMock.hasAdminAccess(_) >> true
    		}
    	} else {
    		1 * shiroServiceMock.userDetails(_) >> { throw new IllegalArgumentException() }
    	}

    	def result = service.addAdmin(username)
    	
    	expect:
    		result == expected

    	where:
    		username  | notFound  | mockError | expected
    		"UP2H"    | false     | false     | [adminAcess: true, username: "UP2H", name: "Epaminondas"]  
    		"UQ4N"    | false     | true      | [adminAcess: false, username: "UQ4N", name: "Epaminondas"]  
    		"INVALID" | true      | false     | null
    }

    def "Test revoke admin access"() {
        (_..2) * shiroServiceMock.userDetails(_) >> [name: "Epaminondas"]

    	if (users.size() == 1) {
    		1 * shiroServiceMock.hasAdminAccess(_) >> true
    		1 * shiroServiceMock.revokeAdminAccess(_) >> true
    	} else if (users.size() == 2) {
    		2 * shiroServiceMock.hasAdminAccess(_) >> true
    		2 * shiroServiceMock.revokeAdminAccess(_) >> true
    	} else {
    		1 * shiroServiceMock.hasAdminAccess("UP2H") >> true
    		1 * shiroServiceMock.revokeAdminAccess("UP2H") >> true
    		1 * shiroServiceMock.hasAdminAccess("UPN1") >> true
    		1 * shiroServiceMock.revokeAdminAccess("UPN1") >> true
    		1 * shiroServiceMock.hasAdminAccess("UQ4N") >> false
    	}
    	def params = [users: users]
    	users.each { user ->
	   		params[user] = "Epaminondas"
	   	}
    	def result = service.removeAdmins(params)
    	expect:
    		result == expected
    	where:
    		users                  | expected 
    		["UP2H"]			   | [[username:"UP2H", name: "Epaminondas"]]
    		["UP2H","UPN1"]        | [[username:"UP2H", name: "Epaminondas"],[username:"UPN1", name: "Epaminondas"]]	
    		["UP2H","UPN1","UQ4N"] | [[username:"UP2H", name: "Epaminondas"],[username:"UPN1", name: "Epaminondas"]]
    }

}