package br.com.petrobras.paap

import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([SampleGroup2m, SampleGroup10m, SampleGroup1h, SampleGroup6h, SampleGroup24h])
class SampleGroupSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test create simple"()
    {
        TagPI tag = new TagPI(name: 'teste')
        when:
            SampleGroup singleSample = SampleGroupSingle.createSingle(new Date(1000), tag, 10)

        then:
            singleSample.date == new Date(1000)
            singleSample.tag == tag
            singleSample.min == 10
            singleSample.max == 10
            singleSample.mean == 10
            singleSample.sum == 10
            singleSample.num == 1
    }

    void "test sample groups durations" () {
        expect:
            new SampleGroup2m().getDuration().toMilliseconds() == 2 * 60 * 1000
            new SampleGroup10m().getDuration().toMilliseconds() == 10 * 60 * 1000
            new SampleGroup1h().getDuration().toMilliseconds() == 60 * 60 * 1000
            new SampleGroup6h().getDuration().toMilliseconds() == 6 * 60 * 60 * 1000
            new SampleGroup24h().getDuration().toMilliseconds() == 24 * 60 * 60 * 1000
            new SampleGroupSingle().getDuration() == null
    }
}
