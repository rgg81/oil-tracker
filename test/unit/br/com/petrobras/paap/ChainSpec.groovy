package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.dsl.Chain
import static br.com.petrobras.paap.dsl.ProcessType.*

import br.com.petrobras.paap.job.*
import org.springframework.jndi.JndiObjectFactoryBean
import org.springframework.jndi.JndiTemplate

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class ChainSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

	def doWithSpring = {
        piDataService(PIDataService)
        readPIService(ReadPIService)
        tagPIService(TagPIService)
        processTagService(ProcessTagService)
        reloadQueueService(ReloadQueueService)
        wsMessageService(WSMessageService)
        eventService(EventService)
    }


    void "test chain build"() {

        def chain = new Chain().with(pireader).with(tagcalculator).with(filters).with(creategroups).with(save).with(jms)
        expect:
			chain.first.class == ProcessTagWithoutCalculation
			chain.first.next.class == ProcessTagWithCalculation
            chain.first.next.next.class == ProcessFilters
            chain.first.next.next.next.class == ProcessCreateSampleGroups
			chain.first.next.next.next.next.class == ProcessDataSaveSamples
            chain.first.next.next.next.next.next.class == ProcessDataJMS
    }
}
