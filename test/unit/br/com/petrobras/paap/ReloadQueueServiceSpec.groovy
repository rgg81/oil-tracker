package br.com.petrobras.paap

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll
import javax.jms.*
import java.util.Enumeration
import org.springframework.jndi.JndiObjectFactoryBean
import grails.test.mixin.web.ControllerUnitTestMixin

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ReloadQueueService)
@Mock([TagPI])
@TestMixin(ControllerUnitTestMixin)
class ReloadQueueServiceSpec extends Specification {

	def factory 
	def queue
	def con
   	def session
    def browser
    def producer


    def setup() {
    	factory = Mock(QueueConnectionFactory)
		queue = Mock(Queue)
		con = Mock(QueueConnection)
   		session =  Mock(QueueSession)
    	browser = Mock(QueueBrowser)
    	producer = Mock(MessageProducer)
    	service.jmsConnectionFactory = factory
    	service.jmsQueueResetTag = queue  
    }

    def cleanup() {
    }

    void "test browse tags"() {

    	setup: 	
		
			Vector tags = new Vector()
			for(i in 1..5){
				def json = '{"'+i+'":true }'
				def message = Mock(Message)
				message.metaClass.getText = {-> json}
	      		tags.add(message)
	      	}

	      	for( i in 1..5){
	      		for( j in 11..20){
					def json = '{"'+j +'": false}'
					def message = Mock(Message)
					message.metaClass.getText = {-> json}
		      		tags.add(message)
	      		}
	      	}

    	when:

    		def ret = service.browse()
        
        then:
	        1 * factory.createQueueConnection() >> con
	        1 * con.start()
	        1 * con.createQueueSession(false,Session.AUTO_ACKNOWLEDGE) >> session
	        1 * session.createBrowser(queue) >> browser
	        1 * browser.getEnumeration() >> tags.elements()
	        1 * con.close()
	        ret.size() == 15
	        ret.findAll{it.value == false}.size() == 10
	        ret.findAll{it.value == true}.size() == 5
	        ret.keySet().getAt(0).class == Long
	        ret.keySet() == [1, 2, 3, 4, 5, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20] as Set


    }

     void "test push snapshot tag"() {

    	setup:
    		def message = Mock(TextMessage)
			def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
			def msgCheck = '['+t1.id+']'
    	when:
    		service.push(queue,[t1] as Set)
        
        then:
	        1 * factory.createQueueConnection() >> con
	        1 * con.createQueueSession(false,Session.AUTO_ACKNOWLEDGE) >> session
	        1 * session.createProducer(queue) >> producer
	        1 * session.createTextMessage() >> message
	        1 * message.setText(msgCheck)
	        1 * producer.send(message)
	        1 * con.close()
    }

    void "test push calculated tag"() {

    	setup:
    		def message = Mock(TextMessage)
			def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
			def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
			def calc = new TagPI(name: 'calc', formula: "'tag1'+'tag2'", tagType: TagPIType.CALCULATED).save(flush: true)
			def msgCheck = '['+calc.id+','+t1.id+','+t2.id+']'
    	when:
    		service.push(queue,[calc,t1,t2] as Set)
        
        then:
	        1 * factory.createQueueConnection() >> con
	        1 * con.createQueueSession(false,Session.AUTO_ACKNOWLEDGE) >> session
	        1 * session.createProducer(queue) >> producer
	        1 * session.createTextMessage() >> message
	        1 * message.setText(msgCheck)
	        1 * producer.send(message)
	        1 * con.close()
    }

    @Unroll
     void "test read tags"(threshold,result_tag_count) {
 		given:
    		def msgs = ['["3","1","2","4"]','["5","6","7"]','["9","10","11"]',null]

			def consumer = Mock(MessageConsumer)
			msgs = msgs.collect{
				if(!it)
					return it
				def message = Mock(Message)
				message.metaClass.getText = {-> it}
				message
			}
			
        and:
        	1 * factory.createQueueConnection() >> con
	        1 * con.start()
	        1 * con.createQueueSession(true,Session.SESSION_TRANSACTED) >> session
	        1 * session.createConsumer(queue) >> consumer
	        consumer.receive(_) >>> msgs
	        1 * session.commit()
	        1 * con.close()
			
			def result
	        service.read(queue,{tags -> result = tags},threshold)

        expect:
        	
	        result.size() == result_tag_count

		where:	    
			threshold 	| 	result_tag_count 
			1 			| 	4				 
			4 			| 	4				 
			5			|	7					
			7			|	7
			8			|	10
			10			|	10
    }

    //TODO rollback read
}
