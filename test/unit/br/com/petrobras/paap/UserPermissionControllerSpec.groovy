package br.com.petrobras.paap



import grails.test.mixin.*
import spock.lang.*

@TestFor(UserPermissionController)
@Mock([UserPermission,TagPI,UEP])
class UserPermissionControllerSpec extends Specification {

    def shiroServiceMock = Mock(ShiroService)
    def userPermissionServiceMock = Mock(UserPermissionService)
    def uepServiceMock = Mock(UepService)

    def setup() {        
        controller.shiroService = shiroServiceMock
       	controller.userPermissionService = userPermissionServiceMock
       	controller.uepService = uepServiceMock
    }

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test edit user permission"() {

    	if (username == "UQ4N")
    		1 * userPermissionServiceMock.findUser(username) >> [[username:"UQ4N", name:"Epaminondas", permissions:[uo:["UO-BS", "UO-BC", "UO-ES"]]]]
    	else if (username == "UP2H")
    		1 * userPermissionServiceMock.findUser(username) >> [[username:"UP2H", name:"Epaminondas", permissions:[:]]]

    	else
    		1 * userPermissionServiceMock.findUser(username) >> []
    	
    	1 * uepServiceMock.getAllUEPs(_) >> [[name:"UO-ES", values:[[name:"FPSO Norte Capixaba", id: 1], [name:"FPSO-CDAN", id: 2]]],[name:"UO-RIO", values:[[name:"FPSO-NIT", id:3], [name:"P-40",id: 4]]], [name:"UO-BS", values:[[name:"FPSO Ilhabela", id:5], [name:"FPSO Mangaratiba",id:6]]]]
        (_..1) * uepServiceMock.getListOfRegisteredAttributes("uo") >> ["UO-ES", "UO-RIO","UO-BS"]

    	params.username = username
    	controller.edit()
	   	expect:
	   		view == expectedView
	   		model == expectedModel
    	where:
    		username  | expectedView            | expectedModel
    		"UQ4N"    | "/userPermission/edit"  | [permissions:[[username:"UQ4N", name:"Epaminondas", permissions:[uo:["UO-BS", "UO-BC", "UO-ES"]]]], uos: [[name:"UO-ES", values:[[name:"FPSO Norte Capixaba", id: 1], [name:"FPSO-CDAN", id: 2]]],[name:"UO-RIO", values:[[name:"FPSO-NIT", id:3], [name:"P-40",id: 4]]], [name:"UO-BS", values:[[name:"FPSO Ilhabela", id:5], [name:"FPSO Mangaratiba",id:6]]]]]
    		"UP2H"	  | "/userPermission/edit"  | [permissions:[[username:"UP2H", name:"Epaminondas", permissions:[:]]], uos: [[name:"UO-ES", values:[[name:"FPSO Norte Capixaba", id: 1], [name:"FPSO-CDAN", id: 2]]],[name:"UO-RIO", values:[[name:"FPSO-NIT", id:3], [name:"P-40",id: 4]]], [name:"UO-BS", values:[[name:"FPSO Ilhabela", id:5], [name:"FPSO Mangaratiba",id:6]]]]]
    		"INVALID" | "/userPermission/index" | [uos: ["UO-ES", "UO-RIO","UO-BS"]]

    }

    void "Test edit permissions for a specific UO"() {
    	if (uoname == "UO-BS")
    		1 * userPermissionServiceMock.findUsersByAttribute("uo",uoname) >> [[username:"UP2H", name:"Epaminondas"], [username:"UQ4N", name:"Epaminondas"]]
    	else if (uoname == "UO-RIO")
    		1 * userPermissionServiceMock.findUsersByAttribute("uo",uoname) >> [[username:"UP2H", name:"Epaminondas"]]
    	else if (uoname == "UO-TEST")
    		1 * userPermissionServiceMock.findUsersByAttribute("uo",uoname) >> []

    	params.attribute = "uo"
    	params.value = uoname
    	
    	controller.editAttribute()

    	expect:
    		view == expectedView
    		model.attribute == expectedModel.attribute
            model.value == expectedModel.value
            model.users == expectedModel.users
            model.userCount == expectedModel.userCount
    	where:
    		uoname    | expectedView                    | expectedModel
    		"UO-BS"   | "/userPermission/editAttribute" | [attribute: "uo", value: "UO-BS", users: [[username:"UP2H", name:"Epaminondas"],[username:"UQ4N", name:"Epaminondas"]], userCount:2]
    		"UO-RIO"  | "/userPermission/editAttribute" | [attribute: "uo", value: "UO-RIO", users: [[username:"UP2H", name:"Epaminondas"]], userCount:1]
    		"UO-TEST" | "/userPermission/editAttribute" | [attribute: "uo", value: "UO-TEST", users: [], userCount:0]
    }

    void "Test update user's permissions"() {
    	def params = [:]
    	params.username = username
   		uolist.each { uo -> 
    		params[uo] = "on"
    	}

    	if (username == "UP2H") {
    		1 * userPermissionServiceMock.updateUserPermissions(_) >> [username: "UP2H", attribute:"uo", value:"UO-BC"]
    		1 * userPermissionServiceMock.updateUserRolePermissions(_) >> [hasAccess: true]
    	}

   		controller.update()

   		expect:
   			response.redirectUrl == expectedView

   		where:
   			username | uolist             | expectedView
   			"UP2H"	 | ["UO-BC","UO-RIO"] | "/admin/userPermission/edit?username="
    }

    void "Test remove user permissions in batch"() {
    	if (users instanceof String) {
    		if (users == "UPN1")
    			1 * userPermissionServiceMock.removeUserPermissionsBatch(_) >> [[username:"UPN1", name: "Epaminondas"]]
    		else
    		 	1 * userPermissionServiceMock.removeUserPermissionsBatch(_) >> []
    	} else {
    		1 * userPermissionServiceMock.removeUserPermissionsBatch(_) >> [[username:"UP2H", name: "Epaminondas"],[username:"UQ4N", name: "Epaminondas"]]
    	}
    	
    	params.users = users
    	params.attribute = "uo"
    	params.value = uo
    	controller.updateAttribute()
    	expect:
    		response.redirectUrl == expectedView
    		flash.users == removedUsers
    	where:
    		users           | uo       | removedUsers																     | expectedView
    		["UP2H","UQ4N"] | "UO-RIO" | [[username:"UP2H", name: "Epaminondas"],[username:"UQ4N", name: "Epaminondas"]] | "/admin/userPermission/editAttribute?displayText=UO-RIO&attribute=uo&value=UO-RIO"
    		"UPN1"		    | "UO-BC"  | [[username:"UPN1", name: "Epaminondas"]] 										 | "/admin/userPermission/editAttribute?displayText=UO-BC&attribute=uo&value=UO-BC"
    		"INVALID"       | "UO-BS"  | null																			 | "/admin/userPermission/editAttribute?displayText=UO-BS&attribute=uo&value=UO-BS"
    }

    void "Test add user permission for a UO"() {

    	params.username = user
    	params.attribute = "uo"
    	params.value = uo

    	1 * userPermissionServiceMock.addPermission(_) >> expectedModel

    	controller.addPermission()

    	expect:
    		flash.error == error

    	where:
    		user      | uo      | expectedModel 										 | error 													| expectedView
    		"UP2H"    | "UO-BC" | [success:true, username: "UP2H", name:"Epaminondas"]   | null  													| "/admin/userPermission/editAttribute?attribute=uo&value=UO-BC"
    		"UPN1"    | "UO-BC" | [success:false, username: "UPN1", name:"Epaminondas"]  | "UserPermissions.error.user.already.has.permission"      | "/admin/userPermission/editAttribute?attribute=uo&value=UO-BC"
    		"INVALID" | "UO-BC" | null											    	 | "UserPermission.user.not.found"      					| "/admin/userPermission/editAttribute?attribute=uo&value=UO-BC"
    }

    void "Test add user permission for a UEP"() { 
        params.username = user
        params.attribute = "id"
        params.value = 1
        new UEP(id:1, name: 'P-40', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-40', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
 
        1 * userPermissionServiceMock.addPermission(_) >> expectedModel

        controller.addPermission()

        expect:
            flash.error == error

        where:
            user      | expectedModel                                          | error                                                    | expectedView
            "UP2H"    | [success:true, username: "UP2H", name:"Epaminondas"]   | null                                                     | "/admin/userPermission/byUEP?displayText=P-40&attribute=id&value=1"
            "UPN1"    | [success:false, username: "UPN1", name:"Epaminondas"]  | "UserPermissions.error.user.already.has.permission"      | "/admin/userPermission/byUEP?displayText=P-40&attribute=id&value=1"
            "INVALID" | null                                                   | "UserPermission.user.not.found"                          | "/admin/userPermission/byUEP?displayText=P-40&attribute=id&value=1"
    }

    void "Test remove user permissions from UEP in batch"() {
        new UEP(id:1, name: 'P-40', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-40', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
 
        if (users instanceof String) {
            if (users == "UPN1")
                1 * userPermissionServiceMock.removeUserPermissionsBatch(_) >> [[username:"UPN1", name: "Epaminondas"]]
            else
                1 * userPermissionServiceMock.removeUserPermissionsBatch(_) >> []
        } else {
            1 * userPermissionServiceMock.removeUserPermissionsBatch(_) >> [[username:"UP2H", name: "Epaminondas"],[username:"UQ4N", name: "Epaminondas"]]
        }
        
        params.users = users
        params.attribute = "id"
        params.value = 1
        controller.updateAttribute()
        expect:
            response.redirectUrl == expectedView
            flash.users == removedUsers
        where:
            users           | removedUsers                                                                    | expectedView
            ["UP2H","UQ4N"] | [[username:"UP2H", name: "Epaminondas"],[username:"UQ4N", name: "Epaminondas"]] | "/admin/userPermission/byUEP?displayText=P-40&attribute=id&value=1"
            "UPN1"          | [[username:"UPN1", name: "Epaminondas"]]                                        | "/admin/userPermission/byUEP?displayText=P-40&attribute=id&value=1"
            "INVALID"       | null                                                                            | "/admin/userPermission/byUEP?displayText=P-40&attribute=id&value=1"
    }
   
}
