package br.com.petrobras.paap

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
import groovy.time.TimeCategory
import static plastic.criteria.PlasticCriteria.*
import grails.converters.JSON

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(PIDataController)
@Mock([UEP,Sample,SampleGroup2m,SampleGroup10m,SampleGroup1h,TagPI,SitopBdo])
class PIDataControllerSpec extends Specification {

	def uepService
	def piDataService

	def definedNow = Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:30:10")

	def setup() {
		uepService = Mock(UepService)
		uepService.metaClass.snapShotAverage24hours = { [:] }
        controller.uepService = uepService

        piDataService = Spy(PIDataService)
		piDataService.metaClass.now = { definedNow }
        controller.piDataService = piDataService

		// Define 'now' function for controller
		controller.metaClass.now = { definedNow }
		Date.metaClass.truncateToSeconds = { seconds -> new Date( delegate.time - delegate.time % (1000*seconds) ) }
    }

    def cleanup() {
		Date.metaClass.truncateToSeconds = null
    }

	private createSampleGroup(clazz, date, value, tag) {
		clazz.newInstance(date: date, mean: value, tag: tag, min: value, max: value, sum: value, variance: 0, num: 1 )
	}

    void "test lastsamples with all resolution"() {
        def tag = new TagPI(name: 'not relevant', formula: '1+1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()

		// Note: definedNow = 14:30:10

		// Samples before request
		def s0 = new Sample(date: Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 13:28:00"), value: 1, tag: tag).save()
		def s1 = new Sample(date: Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:28:00"), value: 2, tag: tag).save()

		// Sample equals to request
		def s2 = new Sample(date: Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:28:10"), value: 3, tag: tag).save()

		// Sample after request
		def s3 = new Sample(date: Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:28:20"), value: 3, tag: tag).save()

		// Sample after request
		def s4 = new Sample(date: Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:29:40"), value: 4, tag: tag).save()

		def uep = new UEP( name: 'Mexilhão Condensado', uo: 'UO-BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save()

		when: 'request for all samples'
			params.minutes = '2'
			params.resolution = 'all'
			controller.lastsamples("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			response.json == [[s2.date.time, s2.value], [s3.date.time, s3.value], [s4.date.time, s4.value]]

		when: 'request to uepInitialData, whith returns all data on highSamples for 1 hour'
			response.reset()
			controller.uepInitialData("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			response.json.highSamples == [[s1.date.time, s1.value], [s2.date.time, s2.value], [s3.date.time, s3.value], [s4.date.time, s4.value]]


		when: 'no access to uep'
			response.reset()
        	controller.uepInitialData("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403

    }

	void "test lastsamples with high resolution"() {
		def tag = new TagPI(name: 'not relevant', formula: '1+1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()

		// Note: definedNow = 14:30:10

		// Sample before request
		def s1 = createSampleGroup(SampleGroup2m, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:24:00"), 1, tag).save()
		// Sample equals to truncated request
		def s2 = createSampleGroup(SampleGroup2m, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:26:00"), 2, tag).save()
		// Sample after request
		def s3 = createSampleGroup(SampleGroup2m, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:28:00"), 3, tag).save()
		// Sample after request
		def s4 = createSampleGroup(SampleGroup2m, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:30:00"), 4, tag).save()

		def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save()

		when: 'request for high samples'
			params.minutes = '4'
			params.resolution = 'high'
			controller.lastsamples("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			response.json == [[s3.date.time, s3.mean], [s4.date.time, s4.mean]]

		when: 'no access to uep'
			response.reset()
        	controller.uepInitialData("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403

	}

	void "test lastsampleswith medium resolution"() {
		def tag = new TagPI(name: 'not relevant', formula: '1+1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()

		// Note: definedNow = 14:30:10

		// Samples before request
		def s0 = createSampleGroup(SampleGroup10m, Date.parse("dd/MM/yyyy HH:mm:ss", "23/04/2015 14:30:00"), 1, tag).save()
		def s1 = createSampleGroup(SampleGroup10m, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:00:00"), 1, tag).save()
		// Sample equals to truncated request
		def s2 = createSampleGroup(SampleGroup10m, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:10:00"), 2, tag).save()
		// Sample after request
		def s3 = createSampleGroup(SampleGroup10m, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:20:00"), 3, tag).save()
		// Sample after request
		def s4 = createSampleGroup(SampleGroup10m, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:30:00"), 4, tag).save()

		def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save()

		when: 'request for medium samples'
			params.minutes = '20'
			params.resolution = 'medium'
			controller.lastsamples("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			response.json == [[s3.date.time, s3.mean], [s4.date.time, s4.mean]]

		when: 'request to uepInitialData, whith returns medium data on lowSamples for 24h'
			response.reset()
			controller.uepInitialData("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
		piDataService.average24hours(uep.id) >> 1.0	// not relevant
			response.json.lowSamples == [[s1.date.time, s1.mean], [s2.date.time, s2.mean], [s3.date.time, s3.mean], [s4.date.time, s4.mean]]

		when: 'no access to uep'
			response.reset()
        	controller.uepInitialData("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403

	}

	void "test lastsamples with low resolution"() {
		def tag = new TagPI(name: 'not relevant', formula: '1+1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()

		// Note: definedNow = 14:30:10

		// Sample before request
		def s1 = createSampleGroup(SampleGroup1h, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 11:00:00"), 1, tag).save()
		// Sample equals to truncated request
		def s2 = createSampleGroup(SampleGroup1h, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 12:00:00"), 2, tag).save()
		// Sample after request
		def s3 = createSampleGroup(SampleGroup1h, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 13:00:00"), 3, tag).save()
		// Sample after request
		def s4 = createSampleGroup(SampleGroup1h, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:00:00"), 4, tag).save()

		def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save()

		when: 'request for low samples'
			params.minutes = '120'
			params.resolution = 'low'
			controller.lastsamples("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			response.json == [[s3.date.time, s3.mean], [s4.date.time, s4.mean]]

		when: 'no access to uep'
			response.reset()
        	controller.uepInitialData("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403

	}

	void "test lastsamples max and negative minutes"() {
		def tag = new TagPI(name: 'not relevant', formula: '1+1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()

		// Note: definedNow = 14:30:10

		// Sample more than 24h before definedNow
		def s1 = new Sample(date: Date.parse("dd/MM/yyyy HH:mm:ss", "23/04/2015 14:30:00"), value: 1, tag: tag).save()
		// Sample exactly 24 before definedNow
		def s2 = new Sample(date: Date.parse("dd/MM/yyyy HH:mm:ss", "23/04/2015 14:30:10"), value: 2, tag: tag).save()
		// Sample inside 24h since definedNow
		def s3 = new Sample(date: Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:28:00"), value: 3, tag: tag).save()

		def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save()



		when: 'no access to uep'
			response.reset()
        	controller.uepInitialData("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403

	}

	void "test lastsamples default values"() {
		def tag = new TagPI(name: 'not relevant', formula: '1+1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()

		// Note: definedNow = 14:30:10

		// Sample more than 24h before definedNow
		// Sample before request
		def s1 = createSampleGroup(SampleGroup1h, Date.parse("dd/MM/yyyy HH:mm:ss", "23/04/2015 14:30:00"), 1, tag).save()
		// Sample equals to truncated request
		def s2 = createSampleGroup(SampleGroup1h, Date.parse("dd/MM/yyyy HH:mm:ss", "23/04/2015 14:30:10"), 2, tag).save()
		// Sample after request
		def s3 = createSampleGroup(SampleGroup1h, Date.parse("dd/MM/yyyy HH:mm:ss", "24/04/2015 14:28:00"), 3, tag).save()

		def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save()

		when: 'requested wihtout parameters'
			controller.lastsamples("${uep.id}")
		then: 'it should return samples of 1h (low) within 24h'
			1 * uepService.hasPermission(_) >> true
			response.json == [[s2.date.time, s2.mean], [s3.date.time, s3.mean]]

		when: 'no access to uep'
			response.reset()
        	controller.uepInitialData("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403

	}
	void "test lastValue function"(){
		given:
	        def tag = new TagPI(name: 'not relevant', formula: '1 + 1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()
        	def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save(flush:true)

		when: 'request to uepInitialData'
			response.reset()
			controller.uepInitialData("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			1 * uepService.lastValue(uep) >> 5
			piDataService.average24hours(uep.id) >> 1.0	// not relevant
			response.json.lastReceivedValue == 5

	}
	void "test alert controller"(){

		def tag = new TagPI(name: 'not relevant', formula: '1 + 1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()
        def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save(flush:true)

		when: "alert 1"
			controller.alert("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			piDataService.alert(uep.id) >> 1
			response.json.alert == 1

		when: 'request to uepInitialData'
			response.reset()
			controller.uepInitialData("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			piDataService.alert(uep.id) >> 1
			piDataService.average24hours(uep.id) >> 1.0	// not relevant
			response.json.alert == 1

		when: 'no access to uep'
			response.reset()
        	controller.alert("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403
	}
	void "test average24hours controller"(){

		def tag = new TagPI(name: 'not relevant', formula: '1 + 1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()
        def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save(flush:true)

		when: "valid average 1"
			controller.average24hours("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			piDataService.average24hours(uep.id) >> 200.0
			response.json.avg == 200.0

		when: 'request to uepInitialData'
			response.reset()
			controller.uepInitialData("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			piDataService.average24hours(uep.id) >> 200.0
			response.json.avg24 == 200.0


		when: 'no access to uep'
			response.reset()
        	controller.average24hours("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403

	}
	void "test sitopBdo controller"(){
		setup:
	        def tag = new TagPI(name: 'not relevant', formula: '1 + 1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()
	        def tag2 = new TagPI(name: 'not relevant 2', formula: '1 + 1 + 1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()
        	def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save(flush:true)

        	def uep_no_sitop = new UEP( name: 'Mexilhão Condensado 2', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag2).save(flush:true)

			def s1 = new SitopBdo(expected: 100.0, preliminary: 150.0, sitopDate: new Date(10),uep: uep ).save()
			def s2 = new SitopBdo(expected: 200.0, preliminary: 250.0, sitopDate: new Date(20),uep: uep).save()

		when: 'sitop ok'
			controller.sitopBdo("${uep.id}")

		then:
			1 * uepService.hasPermission(_) >> true
			response.json.expected == 200.0
			response.json.preliminary == 250.0

		when: 'request to uepInitialData'
			response.reset()
			controller.uepInitialData("${uep.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			piDataService.average24hours(uep.id) >> 1.0	// not relevant
			response.json.expected == 200.0
			response.json.preliminary == 250.0

		when: "no sitop"
			response.reset()
			controller.sitopBdo("${uep_no_sitop.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			response.json == [:]

		when: 'request to uepInitialData'
			response.reset()
			controller.uepInitialData("${uep_no_sitop.id}")
		then:
			1 * uepService.hasPermission(_) >> true
			piDataService.average24hours(uep_no_sitop.id) >> 1.0	// not relevant
			response.json.expected.equals(null)
			response.json.preliminary.equals(null)


		when: 'no access to uep'
			response.reset()
        	controller.sitopBdo("${uep.id}")
        then:
            1 * uepService.hasPermission(_) >> false
        	response.status == 403
	}

	void "test lastsamples by UEP with time interval option"() {
		def tag = new TagPI(name: 'not relevant', formula: '1+1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()

		def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
				tag: tag).save()

		when:
		controller.lastSamplesUep("${uep.id}", 24)
		then:
		1 * piDataService.lastsamplesfortag(tag, 60 * 24 * 1, 'medium')

		when:
		controller.lastSamplesUep("${uep.id}", 48)
		then:
		1 * piDataService.lastsamplesfortag(tag, 60 * 24 * 2, 'medium')

		when:
		controller.lastSamplesUep("${uep.id}", 72)
		then:
		1 * piDataService.lastsamplesfortag(tag, 60 * 24 * 3, 'medium')

	}
}
