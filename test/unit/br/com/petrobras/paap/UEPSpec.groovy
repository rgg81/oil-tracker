package br.com.petrobras.paap

import grails.test.mixin.TestFor
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */

@TestFor(UEP)
class UEPSpec extends ConstraintUnitSpec {


    @Unroll("test uep all constraints #field is #error")
    void "test constraint"() {
        when:
        def obj = new UEP("$field": val)

        then:
        validateConstraints(obj, field, error)

        where:
        error                  | field          | val
        'blank'                | 'name'         | ''
        'blank'                | 'uo'           | ''
        'blank'                | 'department'   | ''
        'blank'                | 'oceanCurrent' | ''
        'blank'                | 'oilWell'      | ''
        'blank'                | 'tag'          | ''
        'valid'                | 'name'         | 'UEP'
        'valid'                | 'uo'           | 'UO-RIO'
        'valid'                | 'department'   | 'PRESAL'
        'valid'                | 'oceanCurrent' | 'OESTE'
        'valid'                | 'oilWell'      | 'SUL'
        'valid'                | 'tag'          | getTagPI('Valid Name', 'Any Formula', TagPIType.CALCULATED, TagPILocation.UO_BS) 
        'onlyCalculated'       | 'tag'          | getTagPI('Valid Name', 'Any Formula', TagPIType.SNAPSHOT, TagPILocation.UO_BS) 
        'onlyCalculated'       | 'tag'          | getTagPI('Valid Name', 'Any Formula', TagPIType.ACCUMULATOR, TagPILocation.UO_BS) 

    }
}
