package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*


/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI])
class PiExpressionBuilderSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test expression only one variable, name different from formula"() {

        def formula = "('taga')/3"
        def tagValues = ['taga' : 15.0d] 
        int i = 0;
        tagValues.each{ k, v ->
                new TagPI(id: i++ ,name: "any name" + k,formula:k, tagType: TagPIType.SNAPSHOT,tagLocation: TagPILocation.UO_RIO).save(flush:true)
        }

        TagPIExpression expression = new TagPIExpressionBuilder(formula).variables(tagValues.keySet()).build()
        expression.setVariable('taga',15.0d)
        
        expect:
            expression.evaluate() ==  5
    }

    void "test expression normalized formula"() {

    	def formula = '(t1 + t2 + t3)/3'
    	def tagValues = ['taga' : 5.0d,'tagb' : 10.0d,'tagc' : 15.0d] 
    	int i = 0;
    	tagValues.each{ k, v ->
    		    new TagPI(id: i++ ,name: "any name" + k,formula:k, tagType: TagPIType.SNAPSHOT,tagLocation: TagPILocation.UO_RIO).save(flush:true)
    	}

    	TagPIExpression expression = new TagPIExpressionBuilder(formula).variables(tagValues.keySet()).build()
    	expression.setVariables(tagValues)
    	
    	expect:
			expression.evaluate() ==  10
    }


    void "test expression normalized formula passing tags"() {

    	def formula = '(t1 + t2 + t3)/3'
    	def tagValues = ['taga' : 5.0d,'tagb' : 10.0d,'tagc' : 15.0d] 
    	int i = 0;
    	def tags = []
    	tagValues.each{ k, v ->
    		    tags.push new TagPI(id: i++ ,name: "any name" + k,formula:k, tagType: TagPIType.SNAPSHOT,tagLocation: TagPILocation.UO_RIO).save(flush:true)
    	}

    	TagPIExpression expression = new TagPIExpressionBuilder(formula,tags).variables(tagValues.keySet()).build()
    	expression.setVariables(tagValues)
    	
    	expect:
			expression.evaluate() ==  10
    }

    @Unroll
    void "test expression formula #formula with real names "(formula,tagValues,expectedresult) {
    	

		tagValues = tagValues as HashMap<String,Double>
    	int i = 0;
    	tagValues.each{ k, v ->
    		    new TagPI(id: i++ ,name: "any name" + k,formula:k, tagType: TagPIType.SNAPSHOT,tagLocation: TagPILocation.UO_RIO).save(flush:true)
    	} 	
    	
        def result
        try{ 
            TagPIExpression expression = new TagPIExpressionBuilder(formula).variables(tagValues.keySet()).build()
            expression.setVariables(tagValues)
            result = expression.evaluate()

        }catch(Exception e){
            println e.message
            result = 'exception'
        }

    	expect:
			result==  expectedresult
		where:
			formula 						    | tagValues						              	| expectedresult
			"('tag-a' + 'tag-b' + 'tag-c')/3"   | ['tag-a' : 5d,'tag-b' : 10d,'tag-c' : 15d]	|   10
			"('tag+a' + 'tag+b' + 'tag+c')/3"   | ['tag+a' : 5d,'tag+b' : 10d,'tag+c' : 15d]	|   10
			"'tag-a'/2 + 'tag-c'/3 + 'tag-a'/2" | ['tag-a' : 10d,'tag-c' : 15d]				    |	15	
            "'a'/2 + 'a-b'/3 + 'a'/2"           | ['a' : 10d,'a-b' : 15d]                       |   15
            "a/2 + 'a-b'/3 + 'a/2"              | ['a' : 10d,'a-b' : 15d]                       |   'exception'
            "a/2 + 'a-b'/3 + a/2"               | ['a' : 10d,'a-b' : 15d]                       |   'exception'

    }

}
