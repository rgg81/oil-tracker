package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.timeseries.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI,Sample,EventTagPI])
class InterpolatorDataStrategySpec extends Specification {

	def grailsApplication
    def readPIService = Mock(ReadPIService)

    def setup() {
    }

    def cleanup() {
    }

    void "test interpolate data strategy in memory, begin - end range doesn't matter"() {
    	setup:
        def begin = new Date(10 * 1000)
        def end = new Date(50 * 1000)
        def t1 = new TagPI(name: 't1', formula:'t1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        def t2 = new TagPI(name: 't2', formula:'t2', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        def t3 = new TagPI(name: 't4', formula:"'t1' + 't2'", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)

        def samplesValues = [[20,100],[3000,200],[40,150],[70,150]]

        def tagSamplesMap = [:]
		tagSamplesMap[t1] = [samples:[]]
		tagSamplesMap[t2] = [samples:[]]

		samplesValues.each{
			tagSamplesMap[t1].samples.push(new TimeSeriesSample(time:new Date(it[0]*1000),value:it[1]))
		}

    	def strategy = new InterpolatorDataStrategy(readPIService,grailsApplication,[memory:true,tagSamplesMap:tagSamplesMap])
    	when: 
    		def ret = strategy.getTimeSeriesSamples(t1,begin,end)
    	then:
    		ret.size() == 4

    	when:
    		ret = strategy.getTimeSeriesSamples(t2,begin,end)
    	then:
    		ret.size() == 1
    		ret[0].value == 0

        when:
            ret = strategy.getTimeSeriesSamples(t3,begin,end)
        then:
            ret.size() == 1
            ret[0].value == 0

    	when:
            new Sample(date:new Date(30 * 1000), value: 100, tag:t3).save(flush:true)
    		ret = strategy.getTimeSeriesSamples(t3,begin,end)
    	then:
    		ret.size() == 1
    		ret[0].value == 100


    }


    /*def "test when we don't have a spaced snapshot sample stored in the database, get one from PI"() {
        setup:
            def begin = new Date(1 * 60 * 60 * 1000)
            def end = new Date(25 * 60 * 60 * 1000)
            def t3 = new TagPI(name: 't3', formula:'t3', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

            def strategy = new InterpolatorDataStrategy(readPIService,grailsApplication)

            1 * readPIService.readLastDataFromTag(_,_) >> [new TimeSeriesSample(time: new Date(30*60*1000), value: 70)]
        
        when:
            def ret = strategy.getTimeSeriesSamples(t3,begin,end)
            def samples = Sample.findAllByTag(t3)
        then:
            samples.size() == 1
            samples.first().value == 70
            ret.size() == 1
            ret[0].value == 70
    }

    def "test get a spaced snapshot sample older than the time specified - from Database"() {
        setup:
            def begin = new Date(1 * 60 * 60 * 1000)
            def end = new Date(25 * 60 * 60 * 1000)
            def t1 = new TagPI(name: 't1', formula:'t1', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
            new Sample(date: new Date(30 * 60 * 100) ,value: 80 ,tag:t1).save(flush:true,failOnError:true)

            def strategy = new InterpolatorDataStrategy(readPIService,grailsApplication)

            0 * readPIService.readLastDataFromTag(_,_)
        
        when:
            def ret = strategy.getTimeSeriesSamples(t1,begin,end)
        then:
            ret.size() == 1
            ret[0].value == 80
    }*/

}
