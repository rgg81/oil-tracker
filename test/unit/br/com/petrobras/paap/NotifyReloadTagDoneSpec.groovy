package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.job.NotifyReloadTagDone

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI])
class NotifyReloadTagDoneSpec extends Specification {

	def t1, t2
	def process
    def setup() {
    	t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        process = new NotifyReloadTagDone(Mock(ReloadQueueService),grailsApplication)
    }

    def cleanup() {
    }

    void "test processInternal null data"() {
    	when:
    	process.processInternal(null)

    	then:
    	thrown(IllegalStateException)
    }

    void "test processInternal invalid data"() {
    	when:
    	process.processInternal([])

    	then:
    	thrown(IllegalStateException)
    }

    void "test processInternal empty data"() {
    	when:
    	process.processInternal([:])

    	then:
    	notThrown() 
    }

    void "test processInternal valid data"() {
        def data = [(t1):[],(t2):[]]
    	when:
    	def r = process.processInternal(data)
    	then:
    		1*process.reloadQueueService.pushToEndQueue(data.keySet()) 
    }
}
