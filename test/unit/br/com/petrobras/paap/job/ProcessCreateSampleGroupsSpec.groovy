package br.com.petrobras.paap.job

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.SampleGroupAggregator
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.TagPIType
import br.com.petrobras.paap.TagPILocation
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import br.com.petrobras.paap.SampleGroup2m
import br.com.petrobras.paap.SampleGroup1h
/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock(TagPI)
class ProcessCreateSampleGroupsSpec extends Specification {

	def aggregator

    def setup() {
    	aggregator = Mock(SampleGroupAggregator)
    }

    def cleanup() {
    }

    void "test tags ignore tags <> calculated"() {

    	setup:

	    	def tags = [:]
	    	def calc = [:]
    		//def tagcalc = new TagPI(name: "tagcalc",formula:"tagsnap * 2", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save()

    		def p = new ProcessCreateSampleGroups(grailsApplication)
    		p.aggregator = aggregator

    	when:
    		def r = p.processInternal([:])

    	then:
    		r == [:]
    	
    	when: 'only snapshot'

    		for(i in 1..3){
    			tags.put(
    				new TagPI(name: "t${i}",formula:"t${i}", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(),
    				[samples:[new TimeSeriesSample(time:new Date(i*1000), value:300d)]]
    			)
    		}
    		r = p.processInternal(tags)

    	then:
    		0 * aggregator.executeUpdates()
    		r.keySet() == tags.keySet()

    	when: 'snapshot and calculated'
			for(i in 1..3){
    			tags.put(
    				new TagPI(name: "calc${i}",formula:"1 + 1", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(),
    				[samples:[new TimeSeriesSample(time:new Date(i*1000), value:300d)]]
    			)
    		}
    		r = p.processInternal(tags)
    	then:
    		3 * aggregator.executeUpdates() >> 	[
    												SampleGroup2m : [new TimeSeriesSample(time:new Date(1000), value:200d)],
    												SampleGroup1h :	[new TimeSeriesSample(time:new Date(1000), value:250d)]
    											]
			r.keySet() == tags.keySet()    		
    }
}
