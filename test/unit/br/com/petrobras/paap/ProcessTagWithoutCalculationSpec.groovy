package br.com.petrobras.paap

import br.com.petrobras.paap.job.ProcessData
//import br.com.petrobras.paap.*
import br.com.petrobras.paap.job.ProcessTagWithoutCalculation
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import grails.test.runtime.FreshRuntime
import groovy.time.TimeCategory
import spock.lang.Specification
//import static plastic.criteria.PlasticCriteria.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@FreshRuntime
@Mock([TagPI,Sample,Event,EventTagPI])
class ProcessTagWithoutCalculationSpec extends Specification {

    def doWithConfig(c) {
        c.config.paap.job.maxMinutesToGet = 10
    }

    def setup() {
    }

    def cleanup() {
    }

    private TimeSeriesSample tsSample(time, value) {
        return new TimeSeriesSample(time: new Date(time), value: value)
    }

    void "test accumulator tag below min interval"() {
        
        def tag = new TagPI(name: "TAGNAME",formula:"TAGNAME", tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

        ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService), Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication 

        def samplesInterval = (long) (grailsApplication.config.paap.processdata.MIN_MS_BETWEEN_SAMPLES / 2)
        
        when:
            def data = p.processInternal([end: new Date(100), begin: new Date(50)])

        then:
            // Assegura que o intervalo mínimo entre samples é divisível por 2 (para cálculo do samplesInterval)
            grailsApplication.config.paap.processdata.MIN_MS_BETWEEN_SAMPLES % 2 == 0

            // Este teste assegura que somente o terceido sample será considerado (ele é o único da lista cujo intervalo para um outro
            // elemento da lista é maior do que MIN_MS_BETWEEN_SAMPLES)

            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >>["TAGNAME":[tsSample(0, 1), tsSample(1*samplesInterval, 2), tsSample(2*samplesInterval, 3), tsSample(3*samplesInterval, 4)]]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]

            data.size() == 1
            data[tag].samples.size() == 1
            data[tag].samples[0] == tsSample(2*samplesInterval, 2 / (2*samplesInterval) * grailsApplication.config.paap.processdata.MS_IN_AN_HOUR)
    }

    void "test accumulator tag"() {
        def tag = new TagPI(name: "TAGNAME",formula:"TAGNAME", tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

        ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService),Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication 

        def samplesInterval = 30 * 1000
        def msInAnHour = grailsApplication.config.paap.processdata.MS_IN_AN_HOUR
        def valueRate = msInAnHour / samplesInterval

        def tagsValues = [
            tsSample(0, 1000),
            tsSample(1*samplesInterval, 2000),
            tsSample(2*samplesInterval, 3000),
            tsSample(3*samplesInterval, 5000),
            // Adiciona simulação de tag acumulada que reinicia (anda para trás)
            tsSample(4*samplesInterval, 100),
            tsSample(5*samplesInterval, 1100),
        ]

        when:
            def data = p.processInternal([end: new Date(100), begin: new Date(50)])

        then:

            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["TAGNAME": tagsValues]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]

            data.size() == 1
            data[tag].samples.size() == 4
            data[tag].samples[0] == tsSample(1*samplesInterval, 1000 * valueRate)
            data[tag].samples[1] == tsSample(2*samplesInterval, 1000 * valueRate)
            data[tag].samples[2] == tsSample(3*samplesInterval, 2000 * valueRate)
            data[tag].samples[3] == tsSample(5*samplesInterval, 1000 * valueRate)
    }

	void "test 5 tags stored"() {


    	for(i in 1..5){
    		new TagPI(name: "TAGNAME${i}",formula:"TAGNAME${i}", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
    	}
    	ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService),Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication

    	when:
    	def data = p.processInternal([end: new Date(100), begin: new Date(50)])
    	then:
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >>["TAGNAME1":[tsSample(0, 1000)],"TAGNAME2":[tsSample(0, 1000)],"TAGNAME3":[tsSample(0, 1000)],"TAGNAME4":[tsSample(0, 1000)],"TAGNAME5":[tsSample(0, 1000)]]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
    		data.size() == 5
    }

	void "test no tag stored"() {
    
    	ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService),Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication
    	when:
    	def data = p.processInternal([end: new Date(100), begin: new Date(50)])
    	then:
    		1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> [:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
    		data.size() == 0
    }

    void "test 1 tag stored and 2 samples already stored"() {
		def tag = new TagPI(name: "TAGNAME",formula:"TAGNAME", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
		def sampleDate
		for(i in 1..2){
			use ( TimeCategory ) {
            	sampleDate = new Date(1000) + i.minutes
			}
			new Sample(date:sampleDate,value:20.0,tag: tag).save(flush:true,failOnError:true)
		}
		ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService),Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication

    	when:
        def data = p.processInternal([end: sampleDate, begin: new Date(0)])
    	then:

            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,sampleDate) >>["TAGNAME":[tsSample(0, 1000), tsSample(1, 2000)]]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]

    		data.size() == 1
    		data[tag].samples[0] == tsSample(0, 1000)
            data[tag].samples[1] == tsSample(1, 2000)
    }

    void "test 2 tags from different pi locations"() {

        def tag1 = new TagPI(name: "TAGNAME1",formula:"TAGNAME1", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        def tag2 = new TagPI(name: "TAGNAME2",formula:"TAGNAME2", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES).save(flush:true,failOnError:true)

        ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService),Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication
        when:
        def data = p.processInternal([end: new Date(1), begin: new Date(0)])
        then:
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["TAGNAME1":[tsSample(0, 1000), tsSample(1, 2000)]]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>["TAGNAME2":[tsSample(0, 1000), tsSample(1, 2000)]]

            data.size() == 2
    }

    void "test accumulated tags wth empty sample"() {
        def tag1 = new TagPI(name: "TAGNAME1",formula:"TAGNAME1", tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

        ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService),Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication
        when:
        def data = p.processInternal([end: new Date(1), begin: new Date(0)])
        then:

            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["TAGNAME1":[]]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]

            data.size() == 1
            data[tag1].samples.isEmpty()

    }

    void "test accumulated tags wth only one sample and new attempty with 2"() {
        def tag1 = new TagPI(name: "TAGNAME1",formula:"TAGNAME1", tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        def today = new Date()
        def todayMinus10minutes = new Date(today.time - 1000*60*10)
        def todayMinusMinMinutesBetweenSamples = new Date(todayMinus10minutes.time - grailsApplication.config.paap.processdata.MIN_MS_BETWEEN_SAMPLES)

        ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService),Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication
        when:
        def data = p.processInternal([end: today, begin: todayMinus10minutes])
        then:

            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["TAGNAME1":[tsSample(today.time, 1000)]]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
            1 * p.readPIService.readDataFromTagInTimePeriod(tag1.formula,_,_,todayMinusMinMinutesBetweenSamples) >> [tsSample(todayMinus10minutes.time, 1000),tsSample(todayMinusMinMinutesBetweenSamples.time, 1000)]
            data.size() == 1
            data[tag1].samples[0] == tsSample(today.time, 0)
    }

    void "test accumulated tags wth only one sample and new attempty with 1"() {
        def tag1 = new TagPI(name: "TAGNAME1",formula:"TAGNAME1", tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        def today = new Date()
        def todayMinus10minutes = new Date(today.time - 1000*60*10)
        ProcessData p = new ProcessTagWithoutCalculation(Mock(ReadPIService),Spy(ProcessTagService), grailsApplication)
        p.processTagService.grailsApplication = grailsApplication
        when:
        def data = p.processInternal([end: today, begin: todayMinus10minutes])
        then:
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["TAGNAME1":[tsSample(today.time, 1000)]]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
            1 * p.readPIService.readDataFromTagInTimePeriod(tag1.formula,_,_,_) >> [tsSample(today.time, 1000)]
            data.size() == 1
            data[tag1].samples.isEmpty()
    }

    def "test a spaced tag"() {
        setup:
            def readPIService = Mock(ReadPIService)

            def begin = new Date(50 * 1000)
            def end = new Date(100 * 1000)
            def today = new Date()
            def t1 = new TagPI(name: 't1', formula:'t1', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
            def p = new ProcessTagWithoutCalculation(readPIService,Spy(ProcessTagService),grailsApplication) 
            
        when: 'older than begin sample arrives from pi'
            def ret = p.processInternal([end:end,begin:begin])

        then: 'time updated to end'
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["t1":[tsSample(40*1000, 70)]]
            ret[t1].samples[0].value == 70
            ret[t1].samples[0].time == end

        when: 'on older than begin and others in range -  samples arrives from pi'
            ret = p.processInternal([end:end,begin:begin])

        then: 'time updated to end'
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["t1":[tsSample(40*1000, 70),tsSample(60*1000, 80),tsSample(70*1000, 90)]]
            ret[t1].samples[0].value == 70
            ret[t1].samples[0].time == new Date(40*1000)
            ret[t1].samples[1].value == 80
            ret[t1].samples[1].time == new Date(60*1000)
            ret[t1].samples[2].value == 90
            ret[t1].samples[2].time == end

        when: 'no samples from pi (should not happen but it does)'
            ret = p.processInternal([end:end,begin:begin])

        then: 'time updated to end'
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["t1":[]]
            ret[t1].samples == [] as SortedSet
    }

    /* ======= REMOVER EM 24/7 SE TUDO CORRER MARAVILHOSAMENTE BEM ===========
    def "test when we don't have a spaced snapshot sample stored in the database, get one from PI"() {
        setup:
            def readPIService = Mock(ReadPIService)

            def begin = new Date(1 * 60 * 60 * 1000)
            def end = new Date(25 * 60 * 60 * 1000)
            def today = new Date()
            def t1 = new TagPI(name: 't1', formula:'t1', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

            def p = new ProcessTagWithoutCalculation(readPIService,Spy(ProcessTagService),grailsApplication)
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["t1":[]]
            
            1 * readPIService.readLastDataFromTag(_,_) >> [new TimeSeriesSample(time: new Date(30*60*1000), value: 70)]
        
        when:
            def ret = p.processInternal([end:end,begin:begin])
        then:
            ret[t1].samples[0].value == 70
    }

    def "test get a spaced snapshot sample older than the time specified - from Database"() {
        setup:
            def readPIService = Mock(ReadPIService)
            def begin = new Date(1 * 60 * 60 * 1000)
            def end = new Date(25 * 60 * 60 * 1000)
            def today = new Date()
            def t1 = new TagPI(name: 't1', formula:'t1', tagType: TagPIType.SPACED_SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
            new Sample(date: new Date(30 * 60 * 100) ,value: 80 ,tag:t1).save(flush:true,failOnError:true)

            def p = new ProcessTagWithoutCalculation(readPIService,Spy(ProcessTagService),grailsApplication)
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BC,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_RIO,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_ES,_,_) >>[:]
            1 * p.processTagService.readDataFromLocation(TagPILocation.UO_BS,_,_) >> ["t1":[]]
            0 * readPIService.readLastDataFromTag(_,_)
        
        when:
            def ret = p.processInternal([end:end,begin:begin])
        then:
            ret[t1].samples[0] == null
    }*/
}
