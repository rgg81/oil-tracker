package br.com.petrobras.paap

import br.com.petrobras.paap.job.ProcessData
import br.com.petrobras.paap.job.ProcessDataJMS
import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import groovy.time.TimeCategory
import spock.lang.Specification
import static plastic.criteria.PlasticCriteria.*
import br.com.petrobras.paap.ws.WSMessageType

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI,Sample,UEP,Event])
class ProcessDataJMSSpec extends Specification {

    def wsMessageService

    def setup() {
        wsMessageService = Mock(WSMessageService)
    }

    def cleanup() {
    }

    void "test send event called" () {
        setup:
            ProcessData p = new ProcessDataJMS(wsMessageService, grailsApplication)
        when:
            p.processInternal()
        then:
            1 * wsMessageService.sendEvent(WSMessageType.NEW_DATA,_)
    }


  /*
    void "test jms send data for all UEPs"() {

        //tags nao calculadas
        
        def t1 = new TagPI(name: "t1",formula:"t1", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        def t2 = new TagPI(name: "t2",formula:"t2", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        def f1 = new TagPI(name: "f(t1)",formula:"t1 + t1", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)
        def f2 = new TagPI(name: "f(t2)",formula:"t2 + t2", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)
        def f3 = new TagPI(name: "f3",formula:"t1 + t1", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)
        def uep1 = new UEP( name: 'Merluza Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: f1 ).save() ;
        def uep2 = new UEP( name: 'Ilha Bela', uo: 'XX', department: 'XXX', asset: 'XX', oceanCurrent: 'east', oilWell: 'XXX',
                tag: f2 ).save() ;
        
        def data = [:]
        data.put(t1,[])
        data.put(t2,[])
        data.put(f1,[])
        data.put(f2,[])
        data.put(f3,[])

        mockCriteria([Sample])

        when:
            ProcessData p = new ProcessDataJMS(new WSMessageService(), new PIDataService(),new EventService(),_)
        then:
            p.convertTagToUEPAndAddExtraData(data).size() == 2
            p.convertTagToUEPAndAddExtraData(data).keySet() == [uep1.id,uep2.id] as Set
    }


     void "test jms addExtraData"() {

        //tags nao calculadas
        
        def t1 = new TagPI(name: "t1",formula:"1", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        def t2 = new TagPI(name: "t2",formula:"2", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)


        use ( TimeCategory ) {
            new Sample(date: new Date() - 1.hour ,value: 100,tag:t1).save(flush:true)
            new Sample(date: new Date() - 6.hour ,value: 120,tag:t1).save(flush:true)
            new Sample(date: new Date() - 25.hour ,value: 9999,tag:t1).save(flush:true) // out of 24h range
            new Sample(date: new Date() - 13.hour ,value: 90,tag:t2).save(flush:true)
            new Sample(date: new Date() - 19.hour ,value: 100,tag:t2).save(flush:true)
            new Sample(date: new Date() - 25.hour ,value: 9999,tag:t2).save(flush:true) // out of 24h range
        }

        def uep1 = new UEP( name: 'Merluza Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: t1 ).save() ;
        def uep2 = new UEP( name: 'Ilha Bela', uo: 'XX', department: 'XXX', asset: 'XX', oceanCurrent: 'east', oilWell: 'XXX',
                tag: t2 ).save() ;

        def dataUep1 = [:]
        def dataUep2 = [:]

        mockCriteria([Sample])
        
        when:
            ProcessData p = new ProcessDataJMS(new WSMessageService(), new PIDataService(),new EventService(),_)
            p.addExtraData(uep1.id, t1.id, dataUep1)
            p.addExtraData(uep2.id, t2.id, dataUep2)
        then:
            dataUep1['24h_avg'] == 110.0
            dataUep2['24h_avg'] == 95.0
    }*/

}
