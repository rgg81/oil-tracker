package br.com.petrobras.paap

import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import groovy.time.TimeCategory
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([SampleGroup2m, SampleGroup10m, SampleGroup1h, SampleGroup6h, SampleGroup24h, TagPI])
class SampleGroupAggregatorSpec extends Specification {

    private final static double tolerance = 1e-10

    def setup() {
        Date.metaClass.truncateToSeconds = { seconds -> new Date( delegate.time - delegate.time % (1000*seconds) ) }
    }

    def cleanup() {
        Date.metaClass.truncateToSeconds == null
    }

    void "test sample groups durations"()
    {
        use(TimeCategory) {
            expect:
                new SampleGroup2m().getDuration() == 2.minutes
                new SampleGroup10m().getDuration() == 10.minutes
                new SampleGroup1h().getDuration() == 1.hour
                new SampleGroup6h().getDuration() == 6.hours
                new SampleGroup24h().getDuration() == 24.hours
        }
    }

    void "test group without previous values"()
    {
        TagPI tag = new TagPI(name: 'TagTest', formula: 'TESTE', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

        SampleGroupAggregator simpleAggregator = new SampleGroupAggregator(SampleGroup2m.class, null)

        Date date = new Date(1426547589299) // Mon Mar 16 20:13:09 BRT 2015
        Date truncated2MinDate = date.truncateToSeconds((int)(TimeCategory.getMinutes(2).toMilliseconds() / 1000))


        when: 'Insert first without executeUpdates'
            SampleGroup simpleGroup = SampleGroupSingle.createSingle(date, tag, 10)
            simpleAggregator.update(simpleGroup)
        then:
            SampleGroup2m.count() == 0

        when: 'Updating database'
            simpleAggregator.executeUpdates()
            SampleGroup2m firstSample2m = SampleGroup2m.first()
        then:
            firstSample2m.date == truncated2MinDate
            firstSample2m.tag == tag
            firstSample2m.min == 10
            firstSample2m.max == 10
            firstSample2m.mean == 10
            firstSample2m.sum == 10
            firstSample2m.num == 1
            firstSample2m.variance == 0
            SampleGroup2m.count() == 1

        when: 'Insert second, after 30s, but still inside the 20:12 group'
            SampleGroup simpleGroup2 = SampleGroupSingle.createSingle(new Date(date.getTime() + 30 * 1000), tag, 20)
            simpleAggregator.update(simpleGroup2)
            simpleAggregator.executeUpdates()
            firstSample2m = SampleGroup2m.first()
        then:
            firstSample2m.date == truncated2MinDate
            firstSample2m.tag == tag
            firstSample2m.min == 10
            firstSample2m.max == 20
            firstSample2m.mean == 15
            firstSample2m.sum == 30
            firstSample2m.num == 2
            firstSample2m.variance == 25
            SampleGroup2m.count() == 1

        when: 'Insert third, after 2min from first and on the new the 20:14 group'
            SampleGroup simpleGroup3 = SampleGroupSingle.createSingle(new Date(date.getTime() + 120 * 1000), tag, 30)
            simpleAggregator.update(simpleGroup3)
            simpleAggregator.executeUpdates()
            firstSample2m = SampleGroup2m.first()
            def lastSample2m = SampleGroup2m.last()
        then:
            firstSample2m.date == truncated2MinDate
            firstSample2m.tag == tag
            firstSample2m.min == 10
            firstSample2m.max == 20
            firstSample2m.mean == 15
            firstSample2m.sum == 30
            firstSample2m.num == 2
            firstSample2m.variance == 25

            lastSample2m.date == new Date(truncated2MinDate.getTime() + 120 * 1000)
            lastSample2m.tag == tag
            lastSample2m.min == 30
            lastSample2m.max == 30
            lastSample2m.mean == 30
            lastSample2m.sum == 30
            lastSample2m.num == 1
            lastSample2m.variance == 0

            SampleGroup2m.count() == 2
    }

    void "test combinate two single groups"()
    {
        TagPI tag = new TagPI(name: 'teste')
        SampleGroup sampleA = SampleGroupSingle.createSingle(new Date(1000), tag, 10)
        SampleGroup sampleB = SampleGroupSingle.createSingle(new Date(1000), tag, 20)

        when:
        SampleGroup combinatedSampleA = SampleGroupAggregator.combinate(sampleA, sampleB)

        then:
        combinatedSampleA.date == new Date(1000)
        combinatedSampleA.tag == tag
        combinatedSampleA.min == 10
        combinatedSampleA.max == 20
        combinatedSampleA.mean == 15
        combinatedSampleA.sum == 30
        combinatedSampleA.num == 2
        combinatedSampleA.variance == 25
    }

    void "test combinate two multi groups"()
    {
        TagPI tag = new TagPI(name: 'teste')

        SampleGroup rootSample = SampleGroupSingle.createSingle(new Date(1000), tag, 20) ;

        def newSamplesA = [
                SampleGroupSingle.createSingle(new Date(1000), tag, 10),
                SampleGroupSingle.createSingle(new Date(1000), tag, 20),
                SampleGroupSingle.createSingle(new Date(1000), tag, 30)
        ]

        def newSamplesB = [
                SampleGroupSingle.createSingle(new Date(1000), tag, 5),
                SampleGroupSingle.createSingle(new Date(1000), tag, 35)
        ]

        when:
        newSamplesA.each { newSample ->
            SampleGroupAggregator.combinate(rootSample, newSample)
        }
        then:
        rootSample.date == new Date(1000)
        rootSample.tag == tag
        rootSample.min == 10
        rootSample.max == 30
        rootSample.sum == (20+10+20+30)
        rootSample.num == 4
        rootSample.mean == rootSample.sum / rootSample.num
        Math.abs(rootSample.variance-50) < tolerance

        when:
        newSamplesB.each { newSample ->
            SampleGroupAggregator.combinate(rootSample, newSample)
        }
        then:
        rootSample.date == new Date(1000)
        rootSample.tag == tag
        rootSample.min == 5
        rootSample.max == 35
        rootSample.sum == (20+10+20+30+5+35)
        rootSample.num == 6
        rootSample.mean == rootSample.sum / rootSample.num
        Math.abs(rootSample.variance - 650 / 6) < tolerance
    }

    /*
    // Esse teste tem algum problema onde o '1 * secondAggregator.executeUpdates()' gera valor null num método anterior
    void "test next aggregator"() {
        TagPI tag = new TagPI(name: 'TagTest', formula: 'TESTE', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

        SampleGroupAggregator secondAggregator = Mock()
        secondAggregator.update(_) >> {}
        secondAggregator.executeUpdates >> [b: 2]
        SampleGroupAggregator firstAggregator = new SampleGroupAggregator(SampleGroup2m.class, secondAggregator)

        Date date = new Date(1426547589299) // Mon Mar 16 20:13:09 BRT 2015
        SampleGroup simpleGroup = SampleGroupSingle.createSingle(date, tag, 10)

        when: 'Insert first'
            firstAggregator.update(simpleGroup)
            def result = firstAggregator.executeUpdates()
        then: 'Second must be updated as well'
            1 * secondAggregator.update(simpleGroup)
        then:
            1 * secondAggregator.executeUpdates()
            (result*.key as Set) == (['SampleGroup2m', 'b'] as Set)
            result.b == 2
    }
    */
}
