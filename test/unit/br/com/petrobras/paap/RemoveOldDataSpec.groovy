package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import br.com.petrobras.paap.job.RemoveOldData
import static plastic.criteria.PlasticCriteria.*
import spock.lang.Unroll


/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([Sample,TagPI])
class RemoveOldDataSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def mapToTimeSeriesSample = { anArray ->
        anArray.collect {
            new TimeSeriesSample(time:new Date(it[0]*1000), value: it[1])
        }
    }

    @Unroll
    void "test remove samples"(newsamples,lastsamples,passingsamples) {

		setup:
			mockCriteria([Sample])
			def t1 = new TagPI(name: "tag1",formula:"tag1", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
			def t2 = new TagPI(name: "tag2",formula:"tag2", tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

			def tags = [t1,t2]

			newsamples = mapToTimeSeriesSample(newsamples)
			passingsamples = mapToTimeSeriesSample(passingsamples)

			def data = [:]

			tags.each{ t ->
				data[t] = [samples:newsamples]
				lastsamples.each{
					new Sample(date:new Date(it[0]*1000),value:it[1],tag:t).save(flush:true)	
				}
			}
			
			def result = new RemoveOldData().processInternal(data)
		expect:
			result[t1].samples == passingsamples
			result[t2].samples == passingsamples
		where:
			newsamples 										| lastsamples 			| passingsamples
			[[10,200],[20,202],[30,203],[40,204],[50,205]]	| [[20,211],[30,210]]	| [[40,204],[50,205]]
			[[10,200],[20,202],[30,203],[40,204],[50,205]]	| [[30,210]]			| [[40,204],[50,205]]
			[[10,200],[20,202],[30,203],[40,204],[50,205]]	| []					| [[10,200],[20,202],[30,203],[40,204],[50,205]]
			[[50,215]]										| []					| [[50,215]]
			[]												| [[20,211],[30,210]]	| []

    }

}
