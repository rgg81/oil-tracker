package br.com.petrobras.paap

import grails.test.mixin.TestFor
import spock.lang.Specification

import groovy.time.TimeCategory
import static plastic.criteria.PlasticCriteria.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(PIDataService)
@Mock([UEP,Sample,SampleGroup2m,SampleGroup10m,SampleGroup1h,TagPI])
class PIDataServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test avg 24 hrs"() {
    	def yesterday = new Date( new Date().getTime() - 1000*60*60*24)
		
		def t1 = new TagPI(name: "t1",formula:"1", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
    	
		use ( TimeCategory ) {
			new Sample(date: yesterday - 1.hour ,value: 100,tag:t1).save(flush:true)
			new Sample(date: yesterday - 6.hour ,value: 120,tag:t1).save(flush:true)
			new Sample(date: yesterday - 13.hour ,value: 100,tag:t1).save(flush:true)
			new Sample(date: yesterday - 19.hour ,value: 120,tag:t1).save(flush:true)
		}

		use ( TimeCategory ) {
			new Sample(date: yesterday + 1.hour ,value: 10,tag:t1).save(flush:true)
			new Sample(date: yesterday + 6.hour ,value: 12,tag:t1).save(flush:true)
			new Sample(date: yesterday + 13.hour ,value: 10,tag:t1).save(flush:true)
			new Sample(date: yesterday + 19.hour ,value: 12,tag:t1).save(flush:true)
		}

		def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: t1).save(flush:true)

		mockCriteria([Sample])
		when:
    	def r = service.average24hours(uep.id)
    	then:
    		r == 11.0
    }

    void "test sum avg 24 hrs"() {
    	def uepServiceMock = Mock(UepService)
	    service.uepService = uepServiceMock
    	def yesterday = new Date( new Date().getTime() - 1000*60*60*24)

		def t1 = new TagPI(name: "t1",formula:"1", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
		def t2 = new TagPI(name: "t2",formula:"2", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
		def t3 = new TagPI(name: "t3",formula:"3", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)

		use ( TimeCategory ) {
			new Sample(date: yesterday + 1.hour ,value: 100,tag:t1).save(flush:true)
			new Sample(date: yesterday + 6.hour ,value: 120,tag:t1).save(flush:true)
			new Sample(date: yesterday + 13.hour ,value: 100,tag:t1).save(flush:true)
			new Sample(date: yesterday + 19.hour ,value: 120,tag:t1).save(flush:true)
		}

		use ( TimeCategory ) {
			new Sample(date: yesterday + 1.hour ,value: 10,tag:t2).save(flush:true)
			new Sample(date: yesterday + 6.hour ,value: 12,tag:t2).save(flush:true)
			new Sample(date: yesterday + 13.hour ,value: 10,tag:t2).save(flush:true)
			new Sample(date: yesterday + 19.hour ,value: 12,tag:t2).save(flush:true)
		}

		use ( TimeCategory ) {
			new Sample(date: yesterday + 1.hour ,value: 11,tag:t3).save(flush:true)
			new Sample(date: yesterday + 6.hour ,value: 13,tag:t3).save(flush:true)
			new Sample(date: yesterday + 13.hour ,value: 11,tag:t3).save(flush:true)
			new Sample(date: yesterday + 19.hour ,value: 13,tag:t3).save(flush:true)
		}

		def uep1 = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: t1).save(flush:true)

		def uep2 = new UEP( name: 'Ilha Bela', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: t2).save(flush:true)

		mockCriteria([Sample])
		when:
    	def r = service.sumAverage24hours()
    	then:
    		1*uepServiceMock.getUEPsWithPermission() >> [uep1, uep2]
    		r == 121.0

    }

    void "test alert"(longWindowValues, shortWindowValues, alert, desc) {
		def sampleLimitTime

		use ( TimeCategory ) {
    		sampleLimitTime = new Date() - 50.minutes
    	}
    	
    	def t1 = new TagPI(name: "t1",formula:"1", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
    	
    	setup:'setting short window, long window and alert percent'

	    	use ( TimeCategory ) {
				new Sample(date: sampleLimitTime - 1.minute ,value: shortWindowValues[0] * PIDataService.LOW_PRODUCTION ,tag:t1).save(flush:true)
				new Sample(date: sampleLimitTime - 2.minute ,value: shortWindowValues[1] * PIDataService.LOW_PRODUCTION,tag:t1).save(flush:true)
				new Sample(date: sampleLimitTime - 3.minute ,value: shortWindowValues[2] * PIDataService.LOW_PRODUCTION ,tag:t1).save(flush:true)
				new Sample(date: sampleLimitTime - 4.minute ,value: shortWindowValues[3] * PIDataService.LOW_PRODUCTION,tag:t1).save(flush:true)
			}

			use ( TimeCategory ) {
				new Sample(date: sampleLimitTime - 10.minute ,value: longWindowValues[0] * PIDataService.LOW_PRODUCTION,tag:t1).save(flush:true)
				new Sample(date: sampleLimitTime - 15.minute ,value: longWindowValues[1] * PIDataService.LOW_PRODUCTION,tag:t1).save(flush:true)
				new Sample(date: sampleLimitTime - 20.minute ,value: longWindowValues[2] * PIDataService.LOW_PRODUCTION,tag:t1).save(flush:true)
				new Sample(date: sampleLimitTime - 25.minute ,value: longWindowValues[3] * PIDataService.LOW_PRODUCTION,tag:t1).save(flush:true)
			}

	    	def uep = new UEP( 	name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', 
	    				asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
	    				shortWindow : 5, longWindow: 30, alertPercent: 25,
		                tag: t1).save(flush:true)
			mockCriteria([Sample])

    	when:
			def r = service.alert(uep.id)
		then:
			r == alert
		where:
			longWindowValues | shortWindowValues	|  alert | desc
			[10,10,11,12] 	 | 	[100,120,110,10]	|	1 	 | 'subida abrupta'
			[100,120,110,10] |	[10,10,11,12] 	 	|	-1 	 | 'descida abrupta'
			[10,10,11,12]	 |	[10,10,11,12] 	 	|	null | 'sem alerta'


    }
}
