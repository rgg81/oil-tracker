package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import spock.lang.Specification
import org.quartz.JobDetail
import org.quartz.JobDataMap
import br.com.petrobras.paap.job.ReloadTagsJob
import grails.test.mixin.web.ControllerUnitTestMixin
import org.quartz.JobExecutionContext
import br.com.petrobras.paap.dsl.Chain
import br.com.petrobras.paap.job.ReadJMS
import org.codehaus.groovy.grails.commons.InstanceFactoryBean
/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(ControllerUnitTestMixin)
@Mock([TagPI])

class ReloadTagsJobSpec extends Specification {
	
	def context
	def detail
	def datamap
	def chain
	def closure
    def reloadQueueService = Mock(ReloadQueueService)

    def setup() {
    	context = Mock(JobExecutionContext)
    	detail = Mock(JobDetail)
    	datamap = Mock(JobDataMap)
    	chain = Mock(Chain)
    	GroovyMock(Chain,global:true)
    }
    def cleanup() {
    	
    }

    def doWithSpring = {
        reloadQueueService(InstanceFactoryBean,  reloadQueueService, ReloadQueueService) 
    }


    void "test job invalid entries"() {
    	setup:
    		def job = new ReloadTagsJob()
    	when: 'empty tags'
    		job.execute(context)
    	then:
    		notThrown(Exception)
    		 1 * context.getJobDetail() >> detail
    		 1 * detail.getJobDataMap() >> datamap
    		 1 * datamap.getString(ReadJMS.TAGS_JSON_ARRAY) >> '[]'

    		 0 * Chain.newInstance(_) >> chain
    		 0 * chain.with(_) >> chain
    		 0 * chain.process()
    		 

    	when: 'empty json'
			job.execute(context)
    	then:
    		notThrown(Exception)
    		 1 * context.getJobDetail() >> detail
    		 1 * detail.getJobDataMap() >> datamap
    		 1 * datamap.getString(ReadJMS.TAGS_JSON_ARRAY) >> ''

    		 0 * Chain.newInstance(_) >> chain
    		 0 * chain.with(_) >> chain
    		 0 * chain.process()
			

    	when: 'ids with no tags in db'
    		job.execute(context)
    	then:
    		notThrown(Exception)
    	    1 * context.getJobDetail() >> detail
    		1 * detail.getJobDataMap() >> datamap
    		1 * datamap.getString(ReadJMS.TAGS_JSON_ARRAY) >> '[1,2]'

    		0 * Chain.newInstance(_) >> chain
    		0 * chain.with(_) >> chain
    		0 * chain.process()
    		
    }

    void "test job valid entries"() {
    	setup:
    		def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
			def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
    		def job = new ReloadTagsJob()
    	when:
    		job.execute(context)
    	then:
    		 1 * context.getJobDetail() >> detail
    		 1 * detail.getJobDataMap() >> datamap
    		 1 * datamap.getString(ReadJMS.TAGS_JSON_ARRAY) >> "[${t1.id},${t2.id}]"

    		 1 * Chain.newInstance(_) >> chain
    		 8 * chain.with(_) >> chain
    		 1 * chain.process()
    }


    void "test job exception rollback"() {
        setup:
            def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def job = new ReloadTagsJob()
            def reloadQueueService = job.reloadQueueService
        when:
            job.execute(context)
        then:
            1 * context.getJobDetail() >> detail
            1 * detail.getJobDataMap() >> datamap
            1 * datamap.getString(ReadJMS.TAGS_JSON_ARRAY) >> "[${t1.id},${t2.id}]"
            1 * Chain.newInstance(_) >> chain
            8 * chain.with(_) >> chain
            1 * chain.process() >> {throw new Exception("Erro job reload")}
            t1.reloadStatus == ReloadStatus.FAILED
            t2.reloadStatus == ReloadStatus.FAILED
    }
}
