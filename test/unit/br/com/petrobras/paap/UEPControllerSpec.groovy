package br.com.petrobras.paap

import grails.test.mixin.*
import spock.lang.*
import org.codehaus.groovy.grails.commons.InstanceFactoryBean
import grails.test.mixin.web.ControllerUnitTestMixin

@TestFor(UEPController)
@Mock([UEP,TagPI])
@TestMixin(ControllerUnitTestMixin)
class UEPControllerSpec extends Specification {
    
    def uepService
    def tagPIService
    def shiroService

    void setup() {
        uepService = Mock(UepService)
        tagPIService = Mock(TagPIService)
        shiroService = Mock(ShiroService)

        controller.tagPIService = tagPIService
        controller.uepService = uepService
        controller.shiroService = shiroService
    }

    def populateValidParams(params) {
        TagPI tag = new TagPI(name: 'tag1', formula: '1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush: true)
        params["name"] = "FPSO Ilhabela"
        params["uo"] = "BS"
        params["department"] = "PRESAL"
        params["asset"] = "PRESAL"
        params["oceanCurrent"] = "east"
        params["oilWell"] = "poço"
        params["tag"] = tag
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            1 * controller.shiroService.isAdmin() >> true
            !model.uos
    }

    void "Test the index action returns the correct model and user is a manager"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            1 * controller.shiroService.isAdmin() >> false
            1 * controller.shiroService.isManager() >> true
            !model.uos
    }

    void "Test the index action returns the correct model when we have at least one UEP"() {  
            
        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            1 * controller.shiroService.isAdmin() >> true
            1 * uepService.getAllUEPs(_) >> [[name:"UO-ES", values:["FPSO 1", "FPSO 2"]]];
            model.uos.size() == 1
    }

    void "Test the index action returns the correct model when we have at least one UEP and user is a manager"() {  
            
        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            1 * controller.shiroService.isAdmin() >> false
            1 * controller.shiroService.isManager() >> true
            1 * uepService.getUEPsWithPermission() >> [[name:"FPSO 1"], [name: "FPSO 2"]]
            1 * uepService.getAndGroupUEPS(_,_)   >> [[name:"UO-ES", values:["FPSO 1", "FPSO 2"]]];
            model.uos.size() == 1
    }


    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.UEPInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        setup:
            populateValidParams(params)
            def uep =  new UEP(params)

        when:"The save action is executed with a valid instance"
            
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'

            controller.save(uep)


        then:"A redirect is issued to the show action"
            1 * uepService.save(_) >>  uep.save()
            response.redirectedUrl == '/admin/UEP/show/1'
            controller.flash.message != null


        when:"The save action is executed with an invalid instance"
            response.reset()
            params.clear()

            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            uep = new UEP()
            uep.validate()
            controller.save(uep)

        then:"The create view is rendered again with the correct model"
            model.UEPInstance!= null
            view == 'create'
    }

    void "Test that the show action returns the correct model"() {
        //def relatedUep = new UEP(name: 'P-43', uo: 'UO-BS', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
        //                    tag: new TagPI(name: 'Tag P-43', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save()
        def uep


        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            uep = new UEP(params).save()
            
            controller.show(uep)

        then:"A model is populated containing the domain instance"
            1 * tagPIService.isReloading(_) >> false
            //1 * uepService.findRelatedUeps(_) >> ([relatedUep] as Set)
            model.UEPInstance == uep
            model.reloading == false
            //model.relatedUeps == ([relatedUep] as Set)
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def UEP = new UEP(params)
            controller.edit(UEP)

        then:"A model is populated containing the domain instance"
            model.UEPInstance == UEP
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/admin/UEP/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def UEP = new UEP()
            UEP.validate()
            controller.update(UEP)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.UEPInstance == UEP

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            UEP = new UEP(params).save(flush: true)
            controller.update(UEP)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/admin/UEP/show/$UEP.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        setup:"A domain instance is created"
            
            populateValidParams(params)
            def uep = new UEP(params).save(flush: true)


        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/admin/UEP/index'
            flash.message != null

        

        when:"The domain instance is passed to the delete action"
            response.reset()
            controller.delete(uep)

        then:"The instance is deleted"
            1 * uepService.delete(uep)
            response.redirectedUrl == '/admin/UEP/index'
            flash.message != null
    }

      void "test reload action"() {

        setup:
            def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def calc = new TagPI(name: 'tagcalc', formula: "'tag1' + 'tag1'", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def uep = new UEP(name: "P-40 ES", uo: 'UO-ES', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                                tag: calc).save();
            controller.tagPIService = Mock(TagPIService)
            request.method = 'POST'
        when:
            controller.reload(uep)

        then:
            response.status == 400
            0 * controller.tagPIService.reloadtags(_)

        when:
            response.reset()
            params["reloadTags"] = []
            controller.reload(uep)
        then: 
            response.status == 400
            0 * controller.tagPIService.reloadtags(_)

        when:
            response.reset()
            params["reloadTags"] = calc.id as String
            controller.reload(uep)
        then: 
            response.status == 302
            1 * controller.tagPIService.reloadtags([calc])

        when:
            response.reset()
            params["reloadTags"] = [calc.id as String,t1.id as String,t2.id as String]
            controller.reload(uep)
        then: 
            response.status == 302
            1 * controller.tagPIService.reloadtags([calc,t1,t2])

    }

    void "test reloadUEPS"() {
        setup:
            controller.tagPIService = Mock(TagPIService)

            def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def calc = new TagPI(name: 'tagcalc', formula: "'tag1' + 'tag1'", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush: true)
            
            def t3 = new TagPI(name: 'tag3', formula: 'tag3', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t4 = new TagPI(name: 'tag4', formula: 'tag4', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def calc1 = new TagPI(name: 'tagcalc2', formula: "'tag3' + 'tag4'", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush: true)
            
            println "" + [t1,t2,calc].class

        when:
            request.json = [[],["${t1.id}","${t2.id}","${calc.id}"],["${t3.id}","${t4.id}","${calc1.id}"],[]]
            request.method = 'POST'
            controller.reloadUEPS()
        then:
            1 * controller.tagPIService.reloadtags([t1,t2,calc,t3,t4,calc1])
            response.status == 200

        when:
            response.reset()
            request.json = [[999],["${t1.id}","${t2.id}","${calc.id}"]]
            request.method = 'POST'
            controller.reloadUEPS()
        then:
            0 * controller.tagPIService.reloadtags(_)            
            response.status == 400
         when:
            response.reset()
            request.json = [["${t1.id}","${t2.id}","${calc.id}"]]
            request.method = 'POST'
            controller.reloadUEPS()
        then:
            1 * controller.tagPIService.reloadtags(_) >> {throw new Exception("teste")}
            response.status == 500


    }
}
