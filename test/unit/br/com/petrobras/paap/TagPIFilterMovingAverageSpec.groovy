package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesSample
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(TagPIFilterMovingAverage)
@Mock([TagPI, TagPIFilter, TagPIFilterMovingAverage, TagPIFilterKalman, TagPIFilterMinMax, TagPIFilterMovingAverageSample])
class TagPIFilterMovingAverageSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test moving average collection"()
    {
        TagPIFilterMovingAverage average = new TagPIFilterMovingAverage(10, 0)
        TagPI tag = new TagPI(name: 'tag', formula: 'tag', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS)
        tag.addToFilters(average)

        when: 'samples must start empty'
            tag.save()
        then:
            average.samples == null

        when: 'added first sample'
            def result = average.addAndGetResult(sample(40, 1))
        then:
            result == 1
            average.samples.size() == 1

        when: 'added sample inside window'
            result = average.addAndGetResult(sample(45, 3))
        then:
            result == 1.5
            average.samples.size() == 2

        when: 'added sample outside window'
            result = average.addAndGetResult(sample(55, 5))
        then:
            result == 4
            average.samples.size() == 3

        when: 'added sample way outside window'
            result = average.addAndGetResult(sample(70, 2))
        then:
            result == 3
            average.samples.size() == 2

        when: 'replace last sample'
            result = average.addAndGetResult(sample(70, 5))
        then:
            result == 5
            average.samples.size() == 2
    }

    void "test moving average on large collection"()
    {
        TagPIFilterMovingAverage average = new TagPIFilterMovingAverage(100, 0)
        TagPI tag = new TagPI(name: 'tag', formula: 'tag', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS)
        tag.addToFilters(average)
        tag.save()

        when: 'added a lot of samples'
            for( int i=1; i<1000; i++ ) {
                average.addAndGetResult(sample(i, i))
            }
            def result = average.addAndGetResult(sample(1000, 1000))
        then: 'must handle correctly'
            average.samples.size() == 102
            result == ((900+1000) * 101 / 2) / 101 // Média da soma da PA entre 900 e 1000
    }

    private TimeSeriesSample sample(long time, double value) {
        new TimeSeriesSample(time: new Date(time), value: value)
    }
}
