package br.com.petrobras.paap



import grails.test.mixin.*
import spock.lang.*

@TestFor(TagPIController)
@Mock([TagPI,Event,EventTagPI,EventService,Sample])
class TagPIControllerSpec extends Specification {

    def shiroServiceMock = Mock(ShiroService)
    def tagPIServiceMock = Mock(TagPIService)

    void setup() {
        controller.tagPIService = tagPIServiceMock
        controller.shiroService = shiroServiceMock
        //controller.validation.metaClass = {params-> [samples:[]]}
    }

    def populateValidParams(params) {
        new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        params["name"] = 'FPSO Ilhabela'

        params["formula"] = "('tag1' + 'tag2') * 6.2898 * 24 * 0.45"
        params["tagType"] = TagPIType.CALCULATED.toString()

        params["filters"] = null
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.tagPIInstanceList
            model.tagPIInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.tagPIInstance!= null
    }

    void "Test the save action with exception"() {
        setup:
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def tagPI = new TagPI()

        when:"The save action is executed with an invalid instance"
            tagPI.validate()
            controller.save(tagPI)

        then:"The create view is rendered again with the correct model"
            1 * controller.tagPIService.update(_,_) >> { throw new RuntimeException("invalid params") }
            model.tagPIInstance!= null
            view == 'create'

    }

    void "Test the save action correctly persists an instance"() {
        setup:
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'

            populateValidParams(params)
            def tagPI = new TagPI(params)
        when:"The save action is executed with a valid instance"

            controller.save(tagPI)

        then:"A redirect is issued to the show action"
            1 * controller.tagPIService.update(tagPI,null) >> tagPI.save()
            response.redirectedUrl == '/admin/tagPI/show/3'
            controller.flash.message != null

    }

    void "Test that the show action returns the correct model"() {        
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            1 * controller.tagPIService.hasAccessToTag(_) >> true
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def tagPI = new TagPI(params)
            controller.tagPIService.hasAccessToTag(_) >> true
            controller.tagPIService.isRealoding(_) >> false
            controller.show(tagPI)

        then:"A model is populated containing the domain instance"
            model.tagPIInstance == tagPI

        when: "User is a manager and has access to the specified tag"            
            controller.show(tagPI)
                    
        then: "A model is populated with the domain instance and the user is redirected to the showFormula view"
            controller.tagPIService.isRealoding(_) >> false
            controller.shiroService.isAdmin() >> false
            1 * controller.tagPIService.hasAccessToTag(_) >> true
            model.tagPIInstance == tagPI
            view == 'showFormula'

        when: "User is a manager and doesn't have access to the specified tag"            
            controller.show(tagPI)
                    
        then: "User is redirected to the unauthorized page"
            controller.tagPIService.isRealoding(_) >> false
            1*controller.tagPIService.hasAccessToTag(_) >> false
            response.redirectedUrl == '/unauthorized'

    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            1 * controller.tagPIService.hasAccessToTag(_) >> true
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            1 * controller.tagPIService.hasAccessToTag(_) >> true
            def tagPI = new TagPI(params)
            controller.edit(tagPI)

        then:"A model is populated containing the domain instance"
            model.tagPIInstance == tagPI

        when: "User is a manager and has access to edit the specified tag"  
            controller.edit(tagPI)

        then: "A model is populated and the user is redirected to the editFormula view"
            controller.shiroService.isAdmin() >> false
            1 * controller.tagPIService.hasAccessToTag(_) >> true
            model.tagPIInstance == tagPI
            view == 'editFormula'

        when: "User is a manager and doesn't have access to the specified tag" 
            response.reset()           
            controller.edit(tagPI)
                    
        then: "User is redirected to the unauthorized page"
            controller.shiroService.isAdmin() >> false
            1*controller.tagPIService.hasAccessToTag(_) >> false
            response.redirectedUrl == '/unauthorized'
    }

    void "Test the update action performs an update domain doesn't exist"() {
        setup:
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
    
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            0 * controller.tagPIService.update(_,_)
            response.redirectedUrl == '/admin/tagPI/index'
            flash.message != null
    }

    void "Test the update action performs an update on a invalid domain instance"() {
        setup:
            def tagPI = new TagPI()
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'

        when:"An invalid domain instance is passed to the update action"
            tagPI.validate()
            controller.update(tagPI)

        then:"The edit view is rendered again with the invalid instance"
            1 * controller.tagPIService.update(tagPI,_) >> { throw new RuntimeException("invalid params") }
            view == 'edit'
            model.tagPIInstance == tagPI
    }

    void "Test the update action performs an update on a valid domain instance"() {
        setup:
            populateValidParams(params)
            def tagPI = new TagPI(params).save(flush: true)

            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
        when:"A valid domain instance is passed to the update action"
            controller.update(tagPI)

        then:"A redirect is issues to the show action"
            1 * controller.tagPIService.update(tagPI,_) >> tagPI.save()
            response.redirectedUrl == "/admin/tagPI/show/$tagPI.id"
            flash.message != null
    }

    void "Test that the delete action deletes with null"() {
        setup:
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            0 * controller.tagPIService.update(_,_)
            response.redirectedUrl == '/admin/tagPI/index'
            flash.message != null
    }


    void "Test that the delete action deletes valid exiting instance"() {
        setup:
            populateValidParams(params)
            def tagPI = new TagPI(params).save(flush: true)
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'

        when:"The domain instance is passed to the delete action"
            controller.delete(tagPI)

        then:"The instance is deleted"
            1 * controller.tagPIService.delete(tagPI) >> tagPI.save()
            response.redirectedUrl == '/admin/tagPI/index'
            flash.message != null
    }


    void "Test that the delete with TagPIReferenceException"() {
        setup:
            populateValidParams(params)
            def tagPI = new TagPI(params).save(flush: true)
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'

        when:
            controller.delete(tagPI)

        then:
            1 * controller.tagPIService.delete(tagPI) >> { throw new RuntimeException("e", new TagPIReferenceException('e')) }
            view == '/tagPI/show'
    }

    void "Test that the delete with RuntimeException"() {
        setup:
            populateValidParams(params)
            def tagPI = new TagPI(params).save(flush: true)
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'

        when:
            controller.delete(tagPI)

        then:
            1 * controller.tagPIService.delete(tagPI) >> { throw new RuntimeException("error",new Exception('e'))}
            response.redirectedUrl == "/admin/tagPI/show/${tagPI.id}"
            flash.message != null
    }


    void "Test childrentags just id and name (bugs events should not come with tags)"() {
        setup:
            def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)

            def tag = Mock(TagPI)
        when:
            controller.childrentags(tag)
        then:
            1 * tag.getPossibleChildrenTags() >> [t1,t2]
            response.json  == [[id:1, name:'tag1'], [id:2, name:'tag2']]
    }

    void "Test relatedUeps"() {
        setup:
            def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            controller.uepService = Mock(UepService)
            
        when:
            controller.relatedUeps()
        then: 
            response.status == 400
            0 * controller.uepService.findRelatedUepsFromTags(_) 


        when:
            response.reset()
            params["ids"] = ""
            controller.relatedUeps()
        then: 
            response.status == 400
            0 * controller.uepService.findRelatedUepsFromTags(_) 


        when:
            response.reset()
            params["ids"] = t1.id as String
            controller.relatedUeps()

        then:
            1 * controller.uepService.findRelatedUepsFromTags([t1]) >> ["uep1"]
            response.status == 200
            response.json == ["uep1"]

        when:
            response.reset()
            params["ids"] = [t1.id as String,t2.id as String]
            controller.relatedUeps()

        then:
            1 * controller.uepService.findRelatedUepsFromTags([t1,t2]) >> ["uep1","uep2"]
            response.status == 200
            response.json == ["uep1","uep2"]

    }

    void "test reload action"() {

        setup:
            def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def calc = new TagPI(name: 'tagcalc', formula: "'tag1' + 'tag1'", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush: true)

            controller.tagPIService = Mock(TagPIService)

        when:
            controller.reload(calc)

        then:
            response.status == 400
            0 * controller.tagPIService.reloadtags(_)

        when:
            response.reset()
            params["reloadTags"] = []
            controller.reload(calc)
        then: 
            response.status == 400
            0 * controller.tagPIService.reloadtags(_)

        when:
            response.reset()
            params["reloadTags"] = calc.id as String
            controller.reload(calc)
        then: 
            response.status == 302
            1 * controller.tagPIService.reloadtags([calc])

        when:
            response.reset()
            params["reloadTags"] = [calc.id as String,t1.id as String,t2.id as String]
            controller.reload(calc)
        then: 
            response.status == 302
            1 * controller.tagPIService.reloadtags([calc,t1,t2])

    }

    void "test update formula by a manager"() {
        setup: 
            def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def calc = new TagPI(id: 2, name: 'tagcalc', formula: "'tag1' + 'tag1'", tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save(flush: true)
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
        
        when: "user doesn't have access to the specified tag"
            response.reset()
            controller.updateFormula(calc.id)
        then: "user is redirected to the unauthorized page"
            1 * controller.tagPIService.hasAccessToTag(_) >> false
            response.redirectedUrl == '/unauthorized'

        when: "user has access and inputs a valid formula"
            response.reset()
            params["formula"] = "1+1"
            controller.updateFormula(calc.id)
        then: "update is done and user is redirected to the show action displaying the updated tag"
            1 * controller.tagPIService.hasAccessToTag(_) >> true
            1 * controller.tagPIService.update(calc, _) >> calc.save()
            response.redirectedUrl == "/admin/tagPI/show/$calc.id"
            flash.message != null


        when: "user has access and inputs a invalid formula"
            response.reset()
            controller.updateFormula(calc.id)
        then: "user is redirect to the edit screen to correct the errors"
            1 * controller.tagPIService.hasAccessToTag(_) >> true
            1 * controller.tagPIService.update(calc,_) >> { throw new RuntimeException("invalid params") }
            view == 'editFormula'
            model.tagPIInstance == calc


    }
}
