package br.com.petrobras.paap

import spock.lang.Specification

/**
 * Created by roberto on 12/03/15.
 */

abstract class ConstraintUnitSpec extends Specification {

    def getTagPI(name, formula, type, location) {
        new TagPI(name:name, formula: formula, tagType: type, tagLocation: TagPILocation.UO_BS)
    }

    def getAnyValidTag(){
        getTagPI('Valid Name', 'Any Formula', TagPIType.SNAPSHOT, TagPILocation.UO_BS)
    }

    void validateConstraints(obj, field, error) {
        def validated = obj.validate()


        if (error && error != 'valid') {
            assert !validated
            assert obj.errors[field]
        } else {
            assert !obj.errors[field]
        }
    }
}
