package br.com.petrobras.paap

import grails.test.mixin.TestFor
import grails.validation.ValidationException
import spock.lang.Specification
import static plastic.criteria.PlasticCriteria.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(TagPI)
class TagPISpec extends Specification {


    void "test formula"() {
        when: 'save a tag with a formula which has a tag that doesnt exist'
        new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        def tag3 = new TagPI(name: 'tag3', formula: "'tag1' + 'tag2' + 'tag_nao_existente'", tagType: TagPIType.CALCULATED)
        tag3.save(flush: true)

        then: 'shoud throw an exception'
        thrown(ValidationException)

        when: 'save a tag with a correct formula'
        def tag4 = new TagPI(name: 'tag4', formula: "'tag1' + 'tag2'", tagType: TagPIType.CALCULATED)
        tag4.save(flush: true)

        then: 'shoud save'
        assertNotNull TagPI.findByName('tag4')
    }

    void "test unique name"() {
        when: 'save a tag with a name that already exists'
        new TagPI(name: 'tag5', formula: 'tag5', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        def tag5 = new TagPI(name: 'tag5', formula: 'tag5', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS)
        tag5.save(flush: true)

        then: 'shoud throw an exception'
        thrown(ValidationException)

    }

    void "test tag location"() {
        when: 'save a non-calculated tag without location'
        new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT).save(flush: true)

        then: 'shoud throw an exception'
        thrown(ValidationException)
    }

    void "test tag location CALCULATED type "() {
        new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        

        when: 'save a calculated tag without location'
        def calc1 = new TagPI(name: 'tag a', formula: "'tag1' + 1", tagType: TagPIType.CALCULATED,tagLocation: null).save(flush: true)

        then: 'shoud not throw an exception'
        notThrown(ValidationException)
        calc1.hasErrors() == false


        when: 'validate calcucated tag with invalid location'
            def calc2 = new TagPI(name: 'tag b', formula: "'tag1' + 1", tagType: TagPIType.CALCULATED,tagLocation: "invalid")
            calc2.validate()
        then: 'has errors'
            calc2?.hasErrors() == true
        


        when: 'save a calculated tag with invalid location'
            def calc3 = new TagPI(name: 'tag c', formula: "'tag1' + 1", tagType: TagPIType.CALCULATED,tagLocation: "invalid")
            calc3.save(flush: true)
        then: 'shoud throw an exception'
            thrown(ValidationException)    

    }

    void "test find formula tags"() {
        setup:
            def t1 = new TagPI(name: 't1-name', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 't2-name', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def calc = new TagPI(name: 'calc-name', formula: "'tag1' * 'tag2'", tagType: TagPIType.CALCULATED).save(flush: true)

        when:
            def tags = calc.possibleChildrenTags
        
        then:
            tags.contains(t1) == true
            tags.contains(t2) == true
    }



    void "test find parent tags for tag"() {
        setup:
            def t1 = new TagPI(name: 't1-name', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 't2-name', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def calc = new TagPI(name: 'calc-name', formula: "'tag1' * 'tag2'", tagType: TagPIType.CALCULATED).save(flush: true)
            def calc2 = new TagPI(name: 'calc-name 1 ', formula: "'tag1'", tagType: TagPIType.CALCULATED).save(flush: true)
            def calc3 = new TagPI(name: 'calc-name 2', formula: "'tag1' + 1", tagType: TagPIType.CALCULATED).save(flush: true)
           
        when:
            def tags = t1.getPossibleParentTags()
        
        then:
            tags.contains(calc) == true
            tags.contains(calc2) == true
            tags.contains(calc3) == true
    }

     void "test find tags"() {
        setup:
            mockCriteria([TagPI])
            def t1 = new TagPI(name: 't1-name', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
            def t2 = new TagPI(name: 't2-name', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES).save(flush: true)
            def reloading = new TagPI(name: 'r-name', formula: 'r', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES, reloading:true).save(flush: true)

            def calc = new TagPI(name: 'calc-name', formula: "'tag1' * 'tag2'", tagType: TagPIType.CALCULATED).save(flush: true)
            def reloading_calc = new TagPI(name: 'r-calc-name', formula: "'tag1' * 'tag2'/'tag2'", tagType: TagPIType.CALCULATED,, reloading:true).save(flush: true)

        when: 'all pi'
            def tags = TagPI.findPiTags()
        
        then:
            tags.contains(t1) == true
            tags.contains(t2) == true
            tags.contains(reloading) == false
            

        when: 'pi from location'
            tags = TagPI.findPiTagsFromLocation(TagPILocation.UO_ES)
        
        then:
            tags.contains(t1) == false
            tags.contains(t2) == true
            tags.contains(reloading) == false

        when: 'calc'
            tags = TagPI.findCalculatedTags()
        
        then:
            tags.contains(t1) == false
            tags.contains(t2) == false
            tags.contains(reloading) == false
            tags.contains(calc) == true
            tags.contains(reloading_calc) == false
            
    }

    void "test unique formula not for calculated"() {
        when: 'save a tag with a formula that already exists'
        new TagPI(name: 'tag5', formula: 'tag5', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        def tag5 = new TagPI(name: 'tag5.1', formula: 'tag5', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS)
        tag5.save(flush: true)

        then: 'shoud throw an exception'
        thrown(ValidationException)

        when: 'update a tag with the same formula'
        new TagPI(name: 'tag6', formula: 'tag6', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        def tag = TagPI.findByFormula('tag6')
        tag.formula = 'tag6'
        tag.save(flush: true)

        then: 'shoud save'
        assertNotNull TagPI.findByName('tag6')

    }
}

