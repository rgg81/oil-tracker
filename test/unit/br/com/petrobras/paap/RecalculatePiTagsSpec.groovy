package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.job.RecalculatePiTags

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI])
class RecalculatePiTagsSpec extends Specification {

	def t1,t2,t3,calc
    def readPIService = Mock(ReadPIService)

	def doWithConfig(c) {
    	c.paap.job.calculatedTagsIntervalInSeconds = 10
    }
 
    def cleanup() {
        Date.metaClass.truncateToSeconds == null
    }


    def setup() {
        Date.metaClass.truncateToSeconds = { seconds -> new Date( delegate.time - delegate.time % (1000*seconds) ) }
		t1 = new TagPI(name: 't1-name',formula:'t1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        t2 = new TagPI(name: 't2-name',formula:'t2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        t3 = new TagPI(name: 't3-name',formula:'t3', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
		calc = new TagPI(name: 'calc',formula:"'t1' + 't2' + 't3'", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)
    }



	void "test null data"() { // TODO esse teste on delete Tags
        def p = new RecalculatePiTags(readPIService, grailsApplication)
        
        when:
			p.processInternal()
        then:
        	thrown IllegalStateException
        	
	}

	void "test duplicate tag entry"() { // TODO esse teste on delete Tags
		setup:
      
		def t22 = TagPI.get(t2.id)

        
        def data = [:]
        data[t1] = [samples:[]]
        data[t2] = [samples:[]]
        data[t3] = [samples:[]]
        data[calc] = [samples:[]]

        data[t22] = [samples:[]]

        def p = new RecalculatePiTags(readPIService, grailsApplication)
        
        when:
			def result = p.processInternal(data)
        then:
        	result.size() == 4
            
    }

    void "test flatten children tags"(){
		def calc2 = new TagPI(name: 'calc2',formula:"'t2' + 't3'", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)
		def calc3 = new TagPI(name: 'calc3',formula:"('t2' + 't3')/'t1' ", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)

		def data = [:]
		data[calc] = [samples:[]]
        data[calc2] = [samples:[]]
        data[calc3] = [samples:[]]
        data[t1] = [samples:[]]
        data[t2] = [samples:[]]
        data[t3] = [samples:[]]

         
		def p = new RecalculatePiTags(readPIService, grailsApplication)
		p.metaClass.processCalculatedTag = { now, yesterday, calculatedTags, variablesTags -> 
				assert calculatedTags == [calc,calc2,calc3] as Set
				assert  variablesTags == [t1,t2,t3] as Set
				[:]
		}
		
		when:
			def result = p.processInternal(data)
        then:
        	notThrown(Exception)
        	

    }
}
