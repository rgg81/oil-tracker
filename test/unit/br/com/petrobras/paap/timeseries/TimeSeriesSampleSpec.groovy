package br.com.petrobras.paap.timeseries

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class TimeSeriesSampleSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test compare with other"() {
    	TimeSeriesSample sampleA = new TimeSeriesSample(time: new Date(1000), value: 10)
    	TimeSeriesSample sampleB = new TimeSeriesSample(time: new Date(1500), value: 10)
    	TimeSeriesSample sampleA2 = new TimeSeriesSample(time: new Date(1000), value: 10)

    	expect:
    		(sampleA <=> sampleB) == -1
    		(sampleB <=> sampleA) == 1
    		(sampleA2 <=> sampleA) == 0
    }

    void "test compare with time"() {
    	TimeSeriesSample sample = new TimeSeriesSample(time: new Date(1000), value: 10)
    	Date timeAfter = new Date(1500)
    	Date timeBefore = new Date(500)
    	Date timeEquals = new Date(1000)

    	expect:
    		(sample <=> timeAfter) == -1
    		(sample <=> timeBefore) == 1
    		(sample <=> timeEquals) == 0
    }

}
