package br.com.petrobras.paap.timeseries

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class TimeSeriesInterpolatorSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    void "test interpolate numbers: #comentario"(datesValues, time, expectedTime, expectedValue,comentario) {
        TimeSeriesInterpolator interpolator = new TimeSeriesInterpolator(datesValues.collect { new TimeSeriesSample(time: new Date(it[0]), value: it[1]) } as Set )

        Date searchDate = new Date(time)
        
        def interpolatedSample = interpolator.getSampleAt(searchDate)

    	expect:
    		interpolatedSample == expectedValue

        where:
                                     datesValues                            | time | expectedTime | expectedValue   |   comentario
            [                                                             ] | 5    | null         | null            | 'Sem dados'   
            [[10,10]                                                      ] | 5    | 5            | 10              | 'Um dado com tempo anterior'
            [[10,10]                                                      ] | 10   | 10           | 10              | 'Um dado com tempo durante'
            [[10,10]                                                      ] | 15   | 15           | 10              | 'Um dado com tempo posterior'
            [[10,10], [20,20]                                             ] | 5    | 5            | 10              | 'Dois dados com tempo anterior'
            [[10,10], [20,20]                                             ] | 10   | 10           | 10              | 'Dois dados com tempo inicial'
            [[10,10], [20,20]                                             ] | 15   | 15           | 15              | 'Dois dados com tempo durante'
            [[10,10], [20,20]                                             ] | 20   | 20           | 20              | 'Dois dados com tempo final'
            [[10,10], [20,20]                                             ] | 25   | 25           | 20              | 'Dois dados com tempo posterior'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 5    | 5            | 10              | 'Multiplos dados com tempo anterior'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 15   | 15           | 15              | 'Multiplos dados com tempo durante #1'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 20   | 20           | 20              | 'Multiplos dados com tempo durante #2'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 22   | 22           | 20              | 'Multiplos dados com tempo durante #3'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 30   | 30           | 20              | 'Multiplos dados com tempo durante #4'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 35   | 35           | 20              | 'Multiplos dados com tempo durante #5'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 40   | 40           | 20              | 'Multiplos dados com tempo durante #6'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 45   | 45           | 35              | 'Multiplos dados com tempo durante #7'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 52   | 52           | 54              | 'Multiplos dados com tempo durante #8'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 55   | 55           | 60              | 'Multiplos dados com tempo durante #9'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 70   | 70           | 70              | 'Multiplos dados com tempo final'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 75   | 75           | 70              | 'Multiplos dados com tempo posterior #1'
            [[10,10], [20,20], [30,20], [40,20], [50,50], [55,60], [70,70]] | 100  | 100          | 70              | 'Multiplos dados com tempo posterior #2'
            [[10,10], [20,20], [20,20], [40,20], [50,50], [55,60], [70,70]] | 20   | 20           | 20              | 'Dois pontos iguais (mesmo valor e data) seguidos'            
            [[10,10], [20,20], [20,30], [40,20], [50,50], [55,60], [70,70]] | 20   | 20           | 25              | 'Dois pontos com a mesma data e valor diferentes seguidos'
    }

    void "test binary search"(dates, search, expectedIndex) {
    	TimeSeriesInterpolator interpolator = new TimeSeriesInterpolator(dates.collect { new TimeSeriesSample(time: new Date(it*10000), value: 10) } as Set )

    	Date searchDate = new Date(search*10000)
    	
    	int result = interpolator.binaryFindClosestTimeSerie(searchDate, 0, dates.size()-1)

    	expect:
    		result == expectedIndex

    	where:
    		dates							| search | expectedIndex
    		[10, 20, 30, 40, 50, 60, 70]	| 5		 | -1
    		[10, 20, 30, 40, 50, 60, 70]	| 10	 | 0
    		[10, 20, 30, 40, 50, 60, 70]	| 15	 | 0
    		[10, 20, 30, 40, 50, 60, 70]	| 20	 | 1
    		[10, 20, 30, 40, 50, 60, 70]	| 21	 | 1
    		[10, 20, 30, 40, 50, 60, 70]	| 65	 | 5
    		[10, 20, 30, 40, 50, 60, 70]	| 70	 | 6
    		[10, 20, 30, 40, 50, 60, 70]	| 71	 | 6
    		[10, 20, 30, 40, 50, 60, 70]	| 80	 | 6
    }

    void "test interpolate two numbers internall"(timeA, timeB, middleTime, interpolatedValue) {
    	TimeSeriesSample sampleA = new TimeSeriesSample(time: new Date(timeA), value: 10)
    	TimeSeriesSample sampleB = new TimeSeriesSample(time: new Date(timeB), value: 20)

    	Date timeBetween = new Date(middleTime)

    	Double interpolatedSample = TimeSeriesInterpolator.interpolate(timeBetween, sampleA, sampleB)

    	expect:
    		interpolatedSample == interpolatedValue

	    where:
	    	timeA | timeB | middleTime | interpolatedValue
	    	1000  | 2000  | 1500       | 15
	    	1000  | 2000  | 1900       | 19
	    	1000  | 2000  | 1000       | 10
	    	1000  | 2000  | 2000       | 20
	    	1000  | 2000  | 500        | 5
	    	1000  | 2000  | 3000       | 30
    }

    void "test getInterpolatedDistanceAt"() {
        TimeSeriesSample sampleA = new TimeSeriesSample(time: new Date(2000), value: 10)
        TimeSeriesSample sampleB = new TimeSeriesSample(time: new Date(5000), value: 20)

        TimeSeriesInterpolator interpolator = new TimeSeriesInterpolator([sampleA, sampleB])

        expect:
            interpolator.getInterpolatedDistanceAt(new Date(500)) == 1500
            interpolator.getInterpolatedDistanceAt(new Date(2200)) == 200
            interpolator.getInterpolatedDistanceAt(new Date(4500)) == 500
            interpolator.getInterpolatedDistanceAt(new Date(6000)) == 1000
    }
}
