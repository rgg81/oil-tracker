package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.job.ReloadPiTags

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI])
class ReloadPiTagsSpec extends Specification {
	
	def readPIService
	def processTagService

    def setup() {
    	readPIService = Mock(ReadPIService)
    	processTagService = Mock(ProcessTagService)
    }

    def cleanup() {
    }

    void "test tag only calculated"() {
    	setup:
			def t1 = new TagPI(name: 'tag1', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
			def t2 = new TagPI(name: 'tag2', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
			def acc = new TagPI(name: 'tag3', formula: 'tag3', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_BS).save(flush: true)
			def calc = new TagPI(name: 'calc', formula: "'tag1'+'tag2'+'tag3'", tagType: TagPIType.CALCULATED).save(flush: true)

			def a1 = new TagPI(name: 'a1', formula: 'a1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES).save(flush: true)
			def a2 = new TagPI(name: 'a2', formula: 'a2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_ES).save(flush: true)
			def acc2 = new TagPI(name: 'a3', formula: 'a3', tagType: TagPIType.ACCUMULATOR, tagLocation: TagPILocation.UO_ES).save(flush: true)
			def calc2 = new TagPI(name: 'calc2', formula: "'a1'+'a2'+'a3'", tagType: TagPIType.CALCULATED).save(flush: true)

			def tagsToReset = [t1,t2,acc,calc,a1,a2,acc2,calc2]

			def process = new ReloadPiTags(readPIService,processTagService, grailsApplication)
		
		when:
			def tags = process.processInternal(tagsToReset)

		then:
			1 * readPIService.readDataFromTagsInTimePeriod(_,TagPILocation.UO_BS,_,_) >> [  (t1.formula):[1,2,3,1,2,3],
																		 					(t2.formula):[1,2,3,1,2,3],
																		 					(acc.formula):[1,2,3,1,2,3]]
			1 * readPIService.readDataFromTagsInTimePeriod(_,TagPILocation.UO_ES,_,_) >> [  (a1.formula):[2,3,4,2,3,4],
																		 					(a2.formula):[2,3,4,2,3,4],
																		 					(acc2.formula):[2,3,4,2,3,4]]
			1 * processTagService.transformAccumulatedSamples(_,acc) >> [4,5,6,2,4,6]
			1 * processTagService.transformAccumulatedSamples(_,acc2) >> [4,5,6,2,4,6]
			tags[t1].samples == [1,2,3] as Set
			tags[t2].samples == [1,2,3] as Set
			tags[acc].samples == [2,4,5,6] as Set

			tags[a1].samples == [2,3,4] as Set
			tags[a2].samples == [2,3,4] as Set
			tags[acc2].samples == [2,4,5,6] as Set
			tags.size() == 8
    }
}
