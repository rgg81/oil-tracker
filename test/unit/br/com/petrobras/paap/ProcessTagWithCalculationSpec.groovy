package br.com.petrobras.paap

import br.com.petrobras.paap.job.ProcessTagWithCalculation
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*
// import mockCriteria() static method
import static plastic.criteria.PlasticCriteria.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI,Sample,Event,EventTagPI])
class ProcessTagWithCalculationSpec extends Specification {

    def readPIService = Mock(ReadPIService)

    def doWithConfig(c) {
        c.paap.job.calculatedTagsIntervalInSeconds = 10
    }

	def setup() {
         Date.metaClass.truncateToSeconds = { seconds -> new Date( delegate.time - delegate.time % (1000*seconds) ) }
         mockCriteria([Sample])
    }

    def cleanup() {
        Date.metaClass.truncateToSeconds == null
    }

    private createTagMap(tags){
        def t = [:]
        tags.each{ tag, samplesInfo ->
            //println "n:" + TagPI.findByName(k).tagType
            t[tag.name] = samplesInfo.samples.collect{ sample -> [sample.time.getTime()/1000, sample.value] }
        }
        return t
    }
    private mapKey(k){
        TagPI.findByName(k)
    }


    void "test data truncate"() {
        expect:
        new Date(11000).truncateToSeconds(10) == new Date(10000)
    }

	void "test simple sum 3 tags snapshot 3 tags calculated "() {
        def end = new Date(55000)
        def begin = new Date(5000)

        def tags =  [   't1': [[10,100],[20,102],[30,104],[40,108]],      
                        't2': [[10, 50],[20, 52],[30,54],[40,58]],
                        't3': [[10, 20],[20, 22],[30,24],[40,28]] ]

        def calctags =  [   'f(t1)': [[0,200],[10,200],[20,204],[30,208],[40,216],[50,216]],      
                            'f(t2)': [[0,150],[10,150],[20,154],[30,158],[40,166],[50,166]],
                            'f(t3)': [[0,120],[10,120],[20,124],[30,128],[40,136],[50,136]] ]

        //tags nao calculadas
        tags.each{ key, value ->
            def t = new TagPI(name: key,formula:key, tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
            value.each {
                new Sample(date: new Date(it[0] * 1000), value: it[1], tag: t).save()
            }
        }
        calctags.eachWithIndex{ key,value, i  ->
            new TagPI(name: key,formula:"'t${i+1}' + 't1'", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)
        }

        def p = new ProcessTagWithCalculation(readPIService, grailsApplication)
        def a = p.processInternal([end:end,begin:begin])
        def mapResult = createTagMap(a)

        expect:
            mapResult == calctags
    }

    @Unroll
    void "test function '#function' with 3 snapshot tags and 1 calculated tag"(function,result) {

        def end = new Date(55000)
        def begin = new Date(5000)

        def tags =  [   't1': [[10,100],[20,102],[30,104],[40,108]],
                        't2': [[10, 50],[20, 52],[30,54],[40,58]],
                        't3': [],
                        't4': [[10,0.0001]]]

        tags.each{ key, value ->
            def t = new TagPI(name: key,formula:key, tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
            value.each {
                new Sample(date: new Date((it[0] * 1000).toLong()), value: it[1], tag: t).save()
            }
        }

        //tag calculadas
        new TagPI(name: "f(t1)",formula:function, tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)

        def calctags =  ['f(t1)': result]


        def p = new ProcessTagWithCalculation(readPIService, grailsApplication)
        def a = p.processInternal([end:end,begin:begin])
        def mapResult = createTagMap(a)
        expect:
        mapResult == calctags

        where:
        function                            |                        result
        "2*'t1'"                            | [[0,200],[10,200],[20,204],[30,208],[40,216],[50,216]]
        "'t1' + 't2'"                       | [[0,150],[10,150],[20,154],[30,158],[40,166],[50,166]]
        "('t1' + 't2') * 6.2898 * 24 * 0.65"| [[0,14718.132],[10,14718.132],[20,15110.615520000001],[30,15503.09904],[40,16288.06608],[50,16288.06608]]
        "'t1'/('t1'-'t1')"                  | []
        "'t3'"                              | [[0, 0.0], [10, 0.0], [20, 0.0], [30, 0.0], [40, 0.0], [50, 0.0]]
        "'t1' + 't2' + 't3'"                | [[0, 150.0], [10, 150.0], [20, 154.0], [30, 158.0], [40, 166.0], [50, 166.0]]
        "'t1' + 't2' + 't4'"                | [[0, 150.0001], [10, 150.0001], [20, 154.0001], [30, 158.0001], [40, 166.0001], [50, 166.0001]]
    }

    void "test police for replicating samples outside range now-yesterday"() {
        def end = new Date(55 * 1000)
        def begin = new Date(45 * 1000)

        def tagValues = [[20, 32], [30, 64]];
        def tag = new TagPI(name: 't1', formula:'t1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
        tagValues.each {
            new Sample(date: new Date((it[0] * 1000).toLong()), value: it[1], tag: tag).save()
        }

        new TagPI(name: "f(t1)", formula:"'t1'/2", tagType: TagPIType.CALCULATED).save(flush:true,failOnError:true)

        def p = new ProcessTagWithCalculation(readPIService, grailsApplication)

        when:
            p.grailsApplication.config.paap.processdata.maxDurationForInterpolation = 30000   // 20000 ms in minutes
        then:
            def result1 = createTagMap(p.processInternal([end:end,begin:begin]))
            result1 == ['f(t1)': [[40, 32], [50, 32]]]

        when:
            p.grailsApplication.config.paap.processdata.maxDurationForInterpolation = 20000   // 20000 ms in minutes
        then:
            def result2 = createTagMap(p.processInternal([end:end,begin:begin]))
            result2 == ['f(t1)': [[40, 0], [50, 0]]]
    }
}
