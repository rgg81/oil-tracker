package br.com.petrobras.paap

import grails.test.mixin.TestMixin
import grails.test.runtime.FreshRuntime
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import br.com.petrobras.paap.job.*
import grails.util.Holders
import org.codehaus.groovy.grails.commons.InstanceFactoryBean
import groovy.time.TimeCategory

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@FreshRuntime
class ProcessDataSpec extends Specification {

    def doWithConfig(c) {
        c.numero.valor = 10
    }

	def doWithSpring = {
        readPIService(InstanceFactoryBean, Mock(ReadPIService), ReadPIService) 
    }

    def setup() {
		
	}

    def cleanup() {
    }


    void "test mockConfig 2 levels"() {
    	expect:
    		Holders.grailsApplication.config.numero.valor == 10
    }


    void "test mockBean"() {
    	expect:
    		Holders.grailsApplication.mainContext.getBean('readPIService') != null
    }

}
