package br.com.petrobras.paap

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import groovy.time.TimeCategory
import spock.lang.Specification
import spock.lang.Unroll
import java.text.SimpleDateFormat
import java.text.DateFormat

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(UepService)
@Mock([UEP,TagPI,SitopBdo,SnapshotSamples24h,UserPermission,Sample])
class UepServiceSpec extends Specification {
    
    def shiroServiceMock = Mock(ShiroService)

    def setup() {
        service.shiroService = shiroServiceMock
        new UserPermission(attribute: 'uo', value: 'UO-BS', username: 'UP2H').save()
        new UserPermission(attribute: 'uo', value: 'UO-BC', username: 'UP2H').save()
        new UserPermission(attribute: 'uo', value: 'UO-ES', username: 'UP2H').save()
        new UserPermission(attribute: 'uo', value: 'UO-RIO', username: 'UP2H').save()
    }

    def cleanup() {
    }

    @Unroll
    void "test order uo"(params, expected) {

    	new UEP(name: 'P-40', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-40', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
    	new UEP(name: 'P-41', uo: 'UO-ES', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
    	new UEP(name: 'P-42', uo: 'UO-BC', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-42', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
    	new UEP(name: 'P-43', uo: 'UO-BS', department: 'TBD', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-43', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();



        1 * shiroServiceMock.getLoggedUser() >> "UP2H"
    	def result = service.filterUEP(params)

        expect:	    
        result*.name == expected

	    where:
	    params 														|  	expected
	    [:]															|	['UO-ES','UO-BC','UO-RIO','UO-BS']
	    [filterKey:'uo']											|	['UO-ES','UO-BC','UO-RIO','UO-BS']
	    [filterKey:'department',filterValue: 'SSE',groupKey:'uo']	|	['UO-ES','UO-BC','UO-RIO']
    }

	@Unroll
    void "test order uep"(params, expected){

    	new UEP(name: 'P-41', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
    	new UEP(name: 'P-40', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-40', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
    	new UEP(name: 'P-43', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-43', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
    	new UEP(name: 'P-42', uo: 'UO-RIO', department: 'TIC', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-42', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();

        1 * shiroServiceMock.getLoggedUser() >> "UP2H"
	    def result = service.filterUEP(params)

	    expect:
	    result.find{it.name == 'UO-RIO'}.values*.name == expected
        
	    where:
	    params 														|  	expected
	    [:]															|	['P-40','P-41','P-42','P-43']
	   	[filterKey:'uo']											|	['P-40','P-41','P-42','P-43']
	   	[filterKey:'department',filterValue: 'SSE',groupKey:'uo']	|	['P-40','P-41','P-43']
    }

    @Unroll
    void "test groupKey and filterKey"(params,expected,description) {

        new UEP(name: 'P-41', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
        new UEP(name: 'P-40', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-40', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
        new UEP(name: 'P-43', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-43', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
        new UEP(name: 'P-42', uo: 'UO-RIO', department: 'TIC', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: new TagPI(name: 'Tag P-42', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
        
        1 * shiroServiceMock.getLoggedUser() >> "UP2H"
        def result = service.filterUEP(params)
      
        where:
        params                                                                      |   expected                        |   description
        [filterKey:'department',filterValue: 'SSE',groupKey:'uo']                   |   ['P-40','P-41','P-43']          |   "Valid request"
        [filterKey:'department',filterValue: 'SSE',groupKey:'doesntexist']          |   null                            |   "Invalid groupKey specified"
        [filterKey:'department',filterValue: 'SSE']                                 |   null                            |   "No groupKey specified"
        [filterKey:'doesntexist',filterValue: 'SSE',groupKey:'uo']                  |   null                            |   "Invalid filterKey supplied"
        [filterKey:'department',filterValue: 'NOTAVALUE',groupKey:'doesntexist']    |   null                            |   "Invalid filterKey and groupKey supplied" 
        [filterKey:'department',filterValue: 'NOTAVALUE',groupKey:'uo']             |   null                            |   "Invalid filterValue supplied" 
        [filterKey:'doesntexist',filterValue: 'NOTAVALUE',groupKey:'doesntexist']   |   null                            |   "EVERYTHING MESSED UP"     
        [filterKey:'uo']                                                            |   ['P-40','P-41','P-42','P-43']   |   "Valid filterKey request (UO)"
        [filterKey:'doesntexist']                                                   |   ['P-40','P-41','P-42','P-43']   |   "Invalid filterkey specified"
        [:]                                                                         |   ['P-40','P-41','P-42','P-43']   |   "No filter specified, should return filterKey=UO by default"

    }

    void "test findRelatedUeps"(){
        def t1 = new TagPI(name: 't1-name', formula: 'tag1', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        def t2 = new TagPI(name: 't2-name', formula: 'tag2', tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush: true)
        def calc = new TagPI(name: 'calc-name', formula: "'tag1' * 'tag2'", tagType: TagPIType.CALCULATED).save(flush: true)
        def calc2 = new TagPI(name: 'calc-name 1 ', formula: "'tag1'", tagType: TagPIType.CALCULATED).save(flush: true)
        def calc3 = new TagPI(name: 'calc-name 2', formula: "'tag1' + 1", tagType: TagPIType.CALCULATED).save(flush: true)
        
        def uep = new UEP(name: 'P-41', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: calc).save();
        def uep2 = new UEP(name: 'P-41-1', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: calc2).save();
        def uep3 = new UEP(name: 'P-41-2', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                            tag: calc3).save();
        
        when:
            def result = service.findRelatedUeps(uep)        
        
        then:
            result == [uep2,uep3] as Set

        when:
            result = service.findRelatedUepsFromTags([calc])    
        
        then:
            result == [] as Set
            
        when:
            result = service.findRelatedUepsFromTags([t1])    
        
        then:
            result == [uep,uep2,uep3] as Set

        when:
            result = service.findRelatedUepsFromTags([t2])    
        
        then:
            result == [uep] as Set
    
    }

    @Unroll
    void "test snapshot 24 hours"(dates,description) {
        setup:
        def uep = new UEP(name: 'P-41', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
        
        dates.each{ it -> 
            new SitopBdo(sitopDate: it.date, expected: 0d, preliminary: 0d, uep: uep).save()
            new SnapshotSamples24h(date: it.date, value: it.value, tag: uep.tag).save()
        }

        def result = service.snapShotAverage24hours(uep.id)

        expect:
            result.currentSnapshotValue == dates[0].value
            result.currentSnapshotDate == dates[0].date
            result.previousSnapshotValue == dates[1].value
            result.previousSnapshotDate == dates[1].date

        where:
            dates                                                       |  description
            [[date:dateNow,value:80d],[date:dateYesterday,value:40d]]   | 'hoje e ontem'
            [[date:jul_1,value:80d],[date:jun_30,value:40d]]            | 'passagem de mes'
    }

    void "test snapshot 24 hours 2 "() {
        setup:
            def uep = new UEP(name: 'P-41', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save()).save();
                   
        when: 'no sitop'
            def result = service.snapShotAverage24hours(uep.id)

        then: 
            result == [:]

        when: 'no current spanshot, no previous snapshot'
            new SitopBdo(sitopDate: dateNow, expected: 0d, preliminary: 0d, uep: uep).save()
            result = service.snapShotAverage24hours(uep.id)
    
        then:
            result.currentSnapshotValue == null
            result.currentSnapshotDate == null
            result.previousSnapshotValue == null
            result.previousSnapshotDate == null

        when: 'current ok , no previous snapshot '
            def  current = new SnapshotSamples24h(date: dateNow, value: 10d, tag: uep.tag).save()
            result = service.snapShotAverage24hours(uep.id)

        then:
            result.currentSnapshotValue == 10d 
            result.currentSnapshotDate == dateNow
            result.previousSnapshotValue == null
            result.previousSnapshotDate == null

        when: 'current ok, previous ok'
            new SnapshotSamples24h(date: dateYesterday, value: 20d, tag: uep.tag).save()
            result = service.snapShotAverage24hours(uep.id)

        then:
            result.currentSnapshotValue == 10d 
            result.currentSnapshotDate == dateNow
            result.previousSnapshotValue == 20d
            result.previousSnapshotDate == dateYesterday

        when: ' no current , previous ok'
            current.delete()
            result = service.snapShotAverage24hours(uep.id)

        then:
            result.currentSnapshotValue == null 
            result.currentSnapshotDate == null
            result.previousSnapshotValue == 20d
            result.previousSnapshotDate == dateYesterday
    }
    private static DateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss")
    private static def dateNow = new Date()
    private static def dateYesterday = new Date(dateNow.getTime() - 24 * 60 * 60 * 1000 )
    private static def dateYesterdayYesterday = new Date(dateNow.getTime() - 48 * 60 * 60 * 1000 )  
    private static def jul_1 = formatter.parse("01/07/2015 00:00:00")
    private static def jun_30 = formatter.parse("30/06/2015 00:00:00")

    void "Test check if user has permission"() {
        setup:
        def uep = new UEP(id: 1, name: 'P-41', uo: 'UO-RIO', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: new TagPI(name: 'Tag P-41', formula: '1+1', tagType: TagPIType.CALCULATED).save(flush:true)).save(flush:true);
        def uep2 = new UEP(name: 'P-42', uo: 'UO-BS', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: new TagPI(name: 'Tag P-42', formula: '1+1', tagType: TagPIType.CALCULATED).save(flush:true)).save(flush:true);
        def uep3 = new UEP(name: 'P-43', uo: 'UO-ES', department: 'SSE', asset: 'TBD', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: new TagPI(name: 'Tag P-43', formula: '1+1', tagType: TagPIType.CALCULATED).save(flush:true)).save(flush:true);
        new UserPermission(attribute: 'id', value: '1', username: 'XBTF').save(flush:true)
        new UserPermission(attribute: 'uo', value: 'UO-BS', username: 'XBTF').save(flush:true)
        3 * shiroServiceMock.getLoggedUser() >> "XBTF"
        when: "Check if user has permission to this specific UEP"
            def result = service.hasPermission(uep)
        then: "User should have access to this specific UEP"
            result == true
    
        when: "Check if user has permission for a UO that contains this specific UEP"
            def result2 = service.hasPermission(uep2)
        then: "With access to the uo the user should be able to retrieve this UEP"
            result2 == true

        when: "Check if user has permission for this specific UEP" 
            def result3 = service.hasPermission(uep3)
        then: "User shouldn't have any kind of permission to retrieve this UEP"
            result3 == false
    }

    void "test lastValue function"(){
        given:
            def tag = new TagPI(name: 'not relevant', formula: '1 + 1', tagType: TagPIType.CALCULATED, tagLocation: TagPILocation.UO_BS).save()
            def uep = new UEP( name: 'Mexilhão Condensado', uo: 'BS', department: 'SSE', asset: 'ATP-N', oceanCurrent: 'east', oilWell: 'poço-do-moço',
                tag: tag).save(flush:true)
            def s1,s2

        when: 'no samples'
            def resp1 = service.lastValue(uep)
        then:
            resp1 == null

        when: 'valid samples'
            s1 = new Sample(date: new Date(10),value:1d,tag: tag).save()
            s2 = new Sample(date:new Date(20),value:2d,tag: tag).save()

            def resp2 = service.lastValue(uep)
        then:
            resp2 == s2.value


    }

}
