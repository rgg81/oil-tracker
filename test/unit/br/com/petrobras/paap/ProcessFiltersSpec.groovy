package br.com.petrobras.paap

import br.com.petrobras.paap.job.ProcessFilters
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([TagPI,Sample, TagPIFilterMovingAverage, TagPIFilterMovingAverageSample,TagPIFilterKalman])
class ProcessFiltersSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def grailsApplication

    void "applying moving average filter happy way"() {

        def tags =  ['t1', 't2']
        def today = new Date()

        tags.each{  value ->
            def t = new TagPI(name: value,formula:value, tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
            new TagPIFilterMovingAverage(windowInMs: 1000*60*2, filterOrder:0, tag: t).save(flush:true,failOnError:true)
        }
        def filter = new ProcessFilters(grailsApplication)
        def tagT1 = TagPI.findByFormula('t1')
        def result = [:]
        result.put(tagT1, [
            samples:[
                new TimeSeriesSample(time: new Date(today.time - (1000*60*2).toLong()), value: 115),
                new TimeSeriesSample(time: new Date(today.time - (1000*60*1.5).toLong()), value: 120),
                new TimeSeriesSample(time: new Date(today.time), value: 70)
            ]])
        def a = filter.processInternal(result)

        expect:
            a[tagT1] == [
                samples:[
                    new TimeSeriesSample(time:new Date(today.time - (1000*60*2).toLong()), value:115.0),
                    new TimeSeriesSample(time:new Date(today.time - (1000*60*1.5).toLong()), value:115.625),
                    new TimeSeriesSample(time:new Date(today.time), value:100.625)
                ]]
    }

    @Unroll
    void "applying moving average filter name:#name"() {
        def tags =  ['t1', 't2']

        def mapToTimeSeriesSample = { anArray ->
            anArray.collect {
                new TimeSeriesSample(time:new Date(it[0]), value: it[1])
            }
        }

        tags.each{  value ->
            def t = new TagPI(name: value,formula:value, tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
            new TagPIFilterMovingAverage(windowInMs: 50, filterOrder:0, tag: t).save(flush:true,failOnError:true)
        }
        def filter = new ProcessFilters(grailsApplication)
        def tagT2 = TagPI.findByFormula('t2')

        def result = [:]
        def samplesInput = mapToTimeSeriesSample(input)
        result.put(tagT2, [samples:samplesInput])
        def a = filter.processInternal(result)
        def expectedResult = mapToTimeSeriesSample(output)

        expect:
        println("Date Long Origin:${a[tagT2].samples[1].time.time} Date Long Expected:${expectedResult[1].time.time}")
        a[tagT2].samples == expectedResult

        where:
        name                             | input                                                                           |           output
        'all data in frame window'       | [[0,200],[10,200],[20,204],[30,208],[40,216],[50,216]]                          | [[0,200],[10,200],[20,200.4],[30,201.6],[40,204],[50,207.2]]
        '5 data in frame window'         | [[0,200],[10,200],[20,204],[30,208],[50,216],[60,216]]                          | [[0,200],[10,200],[20,200.4],[30,201.6],[50,206.4],[60,209.6]]
        '5 zeros and one data'           | [[0,0],[10,0],[20,0],[30,0],[40,0],[50,216]]                                    | [[0,0],[10,0],[20,0],[30,0],[40,0],[50,21.6]]
        'one data with error([10,0])'    | [[0,0],[10,0],[20,0],[10,0],[40,0],[50,216]]                                    | [[0,0],[10,0],[20,0],[40,0],[50,21.6]]

    }

    @Unroll
    void "applying Kalman filter name:#name"() {
        def tags =  ['t3']

        def mapToTimeSeriesSample = { anArray ->
            anArray.collect {
                new TimeSeriesSample(time:new Date(it[0]*1000*60*2), value: it[1])
            }
        }

        tags.each{  value ->
            def t = new TagPI(name: value,formula:value, tagType: TagPIType.SNAPSHOT, tagLocation: TagPILocation.UO_BS).save(flush:true,failOnError:true)
            new TagPIFilterKalman(measureVariance: 2000, processVariance: 20, filterOrder: 0, tag: t).save(flush:true,failOnError:true)
        }
        def filter = new ProcessFilters(grailsApplication)
        def tagT3 = TagPI.findByFormula('t3')

        def result = [:]
        def samplesInput = mapToTimeSeriesSample(input)
        result.put(tagT3, [samples:samplesInput])
        def a = filter.processInternal(result)
        def expectedResult = mapToTimeSeriesSample(output)

        expect:
        a[tagT3].samples == expectedResult

        where:
        name                             | input                                                             |           output
        'all data in frame window'       | [[0,200],[1,200],[2,204],[3,208],[4,216],[5,216]]                 | [[0,200],[1,200], [2,201.33333334], [3,203.000000005],[4,205.60000000400004],[5,205.70297030099013]]
        '5 zeros and one data'           | [[0,0],[1,0],[2,0],[3,0],[4,0],[5,216]]                           | [[0,0],[1,0],[2,0],[3,0],[4,0],[5,2.1386138613861387]]
        'one data with error([10,0])'    | [[0,0],[1,0],[2,0],[1,0],[3,0],[4,216],[5,216],[6,216]]           | [[0,0],[1,0],[2,0],[3,0],[4,43.2],[5,44.910891089108915],[6,48.249296184836425]]
        'reset kalman filter state'      | [[0,200],[1,205],[2,0],[4,0],[6,200],[7,216],[8,216],[9,216]]     | [[0,200.0],[1,202.5],[2,135.00000000674999],[4,0],[6,200],[7,216.0],[8,216.0],[9,216.0000000072]]

    }
}
