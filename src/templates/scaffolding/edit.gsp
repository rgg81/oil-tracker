<%=packageName%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid" role="navigation">
				<ul class="nav navbar-nav">
					<li><a class="home" href="\${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
					<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
					<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</ul>
			</div>
		</nav>

		<div id="edit-${domainClass.propertyName}" class="container" role="main">
			<h2><g:message code="default.edit.label" args="[entityName]" /></h2>

			<g:if test="\${flash.message}">
				<div class="alert alert-info" role="status">
					\${flash.message}
				</div>
			</g:if>

			<g:hasErrors bean="\${${propertyName}}">
			<ul class="errors" role="alert">
				<g:eachError bean="\${${propertyName}}" var="error">
				<li <g:if test="\${error in org.springframework.validation.FieldError}">data-field-id="\${error.field}"</g:if>><g:message error="\${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:${propertyName}, action:'update']" method="PUT" <%= multiPart ? ' enctype="multipart/form-data"' : '' %> class="form-horizontal center-block col-sm-8 ">
				<g:hiddenField name="version" value="\${${propertyName}?.version}" />
				<g:render template="form"/>
				<g:actionSubmit class="btn btn-primary" action="update" value="\${message(code: 'default.button.update.label', default: 'Update')}" />
			</g:form>
		</div>
	</body>
</html>
