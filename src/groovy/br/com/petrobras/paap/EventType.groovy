package br.com.petrobras.paap

/**
 * Created by uq4e on 15/04/15.
 */
enum EventType {
    WARNING, INFO, CRITICAL
}
