package br.com.petrobras.paap.dsl

import br.com.petrobras.paap.TagPIType
import br.com.petrobras.paap.job.*
import grails.util.Holders

class Chain {
    def grailsApplication = Holders.grailsApplication
    def readPIService = grailsApplication.mainContext.getBean 'readPIService'
    def wsMessageService = grailsApplication.mainContext.getBean 'wsMessageService'
    def piDataService = grailsApplication.mainContext.getBean 'piDataService'
    def tagPIService = grailsApplication.mainContext.getBean 'tagPIService'
    def processTagService = grailsApplication.mainContext.getBean 'processTagService'
    def reloadQueueService = grailsApplication.mainContext.getBean 'reloadQueueService'
    def eventService = grailsApplication.mainContext.getBean 'eventService'
    def initialdata

	public ProcessData first,last

    public static newInstance(initialdata){
        new Chain(initialdata)
    }
    
    private Chain(initialdata){
        this.initialdata = initialdata
    }

    Chain(){
    }

    
	public with(processOrType, postconfig=null){
		def process

        if (processOrType instanceof ProcessType) {
            switch(processOrType) {
                case ProcessType.pireader:
                    process = new ProcessTagWithoutCalculation(readPIService, processTagService, grailsApplication)
                    break
                case ProcessType.filters:
                    process = new ProcessFilters(grailsApplication)
                    break
                case ProcessType.tagcalculator:
                    process = new ProcessTagWithCalculation(readPIService,grailsApplication)
                    break
                case ProcessType.jms:
                    process = new ProcessDataJMS(wsMessageService, grailsApplication)
                    break
                case ProcessType.save:
                    process = new ProcessDataSaveSamples(grailsApplication)
                    break
                case ProcessType.creategroups:
                    process = new ProcessCreateSampleGroups(grailsApplication)
                    break
                case ProcessType.cleandatabase:
                    process = new ProcessDataCleanDatabase(piDataService,grailsApplication)
                break
                case ProcessType.averagesnapshot:
                    process = new ProcessDataSnapshotSamples(wsMessageService, grailsApplication)
                    break
                case ProcessType.deletetags:
                    process = new DeleteTags(tagPIService, grailsApplication)
                break
                case ProcessType.reloadpitags:
                    process = new ReloadPiTags(readPIService, processTagService, grailsApplication)
                break
                case ProcessType.filterpitags:
                    process = new ProcessFilters(grailsApplication,{it != TagPIType.CALCULATED})
                break
                case ProcessType.filtercalctags:
                    process = new ProcessFilters(grailsApplication,{it == TagPIType.CALCULATED})
                break
                case ProcessType.recalculatepitags:
                    process = new RecalculatePiTags(readPIService,grailsApplication)
                break
                case ProcessType.readjms:
                    process = new ReadJMS(reloadQueueService,wsMessageService,grailsApplication)
                break  
                case ProcessType.notifyreloadingdone:
                    process = new NotifyReloadTagDone(reloadQueueService, grailsApplication)
                break
                case ProcessType.removeolddata:
                    process = new RemoveOldData(grailsApplication)
                break
                default:

                break
            }
        } else if (processOrType instanceof ProcessData) {
            process = processOrType
        } else {
            throw new IllegalArgumentException("Parameter is not ProcessType or ProcessData")
        }


		if(first){
			last.next = process
			last = process
		}else{
			first = last = process
		}

        if (postconfig) {
            postconfig(process)
        }

		return this
	}
	public process(){
		first.process(initialdata)
	}
}

enum ProcessType { pireader,filters,tagcalculator,jms,save,creategroups,dataaccumulator,cleandatabase,deletetags,reloadpitags,filterpitags,filtercalctags,recalculatepitags,readjms,notifyreloadingdone,averagesnapshot,removeolddata}
