package br.com.petrobras.paap.timeseries

import groovy.transform.CompileStatic

/**
 * Created by uq4e on 10/04/15.
 */
@CompileStatic
class TimeSeriesDownsampler
{
    public static TimeSeriesSample[] largestTriangleThreeBucketsTime(List<TimeSeriesSample> data, Integer threshold) {
        if (data == null) {
            throw new NullPointerException("Cannot cope with a null data input array.");
        }
        if (threshold <= 2) {
            throw new RuntimeException("What am I supposed to do with that?");
        }
        if (data.size() <= 2 || data.size() <= threshold) {
            return data.toArray(new TimeSeriesSample[0]);
        }
        TimeSeriesSample []sampled = new TimeSeriesSample[threshold] ;
        int bucket_interval = (int)((data.get(data.size() - 1).time.getTime().longValue() - data.get(0).time.getTime().longValue()) / threshold);
        int sampled_index = 0;
        double every = (double)(data.size() - 2) / (double)(threshold - 2);
        int a = 0, next_a = 0;
        TimeSeriesSample max_area_point = null;
        double max_area, area;

        sampled[sampled_index++] = data.get(a);

        for (int i = 0; i < threshold - 2; i++) {
            double avg_x = 0.0D, avg_y = 0.0D;
            int avg_range_start = (int)Math.floor((i+1)*every) + 1;
            int avg_range_end = (int)Math.floor((i+2)*every) + 1;
            avg_range_end = avg_range_end < data.size() ? avg_range_end : data.size();
            int avg_range_length = (int)(avg_range_end - avg_range_start);
            while (avg_range_start < avg_range_end) {
                avg_x = avg_x + data.get(avg_range_start).time.getTime().doubleValue();
                avg_y += data.get(avg_range_start).value.doubleValue();
                avg_range_start ++;
            }
            avg_x /= avg_range_length;
            avg_y /= avg_range_length;

            int range_offs = (int)Math.floor((i + 0) * every) + 1;
            int range_to = (int)Math.floor((i + 1) * every) + 1;

            double point_a_x = data.get(a).time.getTime().doubleValue();
            double point_a_y = data.get(a).value.doubleValue();

            max_area = area = -1;

            long ending_time = data.get(range_offs).time.getTime().longValue() + bucket_interval;

            while (range_offs < range_to) { // && data.get(range_offs).time.getTime().longValue() < ending_time) {
                area = Math.abs(
                        (point_a_x - avg_x) * (data.get(range_offs).value.doubleValue() - point_a_y) -
                                (point_a_x - data.get(range_offs).time.getTime().doubleValue()) * (avg_y - point_a_y)
                ) * 0.5D;
                if (area > max_area) {
                    max_area = area;
                    max_area_point = new TimeSeriesSample(time: new Date(ending_time), value: data.get(range_offs).value);
                    next_a = range_offs;
                }
                range_offs ++;
            }
            sampled[sampled_index++] = max_area_point;
            a = next_a;
        }

        sampled[sampled_index++] = data.get(data.size() - 1);
        return sampled;
    }

    public static Number[][] largestTriangleThreeBucketsTimeArray(List<Number[]> data, Integer threshold) {
        if (data == null) {
            throw new NullPointerException("Cannot cope with a null data input array.");
        }
        if (threshold <= 2) {
            throw new RuntimeException("What am I supposed to do with that?");
        }
        if (data.size() <= 2 || data.size() <= threshold) {
            return (Number[][]) data.toArray() ;
        }
        Number[][] sampled = new Number[threshold][] ;
        int bucket_interval = (int)((data.get(data.size() - 1)[0].longValue() - data.get(0)[0].longValue()) / threshold);
        int sampled_index = 0;
        double every = (double)(data.size() - 2) / (double)(threshold - 2);
        int a = 0, next_a = 0;
        Number[] max_area_point = null;
        double max_area, area;

        sampled[sampled_index++] = data.get(a);

        for (int i = 0; i < threshold - 2; i++) {
            double avg_x = 0.0D, avg_y = 0.0D;
            int avg_range_start = (int)Math.floor((i+1)*every) + 1;
            int avg_range_end = (int)Math.floor((i+2)*every) + 1;
            avg_range_end = avg_range_end < data.size() ? avg_range_end : data.size();
            int avg_range_length = (int)(avg_range_end - avg_range_start);
            while (avg_range_start < avg_range_end) {
                avg_x = avg_x + data.get(avg_range_start)[0].doubleValue();
                avg_y += data.get(avg_range_start)[1].doubleValue();
                avg_range_start ++;
            }
            avg_x /= avg_range_length;
            avg_y /= avg_range_length;

            int range_offs = (int)Math.floor((i + 0) * every) + 1;
            int range_to = (int)Math.floor((i + 1) * every) + 1;

            double point_a_x = data.get(a)[0].doubleValue();
            double point_a_y = data.get(a)[1].doubleValue();

            max_area = -1;

            long ending_time = data.get(range_offs)[0].longValue() + bucket_interval;

            while (range_offs < range_to) { // && data.get(range_offs)[0].longValue() < ending_time) {
                area = Math.abs(
                        (point_a_x - avg_x) * (data.get(range_offs)[1].doubleValue() - point_a_y) -
                                (point_a_x - data.get(range_offs)[0].doubleValue()) * (avg_y - point_a_y)
                ) * 0.5D;
                if (area > max_area) {
                    max_area = area;
                    max_area_point = [ending_time, data.get(range_offs)[1] ] as Number[] ;
                    next_a = range_offs;
                }
                range_offs ++;
            }
            sampled[sampled_index++] = max_area_point;
            a = next_a;
        }

        sampled[sampled_index] = data.get(data.size() - 1);
        return sampled;
    }
}
