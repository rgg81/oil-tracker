package br.com.petrobras.paap.timeseries

import groovy.transform.CompileStatic
import org.apache.log4j.Logger

class TimeSeriesInterpolator {
	List<TimeSeriesSample> serie

	private static final Logger log = Logger.getLogger(TimeSeriesInterpolator.class)

    public getSeries() {
        serie
    }

	public TimeSeriesInterpolator(Collection<TimeSeriesSample> serie) {
		this.serie = (serie as SortedSet) as List
	}

	@CompileStatic
	private int binaryFindClosestTimeSerie(Date time, int start, int end) {
		int ret = Collections.binarySearch(serie, time)

		// Ajusta o resutlado da função para o retorno esperado
		if ( ret < 0 ) {
			int insertionPoint = -ret-1

			insertionPoint-1
		} else {
			ret
		}
	}

	public Long getInterpolatedDistanceAt(Date desiredTime)
	{
		int num = serie.size()

		TimeSeriesSample choosenSample = {
			if ( num == 0 ) {
				return null
			} else if ( num == 1 ) {
				return serie.first()
			}

			// Busca o índice do item mais próximo
			int closestLowerIndex = binaryFindClosestTimeSerie(desiredTime, 0, num-1)

			// Trata casos triviais (primeiro e último: repetem os respectivos valores)
			if ( closestLowerIndex == -1 ) {
				return serie.first()
			} else if ( closestLowerIndex >= num-1 ) {
				return serie.last()
			} else {
				// Faz a interpolação
				if (Math.abs(serie[closestLowerIndex].time.getTime() - desiredTime.getTime()) < Math.abs(serie[closestLowerIndex+1].time.getTime() - desiredTime.getTime())) {
					return serie[closestLowerIndex]
				} else {
					return serie[closestLowerIndex+1]
				}
			}
		}.call()

		return choosenSample ? Math.abs(desiredTime.getTime() - choosenSample.time.getTime()) :null
	}

	public Double getSampleAt(Date time)
	{
		int num = serie.size()

		if ( num == 0 ) {
			return null
		} else if ( num == 1 ) {
            return serie.first().value
		}

		// Busca o índice do item mais próximo
		int closestLowerIndex = binaryFindClosestTimeSerie(time, 0, num-1)

		// Trata casos triviais (primeiro e último: repetem os respectivos valores)
		if ( closestLowerIndex == -1 ) {
			return serie.first().value
		} else if ( closestLowerIndex >= num-1 ) {
			return serie.last().value
		} else {
			// Faz a interpolação
			TimeSeriesSample closestLower = serie[closestLowerIndex]
			TimeSeriesSample closestHigher = serie[closestLowerIndex+1]
			return interpolate(time, closestLower, closestHigher)
		}
	}

	@CompileStatic
	private static double interpolate(Date time, TimeSeriesSample a, TimeSeriesSample b) {
		long fromSampleADiff = (time.getTime() - a.time.getTime())
		long fromSampleBDiff = (b.time.getTime() - time.getTime())
		long diffSamples = b.time.getTime() - a.time.getTime()

        double resultValue = {
            if(diffSamples == 0) {
                (a.value + b.value) / 2
            } else {
                (a.value * fromSampleBDiff + b.value * fromSampleADiff) / diffSamples
            }
        }.call()


        if(resultValue == Double.NaN) {
            log.error("not a number interpolate: a:${a} b:${b} fromSampleADiff:${fromSampleADiff} fromSampleBDiff:${fromSampleBDiff} date:${time}")
        }


		return resultValue
	}
}