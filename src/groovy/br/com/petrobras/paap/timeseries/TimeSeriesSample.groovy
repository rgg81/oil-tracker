package br.com.petrobras.paap.timeseries

import groovy.transform.AutoClone

@AutoClone
class TimeSeriesSample implements Comparable {
	Date time
	Double value

	int compareTo(obj) {
		if ( obj instanceof TimeSeriesSample ) {
			return time <=> obj.time ?: value <=> obj.value
		} else if ( obj instanceof Date ) {
			return time <=> obj
		} else {
			return -1
		}
    }

    public String toString() {
    	return "TimeSeriesSample[time:${time}, value:${value}]"
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        TimeSeriesSample that = (TimeSeriesSample) o

        if (time != that.time) return false
        if (value != that.value) return false

        return true
    }

    int hashCode() {
        int result
        result = (time != null ? time.hashCode() : 0)
        result = 31 * result + (value != null ? value.hashCode() : 0)
        return result
    }
}