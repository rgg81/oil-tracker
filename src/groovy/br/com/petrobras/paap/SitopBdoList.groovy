package br.com.petrobras.paap

import grails.validation.Validateable
import org.apache.commons.collections4.ListUtils
import org.apache.commons.collections4.Factory

/**
 * Created by uq4e on 06/05/15.
 */
@Validateable
class SitopBdoList {
    List<SitopBdo> sitopBdos = ListUtils.lazyList([], {new SitopBdo()} as Factory)
}
