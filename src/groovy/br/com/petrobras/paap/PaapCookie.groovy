package br.com.petrobras.paap

import javax.servlet.http.Cookie
import org.codehaus.groovy.grails.web.util.WebUtils

class PaapCookie {

	static Boolean setCookie(name,value) {
		def cookie = new Cookie(name,value)
    	cookie.path = "/"
    	WebUtils.retrieveGrailsWebRequest().currentResponse.addCookie(cookie)
	}

	static String getCookie(name) {
		return WebUtils.retrieveGrailsWebRequest().currentRequest.cookies?.find{it.name == name}?.getValue()
	}

	static Boolean deleteCookie(name) {
		def cookie = new Cookie(name,"")
    	cookie.maxAge = 0
    	cookie.path = "/"
    	WebUtils.retrieveGrailsWebRequest().currentResponse.addCookie(cookie)

	}
}