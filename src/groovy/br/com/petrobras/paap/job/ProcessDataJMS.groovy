package br.com.petrobras.paap.job

import br.com.petrobras.paap.PIDataService
import br.com.petrobras.paap.EventService
import br.com.petrobras.paap.UEP
import br.com.petrobras.paap.WSMessageService
import br.com.petrobras.paap.ws.WSMessage
import br.com.petrobras.paap.ws.WSMessageType

/**
 * Created by roberto on 07/05/15.
 */

class ProcessDataJMS extends ProcessData {

    WSMessageService wsMessageService

    ProcessDataJMS(wsMessageService, grailsApplication) {
        super(grailsApplication)
        this.wsMessageService = wsMessageService
    }

    def processInternal(tags) {
        wsMessageService.sendEvent(WSMessageType.NEW_DATA,tags)
        return tags
    }
}
