package br.com.petrobras.paap.job

import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.ReloadStatus
import br.com.petrobras.paap.UEP
import br.com.petrobras.paap.WSMessageService
import br.com.petrobras.paap.ws.WSMessageType
import grails.converters.JSON
import org.apache.log4j.Logger
import org.quartz.JobDetail
import org.quartz.JobKey
import org.quartz.Scheduler
import org.quartz.SimpleTrigger

import static org.quartz.JobBuilder.newJob
import static org.quartz.TriggerBuilder.newTrigger

/**
 * Created by roberto on 07/05/15.
 */
class ReadJMS extends ProcessData {

    public static final String TAGS_JSON_ARRAY = 'TAGS_JSON_ARRAY'
    private final JobKey jobKey = new JobKey("jobreloadtags", "group1")

    def reloadQueueService
    Scheduler scheduler
    WSMessageService wsMessageService

    private static final Logger log = Logger.getLogger(ReadJMS.class)

    public ReadJMS(reloadQueueService,wsMessageService,grailsApplication) {
        super(grailsApplication)
        this.reloadQueueService = reloadQueueService
        this.wsMessageService = wsMessageService
    }

    def processInternal(data) {

        JobDetail job = scheduler.getJobDetail(jobKey)
        if(job == null){
            job = newJob(br.com.petrobras.paap.job.ReloadTagsJob.class)
                    .withIdentity(jobKey)
                    .build();
        }else{
            log.debug "Job jobreloadtags já está sendo executado."
            return data
        }
        
        reloadQueueService.readFromEndQueueAndDo{ tags ->
            if(! tags.isEmpty() ) {
                tags = lockUnlockTags(tags,false)
                wsMessageService.sendEvent(WSMessageType.UEP_RELOAD_FINISHED, tagsToUepIds(tags))
            }
        }
      
        reloadQueueService.readFromStartQueueAndDo{ tags ->
            if( !tags.isEmpty() ) {
                tags = lockUnlockTags(tags,true)
                wsMessageService.sendEvent(WSMessageType.UEP_RELOAD_START, tagsToUepIds(tags))

                String json = tags*.id as JSON
                
                log.debug("Trigger Job 'jobreloadtags' com parametro ${TAGS_JSON_ARRAY} = ${json}")
                job.getJobDataMap().put(TAGS_JSON_ARRAY, json)
                
                SimpleTrigger trigger = newTrigger() 
                .withIdentity("triggerreloadtags", "group1")
                .startNow()
                .build()

                scheduler.scheduleJob(job, trigger)
                scheduler.start()
            }
        }

        
        data
    }

    private tagsToUepIds(tags) {
        tags.collect { tag ->
            UEP.findByTag(tag)?.id
        }.findAll{
            it != null
        }
    }


    private def lockUnlockTags(tags,lock){
        TagPI.withNewSession { session ->
            def tx = session.beginTransaction()
            try{
                tags = tags.collect{ id ->
                    def tag = TagPI.get(id.toLong())
                    tag.reloading = lock
                    if(lock){
                        tag.reloadStatus = ReloadStatus.PROCESSING
                        log.debug("Locking tag for reload : ${tag}")
                    }else{
                        log.debug("UnLocking tag for reload : ${tag}")
                    }
                    tag.save()
                    tag
                }

                tx.commit()
                tags
            }catch(Exception e){
                tx?.setRollbackOnly()
                throw e
            }
        }
    }
}
