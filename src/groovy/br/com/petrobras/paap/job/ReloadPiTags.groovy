package br.com.petrobras.paap.job

import br.com.petrobras.paap.EventTagPI
import br.com.petrobras.paap.EventType
import br.com.petrobras.paap.ReadPIService
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.TagPILocation
import br.com.petrobras.paap.TagPIType
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import org.apache.log4j.Logger

/**
 * Created by roberto on 07/05/15.
 */
class ReloadPiTags extends ProcessData {

    ReadPIService readPIService
    def processTagService

    private static final Logger log = Logger.getLogger(ReloadPiTags.class)

    public ReloadPiTags(readPIService,processTagService, grailsApplication) {
        super(grailsApplication)
        this.readPIService = readPIService
        this.processTagService = processTagService
    }

    def processInternal(tagsToReset) {
        def now = new Date()
        def begin = new Date(now.time - 24 * 60 * 60 * 1000)
     
        log.info "Reloading Tags ${tagsToReset}"

        def tagsByLocation = tagsToReset.findAll({it.tagType != TagPIType.CALCULATED}).groupBy({ tag -> tag.tagLocation })

        def piSamples = tagsByLocation.inject([:]){ result, location, tags -> 
            log.debug "Reloading Tags :: requesting pi location ${location} : formulas : ${tags*.formula}"
            result + readPIService.readDataFromTagsInTimePeriod(tags*.formula, location, begin, now)
        }

        def tags = tagsToReset.collectEntries { tagPI ->
            def samples = []
            if(tagPI.tagType != TagPIType.CALCULATED){
                samples = piSamples[tagPI.formula] ? piSamples[tagPI.formula] : []
                if(tagPI.tagType == TagPIType.ACCUMULATOR){
                    samples = processTagService.transformAccumulatedSamples(samples,tagPI)
                }
            }
            [tagPI, [samples: samples as SortedSet]]
        }
    
        return tags
    }
}
