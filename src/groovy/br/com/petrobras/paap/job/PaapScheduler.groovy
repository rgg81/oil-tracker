package br.com.petrobras.paap.job

import br.com.petrobras.paap.JobLastDateRun
import br.com.petrobras.paap.dsl.Chain
import grails.util.Holders
import groovy.time.TimeCategory
import org.quartz.DisallowConcurrentExecution
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException

import org.apache.log4j.Logger

import static br.com.petrobras.paap.dsl.ProcessType.*

@DisallowConcurrentExecution
class PaapScheduler implements Job {

    private static final Logger log = Logger.getLogger(PaapScheduler.class);
    def grailsApplication = Holders.grailsApplication

    void execute(JobExecutionContext context)
            throws JobExecutionException {

    		log.debug("executando job PaapScheduler");
    		try {

                long start = System.currentTimeMillis()
                def maxTimeToGet = grailsApplication.config.paap.job.maxMinutesToGet
                Date now = new Date()
                Date begin = {
                    use ( TimeCategory ) {
                        def lastDate = JobLastDateRun.withCriteria {
                            maxResults(1)
                            order("lastUpdateDate", "desc")
                        }
                        if(lastDate.isEmpty())
                            now - maxTimeToGet.minutes
                        else
                            lastDate.head().lastUpdateDate
                    }
                }.call()

                def newLastUpdateDate = new JobLastDateRun(lastUpdateDate: now)
                
                // trigger reload tags                
                new Chain().with(readjms, { it.scheduler = context.scheduler }).process()

                ProcessDataDateRange dateRange = new ProcessDataDateRange(now, begin, grailsApplication)

                // Acumula todos os dados para enviar apenas um JMS
                def accumulatedData = [:]

                // Primeira parte: processa e salva os dados de PI
                new Chain()
                    // Trás os dados do PI, suaviza e persiste
                    .with(dateRange).with(pireader).with(filters).with(save).process()

                // Segunda parte: processa as tags calculadas
                new Chain()
                    // Calcula as tags e salva no banco
                    .with(dateRange).with(tagcalculator).with(filters).with(save)
                    .with(creategroups).with(jms).with(cleandatabase).with(averagesnapshot).process()

                // trigger reload tags
                new Chain().with(readjms, { it.scheduler = context.scheduler }).process()
                
                newLastUpdateDate.save()

                long end = System.currentTimeMillis()


                log.info("Job run in ${(end-start)/1000}s")

    		} catch (Exception e) {
    			log.error("error in job", e);
    		}
	}
}