package br.com.petrobras.paap.job

import org.apache.log4j.Logger

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import br.com.petrobras.paap.*


/**
 * Created by roberto on 07/05/15.
 */
class DeleteTags extends ProcessData {
    def tagPIService

    private static final Logger log = Logger.getLogger(DeleteTags.class)

    public DeleteTags( tagPIService, grailsApplication) {
        super(grailsApplication)
        this.tagPIService = tagPIService
    }

    def processInternal(tags) {
        TagPI.withNewSession { session ->
            tags.each{ tag ->
                tag = TagPI.get(tag.id)
                tagPIService.resetTag(tag)
            }
        }
        return tags
    }
}
