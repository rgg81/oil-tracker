package br.com.petrobras.paap.job

import org.apache.log4j.Logger

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import br.com.petrobras.paap.*


/**
 * Created by roberto on 07/05/15.
 */
class NotifyReloadTagDone extends ProcessData {
    
    def reloadQueueService

    private static final Logger log = Logger.getLogger(NotifyReloadTagDone.class)

    public NotifyReloadTagDone( reloadQueueService, grailsApplication) {
        super(grailsApplication)
        this.reloadQueueService = reloadQueueService
    }

    def processInternal(tagsMap) {
        if(tagsMap == null || !(tagsMap instanceof Map) ){
            throw new IllegalStateException()
        }
        
        def tags = tagsMap.keySet()
        TagPI.withTransaction{
            tags.collect{ tag ->
                tag = TagPI.get(tag.id)
                tag.reloadStatus = ReloadStatus.DONE
                tag.save()
            }
        }

        log.debug("notificando fim do reload tags : ${tags}")
        reloadQueueService.pushToEndQueue(tags)
        return tags
    }
}
