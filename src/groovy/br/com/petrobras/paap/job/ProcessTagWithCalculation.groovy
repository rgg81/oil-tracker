package br.com.petrobras.paap.job

import br.com.petrobras.paap.EventTagPI
import br.com.petrobras.paap.EventType
import br.com.petrobras.paap.Sample
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.TagPIType
import br.com.petrobras.paap.InterpolatorDataStrategy
import br.com.petrobras.paap.timeseries.TimeSeriesInterpolator
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import groovy.time.TimeCategory
import br.com.petrobras.paap.TagPIExpressionBuilder
import br.com.petrobras.paap.TagPIExpression
import org.apache.log4j.Logger

/**
 * Created by roberto on 07/05/15.
 */
class ProcessTagWithCalculation extends ProcessData {
    public InterpolatorDataStrategy interpolatorDataStrategy
    private static final Logger log = Logger.getLogger(ProcessTagWithCalculation.class)
    def readPIService

    public ProcessTagWithCalculation(readPIService,grailsApplication) {
        super(grailsApplication)
        this.readPIService = readPIService
    }

    public void setInterpolatorDataStrategy(interpolatorDataStrategy){
        this.interpolatorDataStrategy = interpolatorDataStrategy
    }

    private def buildInterpolators(tagsPI,yesterday,now) {
        tagsPI.collectEntries { tag ->
            def samples = interpolatorDataStrategy.getTimeSeriesSamples(tag, yesterday, now)
            [(tag): new TimeSeriesInterpolator(samples as Set)]
        }
    }

    private def buildExpressions(calculatedTags,formulas){
        calculatedTags.collectEntries { calculatedTag ->
            // Cria TagPIExpression para cada tag calculada
            try {
                // Prepara a engine de expressões
                TagPIExpression expression = new TagPIExpressionBuilder(calculatedTag.formula).variables(formulas).build()

                [calculatedTag, expression]
            } catch (e) {
                log.error("Erro na fórmula \"${calculatedTag.formula}\"", e)
                null
            }
        }
    }

    private def interpolatedValueForTime(time,serie,tag){
        Double interpolatedTagValue = serie.getSampleAt(time)
        Long interpolatedDistance = serie.getInterpolatedDistanceAt(time)

        if (interpolatedDistance > grailsApplication.config.paap.processdata.warningMessage * 60 * 1000) {
            // Valor desatualizado -> remove a variável
            new EventTagPI(date: new Date(), tag:tag, eventType:EventType.WARNING,
                    msg: "Assuming 0 for tag without interpolated value for ${(int)(interpolatedDistance / 60 / 1000)} minutes").save()
            return 0 as Double
        } else {
            if (interpolatedTagValue != null) {
                return interpolatedTagValue
            } else {
                new EventTagPI(date: new Date(), tag:tag, eventType:EventType.WARNING,
                        msg: "Assuming 0 for tag without interpolated value").save()
                log.error("No interpolation for tag ${tag.name} at ${time}")
                return 0 as Double
            }
        }
    }

    private def processCalculatedTag(now, yesterday,calculatedTags,variablesTags) {
        // Cria os dados calculados, a cada itervalo desde a data mais antiga do PI até 'now'
        int calculatedTagInterval = grailsApplication.config.paap.job.calculatedTagsIntervalInSeconds

        def result = [:]
        if (calculatedTags?.size() > 0) {
            
            // Cria interpolador dos samples entre now e yesterday
            def interpolatedTags = buildInterpolators(variablesTags,yesterday,now)

            // Cria expressão para cada tag
            log.debug("Calculando tags '${calculatedTags*.formula}' entre ${yesterday} e ${now}...")
            def expressionForTag = buildExpressions(calculatedTags,variablesTags*.formula as Set)

            def mapVarValues = [:] as HashMap<String, Double>

            boolean expressionHasError = false

            // Começa o loop de cálculo das tags
            def begin = yesterday.truncateToSeconds(calculatedTagInterval)
            while (begin < now) {
                // Popula as variáveis com os valores para "begin"
                interpolatedTags.each { tag, serie ->
                    mapVarValues.put(tag.formula,interpolatedValueForTime(begin,serie,tag))
                }

                // Começa o cálculo das tags
                calculatedTags.each { calculatedTag ->
                    
                    TagPIExpression expression = expressionForTag[calculatedTag]
                    expression.setVariables(mapVarValues)

                    // Cria espaço para armazenar os samples calculados, por tag
                    if(!result[calculatedTag])
                        result[calculatedTag] = [samples: [] as SortedSet]

                    // Calcula e adiciona o TimeSeriesSample
                    def value = null
                    try {                        
                        value = expression.evaluate();
                    } catch (Exception e) {
                        if (!expressionHasError) {
                            // Faz o log apenas uma vez
                            log.error("Erro ao calcular tag ${calculatedTag} para ${begin}: ${e}")
                            expressionHasError = true
                        }
                    }

                    if (value != null) {
                        if (Double.isNaN(value)) {
                            log.error("not a number for tag: ${calculatedTag.name} exp: ${calculatedTag.formula} \n variables values: ${mapVarValues}")
                        } else {
                            result[calculatedTag].samples << new TimeSeriesSample(time: begin, value: value)
                        }
                    }
                }

                use(TimeCategory) {
                    // Incrementa intervalo
                    begin = begin + calculatedTagInterval.seconds
                }
            }
        }
        return result
    }

    def processInternal(data) {
        setInterpolatorDataStrategy(new InterpolatorDataStrategy(readPIService, grailsApplication))
        def calculatedTags = TagPI.findCalculatedTags()
        def variableTags = calculatedTags*.possibleChildrenTags.flatten() as Set
        processCalculatedTag(data.end,data.begin,calculatedTags,variableTags)
    }
}
