package br.com.petrobras.paap.job

/**
 * Created by roberto on 07/05/15.
 */
class ProcessDataDataAccumulator extends ProcessData {

    def accumulatedData

    public ProcessDataDataAccumulator(def accumulatedData, grailsApplication) {
        super(grailsApplication)
        this.accumulatedData = accumulatedData
    }

    def processInternal(data) {
        data?.each { tag, tagInfo ->
            if (!accumulatedData[tag]) {
                accumulatedData[tag] = [:]
            }
            accumulatedData[tag].putAll(tagInfo)
        }
        data
    }
}