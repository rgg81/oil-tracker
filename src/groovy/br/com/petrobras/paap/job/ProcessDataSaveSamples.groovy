package br.com.petrobras.paap.job

import br.com.petrobras.paap.Sample
import org.apache.log4j.Logger

/**
 * Created by roberto on 07/05/15.
 */


class ProcessDataSaveSamples extends ProcessData {
    private static final Logger log = Logger.getLogger(ProcessDataSaveSamples.class)

    public ProcessDataSaveSamples(grailsApplication) {
        super(grailsApplication)
    }

    def processInternal(data) {
        // Data vem como um mapa de tag_id -> [[time: XX, value: XX], ...]
        data.each { tag, tagInfo ->
            def serie = tagInfo.samples
            log.debug("Salvando tag '${tag.formula}'...")
            serie.each { serieSample ->
                Sample sample = Sample.findByTagAndDate(tag, serieSample.time)
                if ( sample == null ) {
                    sample = new Sample(tag: tag, date: serieSample.time, value: serieSample.value)
                } else {
                    sample.value = serieSample.value
                }
                sample.save()
            }
        }
        return data
    }
}