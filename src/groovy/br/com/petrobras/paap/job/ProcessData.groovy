package br.com.petrobras.paap.job

import br.com.petrobras.paap.*
import br.com.petrobras.paap.timeseries.TimeSeriesInterpolator
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import grails.converters.JSON
import groovy.time.TimeCategory
import net.objecthunter.exp4j.Expression
import net.objecthunter.exp4j.ExpressionBuilder

import javax.jms.TextMessage
import javax.jms.TopicConnection
import javax.jms.TopicPublisher
import javax.jms.TopicSession
import org.apache.log4j.Logger

/**
 * Created by roberto on 06/03/15.
 *
 * Modified by everyone since then...
 *
 */

abstract class ProcessData {

    private final Logger logInstance = Logger.getLogger(this.getClass())

    def next
    def grailsApplication

    public ProcessData(grailsApplication) {
        this.grailsApplication = grailsApplication
    }

    abstract def processInternal(data) 

    def process(data) {
        long start = System.currentTimeMillis()
        def processStatus = new ProcessStatus(startTime:start, type: this.class.toString(), status:"processing").save(flush:true)
        try{
            def newData = processInternal(data)            
            long end = System.currentTimeMillis()
            logInstance.debug("Dados processados em ${end-start}ms")
            processStatus.status = "done"
            processStatus.endTime = end
            processStatus.save(flush:true)

            next ? next.process(newData) : newData

        }catch(Exception e){
            long end = System.currentTimeMillis()
            logInstance.error("Job interrompido : " + e.message, e)
            processStatus.status = "error"
            processStatus.msg = e.message
            processStatus.endTime = end
            processStatus.save(flush:true)
            throw e
        }
    }
}

class ProcessDataDateRange extends ProcessData {
    Date end, begin

    public ProcessDataDateRange(Date end, Date begin, grailsApplication) {
        super(grailsApplication)
        this.end = end
        this.begin = begin
    }

    @Override
    def processInternal(data) {
        def dateRange = [begin: begin,end: end]

        if(data) {
            dateRange + data
        } else {
            dateRange
        }
    }
}

class ProcessDataDataOrigin extends ProcessData {

    def originData

    public ProcessDataDataOrigin(def originData, grailsApplication) {
        super(grailsApplication)
        this.originData = originData
    }

    def processInternal(data) {
        originData
    }
}


