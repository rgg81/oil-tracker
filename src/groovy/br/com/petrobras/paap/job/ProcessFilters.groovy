package br.com.petrobras.paap.job

import br.com.petrobras.paap.EventTagPI
import br.com.petrobras.paap.EventType
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.TagPIFilterException
import org.apache.log4j.Logger

/**
 * Created by roberto on 07/05/15.
 */
class ProcessFilters extends ProcessData {

    private static final Logger log = Logger.getLogger(ProcessFilters.class)
    private Closure selector = {it} // Selector inicial nao modifica a coleção

    public ProcessFilters(grailsApplication) {
        super(grailsApplication)
    }

    public ProcessFilters(grailsApplication,Closure tagselector) {
        super(grailsApplication)
        this.selector = tagselector
    }

    def processInternal(tags) {
        tags.findAll( { selector(it.key.tagType) } ).each { TagPI tagPI, tagInfo ->
            def samples = tagInfo.samples
            // Aplica a média móvel para "suavizar" os valores
            if ( !samples.isEmpty() ) {
                TagPI.withTransaction { status ->
                    TagPI t = TagPI.get(tagPI.id)
                    if (t.hasFilter()) {
                        def start = System.currentTimeMillis()
                        for (Iterator iterator = samples.iterator(); iterator.hasNext();) {
                            def sample = iterator.next()
                            try {
                                sample.value = t.applyFilters(sample)
                            } catch (TagPIFilterException e) {
//                                new EventTagPI(date: new Date(), tag:t, eventType:EventType.WARNING, msg: "Error applying filter: '${e.message}'").save()
                                log.debug("expected error while applying filters for sample:${sample} error:${e}")
                                log.debug("removing sample from Set of samples with error")
                                iterator.remove()
                            } catch (Exception e) {
                                log.error("error while applying filters for sample:${sample} error:${e}")
                                log.error("removing sample from Set of samples with error")
                                iterator.remove()
                            }
                        }

                        // Update tag's filter state
                        t.save()
                        def end = System.currentTimeMillis()
                        log.debug("Calculado a media movel de '${tagPI.name}' em ${end-start}ms")
                    }
                }
            }
        }
        tags
    }
}

