package br.com.petrobras.paap.job

import br.com.petrobras.paap.Event
import br.com.petrobras.paap.JobLastDateRun
import br.com.petrobras.paap.ProcessStatus
import br.com.petrobras.paap.TagPIFilterMovingAverageSample
import groovy.time.TimeCategory
import org.apache.log4j.Logger

/**
 * Created by roberto on 07/05/15.
 */
class ProcessDataCleanDatabase extends ProcessData {
    private static final Logger log = Logger.getLogger(ProcessDataCleanDatabase.class)
    def piDataService 

    public ProcessDataCleanDatabase(piDataService,grailsApplication) {
        super(grailsApplication)
        this.piDataService = piDataService
    }

    def processInternal(data) {
        use (TimeCategory) {
            def date = new Date()-73.hour

            log.debug("Removendo samples e eventos anteriores a '${date}'...")
            piDataService.deleteSamplesFromNonReloadingTagsUntil(date)
            
            TagPIFilterMovingAverageSample.executeUpdate('delete from TagPIFilterMovingAverageSample where time <= ?',[date]);
            Event.executeUpdate('delete from Event where date <= ?',[date]);
            ProcessStatus.executeUpdate('delete from ProcessStatus where startTime <= ?',[(new Date() - 3.days).time]) ;
            JobLastDateRun.executeUpdate('delete from JobLastDateRun where lastUpdateDate <= ?',[date]);

            log.debug("Dados antigos descartados")
            return data
        }

    }
}
