package br.com.petrobras.paap.job

import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.TagPIType
import org.apache.log4j.Logger
import br.com.petrobras.paap.InterpolatorDataStrategy

/**
 * Created by roberto on 07/05/15.
 */
class RecalculatePiTags extends ProcessTagWithCalculation {

    private static final Logger log = Logger.getLogger(RecalculatePiTags.class)

    RecalculatePiTags(readPIService,grailsApplication) {
        super(readPIService,grailsApplication)
    }
    
    @Override
    def processInternal(data) {
        if(data == null)
            throw new IllegalStateException("parameter data cannot be null")

        def now = new Date()
        def begin = new Date(now.time - 24 * 60 * 60 * 1000)

        def calculatedTags = data.keySet().findAll{ it.tagType == TagPIType.CALCULATED}
        def childrenTags = calculatedTags*.possibleChildrenTags.flatten() as Set
        setInterpolatorDataStrategy(new InterpolatorDataStrategy(readPIService, grailsApplication,[memory:true,tagSamplesMap:data]))
        
        data + processCalculatedTag(now,begin,calculatedTags,childrenTags)
    }

}
