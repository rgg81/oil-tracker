package br.com.petrobras.paap.job

import br.com.petrobras.paap.SampleGroup
import br.com.petrobras.paap.SampleGroup10m
import br.com.petrobras.paap.SampleGroup1h
import br.com.petrobras.paap.SampleGroup24h
import br.com.petrobras.paap.SampleGroup2m
import br.com.petrobras.paap.SampleGroup6h
import br.com.petrobras.paap.SampleGroupAggregator
import br.com.petrobras.paap.SampleGroupSingle
import org.apache.log4j.Logger
import br.com.petrobras.paap.TagPIType

/**
 * Created by roberto on 07/05/15.
 */
class ProcessCreateSampleGroups extends ProcessData {
    private static final Logger log = Logger.getLogger(ProcessCreateSampleGroups.class)

    private SampleGroupAggregator aggregator =
            new SampleGroupAggregator(SampleGroup2m.class,
                    new SampleGroupAggregator(SampleGroup10m.class,
                            new SampleGroupAggregator(SampleGroup1h.class,
                                    new SampleGroupAggregator(SampleGroup6h.class,
                                            new SampleGroupAggregator(SampleGroup24h.class, null)))))

    public ProcessCreateSampleGroups(grailsApplication) {
        super(grailsApplication)
    }

    def processInternal(tags) {
        log.debug("Calculando agregadores: ${aggregator}")

        tags.collectEntries { tag, tagInfo ->
            log.debug("Agregando tag '${tag.formula}'...")
            
            Map result = [samples: tagInfo.samples]
            if(tag.tagType == TagPIType.CALCULATED){
                tagInfo.samples.each { sample ->
                    SampleGroup rootSampleGroup = SampleGroupSingle.createSingle(sample.time, tag, sample.value)
                    aggregator.update(rootSampleGroup)
                }
                result = result + aggregator.executeUpdates()
            }

            [tag, result]
        }
    }
}
