package br.com.petrobras.paap.job

import br.com.petrobras.paap.dsl.Chain
import grails.util.Holders
import groovy.time.TimeCategory
import org.quartz.DisallowConcurrentExecution
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import groovy.json.JsonSlurper

import org.apache.log4j.Logger

import static br.com.petrobras.paap.dsl.ProcessType.*
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.ReloadStatus

class ReloadTagsJob implements Job {

    private static final Logger log = Logger.getLogger(ReloadTagsJob.class)
    def grailsApplication = Holders.grailsApplication
    def reloadQueueService = grailsApplication.mainContext.getBean 'reloadQueueService'


    void execute(JobExecutionContext context)
            throws JobExecutionException {

		log.debug("executando job ReloadTagsJob")

        def json = context.getJobDetail().getJobDataMap().getString(ReadJMS.TAGS_JSON_ARRAY)
        
        if(json == null || json.isEmpty()){
            log.debug("termino job ReloadTagsJob sem tags recarregadas");
            return
        }

        def tags = new JsonSlurper().parseText(json)
        tags = tags as Set
        tags = tags.collect{ id -> TagPI.get(id.toLong())}.findAll{ it != null }
        
        if(tags.isEmpty()){
            log.debug("termino job ReloadTagsJob sem tags recarregadas");
            return   
        }

        try{
            log.debug("tags recebidos: ${tags}")
            Chain.newInstance(tags)
                .with(deletetags)
                .with(reloadpitags).with(filterpitags)
                .with(recalculatepitags).with(filtercalctags)
                .with(save)
                .with(creategroups)
                .with(notifyreloadingdone)
                .process()
            log.debug("termino job ReloadTagsJob");
        }catch(Exception e){
             TagPI.withTransaction{
                tags.collect{ tag ->
                    tag = TagPI.get(tag.id)
                    tag.reloadStatus = ReloadStatus.FAILED
                    tag.save()
                }
            }
            reloadQueueService.pushToEndQueue(tags as Set)
        }
		
	}
}