package br.com.petrobras.paap.job

import br.com.petrobras.paap.EventTagPI
import br.com.petrobras.paap.EventType
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.TagPIFilterException
import org.apache.log4j.Logger

import br.com.petrobras.paap.Sample
import br.com.petrobras.paap.TagPI

/**
 * Created by flvs on 07/05/15.
 */
class RemoveOldData extends ProcessData {

    private static final Logger log = Logger.getLogger(RemoveOldData.class)

    public RemoveOldData(grailsApplication) {
        super(grailsApplication)
    }

    def processInternal(tags) {
        tags.each { TagPI tagPI, tagInfo ->
            if ( !tagInfo.samples.isEmpty() ) {
                TagPI.withTransaction { status ->
                    TagPI t = TagPI.get(tagPI.id)
                    def lastSample = Sample.withCriteria {
                        maxResults(1)
                        eq("tag",t)
                        order("date", "desc")
                    }
                    if(!lastSample.isEmpty()){
                        def newsamples = tagInfo.samples.findAll { it.time.time > lastSample.first().date.time}
                        log.info("Removing ${tagInfo.samples.size() - newsamples.size()} samples for tag ${t}.")
                        tagInfo.samples = newsamples
                    }
                }
            }
        }
        tags
    }
}