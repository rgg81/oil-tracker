package br.com.petrobras.paap.job

import br.com.petrobras.paap.Sample
import br.com.petrobras.paap.SnapshotSamples24h
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.UEP
import br.com.petrobras.paap.WSMessageService
import br.com.petrobras.paap.ws.WSMessageType
import groovy.time.TimeCategory

/**
 * Created by uq4e on 17/06/15.
 */
class ProcessDataSnapshotSamples extends ProcessData {

    private static final TimeZone zone = TimeZone.getTimeZone("America/Sao_Paulo");
    def HOUR_TO_GENERATE() {
        def dateNow = new Date()
        if( zone.inDaylightTime(dateNow) ) {
            // esta no horario de verão para "America/Sao_Paulo"
            18
        } else {
            17
        }
    }

    WSMessageService wsMessageService

    ProcessDataSnapshotSamples(wsMessageService, grailsApplication) {
        super(grailsApplication)
        this.wsMessageService = wsMessageService
    }

    @Override
    def processInternal(Object data) {
        Calendar now = Calendar.getInstance()
        int hour = now.get(Calendar.HOUR_OF_DAY)
        Calendar today = clearTime(now) // Today (without time)

        // Calculate only after 'HOUR_TO_GENERATE'
        if (hour >= HOUR_TO_GENERATE()) {
            // List all samples for today of after
//            def snapshotsToday = SnapshotSamples24h.findAllByDateGreaterThanEquals(today.time)
            def snapshotsToday = SnapshotSamples24h.executeQuery("select tag.id from SnapshotSamples24h where date >= ?", [today.time])
            // Check if any tag has no SnapshotSamples24h
            if (snapshotsToday.size() < TagPI.count()) {
                // Find outdated tags (not in 'snapshotsToday')
                def tagsToUpdate = TagPI.createCriteria().list {
                    if ( snapshotsToday && !snapshotsToday.isEmpty() ) {  // Ignore criteria if first run of the day
                        not { 'in'("id", snapshotsToday) }
                    }
                }

                // Calculate dates for today at HOUR_TO_GENERATE and yestarday at HOUR_TO_GENERATE
                Calendar todayAtGenerateHour = Calendar.getInstance()
                todayAtGenerateHour.setTime(today.time)
                todayAtGenerateHour.set(Calendar.HOUR_OF_DAY, HOUR_TO_GENERATE())
                Date yesterdayAtGenerateHour = use(TimeCategory) {
                    todayAtGenerateHour.time - 24.hours
                }

                // Calculate the average of all tags in tagsToUpdate between today's HOUR_TO_GENERATE and yesterday's HOUR_TO_GENERATE
                def avgSamples = Sample.createCriteria().list {
                    between 'date', yesterdayAtGenerateHour, todayAtGenerateHour.time
                    'in' ('tag', tagsToUpdate)
                    projections {
                        avg('value')
                        groupProperty('tag')
                    }
                }

                log.debug("Calculating SnapshotSamples24h for tags ${tagsToUpdate*.id} between ${yesterdayAtGenerateHour} and ${todayAtGenerateHour.time}")

                def createdSnapshots = avgSamples.collectEntries { avgSample ->
                    def avg = avgSample[0]
                    def tag = avgSample[1]

                    new SnapshotSamples24h(date:today.time, value:avg, tag:tag).save()
                    log.debug("Created SnapshotSamples24h for tag ${tag.id} at ${today.time}")

                    [tag.id, avg]
                }

                // Prepare message (only for tags associated to an UEP)
                if (!createdSnapshots.isEmpty()) {
                    def updatedTagsIds = createdSnapshots*.key

                    log.debug("updatedTagsIds = ${updatedTagsIds}")

                    def eventMessage = [
                        currentSnapshotDate: today.time,
                        previousSnapshotDate: use(TimeCategory) {
                            today.time - 24.hours
                        }
                    ]

                    UEP.withTransaction { status ->
                        def ueps = UEP.createCriteria().list {
                            'in'('tag.id', updatedTagsIds)
                        }

                        eventMessage.ueps = ueps.collectEntries { UEP uep ->
                            [(uep.id) : createdSnapshots[uep.tag.id]]
                        }
                    }
                    // Build message
                    wsMessageService.sendEvent(WSMessageType.NEW_SNAPSHOT_24H, eventMessage)
                }
            } else {
                log.debug("After ${HOUR_TO_GENERATE()}h but no need to calculate SnapshotSamples24h")
            }
        } else {
            log.debug("Waiting for ${HOUR_TO_GENERATE()}h to start process (now is ${hour}h)")
        }

        return data
    }

    protected Calendar clearTime(Calendar source) {
        Calendar cal = Calendar.getInstance()
        cal.setTime(source.time)
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal
    }
}
