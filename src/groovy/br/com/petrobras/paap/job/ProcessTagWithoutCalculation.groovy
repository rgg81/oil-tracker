package br.com.petrobras.paap.job

import br.com.petrobras.paap.EventTagPI
import br.com.petrobras.paap.EventType
import br.com.petrobras.paap.ReadPIService
import br.com.petrobras.paap.TagPI
import br.com.petrobras.paap.TagPILocation
import br.com.petrobras.paap.TagPIType
import br.com.petrobras.paap.timeseries.TimeSeriesSample
import br.com.petrobras.paap.Sample
import org.apache.log4j.Logger

/**
 * Created by roberto on 07/05/15.
 */
class ProcessTagWithoutCalculation extends ProcessData {

    ReadPIService readPIService
    def processTagService

    private static final Logger log = Logger.getLogger(ProcessTagWithoutCalculation.class)

    public ProcessTagWithoutCalculation(readPIService,processTagService, grailsApplication) {
        super(grailsApplication)
        this.readPIService = readPIService
        this.processTagService = processTagService
    }

    def processInternal(data) {
        def end = data.end
        def begin = data.begin

        def mapTagsValues = TagPILocation.values().inject([:]) { acum, tagLocation ->
            Map<String, Collection> samplesMap = processTagService.readDataFromLocation(tagLocation, new Date(begin.time - grailsApplication.config.paap.processdata.MS_3MINUTE), end)
            log.debug("Quering PI for samples between ${new Date(begin.time - grailsApplication.config.paap.processdata.MS_3MINUTE)} and $end at ${tagLocation} resulted in ${samplesMap?.entrySet()?.collect{it.value.size()}?.sum()} samples")
            acum + samplesMap
        }

        def tagsPI = TagPI.findPiTags()

        // Cria mapa (TagPI -> Samples)
        tagsPI.findAll{ mapTagsValues.get(it.formula) != null }.collectEntries { tagPI ->
            def samples
            
            if(tagPI.tagType == TagPIType.ACCUMULATOR) {
                def accumulatedSamples = mapTagsValues.get(tagPI.formula)
                accumulatedSamples = {
                    if(accumulatedSamples.size() == 1) {
                        log.debug("accumulated size less than 2 accum:${accumulatedSamples}")
                        def samplesMore = readPIService.readDataFromTagInTimePeriod(tagPI.formula, tagPI.tagLocation, 
                            new Date(begin.time - grailsApplication.config.paap.processdata.MS_30MINUTE), 
                            new Date(begin.time - grailsApplication.config.paap.processdata.MIN_MS_BETWEEN_SAMPLES))
                        log.debug("after another request :${samplesMore}")
                        new EventTagPI(date: new Date(), tag:tagPI, eventType:EventType.INFO,
                                msg: "${samplesMore.size()} extra samples were read from PI due to single result from an Accumulator tag").save()
                        if(!samplesMore.isEmpty()) {
                            return [samplesMore.last()] + accumulatedSamples
                        }
                    }
                    accumulatedSamples
                }.call()
                samples = processTagService.transformAccumulatedSamples(accumulatedSamples,tagPI)

            } else if ( tagPI.tagType == TagPIType.SPACED_SNAPSHOT) {
                /*======= TagPIType.SPACED_SNAPSHOT pi mode = 1 outsider, sempre vem um valor valido anterior(inclusive pode vir repetido)!!) ===========*/
                samples = mapTagsValues.get(tagPI.formula)
                if(samples.isEmpty()){
                    new EventTagPI(date: new Date(), tag:tagPI, eventType:EventType.CRITICAL,
                                msg: "spaced tag with samples size zero ( should not happen with mode = 1)").save()
                    samples = []
                }else{
                    samples.last().time = new Date(end.time)
                }

            } else{

                samples = mapTagsValues.get(tagPI.formula)

            }

            [tagPI, [samples: samples as SortedSet]]
        }
    }
}
