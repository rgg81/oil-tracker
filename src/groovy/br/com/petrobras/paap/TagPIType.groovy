package br.com.petrobras.paap

enum TagPIType {
	SNAPSHOT, ACCUMULATOR, CALCULATED, SPACED_SNAPSHOT
}