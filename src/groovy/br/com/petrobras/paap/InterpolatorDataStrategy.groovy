package br.com.petrobras.paap
import br.com.petrobras.paap.job.*
import br.com.petrobras.paap.timeseries.*


class InterpolatorDataStrategy {
	boolean memory = false
	def grailsApplication
	def tagSamplesMap
    def readPIService

	
	InterpolatorDataStrategy(readPIService, grailsApplication) {
		this.grailsApplication = grailsApplication
        this.readPIService = readPIService
	}

	InterpolatorDataStrategy(readPIService,grailsApplication,options) {
		this.grailsApplication = grailsApplication
		this.memory = options?.memory ?: this.memory
		this.tagSamplesMap = options?.tagSamplesMap ?: this.tagSamplesMap
        this.readPIService = readPIService
	}

    def getCurrentValueAndBringItToThePresent(tag,presentTime) {
        def timeseries = readPIService.readLastDataFromTag(tag.formula, tag.tagLocation)
        timeseries.first().time = new Date(presentTime.time)
        Sample.withTransaction {
            new Sample(date: new Date(presentTime.time),value: timeseries.first().value ,tag:tag).save(flush:true,failOnError:true)
        }
        return timeseries
    }

	def getTimeSeriesSamples(tag, begin, end) {
		def samples
		if(memory){
            def entry = tagSamplesMap.find{ it.key.id == tag.id }
            if(entry != null){
                samples = entry.value?.samples?: [new TimeSeriesSample(time: begin, value: 0)] //TODO filter begin/end here??
                
            }
		}
        if(samples == null){
    		samples = Sample.findAllByTagAndDateBetween(tag, begin, end, [sort: 'date', order: 'asc'])
                            .collect { new TimeSeriesSample(time: it.date, value: it.value) }
            if(samples.isEmpty()) {
                samples = {    
                    def lastSampleFoundInTag = Sample.withCriteria {
                        maxResults(1)
                        eq("tag",tag)
                        order("date", "desc")
                    }
                    def lastSampleAge = null

                    if((!lastSampleFoundInTag.isEmpty()) && 
                        ((lastSampleAge=(end.time - lastSampleFoundInTag.first().date.time) ) < grailsApplication.config.paap.processdata.maxDurationForInterpolation))
                    {
                        // Log warning only if older than 'warningMessage'
                        if (lastSampleAge >= grailsApplication.config.paap.processdata.warningMessage) {
                            new EventTagPI(date: new Date(), tag:tag, eventType:EventType.WARNING,
                                    msg: "No samples found within range ${begin} and ${end}. Using last sample which is ${Math.round(lastSampleAge/(1000*60))} minutes old and has value ${lastSampleFoundInTag.first().value}").save()
                        }
                        log.debug("using last sample:${lastSampleFoundInTag} for inteporlation")
                        [new TimeSeriesSample(time: lastSampleFoundInTag.first().date, value: lastSampleFoundInTag.first().value)]
                    }
                    else {
                        if (lastSampleAge) {
                            new EventTagPI(date: new Date(), tag:tag, eventType:EventType.WARNING,
                                    msg: "Assuming value 0 for tag that has no samples for ${Math.round(lastSampleAge/(1000*60))} minutes").save()
                        } else {
                            new EventTagPI(date: new Date(), tag:tag, eventType:EventType.WARNING,
                                    msg: "Assuming value 0 for tag that has no samples").save()
                        }
                        log.debug("assign 0 for value. Time for last sample is too long lastsample:${lastSampleFoundInTag} or not found for tag:${tag.name}")
                        [new TimeSeriesSample(time: begin, value: 0)]
                    }

                }.call()
            }
        }      
        return samples
	}
}
