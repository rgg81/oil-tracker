package br.com.petrobras.paap

import net.objecthunter.exp4j.Expression
import net.objecthunter.exp4j.ExpressionBuilder
import java.util.regex.Pattern
import java.util.regex.Matcher

class  TagPIExpressionBuilder{
	
	def builder
	def tags

	public TagPIExpressionBuilder(String formula){
		tags = TagPI.findAllByTagTypeOrTagTypeOrTagType(TagPIType.SNAPSHOT,TagPIType.ACCUMULATOR,TagPIType.SPACED_SNAPSHOT)
		formula = normalize(formula)
		builder = new ExpressionBuilder(formula)
	}

	public TagPIExpressionBuilder(String formula,tags){
		this.tags = tags
		formula = normalize(formula)
		builder = new ExpressionBuilder(formula)
	}

	def variables(Set<String> variables){
		def normVariables = []
		variables = variables.collect{ name ->
			def tag = tags.grep { t -> t.formula ==  name}[0]
			if(tag == null) throw new Exception ("variavel invalida")
			"t" + tag.id
		}
		builder.variables(variables)
		this
	}

	def build(){
		new TagPIExpression(builder.build(),tags)
	}

	def normalize(String formula){
		def matches = [:]
		def normFormula = new StringBuffer(substituteEscapedTags(formula))
		

		/*tags.each{tag ->
			def escapedFormula = Pattern.quote(tag.formula)
			Pattern p = Pattern.compile(escapedFormula);
    		Matcher m = p.matcher(normFormula.toString());
    		int offset = 0
		    while (m.find()){
		    	//println "normFormula antes: " + normFormula
				normFormula.replace(m.start(0) + offset, m.end(0) + offset, "t${tag.id}")	
				offset = offset + "t${tag.id}".size() - (m.end(0) - m.start(0))
				//println "normFormula depois: " + normFormula
		    }
		}*/

		return normFormula.toString()
	}


	private substituteEscapedTags(String formula){
		def escapeChar = "'"
		def tagStarted = false
		def tagName = new StringBuffer("")
		def escapedFormula = new StringBuffer(formula)
		def cursor = 0

		for(int i=0;i<formula.size();i++){
			if(formula[i] == escapeChar){
				tagStarted =! tagStarted
				if(tagStarted){
					tagName.setLength(0);
				}else{
					def tag = tags.find { t -> t.formula ==  tagName.toString()}
					
					if(!tag) {
						throw new Exception("tag '${tagName}' not found")
					}

					escapedFormula.replace(cursor-tagName.size() - 1, cursor + 1, "t${tag.id}")
					cursor = cursor - tagName.size() - 2 + "t${tag.id}".size() 
				}
			}else if(tagStarted){
				tagName.append(formula[i])
			}
			cursor++
		}
		
		if(tagStarted){
			throw new Exception("Escape char ${escapeChar} missing")
		}

		return escapedFormula.toString()
	}
}

class TagPIExpression{
	Expression expression
	def tags
	public TagPIExpression(Expression expression,tags){
		this.expression = expression
		this.tags = tags
	}

	def evaluate(){
		expression.evaluate()
	}

	def setVariables(HashMap<String,Double> varMap){
		varMap = varMap.collectEntries{k,v->
			def  tag = tags.find { it.formula == k }
			if(tag == null) throw new Exception ("variavel invalida")
			["t"+tag.id , v]
		}
		expression.setVariables(varMap)
		this
	}

	def setVariable(String name, Double value)
	{
		def tag = tags.find { it.formula == name }
		if(tag == null) throw new Exception ("variavel invalida")
		def tagVar = "t"+tag.id;
		expression.setVariable(tagVar, value)
	}
}