package br.com.petrobras.paap

import groovy.time.TimeDuration

interface SampleGroup {
	Date date
	TagPI tag
	Double mean
	Double max
	Double min
	Double variance
	Double num
	Double sum

    public TimeDuration getDuration() ;
}
