package br.com.petrobras.paap.dbdialect
import org.hibernate.dialect.MySQL5Dialect

class MyDialect extends MySQL5Dialect {
   String getTableTypeString() { " ENGINE=MyISAM" }
}