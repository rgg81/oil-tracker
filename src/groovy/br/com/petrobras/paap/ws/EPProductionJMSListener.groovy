package br.com.petrobras.paap.ws

import br.com.petrobras.paap.EventService
import br.com.petrobras.paap.EventTagPI
import grails.util.Holders
import org.springframework.web.context.ServletContextAware

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

import javax.jms.*
import javax.servlet.ServletContext

import org.apache.log4j.Logger
import br.com.petrobras.paap.UepAccessStaticCache

class EPProductionJMSListener implements ServletContextAware, MessageListener {
    
    private static final Logger log = org.apache.log4j.Logger.getLogger(EPProductionJMSListener.class);
    
    TopicConnectionFactory jmsConnectionFactory
    Topic jmsTopicNewPiData

    @Override
    void setServletContext(ServletContext servletContext) {
        log.info("setServletContext: ${servletContext}")

        try {

            log.info("Configuring JMS...")
            TopicConnection con = jmsConnectionFactory.createTopicConnection()
            TopicSession session = con.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE)
            TopicSubscriber subscriber = session.createSubscriber(jmsTopicNewPiData)
            subscriber.setMessageListener(this);

            Runtime.runtime.addShutdownHook {
                try{
                    log.info("Closing JMS...")
                    con.stop()
                    con.close()
                } catch (e) {
                    log.error("Error while closing JMS", e)
                }
            }

            con.start()
            log.info("JMS configured")
        } catch (IOException e) {
            log.error(e.message, e)
        }
    }


    
    @Override
    void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message

        Map map = new JsonSlurper().parseText(message.text) 

        log.debug("Mensagem recebida via JMS")

        // Checa pelo trace antes de chamar o log pois a mensagem serializada por ser muito grande
        if ( log.isTraceEnabled() ) {
            log.trace("Conteudo da mensagem: '${textMessage.text}'")
        }

        if(map.event == WSMessageType.NEW_DATA.typeName){
            Set clients
            synchronized (WSClientService.clients) { 
                clients = new HashSet(WSClientService.clients); //not synced hash
            }

            clients.each { session ->
                def userUepIds = session.userProperties.httpsession.ueps*.id
                def data = map.data.inject([:]) { acc, uepid, uepdata  ->
                    if(uepid == 'events') {
                        // Special case of 'events' -> user must be admin
                        if( session.userProperties.httpsession.isAdmin ) {
                            acc[uepid] = uepdata
                        }
                    } else if ( userUepIds.contains(uepid.toLong()) ){
                        acc[uepid] = uepdata
                    }
                    acc
                }
                WSClientService.sendMessageToClient(session,JsonOutput.toJson([event:map.event,data: data]))
            }
        }else{
            WSClientService.sendMessageToClients(textMessage.text)
        }
    }
}
