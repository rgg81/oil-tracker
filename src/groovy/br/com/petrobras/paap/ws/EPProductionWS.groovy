package br.com.petrobras.paap.ws

import br.com.petrobras.paap.OnlineUser
import grails.util.Environment
import org.apache.log4j.Logger
import org.springframework.web.context.ServletContextAware

import javax.servlet.ServletContext
import javax.servlet.http.HttpSession
import javax.websocket.*
import javax.websocket.server.ServerContainer
import javax.websocket.server.ServerEndpoint

@ServerEndpoint(value = "/pi-ws", configurator = GetHttpSessionConfigurator.class)
class EPProductionWS implements ServletContextAware {

    private static final Logger log = Logger.getLogger(EPProductionWS.class)

    @Override
    void setServletContext(ServletContext servletContext) {
        try{
            ServerContainer serverContainer = servletContext.getAttribute("javax.websocket.server.ServerContainer")
            log.info("serverContainer: ${serverContainer}")
            // This is necessary for Grails to add the endpoint in development.
            // In production, the endpoint will be added by the @ServerEndpoint
            // annotation.
            if (Environment.current == Environment.DEVELOPMENT) {
                log.info("Adding this endpoint in DEVELOPMENT")
                serverContainer?.addEndpoint(EPProductionWS)
            }
        }catch (IOException e) {
            log.error(e.message, e)
        }
    }   

    @OnOpen
    public void onOpen(javax.websocket.Session userSession,EndpointConfig config) {
        log.debug("Opening WS with ${userSession}")
        userSession.getUserProperties().put(
            GetHttpSessionConfigurator.HTTPSESSION_KEY,
            (HttpSession) config.getUserProperties().get(GetHttpSessionConfigurator.HTTPSESSION_KEY)
        )
        WSClientService.addClient(userSession)

        withTransationOnlineUser{ session ->
            new OnlineUser(login: userSession.userPrincipal.name, sessionId:userSession.id).save(flush:true)
        }
    }

    @OnMessage
    public void onMessageFromWS(String message, javax.websocket.Session userSession)
    {
        // Ignored
    }

    @OnClose
    public void onClose(javax.websocket.Session userSession, CloseReason closeReason) {
        log.debug("Closing WS with ${userSession}")
        WSClientService.removeClient(userSession)
        
        withTransationOnlineUser{ session ->
            OnlineUser.executeUpdate("delete OnlineUser where login = ? and sessionId = ?", [userSession.userPrincipal.toString(),userSession.id])
        }
    }

    @OnError
    public void onError(javax.websocket.Session userSession, Throwable t) {
        log.warn("Error WS with ${userSession}")
        withTransationOnlineUser{ session ->
            OnlineUser.executeUpdate("delete OnlineUser where login = ? and sessionId = ?", [userSession.userPrincipal.toString(),userSession.id])
        }
        log.error(t.message, t)
    }

    private withTransationOnlineUser(closure){
        try{
            OnlineUser.withNewSession{ session ->
                try{
                    closure.call(session)
                 }catch(Exception e){
                    log.error("OnlineUser error", e)    
                    status.setRollbackOnly()
                }
            }
        }catch(Exception e){
            log.error("OnlineUser error", e)    
        }
    }
}
