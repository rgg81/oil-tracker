package br.com.petrobras.paap.ws

import javax.websocket.*
import javax.websocket.server.ServerContainer
import javax.websocket.server.ServerEndpoint
import org.apache.log4j.Logger


class WSClientService {
    private static final Logger log = Logger.getLogger(WSClientService.class);
    //http://docs.oracle.com/javase/7/docs/api/java/util/Collections.html#synchronizedList(java.util.List)
	private final static Set<javax.websocket.Session> clients = ([] as Set).asSynchronized()

    /**
     * Iterate through all clients and send a message to them.
     * @param message
     */
    private static void sendMessageToClients(String message) {
        synchronized (clients) {
            Iterator<Session> iterator = clients.iterator()
            while (iterator.hasNext()) {
                try {
                    iterator.next().basicRemote.sendText(message)    
                }catch(Exception e) {
                    log.error("sendMessageToClients error", e)
                }
            }
        }
    }

    private static void sendMessageToClient(javax.websocket.Session userSession,String message) {
        try{
            userSession.basicRemote.sendText(message)
        }catch(Exception e) {
            log.error("sendMessageToClient error", e)
        }
    }

	private static void addClient(javax.websocket.Session clientSession){
		clients.add(clientSession)
	}

	private static void removeClient(javax.websocket.Session clientSession){
		clients.remove(clientSession)	
	}
}
