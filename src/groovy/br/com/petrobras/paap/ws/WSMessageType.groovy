package br.com.petrobras.paap.ws

enum WSMessageType {
    NEW_DATA("newdata"),
    UPDATE_SITOP_BDO("updateSitopBdo"),
    UEP_RELOAD_START("uepReloadStart"),
    UEP_RELOAD_FINISHED("uepReloadFinished"),
    NEW_SNAPSHOT_24H("newSnapshot24h");


    private String typeName


    private WSMessageType(String typeName) {
        this.typeName = typeName
    }

    public String getTypeName() {
        return typeName
    }
}
