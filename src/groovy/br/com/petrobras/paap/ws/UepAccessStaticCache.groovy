package br.com.petrobras.paap

import javax.websocket.*
import javax.websocket.server.ServerContainer
import javax.websocket.server.ServerEndpoint
import org.apache.log4j.Logger


class UepAccessStaticCache {
	private final static Map<String,List> cache = ([:] as Map<String,List>).asSynchronized()

    private static void put(user, ueps) {
        cache.put(user,ueps)
    }

    private static List get(user) {
        if(cache.get(user)){
            cache.get(user)
        }else{
            []
        }
    }
}
