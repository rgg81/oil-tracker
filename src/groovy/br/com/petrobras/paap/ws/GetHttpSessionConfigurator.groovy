package br.com.petrobras.paap.ws

import javax.servlet.http.HttpSession
import javax.websocket.HandshakeResponse
import javax.websocket.server.HandshakeRequest
import javax.websocket.server.ServerEndpointConfig

public class GetHttpSessionConfigurator extends ServerEndpointConfig.Configurator
{
	public static final String HTTPSESSION_KEY = "httpsession"
    @Override
    public void modifyHandshake(ServerEndpointConfig config, 
                                HandshakeRequest request, 
                                HandshakeResponse response)
    {
        HttpSession httpSession = (HttpSession)request.getHttpSession()
       	config.getUserProperties().put(HTTPSESSION_KEY,httpSession)
    }
}