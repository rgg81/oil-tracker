package br.com.petrobras.paap.ws

import org.apache.log4j.Logger
import br.com.petrobras.paap.ws.WSMessageType
import br.com.petrobras.paap.UEP


class WSMessage {

    def piDataService
    def eventService

    private static final INTERNALL_KEY_TO_EXTERNAL = [
        'samples': 'all',
        SampleGroup2m: 'high',
        SampleGroup10m: 'medium',
        SampleGroup1h: 'low',
        SampleGroup6h: 'very-low',
        SampleGroup24h: 'daily'
    ]

    private static final SAMPLES_PROPERTIES = [
        'samples', 
        'SampleGroup2m', 
        'SampleGroup10m', 
        'SampleGroup1h', 
        'SampleGroup6h', 
        'SampleGroup24h'
    ] as Set


    WSMessageType type
    def data
	
    WSMessage(){
    }

    WSMessage(piDataService,eventService){
        this.piDataService = piDataService
        this.eventService = eventService
    }

    def type(WSMessageType type){
        this.type = type
        this
    }

    def data(data){
        this.data = data
        this
    }

    public def build(){
        def resultData = data ?: []

        if(type == WSMessageType.NEW_DATA){
            UEP.withNewSession { session ->
                resultData = UEP.findAll().collectEntries { uep ->
                    def info = resultData.find{ it.key.id == uep.tag.id }?.value ?: [:]
                    
                    info['tagId'] = uep.tag.id
                    info['24h_avg'] = piDataService.average24hours(uep.id)
                    info['alert'] = piDataService.alert(uep.id)

                    info = info.collectEntries { property, value ->
                        [
                            INTERNALL_KEY_TO_EXTERNAL[property] ?: property,
                            SAMPLES_PROPERTIES.contains(property) ? value.collect{ [it.time, it.value] } : value 
                        ]
                    }

                    [uep.id, info]
                }
                resultData.events = eventService.unsafeEventsWarnings() // Use 'unsafe' mode as it will not check for admin user
                resultData
            }
        }

        [event: type.getTypeName()] + (resultData ? [data: resultData] : [])
        
    }
}
