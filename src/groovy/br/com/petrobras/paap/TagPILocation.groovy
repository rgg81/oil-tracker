package br.com.petrobras.paap

/**
 * Created by uq4e on 15/04/15.
 */
enum TagPILocation {
    UO_BS, UO_ES, UO_RIO, UO_BC

    static Boolean isValid(TagPILocation value){
        def location =  TagPILocation.values().find{it -> it == value }
        return location != null
    }

    static Boolean checkTag(String value){
    	def location =  TagPILocation.values().find{it -> it.toString() == value }
        return location != null

    }
}
