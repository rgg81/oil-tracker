package br.com.petrobras.paap

import groovy.time.TimeDuration

/**
 * Created by uq4e on 17/03/15.
 */
class SampleGroupSingle implements SampleGroup {
    Date date
    TagPI tag
    Double mean
    Double max
    Double min
    Double variance
    Double num
    Double sum

    public static SampleGroup createSingle(Date time, TagPI tag, double value) {
        new SampleGroupSingle(date: time, tag: tag, mean: value, max: value, min: value, variance: 0, num: 1, sum: value)
    }

    @Override
    TimeDuration getDuration() {
        return null
    }
}
