package br.com.petrobras.paap

import br.com.petrobras.paap.timeseries.TimeSeriesSample

import org.apache.log4j.Logger

/**
 * Created by uq4e on 16/03/15.
 */
class SampleGroupAggregator
{
    private static final Logger log = Logger.getLogger(SampleGroupAggregator.class)
    Class<SampleGroup> sampleGroupClass 
    SampleGroupAggregator nextAggregator 
    def cachedUpdates = [:]

    public SampleGroupAggregator(Class<SampleGroup> sampleGroupClass, SampleGroupAggregator nextAggregator)
    {
        this.sampleGroupClass = sampleGroupClass ;
        this.nextAggregator = nextAggregator

        // TODO: Check if next aggregator will work (duration must be greater and correctly divide our duration)...
    }

    def update(SampleGroup sample) {
        log.trace("Updating SampleGroupAggregator of class ${sampleGroupClass} with sample ${sample}..." )
        // Instancia um SampleGroup do tipo esperado
        SampleGroup newSample = sampleGroupClass.newInstance(date: sample.date, tag: sample.tag, mean: sample.mean,
                max: sample.max, min: sample.min, variance: sample.variance, num: sample.num, sum: sample.sum)

        int durationInSeconds = newSample.getDuration().toMilliseconds() / 1000
        newSample.date = newSample.date.truncateToSeconds(durationInSeconds)

        // Obtém o SampleGroup para esta classe
        SampleGroup lastSampleForTag = cachedUpdates[newSample.date]

        // Pega do banco, caso necessário
        if ( !lastSampleForTag ) {
            lastSampleForTag = sampleGroupClass.findByTagAndDate(newSample.tag, newSample.date)
        }

        // Caso trivial da primeira tag
        if(!lastSampleForTag) {
            cachedUpdates[newSample.date] = newSample
        } else {
            // Combina os dados
            log.trace("Combinating ${lastSampleForTag} with ${newSample}")
            cachedUpdates[newSample.date] = combinate(lastSampleForTag, newSample)
        }

        // Repassa para o próximo agregador
        if (nextAggregator) {
            nextAggregator.update(sample)
        }
    }

    private static SampleGroup combinate(SampleGroup original, SampleGroup other) {
        double mean = (original.sum  + other.sum) / (original.num + other.num)
        double myMeanDiff = original.mean-mean
        double otherMeanDiff = other.mean-mean

        // Equivalente à função 'VAR.P' do Excel
        original.variance = (original.num * original.variance + other.num * other.variance + original.num * myMeanDiff * myMeanDiff + other.num * otherMeanDiff * otherMeanDiff) / (original.num + other.num)
        original.mean = mean
        if (other.max > original.max) original.max = other.max
        if (other.min < original.min) original.min = other.min
        original.num = original.num + other.num
        original.sum = original.sum + other.sum

        return original
    }

    def executeUpdates() {
        def resultTimeSeries = [] as LinkedList
        cachedUpdates.each { date, updatedSample ->
            updatedSample.save()
            resultTimeSeries << new TimeSeriesSample(time: updatedSample.date, value: updatedSample.mean)
        }
        cachedUpdates.clear()

        def result = [:]

        result[sampleGroupClass.getSimpleName()] = resultTimeSeries

        if (nextAggregator) {
            result += nextAggregator.executeUpdates()
        }
        result
    }

    @Override
    public String toString() {
        return "SampleGroupAggregator{" +
                "sampleGroupClass=" + sampleGroupClass +
                ", nextAggregator=" + nextAggregator +
                '}';
    }
}
