package br.com.petrobras.paap;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.helpers.PatternConverter;
import org.apache.log4j.helpers.PatternParser;
import org.apache.log4j.spi.LoggingEvent;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by uq4e on 17/04/15.
 * From: http://lifeinide.blogspot.com.br/2011/06/host-and-user-name-in-log4j-logs.html
 */
public class ContextPatternLayout extends PatternLayout {

    protected String host;

    protected String getHostname() {
        if (host == null) {
            // Primeiro tenta pegar do weblogic...
            String serverName = System.getProperty("weblogic.Name");
            if (serverName != null) {
                host = "wls:" + serverName ;
            } else {
                try {
                    InetAddress addr = InetAddress.getLocalHost();
                    this.host = addr.getHostName();
                } catch (UnknownHostException e) {
                    this.host = "localhost";
                }
            }
        }
        return host;
    }

    @Override
    protected PatternParser createPatternParser(String pattern) {
        return new PatternParser(pattern) {

            @Override
            protected void finalizeConverter(char c) {
                PatternConverter pc = null;

                if (c == 'h') {
                    pc = new PatternConverter() {
                        @Override
                        protected String convert(LoggingEvent event) {
                            return getHostname();
                        }
                    };
                }

                if (pc == null)
                    super.finalizeConverter(c);
                else
                    addConverter(pc);
            }
        };
    }

}