package br.com.petrobras.paap;

import org.hibernate.dialect.Oracle10gDialect;
import java.sql.Types;

/**
 * Created by uq4e on 20/04/15.
 */
public class PaapOracle10gDialect extends Oracle10gDialect {
    @Override
    protected void registerNumericTypeMappings() {
        super.registerNumericTypeMappings() ;
        registerColumnType( Types.DOUBLE, "BINARY_DOUBLE" );
    }

}
