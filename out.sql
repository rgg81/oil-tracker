-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: changelog.groovy
-- Ran at: 14/05/15 09:30
-- Against: PAAP@jdbc:oracle:thin:@//nestor.petrobras.com.br:1521/prtlcrpd
-- Liquibase version: 2.0.5
-- *********************************************************************

-- Lock Database
-- Changeset changelog.groovy::1431606448868-1::upn1::(Checksum: 3:856a8a4ce88df359864454d5baab4557)
ALTER TABLE tagpifilter ADD cold_start_total NUMBER(10,0);

INSERT INTO DATABASECHANGELOG (AUTHOR, COMMENTS, DATEEXECUTED, DESCRIPTION, EXECTYPE, FILENAME, ID, LIQUIBASE, MD5SUM, ORDEREXECUTED) VALUES ('upn1', '', SYSTIMESTAMP, 'Add Column', 'EXECUTED', 'changelog.groovy', '1431606448868-1', '2.0.5', '3:856a8a4ce88df359864454d5baab4557', 78);

-- Release Database Lock
